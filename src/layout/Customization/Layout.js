// material-ui
import { useTheme } from '@mui/material/styles';
import { FormControl, FormControlLabel, Radio, RadioGroup } from '@mui/material';

// project imports
import useConfig from 'hooks/useConfig';

const Layout = () => {
    const theme = useTheme();
    const { navType, onChangeMenuType } = useConfig();

    return (
        <FormControl component="fieldset" sx={{padding: '8px 12px 0'}}>
            <RadioGroup
                row
                aria-label="layout"
                value={navType}
                onChange={(e) => onChangeMenuType(e.target.value)}
                name="row-radio-buttons-group"
            >
                <FormControlLabel
                    value="light"
                    control={<Radio />}
                    label="Light"
                    sx={{
                        '& .MuiSvgIcon-root': { fontSize: 28 },
                        '& .MuiFormControlLabel-label': { color: theme.palette.grey[900] }
                    }}
                />
                <FormControlLabel
                    value="dark"
                    control={<Radio />}
                    label="Dark"
                    sx={{
                        '& .MuiSvgIcon-root': { fontSize: 28 },
                        '& .MuiFormControlLabel-label': { color: theme.palette.grey[900] }
                    }}
                />
            </RadioGroup>
        </FormControl>
    );
};

export default Layout;
