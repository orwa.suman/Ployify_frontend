import PropTypes from 'prop-types';
import { memo, useState } from 'react';

// material-ui
import { styled, useTheme } from '@mui/material/styles';
import {
    Avatar,
    Card,
    CardContent,
    Grid,
    LinearProgress,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText,
    Typography,
    linearProgressClasses
} from '@mui/material';
import ThumbUpOffAltIcon from '@mui/icons-material/ThumbUpOffAlt';

// assets
import TableChartOutlinedIcon from '@mui/icons-material/TableChartOutlined';
import FeedBackDialog from 'views/application/activities/Feedback/Feedback';

// styles
const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 10,
    borderRadius: 30,
    [`&.${linearProgressClasses.colorPrimary}`]: {
        backgroundColor: theme.palette.mode === 'dark' ? theme.palette.dark.light : '#fff'
    },
    [`& .${linearProgressClasses.bar}`]: {
        borderRadius: 5,
        backgroundColor: theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.main
    }
}));

const CardStyle = styled(Card)(({ theme }) => ({
    background: '#cbab70',
    marginBottom: '22px',
    overflow: 'hidden',
    position: 'relative',
    '&:after': {
        content: '""',
        position: 'absolute',
        width: '107px',
        height: '107px',
        background: '#cbab70',
        borderRadius: '50%',
        top: '-105px',
        right: '-96px'
    }
}));

// ==============================|| PROGRESS BAR WITH LABEL ||============================== //

function LinearProgressWithLabel({ value, ...others }) {
    const theme = useTheme();

    return (
        <Grid container direction="column" spacing={1} sx={{ mt: 1.5 }}>
            <Grid item>
                <Grid container justifyContent="space-between">
                    <Grid item>
                        <Typography
                            variant="h6"
                            sx={{ color: theme.palette.mode === 'dark' ? theme.palette.dark.light : theme.palette.primary[800] }}
                        >
                            Progress
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="h6" color="inherit">{`${Math.round(value)}%`}</Typography>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item>
                <BorderLinearProgress variant="determinate" value={value} {...others} />
            </Grid>
        </Grid>
    );
}

LinearProgressWithLabel.propTypes = {
    value: PropTypes.number
};

// ==============================|| SIDEBAR - MENU CARD ||============================== //

const MenuCard = () => {
    const theme = useTheme();
    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    return (
        <>
            <CardStyle style={{ backgroundColor: '#cbab70' }} onClick={handleClickOpen}>
                <CardContent sx={{ p: 2 }}>
                    <List sx={{ p: 0, m: 0 }}>
                        <ListItem alignItems="flex-start" disableGutters sx={{ p: 0 }}>
                            <ListItemAvatar sx={{ mt: 0 }}>
                                <Avatar
                                    variant="rounded"
                                    sx={{
                                        ...theme.typography.commonAvatar,
                                        ...theme.typography.largeAvatar,
                                        color: '#cbab70',
                                        border: theme.palette.mode === 'dark' ? '1px solid' : 'none',
                                        borderColor: theme.palette.primary.main,
                                        background: theme.palette.mode === 'dark' ? theme.palette.dark.dark : '#fff',
                                        marginRight: '12px',
                                        marginTop: '17px'
                                    }}
                                >
                                    <ThumbUpOffAltIcon />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText
                                sx={{ mt: 0 }}
                                primary={
                                    <Typography variant="subtitle1" sx={{ color: '#fff', cursor: 'pointer' }}>
                                        Feedback
                                    </Typography>
                                }
                                secondary={
                                    <Typography variant="caption" sx={{ color: '#fff', cursor: 'pointer' }}>
                                        {' '}
                                        Please tell us your openion and Feedback.
                                    </Typography>
                                }
                            />
                        </ListItem>
                    </List>
                </CardContent>
            </CardStyle>
            <div style={{ width: '100%' }}>
                <FeedBackDialog open={open} setOpen={setOpen} />
            </div>
        </>
    );
};

export default memo(MenuCard);
