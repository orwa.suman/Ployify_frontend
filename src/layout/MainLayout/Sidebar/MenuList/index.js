import { memo, useEffect } from 'react';

// material-ui
import { Typography } from '@mui/material';

// project imports
import NavGroup from './NavGroup';
import menuItem from 'menu-items';
import plans from 'menu-items/plans';
import { loggedInUser } from 'contexts/JWTContext';
import statistics from 'menu-items/statistics';
import application from 'menu-items/application';
import dashboard from 'menu-items/dashboard';

// ==============================|| SIDEBAR MENU LIST ||============================== //



const MenuList = () => {
    useEffect(() => {
        localStorage.getItem('loggedinUserRole');
        
    }, []);

    const menuItems = {
        items:
            localStorage.getItem('loggedinUserRole') === 'ROLE_USER'
                ? [plans]
                : localStorage.getItem('loggedinUserRole') === 'ROLE_MANAGERUSER'
                ? [plans]
                : localStorage.getItem('loggedinUserRole') === 'ROLE_SUPERUSER'
                ? [dashboard, plans]
                : [dashboard, application, plans]
    };

    const navItems = menuItems?.items?.map((item) => {
        switch (item.type) {
            case 'group':
                return <NavGroup key={item.id} item={item} />;
            default:
                return (
                    <Typography key={item.id} variant="h6" color="error" align="center">
                        Menu Items Error
                    </Typography>
                );
        }
    });

    return <>{navItems}</>;
};

export default memo(MenuList);
