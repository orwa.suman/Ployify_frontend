import React from 'react';
import SeminarCard from './SeminarCard';

export default {
    component: SeminarCard,
    title: 'SeminarCard'
};

const Template = (args) => <SeminarCard {...args} />;

export const Default = Template.bind({});
Default.args = {
    authorImage: 'https://via.placeholder.com/50',
    image: 'https://via.placeholder.com/150',
    title: 'Seminar Title',
    description: 'Seminar Description',
    authorImage: 'https://via.placeholder.com/50'
};
