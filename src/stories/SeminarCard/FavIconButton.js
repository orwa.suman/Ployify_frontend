import { Favorite } from '@mui/icons-material';
import { IconButton } from '@mui/material';

const FavIconButton = ({ checkFavorit, removeFromFavorite, setAsFavorite, row }) => {
    return (
        <IconButton
            style={{
                position: 'absolute',
                top: 0,
                right: 0,
                zIndex: 1,
                backgroundColor: 'white',
                '& .MuiSvgIcon-root': { fontSize: '1.5rem' },
                color: checkFavorit?.isFavorit ? 'red' : '#BDBDBD'
            }}
        >
            <Favorite onClick={() => (checkFavorit?.isFavorit ? removeFromFavorite(row?.id) : setAsFavorite())} />
        </IconButton>
    );
};

export default FavIconButton;
