import * as React from 'react';

import { Button, CardActionArea } from '@mui/material';
import { addActivityToUserFavorites, addActivityToUserHistory, removeActivityFromUserFavorites } from 'services/userService';

import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import FavIconButton from './FavIconButton';
import { Favorite } from '@mui/icons-material';
import { FormattedMessage } from 'react-intl';
import { IconButton } from '@material-ui/core';
import Typography from '@mui/material/Typography';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';

export default function SeminarCard({
    checkFavorit,
    row,
    image,
    title,
    description,
    autherImage,
    authorName,
    addHeading,
    onClick,
    makeMeFavourate,
    setMakeMeFavourate
}) {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [isFavorit, setIsFavorit] = React.useState(checkFavorit?.isFavorit);

    const setAsFavorite = () => {
        addActivityToUserFavorites({ userId: loggedInUser.id, activityId: row?.id, activityNum: 3 })
            .then((res) => res?.data)
            .then(() => {
                setIsFavorit(true);
                setMakeMeFavourate(true);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
        setMakeMeFavourate(false);
    };

    const removeFromFavorite = (activityId) => {
        removeActivityFromUserFavorites({ userId: loggedInUser?.id, activityId, activityNum: 3 })
            .then((res) => res?.data)
            .then(() => {
                setIsFavorit(false);
                setMakeMeFavourate(true);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
        setMakeMeFavourate(false);
    };

    return (
        <div>
            {addHeading && (
                <h4 style={{ marginBottom: '20px' }}>
                    <FormattedMessage id="Last Seminar" /> <br />
                </h4>
            )}
            <Card
                sx={{
                    maxWidth: 300,
                    // height: 350,
                    backgroundColor: '#fff',
                    border: '1px solid rgba(58, 53, 65, 0.12)',
                    transition: 'box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
                    marginTop: '20px',
                    marginRight: !addHeading && '10px'
                }}
            >
                <div className="careployee-cards__image">
                    <div style={{ position: 'relative' }}>
                        <FavIconButton
                            checkFavorit={checkFavorit}
                            removeFromFavorite={removeFromFavorite}
                            setAsFavorite={setAsFavorite}
                            row={row}
                        />

                        <img
                            src={image ? image : '/images/seminare.jpg'}
                            alt="green iguana"
                            onClick={() => navigate('/activities/seminare/quiz/' + row?.id)}
                            style={{ width: '100%', height: '140px', objectFit: 'cover' }}
                        ></img>
                    </div>
                </div>

                <CardActionArea onClick={onClick}>
                    <CardContent>
                        <div>
                            <div style={{ marginTop: '-10px' }}>
                                <div>
                                    <h6>{title ? title : ''}</h6>
                                </div>
                                {/* <Typography sx={{ overflowWrap: 'anywhere', paddingInlineEnd: '30px' }}>{description}</Typography> */}
                            </div>
                        </div>
                        {autherImage && (
                            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <img
                                    src={autherImage || '/images/avatar.png'}
                                    alt={authorName}
                                    height="50"
                                    width="50"
                                    style={{ borderRadius: '50%', marginTop: '10px', objectFit: 'cover' }}
                                />
                                <Typography style={{ overflowWrap: 'anywhere', marginInlineStart: '20px' }}>{authorName} </Typography>
                            </div>
                        )}
                    </CardContent>
                </CardActionArea>
            </Card>
        </div>
    );
}
