import React from 'react';
import GoalCard from './Card';

export default {
    title: 'Components/GoalCard',
    component: GoalCard
};

const Template = (args) => <GoalCard {...args} />;

export const Default = Template.bind({});
Default.args = {
    title: 'Exercise',
    imageSrc: 'https://i.imgur.com/1tMmAXK.png',
    description: 0,
    progress: 50
};
