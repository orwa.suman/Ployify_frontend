import { Box } from '@mui/material';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import LinearProgress from '@mui/material/LinearProgress';
import MilitaryTechIcon from '@mui/icons-material/MilitaryTech';
import PropTypes from 'prop-types';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import WinnerIcon from 'assets/images/winner-icon.svg';
import { loggedInUser } from 'contexts/JWTContext';
import { makeStyles } from '@material-ui/core/styles';
import { useMediaQuery } from '@mui/material';

const useStyles = makeStyles((theme) => ({
    Cardmain: {
        border: '1px solid rgba(58, 53, 65, 0.12)',
        backgroundColor: '#fff',
        borderRadius: theme.shape.borderRadius,
        transition: 'box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
    },
    Cardroot: {
        display: 'flex',
        flexDirection: 'column',
        padding: theme.spacing(2),
        margin: '0px'
    },
    Cardimage: {
        width: 180,
        height: 150,
        borderTopLeftRadius: '0px',
        borderBottomLeftRadius: '0px',
        marginRight: theme.spacing(2)
    },
    Cardtitle: {
        fontWeight: 400,
        marginBottom: theme.spacing(1)
    },
    Carddescription: {},
    progress: {
        width: '100%',
        height: 10,
        backgroundColor: '#F9FAFC',
        marginBottom: '0px',
        padding: '0px'
    },
    progressBar: {
        backgroundColor: '#FFE57F'
    }
}));

function GoalCard({ mainTitle, title, imageSrc, description, progress }) {
    // const classes = useStyles();
    const isMobile = useMediaQuery('(max-width:600px)');

    return (
        <Box
            sx={{
                backgroundColor: 'white',
                padding: '28px',
                border: '1px solid rgba(58, 53, 65, 0.12)',
                borderRadius: '8px'
                // marginTop: '60px'
            }}
        >
            <MilitaryTechIcon sx={{ color: '#cbab70' }} />
            <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Box>
                    <Typography style={{ marginBottom: '10px', fontSize: '16px', color: 'gray' }}>Your Available Points</Typography>
                    <Box sx={{ display: 'flex', alignItems: 'baseline' }}>
                        <Typography style={{ fontWeight: 700, fontSize: '26px' }}>{loggedInUser?.userInfo?.points}</Typography>
                        &nbsp;
                        <Typography style={{ fontSize: '16px' }}> pts.</Typography>
                    </Box>
                </Box>
                <img src={WinnerIcon} style={{ height: 200 }} />
            </Box>
            {loggedInUser?.userInfo?.rewardGoal && (
                <>
                    <Box sx={{ display: 'flex', justifyContent: 'space-between', mb: '4px' }}>
                        <Typography sx={{}}>Points untill next reward</Typography>
                        <Box sx={{ display: 'flex' }}>
                            <Typography style={{ fontWeight: '700', color: 'black' }}>
                                {loggedInUser?.userInfo?.points - loggedInUser?.userInfo?.rewardGoal?.rewardPoints}/
                                {loggedInUser?.userInfo?.rewardGoal?.rewardPoints}
                            </Typography>
                            &nbsp;
                            <Typography style={{ fontSize: '16px' }}> pts</Typography>
                        </Box>
                    </Box>
                    <LinearProgress
                        variant="determinate"
                        value={loggedInUser?.userInfo?.points - loggedInUser?.userInfo?.rewardGoal?.rewardPoints}
                        sx={{
                            backgroundColor: '#cbab7075',
                            '& .MuiLinearProgress-bar': {
                                backgroundColor: '#cbab70'
                            }
                        }}
                    />
                </>
            )}

            {/* {false && (
                <div>
                    <Typography variant="h4" className={classes.Cardtitle} style={{ marginBottom: '20px' }}>
                        {mainTitle}
                    </Typography>
                    <Card className={classes.Cardmain}>
                        <div style={{ display: 'flex', flexWrap: isMobile && 'wrap' }}>
                            <img src={imageSrc} alt={title} className={classes.Cardimage} />
                            <div style={{ width: '100%' }}>
                                <div className={classes.Cardroot}>
                                    <CardContent>
                                        <h5 className={classes.Cardtitle}>{title}</h5>
                                        <p className={classes.Carddescription}>
                                            <span style={{ fontWeight: 'bold' }}> {description}</span> Points until you get reward🏅
                                        </p>
                                    </CardContent>
                                </div>
                                <LinearProgress
                                    variant="determinate"
                                    value={progress}
                                    classes={{
                                        root: classes.progress,
                                        bar: classes.progressBar
                                    }}
                                />
                            </div>
                        </div>
                    </Card>
                </div>
            )} */}
        </Box>
    );
}

GoalCard.propTypes = {
    title: PropTypes.string.isRequired,
    imageSrc: PropTypes.string.isRequired,
    description: PropTypes.number.isRequired,
    progress: PropTypes.number.isRequired
};

export default GoalCard;
