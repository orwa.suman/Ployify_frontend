const Intensity = ({ level }) => {
    return (
        <div>
            {level === 'BEGINNER' ? (
                <div style={{ display: 'flex', marginLeft: '10px' }}>
                    <div style={{ backgroundColor: '#98C84E', width: '25px', height: '10px', borderRadius: '10px' }}></div>
                </div>
            ) : level === 'STANDARD' ? (
                <div style={{ display: 'flex', marginLeft: '10px' }}>
                    <div style={{ backgroundColor: '#A1C84C', width: '25px', height: '10px', borderRadius: '10px' }}></div>
                    <div
                        style={{ backgroundColor: '#F9A528', width: '25px', height: '10px', borderRadius: '10px', marginLeft: '5px' }}
                    ></div>
                </div>
            ) : level === 'ADVANCED' ? (
                <div style={{ display: 'flex', marginLeft: '10px' }}>
                    <div style={{ backgroundColor: '#A1C84C', width: '25px', height: '10px', borderRadius: '10px' }}></div>
                    <div
                        style={{ backgroundColor: '#F9A528', width: '25px', height: '10px', borderRadius: '10px', marginLeft: '5px' }}
                    ></div>
                    <div
                        style={{ backgroundColor: '#F14920', width: '25px', height: '10px', borderRadius: '10px', marginLeft: '5px' }}
                    ></div>
                </div>
            ) : (
                <div style={{ display: 'flex', marginLeft: '10px' }}>
                    <div style={{ backgroundColor: '#A1C84C', width: '25px', height: '10px', borderRadius: '10px' }}></div>
                    <div
                        style={{ backgroundColor: '#F9A528', width: '25px', height: '10px', borderRadius: '10px', marginLeft: '5px' }}
                    ></div>
                    <div
                        style={{ backgroundColor: '#F14920', width: '25px', height: '10px', borderRadius: '10px', marginLeft: '5px' }}
                    ></div>
                    <div style={{ backgroundColor: 'red', width: '25px', height: '10px', borderRadius: '10px', marginLeft: '5px' }}></div>
                </div>
            )}
        </div>
    );
};

export default Intensity;
