// import { createMuiTheme } from '@material-ui/core/styles';

import { createTheme } from '@material-ui/core/styles';

// const theme = createMuiTheme({
const theme = createTheme({
    typography: {
        h1: {
            fontFamily: "'Lato', sans-serif",
            fontWeight: 700
        },
        h2: {
            fontFamily: "'Lato', sans-serif",
            fontWeight: 700
        },
        h3: {
            fontFamily: "'Lato', sans-serif",
            fontWeight: 700
        },
        h4: {
            fontFamily: "'Lato', sans-serif",
            fontWeight: 700
        },
        h5: {
            fontFamily: "'Lato', sans-serif",
            fontWeight: 700
        },
        h6: {
            fontFamily: "'Lato', sans-serif",
            fontWeight: 700
        }
    }
});

export default theme;
