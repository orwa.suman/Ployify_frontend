export const getBase64 = (file) => {
  return new Promise((resolve, reject) => {
      const reader = new FileReader();
      //if (file && file.type.match('image.*')) {
      if (file) {
          reader.readAsDataURL(file);
      }
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
  });
};