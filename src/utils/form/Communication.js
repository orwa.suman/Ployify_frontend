import { openSnackbar } from 'store/slices/snackbar';

export const showMessage = (message, type = 'error') =>
    openSnackbar({
        open: true,
        message,
        variant: 'alert',
        alert: {
            color: type,
            type: type
        }
        // ,close: false
    });
