import { Autocomplete, Grid, TextField } from '@mui/material';
import { useEffect, useState } from 'react';

import Box from '@mui/material/Box';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MDEditor from '@uiw/react-md-editor';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import PropTypes from 'prop-types';
import { makeStyles } from '@mui/styles';
import { useTheme } from '@mui/material/styles';

// material-ui

// assets

const useStyles = makeStyles({
    root: {
        '&.Mui-disabled': {
            backgroundColor: 'transparent'
        }
    }
});

// ==============================|| ACCORDION ||============================== //

const Accordion = ({ itemDescription, defaultExpandedId = null, expandIcon, square, toggle, selectExtraDish, dishlist }) => {
    const theme = useTheme();

    
    

    const [expanded, setExpanded] = useState(null);
    const handleChange = (panel) => (event, newExpanded) => toggle && setExpanded(newExpanded ? panel : false);
    const classes = useStyles();
    function isMarkdown(content) {
        const markdownRegex = /(^|\n)(\s{0,3}#{1,6}\s+)|(\n\s*[-*+]\s)|(\[.+?\]\(.+?\))/g;
        return markdownRegex.test(content);
    }

    const [selectedExtraDish, setSelectedExtraDish] = useState('');
    const handleExtraDishSelection = (value, dishIndex) => {
        selectExtraDish(value, dishIndex);
        setSelectedExtraDish(value.name);
        // Additional code specific to the component...
    };

    useEffect(() => {
        setExpanded(defaultExpandedId);
    }, [defaultExpandedId]);

    return (
        <Box sx={{ width: '100%' }}>
            {itemDescription &&
                itemDescription.map((item, index) => (
                    <MuiAccordion
                        key={item.id}
                        defaultExpanded={(!item.disabled && item.defaultExpand) || !isMarkdown(item.content)}
                        expanded={(!toggle && !item.disabled && item.expanded) || (toggle && expanded === item.id)}
                        disabled={item.disabled || !isMarkdown(item.content)}
                        square={square}
                        onChange={handleChange(item.id)}
                        classes={{ root: classes.root }}
                    >
                        <MuiAccordionSummary
                            expandIcon={
                                isMarkdown(item.content) ? expandIcon || expandIcon === false ? expandIcon : <ExpandMoreIcon /> : null
                            }
                            sx={{
                                color: theme.palette.mode === 'dark' ? 'grey.500' : 'grey.800',
                                fontWeight: 500,
                                backgroundColor: !isMarkdown(item.content) && 'transparent'
                            }}
                        >
                            {item.title}
                        </MuiAccordionSummary>
                        {isMarkdown(item.content) ? (
                            <div data-color-mode="light" style={{ padding: '10px' }}>
                                <MDEditor.Markdown source={item.content} style={{ whiteSpace: 'pre-wrap' }} />
                                {item.hasExtraDish ? (
                                    <div style={{ width: '100%', marginLeft: '20px' }}>
                                        <h5>Extra Dishes: </h5>
                                        {dishlist
                                            ?.filter((dish) => !!dish?.extraDishesList && dish?.extraDishesList?.length)
                                            ?.map((dish, dishIndex) => (
                                                <>
                                                    <Grid container>
                                                        <Grid item xs={12} md={4}>
                                                            <Autocomplete
                                                                style={{ height: '44px' }}
                                                                disablePortal
                                                                options={dish.extraDishesList}
                                                                getOptionLabel={(option) => option.name}
                                                                value={
                                                                    dish.extraDishesList.find((extraDish) => extraDish.selected) || {
                                                                        name: ''
                                                                    }
                                                                }
                                                                onChange={(event, value) => handleExtraDishSelection(value, index - 1)}
                                                                renderInput={(params) => <TextField {...params} />}
                                                            />
                                                        </Grid>
                                                    </Grid>
                                                </>
                                            ))}
                                    </div>
                                ) : (
                                    ''
                                )}
                            </div>
                        ) : (
                            <MuiAccordionDetails>{item.content} </MuiAccordionDetails>
                        )}
                    </MuiAccordion>
                ))}
        </Box>
    );
};

Accordion.propTypes = {
    itemDescription: PropTypes.array,
    defaultExpandedId: PropTypes.string,
    expandIcon: PropTypes.object,
    square: PropTypes.bool,
    toggle: PropTypes.bool
};

export default Accordion;
