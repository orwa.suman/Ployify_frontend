import React from 'react';

// material-ui
import { Grid, Typography, Avatar, Link } from '@mui/material';
import AssignmentIcon from '@mui/icons-material/AssignmentIndTwoTone';

const Author = ({ author, color }) => {
    return (
        <Typography component="span"    sx={{ fontSize: '12px', marginBottom: '24px', marginTop: '40px' }}>
         {/*
           <Link to={`/activities/Author/info/${author.id}/${author.firstName}/${author.lastName}`}></Link>
        */}
           <Avatar
                sx={{
                    width: '24px',
                    height: '24px',
                    display: 'inline-flex',
                    position: 'relative',
                    top: '6px',
                    margin: '0 0'
                }}
                variant="rounded"
                src={author?.picture} 
            >
                <AssignmentIcon size="11px" />
            </Avatar>
            <span style={{ paddingLeft: '8px', color}}>
                {`${author?.firstName} ${author?.lastName}`}
            </span>
        </Typography>
    );
};

export default Author;
