import { Badge, Fab, IconButton } from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';

import { AddShoppingCart } from '@mui/icons-material';
import { Link } from 'react-router-dom';
import ShoppingCartTwoToneIcon from '@mui/icons-material/ShoppingCartTwoTone';
import { sum } from 'lodash';
import { useSelector } from 'store';

// material-ui



// assets



const StyledBadge = styled(Badge)(({ theme }) => ({
    '& .MuiBadge-badge': {
        right: 0,
        top: 3,
        border: `2px solid ${theme.palette.background.paper}`,
        padding: '0 4px'
    }
}));

// ==============================|| CART ITEMS - FLOATING BUTTON ||============================== //
const FloatingCart = ({ fromMain }) => {
    const theme = useTheme();
    const cart = useSelector((state) => state.cart);
    const totalQuantity = sum(cart.checkout.products.map((item) => item.quantity));

    return (
        <Fab
            component={Link}
            // to="/activities/Reciepts/checkout"
            size="large"
            sx={{
                top: '50%',
                position: 'fixed',
                right: 0,
                zIndex: theme.zIndex.speedDial,
                boxShadow: theme.customShadows.warning,
                bgcolor: '#cbab70',
                color: 'warning.light',
                borderRadius: '8px',
                borderTopRightRadius: 0,
                borderBottomRightRadius: 0,
                '&:hover': {
                    bgcolor: '#cbab70',
                    color: 'warning.light'
                }
            }}
        >
            <IconButton disableRipple aria-label="cart" sx={{ '&:hover': { bgcolor: 'transparent' } }} size="large">
                <StyledBadge>{fromMain ? <ShoppingCartTwoToneIcon sx={{ color: 'grey.800' }} /> : <AddShoppingCart />}</StyledBadge>
            </IconButton>
        </Fab>
    );
};

export default FloatingCart;
