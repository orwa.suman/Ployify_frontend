import { Box, Card, CardContent, Grid, LinearProgress, Typography } from '@mui/material';

import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';

// material-ui

// styles
const IconWrapper = styled('div')({
    position: 'absolute',
    left: '-17px',
    bottom: '-27px',
    color: '#fff',
    transform: 'rotate(25deg)',
    '&> svg': {
        width: '100px',
        height: '100px',
        opacity: '0.35'
    }
});

// =============================|| USER NUM CARD ||============================= //

const UserCountCard = ({ primary, secondary, iconPrimary, userRemainingPoints, color, rewardGoalPoints, userActualPoints }) => {
    const IconPrimary = iconPrimary;
    const primaryIcon = iconPrimary ? <IconPrimary fontSize="large" /> : null;
    return (
        <Card
            sx={{
                backgroundColor: '#cbab70 !important',
                '&:hover': {
                    transform: 'scale(1.03)',
                    animation: 'scaleBtn 1 ease-in-out'
                },
                position: 'relative',
                color: '#fff'
            }}
        >
            <CardContent>
                <IconWrapper>{primaryIcon}</IconWrapper>
                <Grid container direction="column" justifyContent="center" alignItems="center" spacing={1}>
                    <Grid item sm={12} style={{ color: 'white' }}>
                        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            {!userRemainingPoints && (
                                <Typography variant="h7" align="center" color="inherit">
                                    {secondary}
                                </Typography>
                            )}

                            {/*   <Grid item sm={12}>
                        <Typography variant="h7" align="center" color="inherit">
                            {primary}
                        </Typography>
                    </Grid> */}
                            {!!userRemainingPoints && (
                                <Box sx={{ display: 'flex', marginInlineStart: 2, justifyContent: 'center', alignItems: 'center' }}>
                                    <LinearProgress
                                        variant="determinate"
                                        value={(userRemainingPoints * 100) / rewardGoalPoints}
                                        sx={{ width: '200px' }}
                                        color="inherit"
                                    />
                                    <Typography variant="caption" component="div" color="white" sx={{ marginInlineStart: '6px' }}>
                                        {/* {(userRemainingPoints * 100) / rewardGoalPoints}% */}
                                        {userActualPoints} / {rewardGoalPoints}
                                    </Typography>
                                </Box>
                            )}
                        </Box>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    );
};

UserCountCard.propTypes = {
    primary: PropTypes.string,
    secondary: PropTypes.string,
    iconPrimary: PropTypes.any,
    color: PropTypes.string
};

export default UserCountCard;
