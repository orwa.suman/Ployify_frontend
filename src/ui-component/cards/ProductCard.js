import { Button, Card, CardContent, Grid, IconButton, Stack, Typography } from '@mui/material';
import { Favorite, PlayCircleRounded } from '@mui/icons-material';
import { useEffect, useState } from 'react';

import AccessTimeIcon from '@mui/icons-material/AccessTime';
import Intensity from 'stories/Intensity/Intensity';
import MainCard from './MainCard';
import PropTypes from 'prop-types';
import SignalCellular1BarIcon from '@mui/icons-material/SignalCellular1Bar';
import SignalCellular3BarIcon from '@mui/icons-material/SignalCellular3Bar';
import SignalCellular4BarIcon from '@mui/icons-material/SignalCellular4Bar';
import SkeletonProductPlaceholder from 'ui-component/cards/Skeleton/ProductPlaceholder';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import { makeStyles } from '@material-ui/core/styles';
import { useTheme } from '@mui/styles';

// material-ui

// project import

// ==============================|| PRODUCT CARD ||============================== //

const useStyles = makeStyles((theme) => ({
    main: {
        border: '1px solid rgba(58, 53, 65, 0.12)',
        width: '290px',
        height: '370px',
        backgroundColor: '#fff',
        borderRadius: theme.shape.borderRadius,
        // transition: 'box-shadow 0.2s ease-in-out',
        '&:hover': {},
        margin: '10px',
        marginRight: '10px'
    }
}));

const ProductCard = ({
    id,
    title,
    information,
    image,
    onAddFavorite,
    removeFromFavourate,
    levelOfAdvance,
    duration,
    picture,
    mostUsed,
    isFavorit,
    makeMeFavourate
}) => {
    const [isLoading, setLoading] = useState(true);

    useEffect(() => {
        setLoading(false);
    }, []);

    const theme = useTheme();
    const classes = useStyles();
    return (
        <>
            {isLoading ? (
                <SkeletonProductPlaceholder />
            ) : (
                <Card content={false} className={classes.main}>
                    <div className="careployee-cards__image">
                        <div style={{ position: 'relative' }}>
                            <IconButton
                                // disabled={isFavorit}
                                onClick={(event) => {
                                    event.stopPropagation();
                                    isFavorit ? removeFromFavourate(id) : onAddFavorite(id);
                                }}
                                sx={{
                                    position: 'absolute',
                                    top: 0,
                                    right: 0,
                                    zIndex: 1,
                                    backgroundColor: 'white',
                                    '& .MuiSvgIcon-root': { fontSize: '1.5rem' },
                                    color: isFavorit ? 'red' : '#BDBDBD'
                                }}
                                size="large"
                            >
                                <Favorite />
                            </IconButton>
                        </div>
                        <img style={{ width: '100%' }} src={picture ? picture : '/images/training.jpg'} alt={title}></img>
                    </div>
                    <CardContent sx={{ p: 2 }} className="careployee-cards__box">
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                {mostUsed ? (
                                    <div class="careployee-cards__box__most-used">
                                        <WhatshotIcon
                                            color="primary"
                                            sx={{
                                                color: theme.palette.orange.dark,
                                                borderColor: theme.palette.orange.main
                                            }}
                                            size="large"
                                        />
                                    </div>
                                ) : (
                                    <></>
                                )}
                                <h6 style={{ textDecoration: 'none' }}>{title}</h6>
                            </Grid>

                            <Grid item xs={12}>
                                <Stack direction="row" justifyContent="space-between" alignItems="center" sx={{ paddingBottom: '5px' }}>
                                    <Grid container spacing={1}>
                                        <div style={{ display: 'flex', alignItems: 'center' }}>
                                            <Intensity level={levelOfAdvance}></Intensity>
                                        </div>
                                    </Grid>
                                </Stack>
                                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Grid container>
                                        <Grid item>
                                            <AccessTimeIcon style={{ width: '20px', height: '20px' }} />
                                        </Grid>
                                        <Grid item>
                                            <Typography
                                                variant="p"
                                                sx={{
                                                    color: theme.palette.mode === 'dark' ? 'white' : 'black',

                                                    paddingLeft: '10px'
                                                }}
                                            >
                                                {duration}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            )}
        </>
    );
};

ProductCard.propTypes = {
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    color: PropTypes.string,
    title: PropTypes.string,
    image: PropTypes.string,
    information: PropTypes.string,
    levelOfAdvance: PropTypes.string,
    duration: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    picture: PropTypes.string
};

export default ProductCard;
