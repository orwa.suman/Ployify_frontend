import { LOGIN, LOGOUT } from 'store/actions';
import { createContext, useEffect, useReducer } from 'react';

import { Chance } from 'chance';
import { DASHBOARD_PATH } from 'config';
import Loader from 'ui-component/Loader';
import PropTypes from 'prop-types';
import accountReducer from 'store/accountReducer';
import axios from 'axios';
import jwtDecode from 'jwt-decode';

// third-party

// reducer - state management

// project imports

const chance = new Chance();

// constant
const initialState = {
    isLoggedIn: false,
    isInitialized: false,
    user: null,
    redirectPath: '/user/questionnaire'
};

export let token = '';
export let loggedInUser = {};
export const BASE_URL = process.env.REACT_APP_API_URL;
export const app_version = process.env.REACT_APP_FRONTEND_VERSION;




const verifyToken = (serviceToken) => {
    if (!serviceToken) {
        return false;
    }
    const decoded = jwtDecode(serviceToken);
    /**
     * Property 'exp' does not exist on type '<T = unknown>(token, options?: JwtDecodeOptions | undefined) => T'.
     */
    return decoded.exp > Date.now() / 1000;
};

const setSession = (serviceToken) => {
    if (serviceToken) {
        localStorage.setItem('serviceToken', serviceToken);
        token = serviceToken;
        axios.defaults.headers.common.Authorization = `Bearer ${serviceToken}`;
    } else {
        localStorage.removeItem('serviceToken');
        delete axios.defaults.headers.common.Authorization;
    }
};

const setUser = (user) => {
    if (user) {
        loggedInUser = user;
    }
};

// ==============================|| JWT CONTEXT & PROVIDER ||============================== //
const JWTContext = createContext(null);

export const JWTProvider = ({ children }) => {
    const [state, dispatch] = useReducer(accountReducer, initialState);
    const getUserByEmail = async (config) => await axios.get(BASE_URL + '/user/getUserByEmail', config);

    const init = async () => {
        try {
            const serviceToken = window.localStorage.getItem('serviceToken');
            if (serviceToken && verifyToken(serviceToken)) {
                setSession(serviceToken);
                const config = {
                    headers: {
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    }
                };

                
                config.params = { userEmail: localStorage.getItem('email') };
                const logged = await getUserByEmail(config);
                
                // loggedInUser = logged.data;
                setUser(logged.data);

                // .then(res => res.data)
                //     .then(r => {
                //         
                //         loggedInUser = r;
                //     })
                //     .catch(e => {
                //         
                //     });

                // const response = await axios.get('/api/account/me');
                // const { user } = response.data;
                dispatch({
                    type: LOGIN,
                    payload: {
                        isLoggedIn: true
                    }
                });
            } else {
                dispatch({
                    type: LOGOUT
                });
            }
        } catch (err) {
            console.error(err);
            dispatch({
                type: LOGOUT
            });
        }
    };

    useEffect(() => {
        init();
    }, []);

    const login = async (email, password, redirectPath = DASHBOARD_PATH) => {
        const response = await axios.post(BASE_URL + '/user/signIn', { email, password });
        const serviceToken = response.data.substring(6);
        setSession(serviceToken);

        localStorage.setItem('email', email);

        const config = {
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        };

        config.params = { userEmail: email };
        const userResponse = await axios.get(BASE_URL + '/user/getUserByEmail', config);
        setUser(userResponse.data);

        localStorage.setItem('email', email);

        // .then(res => res.data)
        // .then(r => {
        //     
        //     loggedInUser = r;
        // })
        // .catch(e => {
        //     
        // });

        dispatch({
            type: LOGIN,
            payload: {
                isLoggedIn: true,
                redirectPath
            }
        });
    };

    const register = async (email, password, firstName, lastName) => {
        // todo: this flow need to be recode as it not verified
        const id = chance.bb_pin();
        const response = await axios.post('/api/account/register', {
            id,
            email,
            password,
            firstName,
            lastName
        });
        let users = response.data;

        if (window.localStorage.getItem('users') !== undefined && window.localStorage.getItem('users') !== null) {
            const localUsers = window.localStorage.getItem('users');
            users = [
                ...JSON.parse(localUsers),
                {
                    id,
                    email,
                    password,
                    name: `${firstName} ${lastName}`
                }
            ];
        }

        window.localStorage.setItem('users', JSON.stringify(users));
    };

    const logout = () => {
        setSession(null);
        localStorage.removeItem('email');
        dispatch({ type: LOGOUT });
    };

    const resetPassword = (email) => console.log(email)

    const updateProfile = () => {};

    const getUserInfoByEmail = async () => {
        const serviceToken = window.localStorage.getItem('serviceToken');
        if (serviceToken && verifyToken(serviceToken)) {
            setSession(serviceToken);
            const config = {
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                params: {
                    userEmail: localStorage.getItem('email')
                }
            };
            const logged = await getUserByEmail(config);
            setUser(logged.data);
        }
    };

    if (state.isInitialized !== undefined && !state.isInitialized) {
        return <Loader />;
    }

    return (
        <JWTContext.Provider value={{ ...state, login, logout, register, resetPassword, updateProfile, getUserInfoByEmail }}>
            {children}
        </JWTContext.Provider>
    );
};

JWTProvider.propTypes = {
    children: PropTypes.node
};

export default JWTContext;
