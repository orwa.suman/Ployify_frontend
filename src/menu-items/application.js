// third-party
import { FormattedMessage } from 'react-intl';
import { IoFitnessSharp } from 'react-icons/io5';
import { IoIosNutrition } from 'react-icons/io';
import { GiMeditation } from 'react-icons/gi';
import { FaGifts } from 'react-icons/fa';
import { HiPresentationChartBar } from 'react-icons/hi';
import { FiTarget } from 'react-icons/fi';
import Face6Icon from '@mui/icons-material/Face6';
import QuizIcon from '@mui/icons-material/Quiz';
// assets
import { IconUserCheck, IconBasket, IconMessages, IconLayoutKanban, IconMail, IconCalendar, IconNfc } from '@tabler/icons';
import { ThumbUpAltOutlined } from '@mui/icons-material';

// constant
const icons = {
    IconUserCheck,
    IconBasket,
    IconMessages,
    IconLayoutKanban,
    IconMail,
    IconCalendar,
    IconNfc,
    IoFitnessSharp,
    IoIosNutrition,
    GiMeditation,
    FaGifts,
    FiTarget,
    HiPresentationChartBar,
    Face6Icon,
    QuizIcon
};

// ==============================|| APPLICATION MENU ITEMS ||============================== //

const application = {
    id: 'application',
    title: <FormattedMessage id="Admin tool" />,
    type: 'group',
    children: [
        {
            id: 'Companies',
            title: <FormattedMessage id="Companies" />,
            type: 'collapse',
            icon: icons.IconUserCheck,
            children: [
                {
                    id: 'All companies',
                    title: (
                        <>
                            <FormattedMessage id="All companies" />
                        </>
                    ),
                    type: 'item',
                    url: '/dashboard/company/list'
                },
                {
                    id: 'New copmany',
                    title: (
                        <>
                            <FormattedMessage id="New company" />
                        </>
                    ),
                    type: 'item',
                    url: '/dashboard/company/form'
                }
            ]
        },
        {
            id: 'users',
            title: <FormattedMessage id="users" />,
            type: 'collapse',
            icon: icons.IconUserCheck,
            children: [
                {
                    id: 'All Users',
                    title: (
                        <>
                            <FormattedMessage id="All Users" />
                        </>
                    ),
                    type: 'item',
                    url: '/dashboard/user/list'
                }
            ]
        },
        {
            id: 'activities',
            title: <FormattedMessage id="activities" />,
            type: 'collapse',
            icon: icons.IconUserCheck,
            children: [
                {
                    id: 'Training',
                    title: <FormattedMessage id="Training" />,
                    type: 'collapse',
                    icon: icons.IoFitnessSharp,
                    children: [
                        {
                            id: 'Training List',
                            title: (
                                <>
                                    <FormattedMessage id="Training list" />
                                </>
                            ),
                            type: 'item',
                            url: '/dashboard/activities/training/list'
                        },
                        {
                            id: 'Training Form',
                            title: (
                                <>
                                    <FormattedMessage id="Training Form" />
                                </>
                            ),
                            type: 'item',
                            url: '/dashboard/activities/training/form'
                        }
                    ]
                },
                {
                    id: 'Training Plan',
                    title: <FormattedMessage id="Training Plans" />,
                    type: 'collapse',
                    icon: icons.IoFitnessSharp,
                    children: [
                        {
                            id: 'TrainingPlan List',
                            title: (
                                <>
                                    <FormattedMessage id="TrainingPlan List" />
                                </>
                            ),
                            type: 'item',
                            url: '/dashboard/activities/training-plan/list'
                        },
                        {
                            id: 'TrainingPlan Form',
                            title: (
                                <>
                                    <FormattedMessage id="TrainingPlan Form" />
                                </>
                            ),
                            type: 'item',
                            url: '/dashboard/activities/training-plan/form'
                        }
                    ]
                },
                {
                    id: 'Recipes',
                    title: <FormattedMessage id="Recipes" />,
                    type: 'collapse',
                    icon: icons.IoIosNutrition,
                    children: [
                        {
                            id: 'Ingredients',
                            title: (
                                <>
                                    <FormattedMessage id="Ingredients" />
                                </>
                            ),
                            type: 'collapse',
                            icon: icons.IoIosNutrition,
                            children: [
                                {
                                    id: 'Ingredients List',
                                    title: (
                                        <>
                                            <FormattedMessage id="Ingredient List" />
                                        </>
                                    ),
                                    type: 'item',
                                    url: '/dashboard/activities/recipes/ingredient/list'
                                },
                                {
                                    id: 'Ingredient Form',
                                    title: (
                                        <>
                                            <FormattedMessage id="Ingredient Form" />
                                        </>
                                    ),
                                    type: 'item',
                                    url: '/dashboard/activities/recipes/ingredient/form'
                                }
                            ]
                        },
                        {
                            id: 'Dish',
                            title: (
                                <>
                                    <FormattedMessage id="Dishes" />
                                </>
                            ),
                            type: 'collapse',
                            icon: icons.IoIosNutrition,
                            children: [
                                {
                                    id: 'Dish List',
                                    title: (
                                        <>
                                            <FormattedMessage id="Dish List" />
                                        </>
                                    ),
                                    type: 'item',
                                    url: '/dashboard/activities/recipes/dish/list'
                                },
                                {
                                    id: 'Dish Form',
                                    title: (
                                        <>
                                            <FormattedMessage id="Dish Form" />
                                        </>
                                    ),
                                    type: 'item',
                                    url: '/dashboard/activities/recipes/dish/form'
                                }
                            ]
                        },
                        {
                            id: 'Recipes',
                            title: (
                                <>
                                    <FormattedMessage id="Recipes" />
                                </>
                            ),
                            type: 'collapse',
                            icon: icons.IoIosNutrition,
                            children: [
                                {
                                    id: 'Recipe List',
                                    title: (
                                        <>
                                            <FormattedMessage id="Recipe List" />
                                        </>
                                    ),
                                    type: 'item',
                                    url: '/dashboard/activities/recipes/list'
                                },
                                {
                                    id: 'Recipe Form',
                                    title: (
                                        <>
                                            <FormattedMessage id="Recipe Form" />
                                        </>
                                    ),
                                    type: 'item',
                                    url: '/dashboard/activities/recipes/form'
                                }
                            ]
                        }
                    ]
                },
                {
                    id: 'Mental Health',
                    title: <FormattedMessage id="Mental Health" />,
                    type: 'collapse',
                    icon: icons.GiMeditation,
                    children: [
                        {
                            id: 'Mental Health List',
                            title: (
                                <>
                                    <FormattedMessage id="Mental Health List" />
                                </>
                            ),
                            type: 'item',
                            url: '/dashboard/activities/mental-health/list'
                        },
                        {
                            id: 'Mental Health form',
                            title: (
                                <>
                                    <FormattedMessage id="Mental Health Form" />
                                </>
                            ),
                            type: 'item',
                            url: '/dashboard/activities/mental-health/form'
                        }
                    ]
                },
                {
                    id: 'Seminare',
                    title: <FormattedMessage id="Seminare" />,
                    type: 'collapse',
                    icon: icons.IconUserCheck,
                    children: [
                        {
                            id: 'Seminare List',
                            title: (
                                <>
                                    <FormattedMessage id="Seminare List" />
                                </>
                            ),
                            type: 'item',
                            url: '/dashboard/activities/seminar/list'
                        },
                        {
                            id: 'Seminare Form',
                            title: <FormattedMessage id="Seminare Form" />,
                            type: 'item',
                            url: '/dashboard/activities/seminar/form'
                        }
                    ]
                }
            ]
        },

        {
            id: 'Rewards',
            title: <FormattedMessage id="Rewards" />,
            type: 'collapse',
            icon: icons.FiTarget,
            children: [
                {
                    id: 'Rewards',
                    title: <FormattedMessage id="Rewards" />,
                    type: 'item',
                    url: '/dashboard/reward/list'
                },
                {
                    id: 'Base Reward Form',
                    title: <FormattedMessage id="Base Reward Form" />,
                    type: 'item',
                    url: '/dashboard/reward/form'
                }
            ]
        },
        {
            id: 'Authors',
            title: <FormattedMessage id="Authors" />,
            type: 'collapse',
            icon: icons.Face6Icon,
            children: [
                {
                    id: 'Authors List',
                    title: (
                        <>
                            <FormattedMessage id="Authors List" />
                        </>
                    ),
                    type: 'item',
                    url: '/dashboard/author/list'
                },
                {
                    id: 'Author Form',
                    title: (
                        <>
                            <FormattedMessage id="Author Form" />
                        </>
                    ),
                    type: 'item',
                    url: '/dashboard/author/form'
                }
            ]
        },
        // {
        //     id: 'Questionnaire',
        //     title: <FormattedMessage id="Questionnaire" />,
        //     type: 'item',
        //     url: '/dashboard/user/questionnaire/list',
        //     icon: icons.QuizIcon
        // },
        {
            id: 'Feedback',
            title: <FormattedMessage id="Feedback" />,
            type: 'item',
            url: '/dashboard/user/feedback/list',
            icon: ThumbUpAltOutlined
        }
    ]
};

export default application;
