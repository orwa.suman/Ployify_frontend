// third-party
import { FormattedMessage } from 'react-intl';
import { IoFitnessSharp, IoNutrition } from 'react-icons/io5';
import { IoIosNutrition } from 'react-icons/io';
import { GiMeditation, GiOfficeChair } from 'react-icons/gi';
import { FaGifts, FaCouch } from 'react-icons/fa';
import { HiOutlinePresentationChartBar } from 'react-icons/hi';
import { RiMentalHealthFill, RiDashboard2Fill } from 'react-icons/ri';
import { FiTarget } from 'react-icons/fi';
import { MdManageAccounts } from 'react-icons/md';

import SportsScoreIcon from '@mui/icons-material/SportsScore';
// assets
import { IconUserCheck, IconBasket, IconMessages, IconLayoutKanban, IconMail, IconCalendar, IconNfc } from '@tabler/icons';

// constant
const icons = {
    IconUserCheck,
    IconBasket,
    IconMessages,
    IconLayoutKanban,
    IconMail,
    IconCalendar,
    IconNfc,
    IoFitnessSharp,
    IoIosNutrition,
    GiMeditation,
    FaGifts,
    GiOfficeChair,
    FaCouch,
    IoNutrition,
    RiMentalHealthFill,
    FiTarget,
    RiDashboard2Fill,
    HiOutlinePresentationChartBar,
    MdManageAccounts,
    SportsScoreIcon
};

// ==============================|| APPLICATION MENU ITEMS ||============================== //

const plans = {
    id: 'activities',
    title: '',
    type: 'group',
    children: [
        {
            id: 'Training at home',
            title: <FormattedMessage id="Training at home" />,
            type: 'item',
            icon: icons.FaCouch,
            url: '/activities/TrainingPlan/cards/HOME'
        },
        {
            id: 'Training at office',
            title: <FormattedMessage id="Training at office" />,
            type: 'item',
            icon: icons.GiOfficeChair,
            url: '/activities/TrainingPlan/cards/OFFICE'
        },
        {
            id: 'Recipes',
            title: <FormattedMessage id="Recipes" />,
            type: 'item',
            icon: icons.IoNutrition,
            url: '/activities/Reciepts/cards'
        },
        {
            id: 'Mental Health',
            title: <FormattedMessage id="Mental Health" />,
            type: 'item',
            icon: icons.RiMentalHealthFill,
            url: '/activities/MentalHealth/cards'
        },
        {
            id: 'Seminare',
            title: <FormattedMessage id="Seminare" />,
            type: 'item',
            icon: icons.IconUserCheck,
            url: '/activities/seminare/cards'
        },
        {
            id: 'Rewards',
            title: <FormattedMessage id="Rewards" />,
            type: 'item',
            icon: icons.SportsScoreIcon,
            url: '/reward/info'
        }
    ]
};

export default plans;
