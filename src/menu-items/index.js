import dashboard from './dashboard';
import application from './application';
import plans from './plans';
import { loggedInUser } from 'contexts/JWTContext';
import statistics from './statistics';

// ==============================|| MENU ITEMS ||============================== //

const menuItems = {
    items:
        loggedInUser.role === 'ROLE_USER'
            ? [plans]
            : loggedInUser.role === 'ROLE_MANAGERUSER'
            ? [plans]
            : loggedInUser.role === 'ROLE_SUPERUSER'
            ? [dashboard, plans]
            : [dashboard, application, plans]
};

export default menuItems;
