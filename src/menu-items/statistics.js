import { FormattedMessage } from 'react-intl';
import { HiPresentationChartBar } from 'react-icons/hi';

const icons = {
    HiPresentationChartBar
};

const statistics = {
    id: 'Statistics',
    title: '',
    type: 'group',
    icon: icons.HiPresentationChartBar,
    children: [
        {
            id: 'Statistics',
            title: <FormattedMessage id="Statistics" />,
            type: 'collapse',
            icon: icons.HiPresentationChartBar,
            children: [
                {
                    id: 'Statistics',
                    title: (
                        <>
                            <FormattedMessage id="Statistics" />
                        </>
                    ),
                    type: 'item',
                    url: '/dashboard/statistics'
                }
            ]
        }
    ]
};

export default statistics;
