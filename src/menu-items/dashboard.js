// third-party
import { FormattedMessage } from 'react-intl';
import { RiDashboard2Fill } from 'react-icons/ri';
import { MdManageAccounts } from 'react-icons/md';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
// assets
import { IconDashboard, IconDeviceAnalytics } from '@tabler/icons';

// constant
const icons = {
    IconDashboard,
    IconDeviceAnalytics,
    RiDashboard2Fill
};

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const dashboard = {
    id: 'dashboard',
    title: '',
    type: 'group',
    icon: icons.RiDashboard2Fill,
    children: [
        {
            id: 'Home',
            title: <FormattedMessage id="Home" />,
            type: 'item',
            url: '/dashboard/default',
            icon: icons.IconDashboard,
            breadcrumbs: false
        },

        {
            id: 'managementTool',
            title: <FormattedMessage id="Management Tool" />,
            type: 'collapse',
            icon: ManageAccountsIcon,
            children: [
                {
                    id: 'Company Hierarchy',
                    title: <FormattedMessage id="Organization" />,

                    type: 'item',
                    url: '/dashboard/management/organization'
                },
                {
                    id: 'Users',
                    title: <FormattedMessage id="Users" />,

                    type: 'item',
                    url: '/dashboard/management/users'
                },
                {
                    id: 'User codes',
                    title: <FormattedMessage id="User codes" />,

                    type: 'item',
                    url: '/dashboard/management/codes'
                },
                {
                    id: 'Rewards',
                    title: <FormattedMessage id="Rewards" />,

                    type: 'item',
                    url: '/dashboard/management/rewards'
                },
                {
                    id: 'Redeemed Rewards',
                    title: <FormattedMessage id="Redeemed Rewards" />,

                    type: 'item',
                    url: '/dashboard/management/redeemed-rewards'
                }
            ]
        }
    ]
};

export default dashboard;
