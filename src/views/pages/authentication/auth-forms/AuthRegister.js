import * as Yup from 'yup';

import {
    Autocomplete,
    Box,
    Button,
    Checkbox,
    Divider,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    TextField,
    Typography,
    useMediaQuery
} from '@mui/material';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import React, { useEffect } from 'react';
import { strengthColor, strengthIndicatorNumFunc } from 'utils/password-strength';

import AnimateButton from 'ui-component/extended/AnimateButton';
import { FormattedMessage } from 'react-intl';
import { Formik } from 'formik';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { showMessage } from 'utils/form/Communication';
import { signUp } from 'services/userService';
import useAuth from 'hooks/useAuth';
import { useDispatch } from 'store';
import { useTheme } from '@mui/material/styles';

// material-ui

// third party

// assets

// ===========================|| FIREBASE - REGISTER ||=========================== //

const FirebaseRegister = ({ ...others }) => {
    const location = useLocation();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const theme = useTheme();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
    const [showPassword, setShowPassword] = React.useState(false);
    const [checked, setChecked] = React.useState(true);
    const [formEnabled, setFormEnabled] = React.useState(false);

    const [strength, setStrength] = React.useState(0);
    const [level, setLevel] = React.useState();
    const [confirmPassword, setConfirmPassword] = React.useState('');

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const changePassword = (value) => {
        const temp = strengthIndicatorNumFunc(value);
        setStrength(temp);
        setLevel(strengthColor(temp));
    };

    const hanldeConfirmChange = (confirmPassword, password) => {
        setConfirmPassword(confirmPassword);
        if (confirmPassword === password) {
            setFormEnabled(true);
        } else {
            setFormEnabled(false);
        }
    };

    useEffect(() => {
        changePassword('123456');
    }, []);

    return (
        <>
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    email: '',
                    department: '',
                    password: '',
                    confirmPassword: '',
                    personalNumber: '',
                    companyName: location.state.company?.companyName,
                    registrationCode: location.state.code.registrationCode
                }}
                validationSchema={Yup.object().shape({
                    email: Yup.string()
                        .max(255)
                        .required(<FormattedMessage id="Required" />),
                    password: Yup.string()
                        .max(255)
                        .required(<FormattedMessage id="Required" />),
                    confirmPassword: Yup.string()
                        .max(255)
                        .required(<FormattedMessage id="Required" />)
                })}
                onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
                    try {
                        const validPassword = values.confirmPassword === values.password;
                        if (!validPassword) {
                            dispatch(showMessage(`Passwords don't match!`));
                            return;
                        }
                        signUp(values)
                            .then((res) => res.data)
                            .then((data) => {
                                navigate('/register/email-verify', {
                                    state: {
                                        password: values.password,
                                        email: values.email,
                                        userId: data
                                    }
                                });
                            })
                            .catch((e) => dispatch(showMessage(e?.response?.data?.message)));
                    } catch (err) {
                        setStatus({ success: false });
                        setErrors({ submit: err.message });
                        setSubmitting(false);
                    }
                }}
            >
                {({ errors, handleBlur, setValues, handleChange, handleSubmit, isSubmitting, touched, values }) => (
                    <form noValidate onSubmit={handleSubmit} {...others}>
                        <Grid container spacing={matchDownSM ? 0 : 2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    fullWidth
                                    label={<FormattedMessage id="First Name" />}
                                    margin="normal"
                                    name="firstName"
                                    type="text"
                                    value={values.firstName}
                                    onChange={handleChange}
                                    defaultValue=""
                                    sx={{ ...theme.typography.customInput }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    fullWidth
                                    label={<FormattedMessage id="Last Name" />}
                                    margin="normal"
                                    name="lastName"
                                    value={values.lastName}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    type="text"
                                    defaultValue=""
                                    sx={{ ...theme.typography.customInput }}
                                />
                            </Grid>
                        </Grid>
                        <FormControl fullWidth error={Boolean(touched.email && errors.email)} sx={{ ...theme.typography.customInput }}>
                            <InputLabel htmlFor="outlined-adornment-email-register">
                                <FormattedMessage id="Email Address" />
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-email-register"
                                type="email"
                                value={values.email}
                                name="email"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                inputProps={{}}
                            />
                            {touched.email && errors.email && (
                                <FormHelperText error id="standard-weight-helper-text--register">
                                    {errors.email}
                                </FormHelperText>
                            )}
                        </FormControl>

                        <FormControl
                            fullWidth
                            error={Boolean(touched.personalNumber && errors.personalNumber)}
                            sx={{ ...theme.typography.customInput }}
                        >
                            <InputLabel htmlFor="outlined-adornment-email-register">
                                <FormattedMessage id="Personal Number" />
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-email-register"
                                type="personalNumber"
                                value={values.personalNumber}
                                name="personalNumber"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                inputProps={{}}
                            />
                            {touched.personalNumber && errors.personalNumber && (
                                <FormHelperText error id="standard-weight-helper-text--register">
                                    {errors.personalNumber}
                                </FormHelperText>
                            )}
                        </FormControl>

                        <Autocomplete
                            style={{ height: '44px', marginBottom: '10px', marginTop: '10px' }}
                            disablePortal
                            options={location?.state?.company?.companyUnits || []}
                            getOptionLabel={(option) => option?.unitName}
                            value={values.unitName}
                            onChange={(event, value) => {
                                setValues({ ...values, department: value?.shortName });
                            }}
                            renderInput={(params) => (
                                <TextField {...params} label={<FormattedMessage id="Deparment" />} error={''} helperText={''} />
                            )}
                        />

                        <FormControl
                            fullWidth
                            error={Boolean(touched.password && errors.password)}
                            sx={{ ...theme.typography.customInput }}
                        >
                            <InputLabel htmlFor="outlined-adornment-password-register">
                                <FormattedMessage id="Password" />
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password-register"
                                type={showPassword ? 'text' : 'password'}
                                value={values.password}
                                name="password"
                                label="Password"
                                onBlur={handleBlur}
                                onChange={(e) => {
                                    handleChange(e);
                                    changePassword(e.target.value);
                                }}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                            size="large"
                                        >
                                            {showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                inputProps={{}}
                            />
                            {touched.password && errors.password && (
                                <FormHelperText error id="standard-weight-helper-text-password-register">
                                    {errors.password}
                                </FormHelperText>
                            )}
                        </FormControl>

                        <FormControl
                            fullWidth
                            error={Boolean(touched.confirmPassword && errors.confirmPassword)}
                            sx={{ ...theme.typography.customInput }}
                        >
                            <InputLabel htmlFor="outlined-adornment-password-register">
                                <FormattedMessage id="Confirm Password" />
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password-register"
                                type={showPassword ? 'text' : 'password'}
                                value={values.confirmPassword}
                                name="confirmPassword"
                                label="Confirm Password"
                                onBlur={handleBlur}
                                onChange={(e) => {
                                    handleChange(e);
                                    // hanldeConfirmChange(e.target.value, values.password);
                                }}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                            size="large"
                                        >
                                            {showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                inputProps={{}}
                            />
                            {touched.confirmPassword && errors.confirmPassword && (
                                <FormHelperText error id="standard-weight-helper-text-password-register">
                                    {errors.confirmPassword}
                                </FormHelperText>
                            )}
                        </FormControl>

                        {strength !== 0 && (
                            <FormControl fullWidth>
                                <Box sx={{ mb: 2 }}>
                                    <Grid container spacing={2} alignItems="center">
                                        <Grid item>
                                            <Box
                                                style={{ backgroundColor: level?.color }}
                                                sx={{ width: 85, height: 8, borderRadius: '7px' }}
                                            />
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="subtitle1" fontSize="0.75rem">
                                                {level?.label}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Box>
                            </FormControl>
                        )}

                        <Grid container alignItems="center" justifyContent="space-between">
                            <Grid item>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={checked}
                                            onChange={(event) => setChecked(event.target.checked)}
                                            name="checked"
                                            color="primary"
                                        />
                                    }
                                    label={
                                        <Typography variant="subtitle1">
                                            <FormattedMessage id="Agree with" /> &nbsp;
                                            <Typography variant="subtitle1" component={Link} to="#">
                                                <FormattedMessage id="Terms and Conditions" />
                                            </Typography>
                                        </Typography>
                                    }
                                />
                            </Grid>
                        </Grid>
                        {errors.submit && (
                            <Box sx={{ mt: 3 }}>
                                <FormHelperText error>{errors.submit}</FormHelperText>
                            </Box>
                        )}

                        <Box sx={{ mt: 2 }}>
                            <AnimateButton disabled={isSubmitting}>
                                <Button disableElevation fullWidth size="large" type="submit" variant="contained" color="secondary">
                                    {/* <FormattedMessage id="Sign up" /> */}
                                    Continue
                                </Button>
                            </AnimateButton>
                        </Box>
                    </form>
                )}
            </Formik>
        </>
    );
};

export default FirebaseRegister;
