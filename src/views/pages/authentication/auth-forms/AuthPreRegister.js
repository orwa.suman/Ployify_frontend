import * as Yup from 'yup';

import {
    Autocomplete,
    Box,
    Button,
    Checkbox,
    Divider,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    TextField,
    Typography,
    useMediaQuery
} from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import React, { useEffect } from 'react';

import AnimateButton from 'ui-component/extended/AnimateButton';
import { FormattedMessage } from 'react-intl';
import { Formik } from 'formik';
import { showMessage } from 'utils/form/Communication';
import { useDispatch } from 'store';
import { useTheme } from '@mui/material/styles';
import { validateRegisterCode } from 'services/RegisterService';

// material-ui



// third party





// assets





// ===========================|| FIREBASE - REGISTER ||=========================== //

const PreRegister = ({ ...others }) => {
    const theme = useTheme();
    const dispatch = useDispatch();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
    const [checked, setChecked] = React.useState(true);
    const navigate = useNavigate();

    return (
        <>
            <Formik
                initialValues={{
                    code: '',
                    submit: null
                }}
                validationSchema={Yup.object().shape({
                    code: Yup.string()
                        .min(5)
                        .max(255)
                        .required(<FormattedMessage id="Required" />)
                })}
                onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
                    try {
                        validateRegisterCode(values.code)
                            .then((res) => res.data)
                            .then((data) => {
                                
                                navigate('/register', { state: { company: data.company, code: data.registrationCode } });
                            })
                            .catch((e) => dispatch(showMessage(e?.response?.data?.message)));
                    } catch (err) {
                        setStatus({ success: false });
                        setErrors({ submit: err.message });
                        setSubmitting(false);
                    }
                }}
            >
                {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values }) => (
                    <form noValidate onSubmit={handleSubmit} {...others}>
                        <FormControl fullWidth error={Boolean(touched.code && errors.code)} sx={{ ...theme.typography.customInput }}>
                            <InputLabel htmlFor="code">
                                <FormattedMessage id="Company Registration Code" />
                            </InputLabel>
                            <OutlinedInput
                                id="code"
                                value={values.code}
                                name="code"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                inputProps={{}}
                            />
                            {touched.code && errors.code && (
                                <FormHelperText error id="standard-weight-helper-text--register">
                                    {errors.code}
                                </FormHelperText>
                            )}
                        </FormControl>

                        <Grid container alignItems="center" justifyContent="space-between">
                            <Grid item>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={checked}
                                            onChange={(event) => setChecked(event.target.checked)}
                                            name="checked"
                                            color="primary"
                                        />
                                    }
                                    label={
                                        <Typography variant="subtitle1">
                                            <FormattedMessage id="Agree with" /> &nbsp;
                                            <Typography variant="subtitle1" component={Link} to="#">
                                                <FormattedMessage id="Terms and Conditions" />
                                            </Typography>
                                        </Typography>
                                    }
                                />
                            </Grid>
                        </Grid>
                        {errors.submit && (
                            <Box sx={{ mt: 3 }}>
                                <FormHelperText error>{errors.submit}</FormHelperText>
                            </Box>
                        )}

                        <Box sx={{ mt: 2 }}>
                            <AnimateButton disabled={isSubmitting}>
                                <Button disableElevation fullWidth size="large" type="submit" variant="contained" color="secondary">
                                    {/* <FormattedMessage id="Sign up" /> */}
                                    Verify Code
                                </Button>
                            </AnimateButton>
                        </Box>
                    </form>
                )}
            </Formik>
        </>
    );
};

export default PreRegister;
