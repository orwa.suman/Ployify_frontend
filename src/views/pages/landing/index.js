// material-ui
import { styled } from '@mui/material/styles';

// project imports
import Header from './Header';
import Feature from './Feature';
import Demos from './Demos';
import Layouts from './Layouts';
import KeyFeature from './KeyFeature';
import Subscribe from './Subscribe';
import Footer from './Footer';
import Customization from 'layout/Customization';
import AppBar from 'ui-component/extended/AppBar';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import NavigateNextRoundedIcon from '@mui/icons-material/NavigateNextRounded';

import { Divider, Typography, Grid, Button, Box, Avatar } from '@mui/material';
import Card from '@mui/material/Card';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import React from 'react';
import { useEffect, useState } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Container } from '@mui/material';

// project imports
import FadeInWhenVisible from './Animation';
import SubCard from 'ui-component/cards/SubCard';
import { gridSpacing } from 'store/constant';

// assets
import PaletteTwoToneIcon from '@mui/icons-material/PaletteTwoTone';
import ReorderTwoToneIcon from '@mui/icons-material/ReorderTwoTone';
import SpeedTwoToneIcon from '@mui/icons-material/SpeedTwoTone';
import { IoFitnessSharp, IoNutrition } from 'react-icons/io5';
import { RiMentalHealthFill } from 'react-icons/ri';

const CustomShadowBox = ({ shadow, label, color }) => {
    const theme = useTheme();

    return (
        <Card sx={{ mb: 3, boxShadow: shadow }}>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    py: 3,
                    bgcolor: color,
                    fontWeight: 'bold',
                    color: theme.palette.background.default
                }}
            >
                {!label && <Box sx={{ color: 'inherit' }}>boxShadow: {shadow}</Box>}
                {label && <Box sx={{ color: 'inherit' }}>{label}</Box>}
            </Box>
        </Card>
    );
};

const HeaderWrapper = styled('div')(({ theme }) => ({
    paddingTop: 30,
    overflowX: 'hidden',
    overflowY: 'clip',
    [theme.breakpoints.down('md')]: {
        paddingTop: 42
    }
}));

const SecondWrapper = styled('div')(({ theme }) => ({
    paddingTop: 160,
    [theme.breakpoints.down('md')]: {
        paddingTop: 60
    }
}));

// =============================|| LANDING MAIN ||============================= //
const mockData = [
    {
        question: 'How do you do?',
        answers: [
            { label: 'A - Correct', correct: true },
            { label: 'B - Correct', correct: true },
            { label: 'Not correct' }
        ]
    },
    {
        question: 'What do you do?',
        answers: [
            { label: 'Jogging - Correct', correct: true },
            { label: 'Running - Correct', correct: true },
            { label: 'Fishing - Not correct' }
        ]
    }
];

export default function Landing() {
    const [quiz, setQuiz] = React.useState({ questions: mockData });
    const [currentQuestion, setCurrentQuestion] = React.useState(0);
    const [quizEnd, setQuizEnd] = React.useState(0);
    const theme = useTheme();

    const nextQuestion = () => {
        let questionScore = 0;
        quiz.questions[currentQuestion].answers.forEach((answer) => {
            if (answer.correct && answer.checked) {
                questionScore++;
            }
        });
        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < quiz.questions.length) {
            setCurrentQuestion(nextQuestion);
        } else {
            setQuizEnd(true);
        }
    };

    const unselectOption = (questionIndex, answerIndex) => {
        const updatedStateForQuiz = { ...quiz };
        updatedStateForQuiz.questions[questionIndex].answers[answerIndex].checked = false;
        setQuiz(updatedStateForQuiz);
    };

    const selectOption = (questionIndex, answerIndex) => {
        if (!quiz.questions[questionIndex].answers[answerIndex].checked) {
            const checkedAnswers = quiz.questions[questionIndex].answers.filter((answer) => answer.checked);
            const correctAnswers = quiz.questions[questionIndex].answers.filter((answer) => answer.correct);
            if (checkedAnswers.length < correctAnswers.length) {
                const updatedStateForQuiz = { ...quiz };
                updatedStateForQuiz.questions[questionIndex].answers[answerIndex].checked = true;

                setQuiz(updatedStateForQuiz);
            }
        } else {
            unselectOption(questionIndex, answerIndex);
        }
    };

    return (
        <>
            <HeaderWrapper id="home">
                <AppBar />
                <Header />
            </HeaderWrapper>
            <SecondWrapper>
                <Container>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12} lg={5} md={10}>
                            <Grid container spacing={2} sx={{ mb: 2 }}>
                                <Grid item xs={12}>
                                    <Grid container spacing={1}>
                                        <Grid item>
                                            <Typography variant="h5" color="primary">
                                                Top Features
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography variant="h2" component="div">
                                        What CAREPLOYEE brings to you?
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography variant="body2">
                                        Berry is a solid dashboard template for your next project, with the following top features.
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container justifyContent="center" spacing={gridSpacing} sx={{ textAlign: 'center' }}>
                                <Grid item md={4} sm={6}>
                                    <FadeInWhenVisible>
                                        <SubCard>
                                            <Grid container justifyContent="center" spacing={2}>
                                                <Grid item>
                                                    <Avatar
                                                        size="xl"
                                                        variant="rounded"
                                                        sx={{
                                                            background:
                                                                theme.palette.mode === 'dark'
                                                                    ? theme.palette.dark[900]
                                                                    : theme.palette.secondary.light,
                                                            color: theme.palette.secondary.main
                                                        }}
                                                    >
                                                        <RiMentalHealthFill size="large" />
                                                    </Avatar>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Typography variant="h3">Mental health / Seminare</Typography>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Typography variant="body2">
                                                        Technology behind Berry is less complicated so you can focus on creating the actual
                                                        web applications.
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </SubCard>
                                    </FadeInWhenVisible>
                                </Grid>

                                <Grid item md={4} sm={6}>
                                    <FadeInWhenVisible>
                                        <SubCard>
                                            <Grid container justifyContent="center" spacing={2}>
                                                <Grid item>
                                                    <Avatar
                                                        size="xl"
                                                        variant="rounded"
                                                        sx={{
                                                            background:
                                                                theme.palette.mode === 'dark'
                                                                    ? theme.palette.dark[900]
                                                                    : theme.palette.success.light,
                                                            color: theme.palette.success.dark
                                                        }}
                                                    >
                                                        <IoFitnessSharp size="large" />
                                                    </Avatar>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Typography variant="h3">Training Plans</Typography>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Typography variant="body2">
                                                        Code that makes it easier and faster to render the page for your web applications.
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </SubCard>
                                    </FadeInWhenVisible>
                                </Grid>

                                <Grid item md={4} sm={6}>
                                    <FadeInWhenVisible>
                                        <SubCard>
                                            <Grid container justifyContent="center" spacing={2}>
                                                <Grid item>
                                                    <Avatar
                                                        size="xl"
                                                        variant="rounded"
                                                        sx={{
                                                            background:
                                                                theme.palette.mode === 'dark'
                                                                    ? theme.palette.dark[900]
                                                                    : theme.palette.primary.light,
                                                            color: theme.palette.primary.main
                                                        }}
                                                    >
                                                        <IoNutrition size="large" />
                                                    </Avatar>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Typography variant="h3">Diet and Nutrition</Typography>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Typography variant="body2">
                                                        Warm color palates and minimally designed interfaces make the user experience more
                                                        comfortable.
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </SubCard>
                                    </FadeInWhenVisible>
                                </Grid>

                                <Grid item xs={12}>
                                    <Card vsx={{ marginTop: '32px', maxWidth: '550px' }}>
                                        <Grid container spacing={2} sx={{ paddingTop: '30px' }}>
                                            <Grid item xs={12}>
                                                <Grid item xs={12}>
                                                    <CustomShadowBox
                                                        color={theme.palette.primary.main}
                                                        shadow={theme.customShadows.primary}
                                                        label={quiz.questions[currentQuestion]?.question}
                                                    />
                                                </Grid>
                                                <Divider></Divider>
                                                {quiz.questions[currentQuestion] ? (
                                                    <Typography
                                                        sx={{ textAlign: 'center', margin: '12px 0 16px' }}
                                                        variant="h4"
                                                        gutterBottom
                                                    >
                                                        {quiz.questions[currentQuestion].answers.filter((answer) => answer.correct).length -
                                                            quiz.questions[currentQuestion].answers.filter((answer) => answer.checked)
                                                                .length}
                                                        Choice left
                                                    </Typography>
                                                ) : (
                                                    ''
                                                )}

                                                <nav aria-label="secondary mailbox folders">
                                                    <List>
                                                        {quiz.questions[currentQuestion]?.answers.map((answer, answerIndex) => (
                                                            <ListItem disablePadding>
                                                                <ListItemButton
                                                                    primary
                                                                    component="a"
                                                                    sx={{
                                                                        background: answer.checked ? 'rgba(204,173,112, 0.6)' : '',
                                                                        marginBottom: '8px'
                                                                    }}
                                                                    href="#simple-list"
                                                                    onClick={() => selectOption(currentQuestion, answerIndex)}
                                                                >
                                                                    <ListItemText primary={answer.label} />
                                                                </ListItemButton>
                                                            </ListItem>
                                                        ))}
                                                    </List>
                                                </nav>
                                                <Button
                                                    sx={{ margin: '12px 0' }}
                                                    variant="outlined"
                                                    onClick={() => nextQuestion()}
                                                    fullWidth
                                                    startIcon={<NavigateNextIcon />}
                                                >
                                                    Next Question
                                                </Button>
                                            </Grid>
                                        </Grid>
                                    </Card>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Container>
            </SecondWrapper>
            <SecondWrapper>
                <Demos />
            </SecondWrapper>
            <SecondWrapper>
                <Layouts />
            </SecondWrapper>
            <SecondWrapper>
                <KeyFeature />
            </SecondWrapper>
            <SecondWrapper>
                <Subscribe />
            </SecondWrapper>
            <Footer />
            <Customization />
        </>
    );
}
