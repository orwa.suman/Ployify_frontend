import React from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';

// project imports
import { Button, Grid, IconButton } from '@mui/material';
import { getBase64 } from '../../../utils/form/fileUpload';

import PhotoCamera from '@mui/icons-material/PhotoCamera';
import { FormattedMessage } from 'react-intl';
// import sharp from 'sharp';
// import fs from 'fs';

// ==============================|| NO/EMPTY Product ||============================== //

const FileUpload = ({ onChange, label, defaultPicture, allowDisplay = true, picture }) => {
    const theme = useTheme();
    const [selectedFile, setSelectedFile] = React.useState('');
    const [showImage, setShowImage] = React.useState(true);

    const MAX_IMAGE_SIZE = 4000000; // 4MB in bytes

    const handleFile = (event) => {
        const file = event.target.files[0];

        if (file && file.type.match('image.*')) {
            // Create a new instance of the FileReader object
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = (event) => {
                const image = new Image();
                image.src = event.target.result;
                image.onload = () => {
                    const canvas = document.createElement('canvas');
                    const ctx = canvas.getContext('2d');
                    const MAX_WIDTH = 800; // maximum width of the image
                    const MAX_HEIGHT = 800; // maximum height of the image
                    let width = image.width;
                    let height = image.height;

                    // Scale down the image if it is too big
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }

                    canvas.width = width;
                    canvas.height = height;
                    ctx.drawImage(image, 0, 0, width, height);

                    // Convert the canvas to a data URL
                    const dataUrl = canvas.toDataURL(file.type);

                    // Compress the image if it is too big
                    if (dataUrl.length > MAX_IMAGE_SIZE) {
                        const quality = MAX_IMAGE_SIZE / dataUrl.length;
                        const compressedDataUrl = canvas.toDataURL(file.type, quality);
                        onChange(compressedDataUrl, file.name);
                        setSelectedFile(compressedDataUrl);
                        setShowImage(true);
                    } else {
                        onChange(dataUrl, file.name);
                        setSelectedFile(dataUrl);
                        setShowImage(true);
                    }
                };
            };
        } else {
            getBase64(file).then((data) => {
                onChange(data, event.target.files[0].name);
                setSelectedFile(data);
                setShowImage(event.target.files[0].type.match('image.*'));
            });
        }
    };

    return (
        <Grid>
            <Grid className="careployee-form__block">
                <label htmlFor="picture" className="careployee-form__label" style={{ marginBottom: '22px' }}>
                    {label ?? 'Picture'}
                </label>
                <Button variant="contained" component="label">
                    Upload
                    <input type="file" name="picture" onChange={handleFile} hidden accept="image/*,.pdf, audio/*" />
                </Button>
                <IconButton color="primary" aria-label="upload picture" component="label">
                    <input type="file" name="picture" onChange={handleFile} hidden accept="image/*,.pdf, audio/*" />
                    <PhotoCamera />
                </IconButton>
            </Grid>
            <Grid>
                {allowDisplay && showImage ? (
                    <img
                        src={picture ? picture : selectedFile || defaultPicture || '/images/training.jpg'}
                        alt="training name"
                        style={{ height: '250px', padding: '16px', borderRadius: '24px' }}
                    />
                ) : selectedFile ? (
                    <FormattedMessage id="File Selected" />
                ) : (
                    ''
                )}
            </Grid>
        </Grid>
    );
};

export default FileUpload;
