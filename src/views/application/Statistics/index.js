// material-ui

import { Box, Button, Card, CardContent, Grid, Stack, TextField, Typography } from '@mui/material';
import { DesktopDatePicker, LocalizationProvider } from '@mui/x-date-pickers';
import { FormattedMessage, useIntl } from 'react-intl';
import { IconBrandFacebook, IconBrandTwitter, IconBrandYoutube } from '@tabler/icons';
import React, { useEffect } from 'react';
import { bigChartData, chartData } from './ChartData';
import {
    getAllStatisticsForAdmin,
    getAllStatisticsForAdminInterval,
    getAllStatisticsForSuperUser,
    getAllStatisticsForSuperUserInterval,
    getAverageUsageTimeOfRecipe,
    getAverageUsageTimeOfSeminar,
    getAverageUsageTimeOfTrainingPlan,
    getMostUsedRecipes,
    getMostUsedSeminars,
    getMostUsedTrainingPlans
} from 'services/AnalyzeToolService';

import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import ApexBarChart from 'views/forms/chart/Apexchart/ApexBarChart';
import ApexColumnChart from 'views/forms/chart/Apexchart/ApexColumnChart';
import ApexPolarChart from 'views/forms/chart/Apexchart/ApexPolarChart';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import Chart from 'react-apexcharts';
import GroupIcon from '@mui/icons-material/Group';
import HowToRegIcon from '@mui/icons-material/HowToReg';
import { Login } from '@mui/icons-material';
import MainCard from 'ui-component/cards/MainCard';
import MyLocationIcon from '@mui/icons-material/MyLocation';
import ReportCard from 'ui-component/cards/ReportCard';
import SubCard from 'ui-component/cards/SubCard';
import TimelineIcon from '@mui/icons-material/Timeline';
import TotalLineChartCard from 'ui-component/cards/TotalLineChartCard';
import { gridSpacing } from 'store/constant';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';
import { useDispatch } from 'react-redux';
import { useTheme } from '@mui/material/styles';

// third party

// ===========================|| WIDGET STATISTICS ||=========================== //

const WidgetStatistics = () => {
    const theme = useTheme();
    const dispatch = useDispatch();
    const [data, setData] = React.useState({
        generic: { numberOfOrderedAccounts: 0, sumAllUsedPoints: 0, sumAllCollectedPoints: 0, signedUpUsers: 0, signInTimes: 0 }
    });
    const [startDate, setStartDate] = React.useState(null);
    const [endDate, setEndDate] = React.useState(null);
    const intl = useIntl();

    useEffect(() => {
        getAverageUsageTimeOfTrainingPlan('HOME')
            .then((res) => res.data)
            .then((data) => {
                
                setData({ ...data, averageUsageTimeOfTrainingPlanHome: data });
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });

        getAverageUsageTimeOfTrainingPlan('OFFICE')
            .then((res) => res.data)
            .then((data) => {
                
                setData({ ...data, averageUsageTimeOfTrainingPlanOffice: data });
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });

        getAverageUsageTimeOfRecipe()
            .then((res) => res.data)
            .then((data) => {
                
                setData({ ...data, averageUsageTimeOfRecipe: data });
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });

        getAverageUsageTimeOfSeminar()
            .then((res) => res.data)
            .then((data) => {
                
                setData({ ...data, averageUsageTimeOfSeminar: data });
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });

        getMostUsedTrainingPlans()
            .then((res) => res.data)
            .then((data) => {
                
                setData({ ...data, mostUsedTrainingPlans: data });
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });

        getMostUsedRecipes()
            .then((res) => res.data)
            .then((data) => {
                
                setData({ ...data, mostUsedRecipes: data });
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });

        getMostUsedSeminars()
            .then((res) => res.data)
            .then((data) => {
                
                setData({ ...data, mostUsedSeminars: data });
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
        /*
        geCountUsedTimeOfReward().then((res) => res.data)
        .then((data) => {
            
            setData({ ...data, countUsedTimeOfReward: data });
        })
        .catch((e) => {
            dispatch(showMessage(e?.response?.data?.message));
        });

        geSumUsedPointsOfReward().then((res) => res.data)
        .then((data) => {
            
            setData({ ...data, sumUsedPointsOfReward: data });
        })
        .catch((e) => {
            dispatch(showMessage(e?.response?.data?.message));
        });
*/
        if (loggedInUser.role === 'ROLE_ADMIN') {
            getAllStatisticsForAdmin()
                .then((res) => res.data)
                .then((data) => {
                    
                    setData({ ...data, generic: data });
                })
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
        if (loggedInUser.role === 'ROLE_SUPERUSER') {
            getAllStatisticsForSuperUser()
                .then((res) => res.data)
                .then((data) => {
                    
                    setData({ ...data, generic: data });
                })
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
    }, []);

    const loadIntervalStatistics = () => {
        (loggedInUser.role === 'ROLE_SUPERUSER'
            ? getAllStatisticsForSuperUserInterval(startDate, endDate)
            : getAllStatisticsForAdminInterval(startDate, endDate)
        )
            .then((res) => res.data)
            .then((data) => {
                
                setData({ ...data, generic: data });
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={6} md={3} className="careployee-form__block">
                <label htmlFor="startDate" className="careployee-form__label">
                    Start date
                </label>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DesktopDatePicker
                        inputFormat="MM/DD/YYYY"
                        value={startDate}
                        onChange={(value) => setStartDate(value)}
                        renderInput={(params) => <TextField fullWidth {...params} />}
                    />
                </LocalizationProvider>
            </Grid>

            <Grid item xs={6} md={3} className="careployee-form__block">
                <label htmlFor="endDate" className="careployee-form__label">
                    End date
                </label>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                    <DesktopDatePicker
                        inputFormat="MM/DD/YYYY"
                        value={endDate}
                        onChange={(value) => setEndDate(value)}
                        renderInput={(params) => <TextField fullWidth {...params} />}
                    />
                </LocalizationProvider>
            </Grid>

            <Grid item xs={6} md={2} className="careployee-form__block">
                <label htmlFor="submitBtn" className="careployee-form__label">
                    &nbsp;
                </label>
                <Button
                    style={{ marginTop: '8px' }}
                    fullWidth
                    onClick={() => loadIntervalStatistics()}
                    variant="contained"
                    color="primary"
                    sx={{ boxShadow: 'none' }}
                >
                    Search
                </Button>
            </Grid>

            <Grid xs={12}>
                <Typography variant="h2" sx={{ padding: '12px 24px 0 32px' }}>
                    <FormattedMessage id="Users" />
                </Typography>
            </Grid>

            <Grid item xs={12} lg={3} sm={6}>
                <ReportCard
                    primary={data.generic?.numberOfOrderedAccounts || 0}
                    secondary="No. of ordered accounts"
                    color="#cbab70"
                    iconSecondary={<GroupIcon fontSize="large" color={theme.palette.secondary.main} />}
                />
            </Grid>

            <Grid item xs={12} lg={3} sm={6}>
                <ReportCard
                    primary={data.generic?.signedUpUsers || 0}
                    secondary={<FormattedMessage id="Number of registrated users" />}
                    color="#cbab70"
                    iconSecondary={<HowToRegIcon fontSize="large" color={theme.palette.secondary.main} />}
                />
            </Grid>

            <Grid item xs={12} lg={3} sm={6}>
                <ReportCard
                    primary={data.generic?.signInTimes || 0}
                    secondary={<FormattedMessage id="Number of logged in users" />}
                    color="#cbab70"
                    iconPrimary={Login}
                />
            </Grid>

            <Grid xs={12}>
                <Typography variant="h2" sx={{ padding: '40px 24px 0 32px' }}>
                    <FormattedMessage id="Points" />
                </Typography>
            </Grid>

            <Grid item xs={12} lg={3} sm={6}>
                <ReportCard
                    primary={data.generic?.sumAllCollectedPoints || 0}
                    secondary="Sum all collected points"
                    color={theme.palette.success.dark}
                    iconPrimary={MyLocationIcon}
                />
            </Grid>

            <Grid item xs={12} lg={3} sm={6}>
                <ReportCard
                    primary={
                        data.generic?.sumAllCollectedPoints &&
                        data.generic?.sumAllCollectedPoints > 0 &&
                        data.generic?.signInTimes &&
                        data.generic?.signInTimes > 0
                            ? data.generic.sumAllCollectedPoints / data.generic.signInTimes
                            : 0
                    }
                    secondary="Avg. sum all collected points"
                    color={theme.palette.success.dark}
                    iconPrimary={AssignmentIndIcon}
                />
            </Grid>

            <Grid item xs={12} lg={3} sm={6}>
                <ReportCard
                    primary={data.generic?.sumAllUsedPoints || 0}
                    secondary="Sum all used points"
                    color={theme.palette.error.main}
                    iconPrimary={MyLocationIcon}
                />
            </Grid>

            <Grid item xs={12} lg={3} sm={6}>
                <ReportCard
                    primary={
                        data.generic?.sumAllUsedPoints &&
                        data.generic?.sumAllUsedPoints > 0 &&
                        data.generic?.signInTimes &&
                        data.generic?.signInTimes > 0
                            ? data.generic.sumAllUsedPoints / data.generic.signInTimes
                            : 0
                    }
                    secondary="Avg. sum all used points"
                    color={theme.palette.error.main}
                    iconPrimary={AssignmentIndIcon}
                />
            </Grid>

            <Grid item xs={12}></Grid>
            {/*
            <Grid item xs={12} sm={6} lg={3}>
                <TotalLineChartCard
                    chartData={chartData}
                    bgColor={theme.palette.success.dark}
                    value={data.averageUsageTimeOfTrainingPlanHome || '08:53'}
                    title="Avg. Time Of System Usage"
                    percentage=""
                />
            </Grid>
*/}

            <Grid xs={12}>
                <Typography variant="h2" sx={{ padding: '12px 24px 0 32px' }}>
                    <FormattedMessage id="Avarage usage times" />
                </Typography>
            </Grid>

            <Grid item xs={12}>
                <ApexColumnChart
                    data={[
                        {
                            name: intl.formatMessage({ id: 'Training plan @office' }),
                            data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
                        },
                        {
                            name: intl.formatMessage({ id: 'Training plan @home' }),
                            data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
                        },
                        {
                            name: intl.formatMessage({ id: 'Mental Health' }),
                            data: [35, 41, 36, 26, 45, 48, 52, 53, 41]
                        },
                        {
                            name: intl.formatMessage({ id: 'Recipes' }),
                            data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
                        },
                        {
                            name: intl.formatMessage({ id: 'Seminare' }),
                            data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
                        },
                        {
                            name: intl.formatMessage({ id: 'Overall' }),
                            data: [35, 41, 36, 26, 45, 48, 52, 53, 41]
                        }
                    ]}
                />
            </Grid>

            <Grid xs={12}>
                <Typography variant="h2" sx={{ padding: '40px 24px 0 32px' }}>
                    <FormattedMessage id="Training Plan @home" />
                </Typography>
            </Grid>

            <Grid item xs={12} sm={6} lg={5}>
                <ApexBarChart
                    data={[88, 67, 65, 34, 22, 13]}
                    labels={['Most used training name', 'second used', 'third', 'fourth', 'fifth', 'sixth']}
                />
            </Grid>

            <Grid item xs={12} sm={4} sx={{ margin: '16px 0 12px' }}>
                <ApexPolarChart data={[330, 80, 60, 20, 10, 80]} labels={['08-11', '11-14', '14-17', '17-20', '20-23', '23-08']} />
            </Grid>

            <Grid item xs={12} sm={6} lg={3} sx={{ marginTop: '50px' }}>
                <TotalLineChartCard
                    chartData={chartData}
                    value={data.averageUsageTimeOfTrainingPlanHome || '08:53'}
                    title="Avg. Usage Time Of Training Plan @ Home"
                    percentage=""
                />
            </Grid>
            <Grid xs={12}>
                <Typography variant="h2" sx={{ padding: '40px 24px 0 32px' }}>
                    <FormattedMessage id="Training Plan @office" />
                </Typography>
            </Grid>

            <Grid item xs={12} sm={6} lg={5}>
                <ApexBarChart
                    data={[88, 67, 65, 34, 22, 13]}
                    labels={['Most used training name', 'second used', 'third', 'fourth', 'fifth', 'sixth']}
                />
            </Grid>

            <Grid item xs={12} sm={4} sx={{ margin: '16px 0 12px' }}>
                <ApexPolarChart data={[380, 80, 60, 20, 10, 80]} labels={['08-11', '11-14', '14-17', '17-20', '20-23', '23-08']} />
            </Grid>

            <Grid item xs={12} sm={6} lg={3}>
                <TotalLineChartCard
                    chartData={chartData}
                    bgColor={theme.palette.error.main}
                    value={data.averageUsageTimeOfTrainingPlanOffice || '05:53'}
                    title="Avg. Usage Time Of Training Plan @ Office"
                    percentage=""
                />
            </Grid>

            <Grid xs={12}>
                <Typography variant="h2" sx={{ padding: '40px 24px 0 32px' }}>
                    <FormattedMessage id="Mental Health" />
                </Typography>
            </Grid>

            <Grid item xs={12} sm={6} lg={5}>
                <ApexBarChart
                    data={[88, 67, 65, 34, 22, 13]}
                    labels={['Most used training name', 'second used', 'third', 'fourth', 'fifth', 'sixth']}
                />
            </Grid>

            <Grid item xs={12} sm={4} sx={{ margin: '16px 0 12px' }}>
                <ApexPolarChart data={[100, 80, 60, 20, 10, 80]} labels={['08-11', '11-14', '14-17', '17-20', '20-23', '23-08']} />
            </Grid>

            <Grid item xs={12} sm={6} lg={3}>
                <TotalLineChartCard
                    chartData={chartData}
                    bgColor={theme.palette.secondary.main}
                    value={data.averageUsageTimeOfMentalHealth || '03:14'}
                    title="Avg. Usage Time Of Mental Health"
                    percentage=""
                />
            </Grid>

            <Grid xs={12}>
                <Typography variant="h2" sx={{ padding: '40px 24px 0 32px' }}>
                    <FormattedMessage id="Recipes" />
                </Typography>
            </Grid>

            <Grid item xs={12} sm={6} lg={5}>
                <ApexBarChart
                    data={[88, 67, 65, 34, 22, 13]}
                    labels={['Most used training name', 'second used', 'third', 'fourth', 'fifth', 'sixth']}
                />
            </Grid>

            <Grid item xs={12} sm={4} sx={{ margin: '16px 0 12px' }}>
                <ApexPolarChart data={[100, 80, 60, 20, 10, 80]} labels={['08-11', '11-14', '14-17', '17-20', '20-23', '23-08']} />
            </Grid>

            <Grid item xs={12} sm={6} lg={3}>
                <TotalLineChartCard
                    chartData={chartData}
                    bgColor={theme.palette.success.dark}
                    value={data.averageUsageTimeOfRecipe || '02:23'}
                    title="Avg. Usage Time Of Recipe"
                    percentage=""
                />
            </Grid>

            <Grid xs={12}>
                <Typography variant="h2" sx={{ padding: '40px 24px 0 32px' }}>
                    <FormattedMessage id="Seminare" />
                </Typography>
            </Grid>

            <Grid item xs={12} sm={6} lg={5}>
                <ApexBarChart
                    data={[88, 67, 65, 34, 22, 13]}
                    labels={['Most used training name', 'second used', 'third', 'fourth', 'fifth', 'sixth']}
                />
            </Grid>

            <Grid item xs={12} sm={4}>
                <ApexPolarChart data={[330, 80, 60, 20, 10, 80]} labels={['08-11', '11-14', '14-17', '17-20', '20-23', '23-08']} />
            </Grid>

            <Grid item xs={12} sm={6} lg={3}>
                <TotalLineChartCard
                    chartData={chartData}
                    bgColor={theme.palette.secondary.main}
                    value={data.averageUsageTimeOfSeminar || '03:14'}
                    title="Avg. Usage Time Of Seminar"
                    percentage=""
                />
            </Grid>

            <Grid item xs={12} className="careployee-cards__most-used__wrapper">
                <Typography variant="h2">
                    <FormattedMessage id="Redeemed Rewards" />
                </Typography>
            </Grid>

            <Grid container sx={{ paddingLeft: '12px', marginTop: '16px' }}>
                <Grid xs={12} md={3} sx={{ padding: '12px' }}>
                    <SubCard>
                        <Card>
                            <div className="careployee-cards__image">
                                <img src={'/images/dish.jpg'}></img>
                            </div>
                            <CardContent sx={{ padding: '24px 12px' }}>
                                <Grid container spacing={1}>
                                    <Grid item xs={12} className="careployee-cards__most-used__wrapper">
                                        <Typography variant="subtitle1">{'Test Reward'}</Typography>
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Stack sx={{ paddingTop: '8px' }}>
                                            <Grid container alignItems="center" sx={{ width: '100%', justifyContent: 'center' }}>
                                                <Grid item>
                                                    <TimelineIcon fontSize="large" />

                                                    <Typography variant="h1" sx={{ paddingTop: '4px' }}>
                                                        35 <br />
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Stack>
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                    </SubCard>
                </Grid>
            </Grid>

            <Grid item xs={12} className="careployee-cards__most-used__wrapper">
                <Typography variant="h2">
                    <FormattedMessage id="Activities" />
                </Typography>
            </Grid>

            <Grid item xs={12} md={12}>
                <MainCard sx={{ '&>div': { p: 0, pb: '0px !important' } }}>
                    <Box sx={{ p: 3 }}>
                        <Grid container direction="column" spacing={3}>
                            <Grid item container spacing={1} alignItems="center">
                                <Grid item>
                                    <Typography variant="h3">
                                        <FormattedMessage id="Comparing activities" />
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography sx={{ mt: -2.5, fontWeight: 400 }} color="inherit" variant="h5">
                                    Department wise monthly sales report
                                </Typography>
                            </Grid>
                            <Grid item container justifyContent="space-around" alignItems="center" spacing={3}>
                                <Grid item>
                                    <Grid container alignItems="center" spacing={1}>
                                        <Grid item>
                                            <Typography
                                                sx={{
                                                    width: 40,
                                                    height: 40,
                                                    color: theme.palette.secondary.main,
                                                    borderRadius: '12px',
                                                    padding: 1,
                                                    bgcolor:
                                                        theme.palette.mode === 'dark'
                                                            ? theme.palette.background.default
                                                            : theme.palette.secondary.light
                                                }}
                                            >
                                                <IconBrandFacebook stroke={1.5} />
                                            </Typography>
                                        </Grid>
                                        <Grid item sm zeroMinWidth>
                                            <Typography variant="h4">
                                                <FormattedMessage id="Training @home" />
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <Grid container alignItems="center" spacing={1}>
                                        <Grid item>
                                            <Typography
                                                sx={{
                                                    width: 40,
                                                    height: 40,
                                                    color: theme.palette.primary.main,
                                                    borderRadius: '12px',
                                                    padding: 1,
                                                    backgroundColor:
                                                        theme.palette.mode === 'dark'
                                                            ? theme.palette.background.default
                                                            : theme.palette.primary.light
                                                }}
                                            >
                                                <IconBrandTwitter stroke={1.5} />
                                            </Typography>
                                        </Grid>
                                        <Grid item sm zeroMinWidth>
                                            <Typography variant="h4">
                                                <FormattedMessage id="Training @office" />
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <Grid container alignItems="center" spacing={1}>
                                        <Grid item>
                                            <Typography
                                                sx={{
                                                    width: 40,
                                                    height: 40,
                                                    color: theme.palette.error.main,
                                                    borderRadius: '12px',
                                                    padding: 1,
                                                    backgroundColor:
                                                        theme.palette.mode === 'dark' ? theme.palette.background.default : '#ffe9e9'
                                                }}
                                            >
                                                <IconBrandYoutube stroke={2} />
                                            </Typography>
                                        </Grid>
                                        <Grid item sm zeroMinWidth>
                                            <Typography variant="h4">
                                                <FormattedMessage id="Mental Health" />
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <Grid container alignItems="center" spacing={1}>
                                        <Grid item>
                                            <Typography
                                                sx={{
                                                    width: 40,
                                                    height: 40,
                                                    color: theme.palette.primary.main,
                                                    borderRadius: '12px',
                                                    padding: 1,
                                                    backgroundColor:
                                                        theme.palette.mode === 'dark'
                                                            ? theme.palette.background.default
                                                            : theme.palette.primary.light
                                                }}
                                            >
                                                <IconBrandTwitter stroke={1.5} />
                                            </Typography>
                                        </Grid>
                                        <Grid item sm zeroMinWidth>
                                            <Typography variant="h4">
                                                <FormattedMessage id="Recipes" />
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item>
                                    <Grid container alignItems="center" spacing={1}>
                                        <Grid item>
                                            <Typography
                                                sx={{
                                                    width: 40,
                                                    height: 40,
                                                    color: theme.palette.error.main,
                                                    borderRadius: '12px',
                                                    padding: 1,
                                                    backgroundColor:
                                                        theme.palette.mode === 'dark' ? theme.palette.background.default : '#ffe9e9'
                                                }}
                                            >
                                                <IconBrandYoutube stroke={2} />
                                            </Typography>
                                        </Grid>
                                        <Grid item sm zeroMinWidth>
                                            <Typography variant="h4">
                                                <FormattedMessage id="Seminare" />
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs zeroMinWidth />
                            </Grid>
                        </Grid>
                    </Box>
                    <Chart
                        {...{
                            ...bigChartData,
                            series: [
                                {
                                    name: intl.formatMessage({ id: 'Training plan @office' }),
                                    data: [0, 30, 79, 150]
                                },
                                {
                                    name: intl.formatMessage({ id: 'Training plan @home' }),
                                    data: [0, 50, 80, 120]
                                },
                                {
                                    name: intl.formatMessage({ id: 'Mental Heatlh' }),
                                    data: [0, 34, 59, 130]
                                },
                                {
                                    name: intl.formatMessage({ id: 'Recipes' }),
                                    data: [0, 30, 79, 50]
                                },
                                {
                                    name: intl.formatMessage({ id: 'Seminare' }),
                                    data: [0, 100, 20, 40]
                                }
                            ]
                        }}
                    />
                </MainCard>
            </Grid>
        </Grid>
    );
};

export default WidgetStatistics;
