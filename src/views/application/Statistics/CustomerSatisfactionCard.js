// material-ui
import { Grid, LinearProgress, Typography } from '@mui/material';

// project imports
import MainCard from 'ui-component/cards/MainCard';
import { gridSpacing } from 'store/constant';

// ===========================|| WIDGET STATISTICS - CUSTOMER SATISFACTION ||=========================== //

// const data = [ {
//     Customer_satisfaction : "89.73",


// },
    


// ];

function  CustomerSatisfactionCard ()  {

    const data = 
        {
            customer_satisfaction: 89.56,
            determinate: 67,
            previous: 65.75,
            Change: 45.01,
            Trend: 78,
        }
    
    return (

  
    <MainCard title="Customer satisfaction">
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <Typography variant="h3" align="center">
                    {/* 89.73% */}
                    {data.customer_satisfaction} %
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <LinearProgress variant="determinate" value={data.customer_satisfaction} color="primary" />
            </Grid>
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={4}>
                        <Grid container spacing={1}>
                            <Grid item xs={12}>
                                <Typography variant="subtitle2">previous</Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography variant="h5">{data.previous}</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={4}>
                        <Grid container spacing={1}>
                            <Grid item xs={12}>
                                <Typography variant="subtitle2">Change</Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography variant="h5">{data.Change}</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={4}>
                        <Grid container spacing={1}>
                            <Grid item xs={12}>
                                <Typography variant="subtitle2">Trend</Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography variant="h5">{data.Trend}</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    </MainCard>
);
}

export default CustomerSatisfactionCard;
