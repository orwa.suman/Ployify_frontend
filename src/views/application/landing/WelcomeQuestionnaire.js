import Landing from '../../pages/landing';

function WelcomeQuestionnaire() {
    const questions = [
        {
            question: 'How do you do?',
            answers: [{ label: 'A - Correct', correct: true }, { label: 'B - Correct', correct: true }, { label: 'Not correct' }]
        },
        {
            question: 'What do you do?',
            answers: [
                { label: 'Jogging - Correct', correct: true },
                { label: 'Running - Correct', correct: true },
                { label: 'Fishing - Not correct' }
            ]
        }
    ];
    const landingData = {questions};
    return <Landing data={landingData}></Landing>;
}

export default WelcomeQuestionnaire;
