// material-ui

import { Button, Card, CardActions, CardContent, Divider, Grid, Stack, Typography } from '@mui/material';
import { DataObjectOutlined, Favorite } from '@mui/icons-material';
import { FormattedMessage, useIntl } from 'react-intl';
import { redeemReward, setRewardAsGoalForUser, setRewardAsSelectedForUser } from 'services/GamificationService';

import ArticleIcon from '@mui/icons-material/Article';
import BillCard from 'ui-component/cards/BillCard';
import Chip from 'ui-component/extended/Chip';
import MilitaryTechIcon from '@mui/icons-material/MilitaryTech';
import React from 'react';
import Rewards from './Rewards';
import SubCard from 'ui-component/cards/SubCard';
import TimelineIcon from '@mui/icons-material/Timeline';
import TrackChangesIcon from '@mui/icons-material/TrackChanges';
import UserCountCard from 'ui-component/cards/UserCountCard';
import WalletIcon from '@mui/icons-material/Wallet';
import { dispatch } from 'store';
import { getUserById } from 'services/userService';
import { gridSpacing } from 'store/constant';
import { loadAllCustomRewardsOfCompany } from 'services/CompanyService';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';
import useAuth from 'hooks/useAuth';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';

// project imports

// assets

// table data
function createData(tid, date, amount, badgeText, badgeType) {
    return { tid, date, amount, badgeText, badgeType };
}

// ==============================|| PROFILE 3 - BILLING ||============================== //

const UserReward = () => {
    const theme = useTheme();
    const { getUserInfoByEmail } = useAuth();
    const user = loggedInUser;
    const intl = useIntl();
    const navigate = useNavigate();
    let [userInfo, setUserInfo] = React.useState(user.userInfo ?? {});
    let [rewards, setRewards] = React.useState([]);
    const [selectedReward, setSelectedReward] = React.useState([]);
    const [goalReward, setGoalReward] = React.useState([]);

    React.useEffect(() => {
        loadRewards();

        
        const lastReward = userInfo?.usedRewardHistory?.[userInfo.usedRewardHistory.length - 1];
        

        setSelectedReward(lastReward);
        setGoalReward(userInfo?.rewardGoal);
    }, []);

    const loadRewards = () => {
        loadAllCustomRewardsOfCompany(loggedInUser.company.id)
            .then((res) => res.data)
            .then((data) => {
                
                setRewards((!!data && data) || []);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const setSelectedRewardForUser = (rewardTitle) => {
        redeemReward(loggedInUser.id, rewardTitle).then((res) => {
            setSelectedReward(res?.data);
            dispatch(showMessage('Reward successfully selected to redeem!', 'success'));
            getUserInfoByEmail();
            window.location.reload();
        });
    };

    const setRewardGoalForUser = (rewardInfo) => {
        setRewardAsGoalForUser(loggedInUser.id, rewardInfo)
            .then((res) => res.data)
            .then((data) => {
                setGoalReward(data);
                dispatch(showMessage('Reward successfully set as goal.', 'success'));
            });
    };

    const rewardStatusTypes = {
        NEW: { label: <FormattedMessage id="NEW" />, color: 'error' },
        PROGRESS: { label: <FormattedMessage id="PROGRESS" />, color: 'secondary' },
        PURCHASED: { label: <FormattedMessage id="PURCHASED" />, color: 'primary' },
        DONE: { label: <FormattedMessage id="DONE" />, color: 'success' }
    };

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} sm={6} md={6}>
                <UserCountCard
                    primary={intl.formatMessage({ id: 'Point to use' })}
                    secondary={'Your Points' + ' ' + (userInfo.points ?? 0).toString()}
                    iconPrimary={WalletIcon}
                    color={theme.palette.secondary.main}
                    onClick={() => navigate('/activities/TrainingPlan/cards/HOME')}
                />
            </Grid>
            <Grid item xs={12} sm={6} md={6}>
                <UserCountCard
                    primary={intl.formatMessage({ id: 'Punkte bis deine ausgewählte Belohnung' })}
                    secondary={
                        goalReward?.rewardPoints <= userInfo?.points ? 'You can redeem your reward goal!' : 'No Goal Reward Selected!'
                    }
                    iconPrimary={TimelineIcon}
                    color={theme.palette.primary.dark}
                    userRemainingPoints={
                        goalReward?.rewardPoints > userInfo?.points ? Math.abs(goalReward?.rewardPoints - userInfo?.points) : null
                    }
                    userActualPoints={userInfo?.points}
                    rewardGoalPoints={goalReward?.rewardPoints}
                />
            </Grid>
            {/*<Grid item xs={12} sm={6} md={4}>
                <UserCountCard
                    primary={intl.formatMessage({ id: 'Gewünschte Belohnung' })}
                    secondary={goalReward?.customReward?.title || 'No Reward set as goal'}
                    iconPrimary={MilitaryTechIcon}
                    color={theme.palette.success.dark}
                />
            </Grid> */}
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                    {selectedReward && (
                        <Grid item xs={12} md={6}>
                            <SubCard title="Redeem Reward" sx={{ height: '504px' }}>
                                {selectedReward && (
                                    <SubCard>
                                        <Card>
                                            <div className="careployee-cards__image">
                                                <img
                                                    // src={lastReward?.usedRewards?.customReward?.picture ? lastReward?.usedRewards?.customReward?.picture : '/images/reward.jpg'}
                                                    // alt={lastReward?.usedRewards?.customReward?.title || '<No Title>'}
                                                    src={
                                                        selectedReward?.usedRewards?.customReward?.picture
                                                            ? selectedReward?.usedRewards?.customReward?.picture
                                                            : '/images/reward.jpg'
                                                    }
                                                    alt={selectedReward?.usedRewards?.customReward?.title || '<No Title>'}
                                                ></img>
                                            </div>
                                            <CardContent sx={{ padding: '24px 12px' }}>
                                                <Grid container spacing={1}>
                                                    <Grid
                                                        item
                                                        xs={12}
                                                        className="careployee-cards__most-used__wrapper"
                                                        sx={{
                                                            display: 'flex',
                                                            justifyContent: 'space-between',
                                                            alignItems: 'center'
                                                        }}
                                                    >
                                                        <Typography variant="subtitle1">
                                                            {selectedReward?.usedRewards?.customReward?.title || '<No Title>'}
                                                        </Typography>
                                                        <div
                                                            style={{
                                                                padding: '10px',
                                                                textAlign: 'center',
                                                                // marginBottom: '12px',
                                                                display: 'flex',
                                                                justifyContent: 'center',
                                                                alignItems: 'center',
                                                                // backgroundColor: '#cbab70',
                                                                color: 'black',
                                                                border: '1px dashed #cbab70',
                                                                borderRadius: '30px'
                                                            }}
                                                        >
                                                            <Typography variant="p">Code to Collect :</Typography>
                                                            <Typography variant="h5">{selectedReward?.redeemReward?.code}</Typography>
                                                        </div>
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Stack sx={{ paddingTop: '8px' }}>
                                                            <Grid
                                                                container
                                                                alignItems="center"
                                                                sx={{ width: '100%', marginBottom: '12px' }}
                                                            >
                                                                <Grid item>
                                                                    <TimelineIcon />
                                                                </Grid>
                                                                <Grid
                                                                    item
                                                                    sx={{
                                                                        display: 'flex',
                                                                        justifyContent: 'space-between',
                                                                        alignItems: 'center'
                                                                    }}
                                                                    md={8}
                                                                >
                                                                    <Typography
                                                                        variant="h4"
                                                                        sx={{
                                                                            paddingTop: '4px',
                                                                            paddingLeft: '16px'
                                                                            // marginBottom: '12px'
                                                                        }}
                                                                    >
                                                                        {selectedReward?.usedPoints || 0} points used
                                                                    </Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Chip
                                                                        size="medium"
                                                                        label={
                                                                            rewardStatusTypes[selectedReward?.redeemReward?.status]?.label
                                                                        }
                                                                        chipcolor={
                                                                            rewardStatusTypes[selectedReward?.redeemReward?.status]?.color
                                                                        }
                                                                        sx={{
                                                                            borderRadius: '4px',
                                                                            textTransform: 'capitalize',
                                                                            fontWeight: 'bold'
                                                                        }}
                                                                    />
                                                                </Grid>
                                                            </Grid>
                                                        </Stack>
                                                    </Grid>
                                                </Grid>
                                            </CardContent>
                                        </Card>
                                    </SubCard>
                                )}
                            </SubCard>
                        </Grid>
                    )}

                    {/* selected REWARD GOAL */}

                    {goalReward && (
                        <Grid item xs={12} md={6}>
                            {goalReward && (
                                <SubCard title="Reward Goal" sx={{ height: '504px' }}>
                                    <SubCard>
                                        <Card>
                                            <div className="careployee-cards__image">
                                                <img
                                                    // src={lastReward?.usedRewards?.customReward?.picture ? lastReward?.usedRewards?.customReward?.picture : '/images/reward.jpg'}
                                                    // alt={lastReward?.usedRewards?.customReward?.title || '<No Title>'}

                                                    src={
                                                        goalReward?.customReward?.picture
                                                            ? goalReward?.customReward?.picture
                                                            : '/images/reward.jpg'
                                                    }
                                                    alt={goalReward?.customReward?.title || '<No Title>'}
                                                ></img>
                                            </div>
                                            <CardContent sx={{ padding: '24px 12px' }}>
                                                <Grid container spacing={1}>
                                                    <Grid item xs={12} className="careployee-cards__most-used__wrapper">
                                                        <Typography variant="subtitle1">
                                                            {goalReward?.customReward?.title || '<No Title>'}
                                                        </Typography>
                                                    </Grid>

                                                    <Grid item xs={12}>
                                                        <Stack sx={{ paddingTop: '8px' }}>
                                                            <Grid container alignItems="center" sx={{ width: '100%' }}>
                                                                <Grid item>
                                                                    <TimelineIcon />
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography
                                                                        variant="h4"
                                                                        sx={{
                                                                            paddingTop: '4px',
                                                                            paddingLeft: '16px',
                                                                            marginBottom: '12px'
                                                                        }}
                                                                    >
                                                                        {goalReward.rewardPoints > userInfo?.points
                                                                            ? goalReward.rewardPoints -
                                                                              userInfo?.points +
                                                                              ' points to get the reward!'
                                                                            : 'You can redeem your reward goal!'}
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Stack>
                                                    </Grid>
                                                </Grid>
                                            </CardContent>
                                        </Card>
                                    </SubCard>
                                </SubCard>
                            )}
                        </Grid>
                    )}
                </Grid>
            </Grid>
            {/* ALL REWARDS */}

            <Typography style={{ padding: '20px', marginTop: '20px' }} variant="h3">
                All Rewards
            </Typography>
            <Grid container sx={{ padding: '30px 24px 24px' }}>
                {(rewards ?? []).map((reward) => (
                    <Grid xs={12} md={3} sx={{ padding: '12px' }}>
                        <SubCard>
                            <Card>
                                <div className="careployee-cards__image">
                                    <img src={reward.customReward.picture || '/images/dish.jpg'} alt={reward.title}></img>
                                </div>
                                <CardContent sx={{ padding: '24px 12px' }}>
                                    <Grid container spacing={1}>
                                        <Grid item xs={12} className="careployee-cards__most-used__wrapper">
                                            <Typography variant="subtitle1">{reward.customReward.title || 'Reward'}</Typography>
                                        </Grid>

                                        <Grid item xs={12}>
                                            <Stack sx={{ paddingTop: '8px' }}>
                                                <Grid container alignItems="center" sx={{ width: '100%', marginBottom: '12px' }}>
                                                    <Grid item>
                                                        <TimelineIcon />
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography
                                                            variant="h4"
                                                            sx={{ paddingTop: '4px', paddingLeft: '16px', marginBottom: '12px' }}
                                                        >
                                                            {reward.rewardPoints || 0} <br />
                                                            points per item
                                                        </Typography>
                                                    </Grid>
                                                </Grid>

                                                <Grid container alignItems="center">
                                                    <Grid item>
                                                        <ArticleIcon />
                                                    </Grid>
                                                    <Grid item>
                                                        <Typography variant="h4" sx={{ paddingTop: '4px', paddingLeft: '16px' }}>
                                                            {reward.availableItemsNum || 0} <br /> items available
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </CardContent>

                                <CardActions sx={{ padding: '4px' }}>
                                    <Grid container>
                                        <Grid item xs={12}>
                                            <>
                                                <Button
                                                    sx={{ marginBottom: '12px' }}
                                                    variant="outlined"
                                                    fullWidth
                                                    startIcon={<MilitaryTechIcon />}
                                                    onClick={(event) => setSelectedRewardForUser(reward.customReward?.title)}
                                                    disabled={reward.rewardPoints > loggedInUser.userInfo?.points}
                                                >
                                                    <FormattedMessage id="Auswählen zum Einlösen!" />
                                                </Button>

                                                <Button
                                                    variant="outlined"
                                                    fullWidth
                                                    startIcon={<TrackChangesIcon />}
                                                    onClick={(event) => setRewardGoalForUser(reward)}
                                                >
                                                    <FormattedMessage id="Add as Goal" />
                                                </Button>
                                            </>
                                        </Grid>
                                    </Grid>
                                </CardActions>
                            </Card>
                        </SubCard>
                    </Grid>
                ))}
            </Grid>
        </Grid>
    );
};

export default UserReward;
