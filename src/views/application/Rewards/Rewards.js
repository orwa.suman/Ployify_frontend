import {
    Avatar,
    Box,
    Button,
    CardActions,
    CardContent,
    CardMedia,
    Checkbox,
    Chip,
    CircularProgress,
    IconButton,
    Menu,
    MenuItem,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TableSortLabel,
    Toolbar,
    Tooltip,
    Typography
} from '@mui/material';
import { deleteRewardById, loadAllBaseRewards, updateRewardById } from 'services/BaseRewardService';
import { setRewardAsGoalForUser, setRewardAsSelectedForUser } from '../../../services/GamificationService';

import Dashboard1 from 'assets/images/widget/dashborad-1.jpg';
import DeleteIcon from '@mui/icons-material/Delete';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import React from 'react';
import { getUserById } from 'services/userService';
import { runInContext as headCells } from 'lodash';
import { loadAllCustomRewardsOfCompany } from 'services/CompanyService';
import { loggedInUser } from '../../../contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';

const prodImage = require.context('assets/images/e-commerce', true);

const mediaSX = {
    width: 90,
    height: 80,
    borderRadius: '12px'
};

const EnhancedTableToolbar = ({ numSelected }) => (
    <Toolbar
        sx={{
            p: 0,
            pl: 2,
            pr: 1,
            color: numSelected > 0 ? 'secondary.main' : 'inherit'
        }}
    >
        {numSelected > 0 ? (
            <Typography sx={{ flex: '1 1 100%' }} color="inherit" variant="h4" component="div">
                {numSelected} Selected
            </Typography>
        ) : (
            <Typography sx={{ flex: '1 1 100%' }} variant="h6" id="tableTitle" component="div">
                Nutrition
            </Typography>
        )}

        {numSelected > 0 && (
            <Tooltip title="Delete">
                <IconButton size="large">
                    <DeleteIcon fontSize="small" />
                </IconButton>
            </Tooltip>
        )}
    </Toolbar>
);

EnhancedTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired
};

function EnhancedTableHead({ onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort, theme, selected }) {
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                <TableCell padding="checkbox" sx={{ pl: 3 }}>
                    <Checkbox
                        color="primary"
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{
                            'aria-label': 'select all desserts'
                        }}
                    />
                </TableCell>
                {numSelected > 0 && (
                    <TableCell padding="none" colSpan={7}>
                        <EnhancedTableToolbar numSelected={selected.length} />
                    </TableCell>
                )}
                {numSelected <= 0 &&
                    headCells.map((headCell) => (
                        <TableCell
                            key={headCell.id}
                            align={headCell.align}
                            padding={headCell.disablePadding ? 'none' : 'normal'}
                            sortDirection={orderBy === headCell.id ? order : false}
                        >
                            <TableSortLabel
                                active={orderBy === headCell.id}
                                direction={orderBy === headCell.id ? order : 'asc'}
                                onClick={createSortHandler(headCell.id)}
                            >
                                {headCell.label}
                                {orderBy === headCell.id ? (
                                    <Typography component="span" sx={{ display: 'none' }}>
                                        {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                    </Typography>
                                ) : null}
                            </TableSortLabel>
                        </TableCell>
                    ))}
                {numSelected <= 0 && (
                    <TableCell sortDirection={false} align="center" sx={{ pr: 3 }}>
                        <Typography
                            variant="subtitle1"
                            sx={{ color: theme.palette.mode === 'dark' ? theme.palette.grey[600] : 'grey.900' }}
                        >
                            Action
                        </Typography>
                    </TableCell>
                )}
            </TableRow>
        </TableHead>
    );
}

function Rewards({ edit, onUserAction }) {
    const dispatch = useDispatch();
    const theme = useTheme();
    let [data, setData] = React.useState([]);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [isLoading, setLoading] = React.useState(true);
    const navigate = useNavigate();

    React.useEffect(() => {
        loadRewards();
    }, []);

    const loadRewards = () => {
        loadAllBaseRewards()
            .then((res) => res.data)
            .then((data) => {
                console.log(data);
                setData((!!data && data) || []);
                setLoading(false);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
                setLoading(false);
            });
    };

    function setSelectedRewardForUser(rewardInfo) {
        const rewardTitle = rewardInfo.customReward.title;
        setRewardAsSelectedForUser(loggedInUser.id, rewardTitle);
    }

    function setRewardGoalForUser(rewardInfo) {
        setRewardAsGoalForUser(loggedInUser.id, rewardInfo)
            .then((res) => res.data)
            .then((data) => {
                loadRewards();
                getUserById(loggedInUser.id)
                    .then((res) => res.data)
                    .then((user) => {
                        onUserAction(user.userInfo);
                    })
                    .catch((e) => {});
            })
            .catch((e) => {});
    }

    function deleteReward(id) {
        deleteRewardById(id)
            .then((res) => res.data)
            .then((data) => {
                loadRewards();
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }

    function updateReward(data) {
        console.log(data);
        navigate('/dashboard/reward/form/' + data.id);
    }

    return (
        <>
            <CardContent>
                <TableContainer>
                    {isLoading && (
                        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                            <CircularProgress size={100} style={{ color: '#cbab70' }} />
                        </Box>
                    )}
                    <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle">
                        {/*<EnhancedTableHead*/}
                        {/*    numSelected={selected.length}*/}
                        {/*    rowCount={15}*/}
                        {/*    theme={theme}*/}
                        {/*    selected={true}*/}
                        {/*/>*/}
                        <TableBody>
                            {data.map((row, index) => {
                                if (typeof row === 'number') return null;
                                const labelId = `enhanced-table-checkbox-${index}`;

                                return (
                                    <TableRow
                                        hover
                                        role="checkbox"
                                        aria-checked={true}
                                        tabIndex={-1}
                                        key={index}
                                        selected={true}
                                        // onClick={(event) => setSelectedRewardForUser(row)}
                                    >
                                        <TableCell align="center" component="th" id={labelId} scope="row" sx={{ cursor: 'pointer' }}>
                                            {/* <Avatar src={row.image && prodImage(`./${row.image}`).default} size="md" variant="rounded" /> */}
                                            <CardMedia
                                                component="img"
                                                image={row.picture || Dashboard1}
                                                title="image"
                                                sx={mediaSX}
                                                onClick={() =>
                                                    window.open('https://www.youtube.com/watch?v=mQR0bXO_yI8&ab_channel=RAYLOWESWINGS')
                                                }
                                            />
                                        </TableCell>
                                        <TableCell component="th" id={labelId} scope="row" sx={{ cursor: 'pointer' }}>
                                            <Typography
                                                // component={Link}
                                                // to={`/e-commerce/product-details/${row.id}`}
                                                variant="subtitle1"
                                                sx={{
                                                    color: theme.palette.mode === 'dark' ? theme.palette.grey[600] : 'grey.900',
                                                    textDecoration: 'none'
                                                }}
                                            >
                                                {row.title}
                                            </Typography>
                                        </TableCell>
                                        <TableCell>{row.description}</TableCell>
                                        <TableCell align="right">
                                            {row.defaultBasePoints} <FormattedMessage id="points per item" />
                                        </TableCell>
                                        <TableCell align="right">
                                            {row.defaultAvailableItemsNum} <FormattedMessage id="items   available" />
                                        </TableCell>
                                        <TableCell align="center">
                                            <Chip
                                                size="small"
                                                label={
                                                    <FormattedMessage
                                                        id={row.defaultAvailableItemsNum !== 0 ? 'verfügbar' : 'ausverkauft'}
                                                    />
                                                }
                                                chipcolor={row.defaultAvailableItemsNum !== 0 ? 'success' : 'error'}
                                                sx={{ borderRadius: '4px', textTransform: 'capitalize' }}
                                            />
                                        </TableCell>
                                        <TableCell align="center" sx={{ pr: 3 }}>
                                            <Button onClick={() => console.log('Icon button clicked')} size="large">
                                                {/*<MoreHorizOutlinedIcon*/}
                                                {/*    fontSize="small"*/}
                                                {/*    aria-controls="menu-popular-card-1"*/}
                                                {/*    aria-haspopup="true"*/}
                                                {/*    sx={{color: 'grey.500'}}*/}
                                                {/*/>*/}
                                            </Button>
                                            <Menu
                                                id="menu-popular-card-1"
                                                anchorEl={anchorEl}
                                                keepMounted
                                                open={Boolean(anchorEl)}
                                                onClose={() => console.log('closed')}
                                                variant="selectedMenu"
                                                anchorOrigin={{
                                                    vertical: 'bottom',
                                                    horizontal: 'right'
                                                }}
                                                transformOrigin={{
                                                    vertical: 'top',
                                                    horizontal: 'right'
                                                }}
                                                sx={{
                                                    '& .MuiMenu-paper': {
                                                        boxShadow: theme.customShadows.z1
                                                    }
                                                }}
                                            >
                                                <MenuItem onClick={() => {}}> Edit</MenuItem>
                                                <MenuItem onClick={() => {}}> Delete</MenuItem>
                                            </Menu>
                                        </TableCell>

                                        <TableCell>
                                            <CardActions sx={{ justifyContent: 'flex-end' }}>
                                                {/* <Button variant="text" size="small" onClick={(event) => setRewardGoalForUser(row)}>
                                                    <FormattedMessage id="Add as Goal" />
                                                </Button>

                                                <Button
                                                    variant="text"
                                                    size="small"
                                                    onClick={(event) => setSelectedRewardForUser(row)}
                                                    disabled={row.defaultBasePoints > loggedInUser.userInfo?.points}
                                                >
                                                    <FormattedMessage id="Auswählen zum Einlösen!" />
                                                </Button> */}

                                                <Button variant="text" size="small" onClick={(event) => deleteReward(row.id)}>
                                                    <FormattedMessage id="Delete" />
                                                </Button>

                                                <Button variant="text" size="small" onClick={(event) => updateReward(row)}>
                                                    <FormattedMessage id="Edit" />
                                                </Button>
                                            </CardActions>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>

                {/*<TableContainer>*/}
                {/*    <Table sx={{minWidth: 560}}>*/}
                {/*        <TableHead>*/}
                {/*            <TableRow>*/}
                {/*                <TableCell>Reward</TableCell>*/}
                {/*                <TableCell sx={{pr: 3}}>Info</TableCell>*/}
                {/*                <TableCell sx={{pl: 3}}>Requirements</TableCell>*/}
                {/*            </TableRow>*/}
                {/*        </TableHead>*/}
                {/*        <TableBody>*/}
                {/*            {data.map((row, index) => (*/}
                {/*                <TableRow hover key={index}>*/}

                {/*                    <TableCell>*/}
                {/*                        <Grid container spacing={2} alignItems="center" sx={{flexWrap: 'nowrap'}}>*/}
                {/*                            <Grid item>*/}
                {/*                                <Avatar alt="User 1" src={row.avatar}/>*/}
                {/*                            </Grid>*/}
                {/*                            <Grid item xs zeroMinWidth>*/}
                {/*                                <Typography component="div" align="left" variant="subtitle1">*/}
                {/*                                    {row.reward.title !== "CUSTOM" ?*/}
                {/*                                        row.reward.title :*/}
                {/*                                        row.customReward.title*/}
                {/*                                    }*/}
                {/*                                </Typography>*/}
                {/*                            </Grid>*/}
                {/*                        </Grid>*/}
                {/*                    </TableCell>*/}
                {/*                    <TableCell sx={{pr: 3}}>*/}
                {/*                        <Typography align="left" component="div" variant="subtitle1">*/}
                {/*                            {row.availableItemsNum} items left*/}
                {/*                        </Typography>*/}
                {/*                        <Typography align="left" component="div" variant="subtitle2">*/}
                {/*                            {row.reward.title !== "CUSTOM" ?*/}
                {/*                                row.reward.description :*/}
                {/*                                row.customReward.description*/}
                {/*                            }*/}
                {/*                        </Typography>*/}
                {/*                    </TableCell>*/}

                {/*                    <TableCell sx={{pl: 3}}>*/}
                {/*                        <Typography align="left" component="div" variant="subtitle1">*/}
                {/*                            {row.rewardPoints} points per item*/}
                {/*                        </Typography>*/}
                {/*                        /!*<Typography align="left" component="div" variant="subtitle2">*!/*/}
                {/*                        /!*    {row.availableItemsNum} items available *!/*/}
                {/*                        /!*</Typography>*!/*/}
                {/*                    </TableCell>*/}

                {/*                    <TableCell sx={{pl: 3}}>*/}
                {/*                        <Button variant="text" size="small" onClick={() => console.log("I AM CLICKED")}>*/}
                {/*                            Add as Goal*/}
                {/*                        </Button>*/}
                {/*                    </TableCell>*/}

                {/*                </TableRow>*/}

                {/*                // <CardActions sx={{justifyContent: 'flex-end'}}>*/}
                {/*                // <Button variant="text" size="small" onClick={() => console.log("I AM CLICKED")}>*/}
                {/*                // Add as Goal*/}
                {/*                // </Button>*/}

                {/*                // </CardActions>*/}

                {/*            ))}*/}
                {/*        </TableBody>*/}
                {/*    </Table>*/}
                {/*</TableContainer>*/}
            </CardContent>
        </>
    );
}

export default Rewards;
