import { Grid, TextField } from '@mui/material';
import {
    addCustomRewardToCompanyRewards,
    loadAllCustomRewardsOfCompany,
    updateCustomRewardToCompanyRewards
} from '../../../services/CompanyService';
import { addReward, getRewardById, updateRewardById } from '../../../services/BaseRewardService';

import FileUpload from 'views/forms/components/FileUpload';
import { FormattedMessage } from 'react-intl';
import { LoadingButton } from '@mui/lab';
import MainCard from '../../../ui-component/cards/MainCard';
import React from 'react';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';
import { uploadToCloudinary } from 'services/CloudinaryService';
import { useDispatch } from 'store';
import { useFormik } from 'formik';
import { useParams } from 'react-router-dom';

function RewardForm(customReward = false, toBeUpdatedReward = null) {
    if (customReward !== true) {
        customReward = false;
    }

    const { id } = useParams();
    const dispatch = useDispatch();

    const [picture, setPicture] = React.useState([]);
    const [pictureName, setPictureName] = React.useState([]);
    const [pictureChanged, setPictureChanged] = React.useState(false);
    const [loading, setLoading] = React.useState(false);

    const formik = useFormik({
        initialValues: {
            title: '',
            description: '',
            defaultBasePoints: undefined,
            defaultAvailableItemsNum: undefined,
            link: '',
            picture: ''
        },
        onSubmit: () => {
            setLoading(true);
            uploadToCloudinary(picture, pictureName, 'reward_', '/Ployify/Reward/' + loggedInUser.company.companyName)
                .then((cloudinary_data) => {
                    if (pictureChanged) {
                        
                        
                        formik.values.picture = cloudinary_data.url;
                    }

                    let data = {};
                    if (customReward) {
                        const customReward = formik.values;
                        data.customReward = customReward;
                        data.rewardPoints = customReward.defaultBasePoints;
                        data.availableItemsNum = customReward.defaultAvailableItemsNum;

                        if (!toBeUpdatedReward?.isUpdateCustomReward) {
                            addCustomRewardToCompanyRewards(loggedInUser.company.id, data)
                                .then(() => {
                                    setLoading(false);
                                    dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success'));
                                })
                                .catch((e) => {
                                    dispatch(showMessage(e?.response?.data?.message));
                                    setLoading(false);
                                });
                        } else {
                            updateCustomRewardToCompanyRewards(loggedInUser.company.id, data)
                                .then(() => {
                                    setLoading(false);
                                    dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success'));
                                })
                                .catch((e) => {
                                    dispatch(showMessage(e?.response?.data?.message));
                                    setLoading(false);
                                });
                        }
                    } else {
                        if (formik.values.id) {
                            updateRewardById(formik.values)
                                .then(() => {
                                    setLoading(false);
                                    dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success'));
                                })
                                .catch((e) => {
                                    dispatch(showMessage(e?.response?.data?.message));
                                });
                        } else {
                            addReward(formik.values)
                                .then(() => {
                                    setLoading(false);
                                    dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success'));
                                })
                                .catch((e) => {
                                    dispatch(showMessage(e?.response?.data?.message));
                                });
                        }
                    }
                })
                .catch((err) => {
                    
                });
        }
    });

    const setFormValues = (row) => {
        if (!row) {
            return;
        }
        formik.setValues({
            id: id,
            title: row.title,
            description: row.description,
            defaultBasePoints: row.defaultBasePoints,
            defaultAvailableItemsNum: row.defaultAvailableItemsNum,
            link: row.link,
            picture: row.picture
        });
    };

    const handleFileUpload = (data, fileName) => {
        // formik.setValues({ ...formik.values, picture })
        setPicture(data);
        setPictureName(fileName);
        setPictureChanged(true);
    };

    React.useEffect(() => {
        if (id) {
            if (customReward) {
                loadAllCustomRewardsOfCompany()
                    .then((res) => res.data)
                    .then((data) => {
                        setFormValues(data.find((item) => item.id === id));
                    })
                    .catch((e) => {
                        dispatch(showMessage(e?.response?.data?.message));
                    });
            } else {
                getRewardById(id)
                    .then((res) => res.data)
                    .then((data) => {
                        setFormValues(data);
                    })
                    .catch((e) => {
                        dispatch(showMessage(e?.response?.data?.message));
                    });
            }
        } else if (toBeUpdatedReward.customReward) {
            setFormValues(toBeUpdatedReward.customReward);
        }
    }, [id, customReward, toBeUpdatedReward]);

    return (
        <Grid>
            <form onSubmit={formik.handleSubmit}>
                <MainCard title={<FormattedMessage id={customReward ? 'Add new Custom Reward' : 'Add new Base Reward'} />}>
                    <Grid item xs={12} className="careployee-form__block">
                        <TextField
                            fullWidth
                            id="title"
                            label={<FormattedMessage id="Title" />}
                            value={formik.values.title}
                            onChange={formik.handleChange}
                        />
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <TextField
                            fullWidth
                            id="description"
                            label={<FormattedMessage id="Description" />}
                            multiline
                            rows={5}
                            value={formik.values.description}
                            onChange={formik.handleChange}
                        />
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <TextField
                            fullWidth
                            id="defaultBasePoints"
                            label={
                                customReward ? <FormattedMessage id="Points to get the reward" /> : <FormattedMessage id="Default points" />
                            }
                            value={formik.values.defaultBasePoints}
                            onChange={formik.handleChange}
                        />
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <TextField
                            fullWidth
                            id="defaultAvailableItemsNum"
                            label={<FormattedMessage id="Number of available items" />}
                            value={formik.values.defaultAvailableItemsNum}
                            onChange={formik.handleChange}
                        />
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <TextField
                            fullWidth
                            id="link"
                            label={<FormattedMessage id="Link to the reward" />}
                            value={formik.values.link}
                            onChange={formik.handleChange}
                        />
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <FileUpload label="Picture" onChange={handleFileUpload}></FileUpload>
                    </Grid>

                    <Grid>
                        <LoadingButton loading={loading} variant="contained" type={'submit'} onClick={formik.submit}>
                            <FormattedMessage id="Submit" />
                        </LoadingButton>
                    </Grid>
                </MainCard>
            </form>
        </Grid>
    );
}

export default RewardForm;
