import {
    Box,
    Chip,
    Collapse,
    IconButton,
    LinearProgress,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography
} from '@mui/material';

import { FormattedMessage } from 'react-intl';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import React from 'react';
import SubCard from 'ui-component/cards/SubCard';
import { getUsersListStyle1 } from 'store/slices/user';
import { loadCompanies } from '../../../../../services/CompanyService';
import { useDispatch } from 'store';
import { useTheme } from '@mui/material/styles';

// material-ui

// project imports

// assets

// assets

// ==============================|| USER LIST 1 ||============================== //

function CollapseRow({ row }) {
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);

    return (
        <>
            <TableRow hover sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell align="right">
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell>
                    <Typography align="left" component="div">
                        {row.companyName}
                    </Typography>
                </TableCell>
                <TableCell>{row.numAllRegistrationCodes || 0}</TableCell>
                <TableCell>{row.numUsedRegistrationCodes || 0}</TableCell>
                <TableCell>{row.numActiveRegistrationCodes || 0}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        {open && (
                            <Box sx={{ margin: 1 }}>
                                <TableContainer>
                                    <SubCard
                                        sx={{ bgcolor: theme.palette.mode === 'dark' ? 'dark.800' : 'grey.50', mb: 2 }}
                                        content={false}
                                    >
                                        <Table size="small" aria-label="purchases">
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell></TableCell>
                                                    <TableCell>
                                                        <FormattedMessage id="Code" />
                                                    </TableCell>
                                                    <TableCell>
                                                        <FormattedMessage id="Used" />
                                                    </TableCell>
                                                    <TableCell>
                                                        <FormattedMessage id="User is active" />
                                                    </TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {(row.registrationCodeList || []).map((code) => (
                                                    <TableRow hover key={code.registrationCode}>
                                                        <TableCell component="th" scope="row"></TableCell>
                                                        <TableCell component="th" scope="row">
                                                            {code.registrationCode}
                                                        </TableCell>
                                                        <TableCell component="th" scope="row">
                                                            {code.used ? (
                                                                <Chip
                                                                    label={<FormattedMessage id="Used" />}
                                                                    size="small"
                                                                    sx={{
                                                                        background:
                                                                            theme.palette.mode === 'dark'
                                                                                ? theme.palette.dark.main
                                                                                : theme.palette.success.light + 60,
                                                                        color: theme.palette.success.dark
                                                                    }}
                                                                />
                                                            ) : (
                                                                <Chip
                                                                    label={<FormattedMessage id="Not Used" />}
                                                                    size="small"
                                                                    sx={{
                                                                        background:
                                                                            theme.palette.mode === 'dark'
                                                                                ? theme.palette.dark.main
                                                                                : theme.palette.orange.light + 80,
                                                                        color: theme.palette.orange.dark
                                                                    }}
                                                                />
                                                            )}
                                                        </TableCell>
                                                        <TableCell component="th" scope="row">
                                                            {code.assignedUser?.active ? (
                                                                <Chip
                                                                    label={<FormattedMessage id="Active" />}
                                                                    size="small"
                                                                    sx={{
                                                                        background:
                                                                            theme.palette.mode === 'dark'
                                                                                ? theme.palette.dark.main
                                                                                : theme.palette.success.light + 60,
                                                                        color: theme.palette.success.dark
                                                                    }}
                                                                />
                                                            ) : (
                                                                <Chip
                                                                    label={<FormattedMessage id="Not Active" />}
                                                                    size="small"
                                                                    sx={{
                                                                        background:
                                                                            theme.palette.mode === 'dark'
                                                                                ? theme.palette.dark.main
                                                                                : theme.palette.orange.light + 80,
                                                                        color: theme.palette.orange.dark
                                                                    }}
                                                                />
                                                            )}
                                                        </TableCell>
                                                    </TableRow>
                                                ))}
                                            </TableBody>
                                        </Table>
                                    </SubCard>
                                </TableContainer>
                            </Box>
                        )}
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
}

const UserList = () => {
    const dispatch = useDispatch();

    let [data, setData] = React.useState([]);
    let [loading, setLoading] = React.useState([]);

    React.useEffect(() => {
        setLoading(true);
        loadCompanies()
            .then((res) => res.data)
            .then(setData)
            .then(() => setLoading(false))
            .catch((e) => {
                
            });
    }, []);

    React.useEffect(() => {
        dispatch(getUsersListStyle1());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <TableContainer>
            {loading && (
                <LinearProgress
                    variant="indeterminate"
                    sx={{
                        backgroundColor: '#cbab7075',
                        '& .MuiLinearProgress-bar': {
                            backgroundColor: '#cbab70'
                        }
                    }}
                />
            )}
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell></TableCell>
                        <TableCell>
                            <FormattedMessage id="Company Name" />
                        </TableCell>
                        <TableCell>
                            <FormattedMessage id="No. assigned code" />
                        </TableCell>
                        <TableCell>
                            <FormattedMessage id="No. used code" />
                        </TableCell>

                        <TableCell>
                            <FormattedMessage id="No. active code" />
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>{data && data?.map((row, index) => <CollapseRow row={row} key={row.id}></CollapseRow>)}</TableBody>
            </Table>
        </TableContainer>
    );
};

export default UserList;
