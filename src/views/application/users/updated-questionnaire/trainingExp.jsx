import { Box, Typography } from '@mui/material';

import Slider from '@mui/material/Slider';
import { useEffect } from 'react';

const TrainingExp = ({ trainingExp, setTrainingExp, answerList, setEnabled }) => {
    const handleChange = (event, newValue) => {
        if (typeof newValue === 'number') {
            setTrainingExp(calculateValueNumber(newValue));
        }
    };

    useEffect(() => {
        setEnabled(trainingExp ?? false);
    }, [trainingExp]);

    const marks = answerList.map((answer, index) => ({
        id: answer.id,
        value: 20 * (index + 1),
        _label: answer.answerText
    }));

    const calculateLabel = (id) => {
        return marks.find((mark) => mark.id === id)?._label;
    };

    const calculateValue = (id) => {
        return marks.find((mark) => mark.id === id)?.value;
    };

    const calculateValueNumber = (number) => {
        return marks.find((mark) => mark.value === number)?.id;
    };

    const valueLabelFormat = (value) => {
        return value;
    };

    return (
        <Box>
            <Typography gutterBottom sx={{ textAlign: 'center', fontSize: '32px', fontWeight: '500' }}>
                {valueLabelFormat(calculateLabel(trainingExp))}
            </Typography>
            <Slider
                value={calculateValue(trainingExp)}
                step={20}
                onChange={handleChange}
                valueLabelDisplay="off"
                marks={marks}
                sx={{
                    '&.MuiSlider-root': {
                        color: '#cbab70'
                    }
                }}
            />
        </Box>
    );
};

export default TrainingExp;
