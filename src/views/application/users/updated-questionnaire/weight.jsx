import { Box, TextField } from '@mui/material';

import { useEffect } from 'react';

// import Picker from 'react-mobile-picker';

const Weight = ({ weight, setWeight, setEnabled }) => {
    let weightArray = [];
    for (var i = 40; i <= 200; i++) {
        weightArray.push(i);
    }

    useEffect(() => {
        setEnabled(weight ?? false);
    }, [weight]);

    return (
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            {/* <Picker value={weight} onChange={setWeight} wheel="normal">
                <Picker.Column name={'weight'}>
                    {weightArray.map((option) => (
                        <Picker.Item key={option} value={option}>
                            {option}
                        </Picker.Item>
                    ))}
                </Picker.Column>
            </Picker> */}
            <TextField
                sx={{
                    '& label.Mui-focused': {
                        color: '#CBAB70'
                    },
                    '& .MuiOutlinedInput-root': {
                        '&.Mui-focused fieldset': {
                            borderColor: '#CBAB70'
                        }
                    }
                }}
                type="number"
                label="Weight"
                variant="outlined"
                value={weight}
                onChange={(e) => setWeight(e.target.value)}
            />
        </Box>
    );
};

export default Weight;
