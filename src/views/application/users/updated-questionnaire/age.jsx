import { Box, TextField } from '@mui/material';

import { useEffect } from 'react';

// import Picker from 'react-mobile-picker';

const Age = ({ age, setAge, setEnabled }) => {
    useEffect(() => {
        setEnabled(age ?? false);
    }, [age]);

    return (
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            {/* <Picker value={age} onChange={setAge} wheel="natural">
                <Picker.Column name={'age'}>
                    {ageArray.map((option) => (
                        <Picker.Item key={option} value={option}>
                            {option}
                        </Picker.Item>
                    ))}
                </Picker.Column>
            </Picker> */}
            <TextField
                sx={{
                    '& label.Mui-focused': {
                        color: '#CBAB70'
                    },
                    '& .MuiOutlinedInput-root': {
                        '&.Mui-focused fieldset': {
                            borderColor: '#CBAB70'
                        }
                    }
                }}
                type="number"
                label="Age"
                variant="outlined"
                value={age}
                onChange={(e) => setAge(e.target.value)}
            />
        </Box>
    );
};

export default Age;
