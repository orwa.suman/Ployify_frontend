import { Box, Grid, Typography } from '@mui/material';

import PhysicallyDemandingIcon from 'assets/images/questionnaire/workplace/physically-demanding-icon.svg';
import SettingIcon from 'assets/images/questionnaire/workplace/setting-icon.svg';
import StadningIcon from 'assets/images/questionnaire/workplace/standing-icon.svg';
import WalkingIcon from 'assets/images/questionnaire/workplace/walking-icon.svg';
import { useEffect } from 'react';

const WorkPlace = ({ answerList, workPlace, setWorkPlace, setEnabled, sitInCar, setSitInCar }) => {
    useEffect(() => {
        setEnabled(workPlace.length ? true : false);
    }, [workPlace]);

    const handleWorkPlace = (id) => {
        let index = workPlace.indexOf(id);
        if (index > -1) {
            const _workPlace = [...workPlace]; // Create a copy of the workPlace array
            _workPlace.splice(index, 1);
            setWorkPlace(_workPlace);
        } else {
            setWorkPlace((prevValues) => [...prevValues, id]);
        }
    };

    const handleSetInCar = () => {
        setSitInCar((prevValue) => !prevValue);
    };

    const HanldeImageByType = (id) => {
        if (id === '65353b98b88f4d080aa85922') return PhysicallyDemandingIcon;
        if (id === '65353b98b88f4d080aa85921') return SettingIcon;
        if (id === '65353b99b88f4d080aa85923') return StadningIcon;
        if (id === '65353b99b88f4d080aa85924') return WalkingIcon;
        else return StadningIcon;
    };

    return (
        <Box>
            <Grid container spacing={2}>
                {answerList?.map((item) => (
                    <Grid item key={item.id} xs={6} onClick={() => handleWorkPlace(item.id)} sx={{ width: '100%' }}>
                        <Box
                            sx={{
                                // width: '100%',
                                height: '90px',
                                border: `1px solid ${
                                    workPlace && workPlace.length > 0 && workPlace.find((id) => id === item.id) ? '#CBAB70' : '#E7E7E7'
                                }`,
                                borderRadius: '7px',
                                boxShadow: '0px 0px 8px 0px rgba(0, 0, 0, 0.13)',
                                padding: '10px 25px',
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'center',
                                justifyContent: 'center',
                                cursor: 'pointer',
                                transitionDuration: '300ms',
                                '&:hover': {
                                    transform: 'scale(1.1)'
                                }
                            }}
                        >
                            <Typography sx={{ color: '#535353', textAlign: 'center', mb: '10px' }}>{item.answerText}</Typography>
                            <img
                                src={HanldeImageByType(item?.id)}
                                style={{
                                    height: '30px'
                                }}
                            />
                        </Box>
                    </Grid>
                ))}
                <Grid item key="sit_in_car" xs={12} onClick={() => handleSetInCar()} sx={{ width: '100%' }}>
                    <Box
                        sx={{
                            // width: '100%',
                            // height: '90px',
                            border: `1px solid ${sitInCar ? '#CBAB70' : '#E7E7E7'}`,
                            borderRadius: '7px',
                            boxShadow: '0px 0px 8px 0px rgba(0, 0, 0, 0.13)',
                            padding: '15px 25px',
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                            cursor: 'pointer',
                            transitionDuration: '300ms',
                            '&:hover': {
                                transform: 'scale(1.1)'
                            }
                        }}
                    >
                        <Typography> Sit in the car more than 1 hour</Typography>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    );
};

export default WorkPlace;
