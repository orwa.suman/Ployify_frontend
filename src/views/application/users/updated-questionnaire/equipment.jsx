import { Box, Grid, Typography } from '@mui/material';

import { useEffect } from 'react';

const Equipment = ({ answerList, equipment, setEquipment, setEnabled }) => {
    useEffect(() => {
        setEnabled(equipment.length ? true : false);
    }, [equipment]);

    const handleselectItem = (id) => {
        let index = equipment.indexOf(id);
        if (index > -1) {
            const _equipment = [...equipment]; // Create a copy of the workPlace array
            _equipment.splice(index, 1);
            setEquipment(_equipment);
        } else {
            setEquipment((prevValues) => [...prevValues, id]);
        }
    };

    return (
        <Box>
            <Grid container spacing={2}>
                {answerList?.map((item) => (
                    <Grid item key={item.id} xs={6} onClick={() => handleselectItem(item)} sx={{ width: '100%' }}>
                        <Box
                            sx={{
                                // width: '100%',
                                // height: '90px',
                                border: `1px solid ${
                                    equipment && equipment.length > 0 && equipment.find((id) => id === item) ? '#CBAB70' : '#E7E7E7'
                                }`,
                                borderRadius: '7px',
                                boxShadow: '0px 0px 8px 0px rgba(0, 0, 0, 0.13)',
                                padding: '15px 25px',
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'center',
                                cursor: 'pointer',
                                transitionDuration: '300ms',
                                '&:hover': {
                                    transform: 'scale(1.1)'
                                }
                            }}
                        >
                            <Typography sx={{ color: '#535353' }}>{item}</Typography>
                        </Box>
                    </Grid>
                ))}
            </Grid>
        </Box>
    );
};

export default Equipment;
