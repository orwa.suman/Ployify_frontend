import { Box, Grid, TextField, Typography } from '@mui/material';
import { useEffect, useState } from 'react';

const Food = ({ answerList, food, setFood, setEnabled }) => {
    const [searchText, setSearchText] = useState('');
    const [modiefiedAnswerList, setModiefiedAnswerList] = useState(answerList);

    useEffect(() => {
        setEnabled(food.length ? true : false);
    }, [food]);

    const handleselectItem = (id) => {
        let index = food.indexOf(id);
        if (index > -1) {
            const _food = [...food]; // Create a copy of the workPlace array
            _food.splice(index, 1);
            setFood(_food);
        } else {
            setFood((prevValues) => [...prevValues, id]);
        }
    };

    const handleChange = (text) => setSearchText(text);

    const searchByName = (array, name) => {
        const matches = array.filter((item) => item.name.toLowerCase().includes(name.toLowerCase()));
        return matches;
    };

    useEffect(() => {
        const searchResults = searchByName(answerList, searchText);
        setModiefiedAnswerList(searchResults);
    }, [searchText]);

    return (
        <Box>
            <Box sx={{ display: 'flex', justifyContent: 'center', marginBottom: '8px' }}>
                <TextField
                    size="small"
                    sx={{
                        '& label.Mui-focused': {
                            color: '#CBAB70'
                        },
                        '& .MuiOutlinedInput-root': {
                            '&.Mui-focused fieldset': {
                                borderColor: '#CBAB70'
                            }
                        }
                    }}
                    type="text"
                    label="Search"
                    variant="outlined"
                    value={searchText}
                    onChange={(e) => handleChange(e.target.value)}
                />
            </Box>
            <Grid container spacing={2}>
                {modiefiedAnswerList?.map((item) => (
                    <Grid item key={item.id} xs={6} onClick={() => handleselectItem(item.id)} sx={{ width: '100%' }}>
                        <Box
                            sx={{
                                // width: '100%',
                                height: '60px',
                                border: `1px solid ${food && food.length > 0 && food.find((id) => id === item.id) ? '#CBAB70' : '#E7E7E7'}`,
                                borderRadius: '7px',
                                boxShadow: '0px 0px 8px 0px rgba(0, 0, 0, 0.13)',
                                padding: '15px 25px',
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                cursor: 'pointer',
                                transitionDuration: '300ms',
                                '&:hover': {
                                    transform: 'scale(1.1)'
                                }
                            }}
                        >
                            <img src={item.picture} width={20} height={20} />
                            <Typography sx={{ color: '#535353', textAlign: 'center' }}>{item.name}</Typography>
                        </Box>
                    </Grid>
                ))}
            </Grid>
        </Box>
    );
};

export default Food;
