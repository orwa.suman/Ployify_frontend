import { Box, TextField } from '@mui/material';

import { useEffect } from 'react';

// import Picker from 'react-mobile-picker';

const Height = ({ height, setHeight, setEnabled }) => {
    let heightArray = [];
    for (var i = 120; i <= 200; i++) {
        heightArray.push(i);
    }
    useEffect(() => {
        setEnabled(height ?? false);
    }, [height]);

    return (
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            {/* <Picker value={height} onChange={setHeight} wheel="normal">
                <Picker.Column name={'height'}>
                    {heightArray.map((option) => (
                        <Picker.Item key={option} value={option}>
                            {option}
                        </Picker.Item>
                    ))}
                </Picker.Column>
            </Picker> */}
            <TextField
                sx={{
                    '& label.Mui-focused': {
                        color: '#CBAB70'
                    },
                    '& .MuiOutlinedInput-root': {
                        '&.Mui-focused fieldset': {
                            borderColor: '#CBAB70'
                        }
                    }
                }}
                type="number"
                label="Height"
                variant="outlined"
                value={height}
                onChange={(e) => setHeight(e.target.value)}
            />
        </Box>
    );
};

export default Height;
