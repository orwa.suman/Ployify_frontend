import { Box, Grid, Typography } from '@mui/material';

import { useEffect } from 'react';

const Sport = ({ answerList, sport, setSport, setEnabled }) => {
    useEffect(() => {
        setEnabled(sport.length ? true : false);
    }, [sport]);

    const handleselectItem = (id) => {
        let index = sport.indexOf(id);
        if (index > -1) {
            const _sport = [...sport]; // Create a copy of the workPlace array
            _sport.splice(index, 1);
            setSport(_sport);
        } else {
            setSport((prevValues) => [...prevValues, id]);
        }
    };

    return (
        <Box>
            <Grid container spacing={2}>
                {answerList?.map((item) => (
                    <Grid item key={item.id} xs={6} onClick={() => handleselectItem(item.id)} sx={{ width: '100%' }}>
                        <Box
                            sx={{
                                // width: '100%',
                                // height: '90px',
                                border: `1px solid ${
                                    sport && sport.length > 0 && sport.find((id) => id === item.id) ? '#CBAB70' : '#E7E7E7'
                                }`,
                                borderRadius: '7px',
                                boxShadow: '0px 0px 8px 0px rgba(0, 0, 0, 0.13)',
                                padding: '15px 25px',
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'center',
                                cursor: 'pointer',
                                transitionDuration: '300ms',
                                '&:hover': {
                                    transform: 'scale(1.1)'
                                }
                            }}
                        >
                            <Typography sx={{ color: '#535353' }}>{item.answerText}</Typography>
                        </Box>
                    </Grid>
                ))}
            </Grid>
        </Box>
    );
};

export default Sport;
