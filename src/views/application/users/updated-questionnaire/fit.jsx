import { Box, Typography } from '@mui/material';

import Slider from '@mui/material/Slider';
import { useEffect } from 'react';

const Fit = ({ fit, setFit, setEnabled }) => {
    const handleChange = (event, newValue) => {
        if (typeof newValue === 'number') {
            setFit(newValue);
        }
    };

    useEffect(() => {
        setEnabled(fit ?? false);
    }, [fit]);

    return (
        <Box>
            <Typography gutterBottom sx={{ textAlign: 'center', fontSize: '32px', fontWeight: '500' }}>
                {fit}
            </Typography>
            <Slider
                value={fit}
                min={1}
                step={1}
                max={8}
                onChange={handleChange}
                valueLabelDisplay="off"
                sx={{
                    '&.MuiSlider-root': {
                        color: '#cbab70'
                    }
                }}
            />
        </Box>
    );
};

export default Fit;
