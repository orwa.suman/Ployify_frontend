import { Box, Typography } from '@mui/material';

import FemaleIcon from 'assets/images/questionnaire/gender/female-icon.svg';
import MaleIcon from 'assets/images/questionnaire/gender/male-icon.svg';
import OtherGenderIcon from 'assets/images/questionnaire/gender/other-gender-icon.svg';
import { useEffect } from 'react';

const GenderCard = ({ img, text, gender, setGender }) => {
    return (
        <Box
            sx={{
                width: '100px',
                height: '90px',
                border: `1px solid ${gender === text ? '#CBAB70' : '#E7E7E7'}`,
                borderRadius: '7px',
                boxShadow: '0px 0px 8px 0px rgba(0, 0, 0, 0.13)',
                padding: '15px 25px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                cursor: 'pointer',
                transitionDuration: '300ms',
                '&:hover': {
                    transform: 'scale(1.1)'
                }
            }}
            onClick={() => setGender(text)}
        >
            <Typography sx={{ fontSize: '16px', color: '#535353', fontWeight: '500', marginBottom: '5px' }}>{text}</Typography>
            <img
                src={img}
                style={{
                    width: '30px'
                }}
            />
        </Box>
    );
};
const Gender = ({ gender, setGender, setEnabled }) => {
    useEffect(() => {
        setEnabled(gender ? true : false);
    }, [gender]);

    return (
        <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
            <GenderCard img={MaleIcon} text={'Male'} setGender={setGender} gender={gender} onClick={() => setGender(gender)} />
            <GenderCard img={FemaleIcon} text={'Female'} setGender={setGender} gender={gender} onClick={() => setGender(gender)} />
            <GenderCard img={OtherGenderIcon} text={'Other'} setGender={setGender} gender={gender} onClick={() => setGender(gender)} />
        </Box>
    );
};

export default Gender;
