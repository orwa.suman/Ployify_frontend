import { Box, Grid, Typography } from '@mui/material';

import OmnivoreIcon from 'assets/images/questionnaire/diet/omnivore-icon.svg';
import PescetarianIcon from 'assets/images/questionnaire/diet/pescetarian-icon.svg';
import VeganIcon from 'assets/images/questionnaire/diet/vegan-icon.svg';
import VegetarianIcon from 'assets/images/questionnaire/diet/vegetarian-icon.svg';
import { useEffect } from 'react';

const Diet = ({ answerList, diet, setDiet, setEnabled }) => {
    
    useEffect(() => {
        setEnabled(diet ? true : false);
    }, [diet]);

    const handleselectItem = (id) => {
        const _diet = [id]; // Create a copy of the workPlace array
        setDiet(_diet);
        // let index = diet.indexOf(id);
        // if (index > -1) {
        //     const _diet = [...diet]; // Create a copy of the workPlace array
        //     _diet.splice(index, 1);
        //     setDiet(_diet);
        // } else {
        //     setDiet((prevValues) => [...prevValues, id]);
        // }
    };

    const HanldeImageByType = (id) => {
        if (id === '6535434db88f4d080aa8593e') return OmnivoreIcon;
        if (id === '6535434db88f4d080aa85941') return PescetarianIcon;
        if (id === '6535434db88f4d080aa85940') return VeganIcon;
        if (id === '6535434db88f4d080aa8593f') return VegetarianIcon;
        else return OmnivoreIcon;
    };

    return (
        <Box>
            <Grid container spacing={2}>
                {answerList?.map((item) => (
                    <Grid item key={item.id} xs={6} onClick={() => handleselectItem(item.id)} sx={{ width: '100%' }}>
                        <Box
                            sx={{
                                height: '90px',
                                border: `1px solid ${diet && diet.length > 0 && diet.find((id) => id === item.id) ? '#CBAB70' : '#E7E7E7'}`,
                                borderRadius: '7px',
                                boxShadow: '0px 0px 8px 0px rgba(0, 0, 0, 0.13)',
                                padding: '10px 25px',
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                cursor: 'pointer',
                                transitionDuration: '300ms',
                                '&:hover': {
                                    transform: 'scale(1.1)'
                                }
                            }}
                        >
                            <Typography sx={{ color: '#535353', textAlign: 'center', mb: '10px' }}>{item.answerText}</Typography>
                            <img
                                src={HanldeImageByType(item?.id)}
                                style={{
                                    height: '40px'
                                }}
                            />
                        </Box>
                    </Grid>
                ))}
            </Grid>
        </Box>
    );
};

export default Diet;
