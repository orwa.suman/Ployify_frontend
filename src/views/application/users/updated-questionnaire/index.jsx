import { Box, Button, DialogActions, Typography } from '@mui/material';
import { forwardRef, useEffect, useReducer, useState } from 'react';
import { getUserQuestionnaire, setUserQuestionnaire } from 'services/userService';
import { useLocation, useNavigate } from 'react-router-dom';

import Age from './age';
import CloseIcon from '@mui/icons-material/Close';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import Diet from './diet';
import Equipment from './equipment';
import Fit from './fit';
import Food from './food';
import Gender from './gender';
import Height from './height';
import IconButton from '@mui/material/IconButton';
import { LOGIN } from 'store/actions';
import LinearProgress from '@mui/material/LinearProgress';
import { LoadingButton } from '@mui/lab';
import QuestionnaireCalendarIcon from 'assets/images/questionnaire/questionnaire-calendar-icon.svg';
import QuestionnaireCookingIcon from 'assets/images/questionnaire/questionnaire-cooking-icon.svg';
import QuestionnaireDumbbellCuateIcon from 'assets/images/questionnaire/questionnaire-dumbbell-exercise-cuate-icon.svg';
import QuestionnaireDumbbellIcon from 'assets/images/questionnaire/questionnaire-dumbbell-exercise-icon.svg';
import QuestionnaireEatingVarieryOffFoodIcon from 'assets/images/questionnaire/questionnaire-eating-a-variety-of-foods-icon.svg';
import QuestionnaireGenderIcon from 'assets/images/questionnaire/questionnaire-gender-icon.svg';
import QuestionnaireHeightIcon from 'assets/images/questionnaire/questionnaire-height-icon.svg';
import QuestionnaireHighSlefEsteemIcon from 'assets/images/questionnaire/questionnaire-high-self-esteem-icon.svg.svg';
import QuestionnaireIcon from 'assets/images/questionnaire/questions-cuate.svg';
import QuestionnaireSportIcon from 'assets/images/questionnaire/questionnaire-sport-icon.svg';
import QuestionnaireWeightIcon from 'assets/images/questionnaire/questionnaire-weight-icon.svg';
import QuestionnaireWorkPlaceIcon from 'assets/images/questionnaire/questionnaire-workplace-icon.svg';
import Slide from '@mui/material/Slide';
import Sport from './sport';
import TrainingExp from './trainingExp';
import Weight from './weight';
import WorkPlace from './workPlace';
import accountReducer from 'store/accountReducer';
import { getNewQuestionnaire } from 'services/QuestionnaireService';
import { loadEquipments } from 'services/TrainingService';
import { loadIngredients } from 'services/IngredientService';
import { loggedInUser } from 'contexts/JWTContext';

const initialState = {
    isLoggedIn: false,
    isInitialized: false,
    user: null,
    redirectPath: '/user/questionnaire'
};

const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const CustomLinearProgress = ({ progress }) => {
    return (
        <LinearProgress
            variant="determinate"
            value={progress}
            sx={{
                backgroundColor: '#cbab7075',
                '& .MuiLinearProgress-bar': {
                    backgroundColor: '#cbab70'
                }
            }}
        />
    );
};

export const Qustionnaire = () => {
    const [state, dispatch] = useReducer(accountReducer, initialState);
    const navigate = useNavigate();
    const location = useLocation();
    const [questionnaireProgress, setQuestionnaireProgress] = useState(20);
    const [questions, setQuestionsList] = useState([]);
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
    const [openDialog, setOpenDialog] = useState(false);
    const [currentQuestion, setCurrentQuestion] = useState({ question: '' });
    const { question } = currentQuestion || { question: '' };
    const [equipmentList, setEquipmentsList] = useState([]);
    const [ingredientsList, setIngredientsList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [oldUserAnswers, setOldUserAnswers] = useState(undefined);
    // Answers
    const [enabled, setEnabled] = useState(false);
    const [gender, setGender] = useState('');
    const [age, setAge] = useState('');
    const [height, setHeight] = useState('');
    const [weight, setWeight] = useState('');
    const [workPlace, setWorkPlace] = useState([]);
    const [sport, setSport] = useState([]);
    const [fit, setFit] = useState('');
    const [trainingExp, setTrainingExp] = useState('');
    const [food, setFood] = useState('');
    const [diet, setDiet] = useState([]);
    const [equipment, setEquipment] = useState([]);
    const [sitInCar, setSitInCar] = useState(false);

    const fetchQuestionnaire = () => {
        getNewQuestionnaire()
            .then((res) => {
                const sortedQuestions = res.data.questionnaireQADtoList.sort((a, b) => parseFloat(a.sequence) - parseFloat(b.sequence));
                setQuestionsList(sortedQuestions);
            })
            .catch((e) => {
                
            });
    };

    const fetchEquipments = () => {
        loadEquipments()
            .then((res) => {
                setEquipmentsList(res.data);
            })
            .catch((e) => {
                
            });
    };

    const fetchIngredients = () => {
        loadIngredients()
            .then((res) => {
                setIngredientsList(res.data);
            })
            .catch((e) => {
                
            });
    };

    const fetchUserOldAnswers = () => {
        getUserQuestionnaire(loggedInUser.id)
            .then((res) => {
                setOldUserAnswers(res.data);
            })
            .catch((e) => {
                
            });
    };

    const goToNextQuestion = () => {
        if (currentQuestionIndex === questions.length - 1) {
            handleUserAnswers();
            return;
        }
        if (currentQuestionIndex < questions.length - 1) {
            setCurrentQuestionIndex((currentIndex) => currentIndex + 1);
        }
    };

    const goToPrevQuestion = () => {
        setCurrentQuestionIndex((currentIndex) => currentIndex - 1);
    };

    const extractCheckedAnserws = (questionnaireQAList, questionType) => {
        let checkedList = [];
        questionnaireQAList
            ?.find((item) => item.question === questionType)
            ?.answerList?.map((item) => {
                if (item.checked) checkedList.push(item.id);
            });
        return checkedList;
    };

    useEffect(() => {
        fetchQuestionnaire();
        fetchEquipments();
        fetchIngredients();
        !!loggedInUser?.id && fetchUserOldAnswers();
    }, []);

    useEffect(() => {
        if (!oldUserAnswers) return;
        const {
            age,
            availableEquipments,
            blackListedIngredientList,
            gender,
            height,
            questionnaireQAList,
            sitInCarMoreThan1Hour,
            userFitnessGrade,
            weight
        } = oldUserAnswers;
        setAge(age);
        setEquipment(availableEquipments);
        setFood(blackListedIngredientList.map((item) => item.id));
        // setDiet(extractCheckedAnserws(questionnaireQAList, 'QUESTION_DIET'));
        setDiet([diet]);
        setTrainingExp(extractCheckedAnserws(questionnaireQAList, 'QUESTION_TRAINING_EXPERIENCE')[0]);
        setWorkPlace(extractCheckedAnserws(questionnaireQAList, 'QUESTION_WORKPLACE'));
        setSport(extractCheckedAnserws(questionnaireQAList, 'QUESTION_SPORT'));
        setHeight(height);
        let _gender = gender.toLowerCase();
        _gender = _gender.charAt(0).toUpperCase() + _gender.slice(1);
        setGender(_gender);
        setWeight(weight);
        setFit(userFitnessGrade);
        setSitInCar(sitInCarMoreThan1Hour);
    }, [oldUserAnswers]);

    useEffect(() => {
        questions.length > 0 && setCurrentQuestionIndex(0);
    }, [questions]);

    useEffect(() => {
        setOpenDialog(true);
        setCurrentQuestion(questions[currentQuestionIndex]);
        setQuestionnaireProgress(((currentQuestionIndex + 1) * 100) / questions.length);
    }, [currentQuestionIndex, questions.length]);

    const HanldeImageByType = (type) => {
        if (type === 'QUESTION_AGE') return QuestionnaireCalendarIcon;
        if (type === 'QUESTION_GENDER') return QuestionnaireGenderIcon;
        if (type === 'QUESTION_HEIGHT') return QuestionnaireHeightIcon;
        if (type === 'QUESTION_WEIGHT') return QuestionnaireWeightIcon;
        if (type === 'QUESTION_TRAINING_EXPERIENCE') return QuestionnaireDumbbellCuateIcon;
        if (type === 'QUESTION_FOOD_TOLERATE') return QuestionnaireCookingIcon;
        if (type === 'QUESTION_DIET') return QuestionnaireEatingVarieryOffFoodIcon;
        if (type === 'QUESTION_EQUIPMENT') return QuestionnaireDumbbellIcon;
        if (type === 'QUESTION_FIT') return QuestionnaireHighSlefEsteemIcon;
        if (type === 'QUESTION_WORKPLACE') return QuestionnaireWorkPlaceIcon;
        if (type === 'QUESTION_SPORT') return QuestionnaireSportIcon;
        else return QuestionnaireDumbbellIcon;
    };
    const submitUserAnswers = (answers) => {
        setLoading(true);
        setUserQuestionnaire(loggedInUser.id || location.state.userId, answers, loggedInUser.token || location.state?.token)
            .then((res) => {
                setOpenDialog(false);
                debugger;
                dispatch({
                    type: LOGIN,
                    payload: {
                        isLoggedIn: true,
                        redirectPath: '/dashboard/default'
                    }
                });
                navigate('/dashboard/default');
                setLoading(false);
            })
            .catch((e) => {
                
                setLoading(false);
            });
    };

    const handleUserAnswers = () => {
        const payload = {
            gender: gender.toUpperCase(),
            age: age,
            height: height,
            weight: weight,
            userFitnessGrade: fit,
            sitInCarMoreThan1Hour: sitInCar,
            questionnaireQAList: [
                {
                    questionnaireQAId: questions.find((item) => item.question === 'QUESTION_DIET').id,
                    answerIdsList: diet
                },
                {
                    questionnaireQAId: questions.find((item) => item.question === 'QUESTION_WORKPLACE').id,
                    answerIdsList: workPlace
                },
                {
                    questionnaireQAId: questions.find((item) => item.question === 'QUESTION_SPORT').id,
                    answerIdsList: sport
                },
                {
                    questionnaireQAId: questions.find((item) => item.question === 'QUESTION_TRAINING_EXPERIENCE').id,
                    answerIdsList: [trainingExp]
                }
            ],
            blackListedIngredientIdsList: food,
            availableEquipments: equipment
        };
        submitUserAnswers(payload);
    };

    return (
        <Box>
            {location?.state?.userId && (
                <img
                    src={QuestionnaireIcon}
                    alt="Berry"
                    style={{
                        width: 400,
                        position: 'absolute',
                        top: '50vh'
                    }}
                />
            )}
            <Dialog open={openDialog} TransitionComponent={Transition} keepMounted>
                <Box sx={{ ml: 2 }}>
                    <IconButton edge="start" color="inherit" onClick={() => setOpenDialog(false)} aria-label="close">
                        <CloseIcon />
                    </IconButton>
                </Box>
                <DialogContent sx={{ overflow: 'hidden' }}>
                    <CustomLinearProgress progress={questionnaireProgress} />
                    <Box
                        sx={{
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'column',
                            width: '420px'
                        }}
                    >
                        <img
                            src={HanldeImageByType(currentQuestion?.question)}
                            style={{
                                height: 280
                            }}
                        />
                        <Box
                            sx={{
                                display: 'flex',
                                alignItems: 'flex-start',
                                flexDirection: 'column',
                                width: '100%',
                                marginBottom: ' 20px'
                            }}
                        >
                            <Typography sx={{ fontSize: '20px', fontWeight: '500', color: 'black', marginBottom: '8px' }}>
                                {currentQuestion?.questionText}
                            </Typography>
                            <Typography sx={{ fontSize: '14px', fontWeight: '500', color: '#535353' }}>
                                {currentQuestion?.description}
                            </Typography>
                        </Box>
                        <Box
                            sx={{
                                overflowX: 'hidden',
                                overflowY: 'auto',
                                width: '100%',
                                height: '270px',
                                padding: '10px',
                                paddingBottom: '60px'
                            }}
                        >
                            {question === 'QUESTION_GENDER' && <Gender gender={gender} setGender={setGender} setEnabled={setEnabled} />}
                            {question === 'QUESTION_AGE' && <Age age={age} setAge={setAge} setEnabled={setEnabled} />}
                            {question === 'QUESTION_HEIGHT' && <Height height={height} setHeight={setHeight} setEnabled={setEnabled} />}
                            {question === 'QUESTION_WEIGHT' && <Weight weight={weight} setWeight={setWeight} setEnabled={setEnabled} />}
                            {question === 'QUESTION_WORKPLACE' && (
                                <WorkPlace
                                    workPlace={workPlace}
                                    sitInCar={sitInCar}
                                    setWorkPlace={setWorkPlace}
                                    answerList={currentQuestion.answerList}
                                    setEnabled={setEnabled}
                                    setSitInCar={setSitInCar}
                                />
                            )}
                            {question === 'QUESTION_SPORT' && (
                                <Sport sport={sport} setSport={setSport} answerList={currentQuestion.answerList} setEnabled={setEnabled} />
                            )}
                            {question === 'QUESTION_FIT' && <Fit fit={fit} setFit={setFit} setEnabled={setEnabled} />}
                            {question === 'QUESTION_TRAINING_EXPERIENCE' && (
                                <TrainingExp
                                    trainingExp={trainingExp}
                                    setTrainingExp={setTrainingExp}
                                    answerList={currentQuestion.answerList}
                                    setEnabled={setEnabled}
                                />
                            )}
                            {question === 'QUESTION_FOOD_TOLERATE' && (
                                <Food food={food} setFood={setFood} answerList={ingredientsList} setEnabled={setEnabled} />
                            )}
                            {question === 'QUESTION_DIET' && (
                                <Diet diet={diet} setDiet={setDiet} answerList={currentQuestion.answerList} setEnabled={setEnabled} />
                            )}
                            {question === 'QUESTION_EQUIPMENT' && (
                                <Equipment
                                    equipment={equipment}
                                    setEquipment={setEquipment}
                                    answerList={equipmentList}
                                    setEnabled={setEnabled}
                                />
                            )}
                        </Box>
                    </Box>
                </DialogContent>
                <DialogActions sx={{ display: 'flex', justifyContent: 'space-between', paddingInline: '30px' }}>
                    {currentQuestionIndex !== 0 && (
                        <LoadingButton
                            variant="contained"
                            onClick={goToPrevQuestion}
                            disabled={!enabled}
                            loading={loading}
                            sx={{
                                backgroundColor: '#cbab70',
                                '&:hover': {
                                    backgroundColor: '#a18759'
                                }
                            }}
                        >
                            Previous
                        </LoadingButton>
                    )}
                    <LoadingButton
                        variant="contained"
                        onClick={goToNextQuestion}
                        disabled={!enabled}
                        loading={loading}
                        sx={{
                            backgroundColor: '#cbab70',
                            '&:hover': {
                                backgroundColor: '#a18759'
                            },
                            ml: 'auto'
                        }}
                    >
                        {currentQuestionIndex === questions.length - 1 ? 'Finish' : 'Next'}
                    </LoadingButton>
                </DialogActions>
            </Dialog>
        </Box>
    );
};

export default Qustionnaire;
