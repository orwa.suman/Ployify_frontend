import * as Yup from 'yup';

import { Button, Grid, TextField } from '@mui/material';
import React, { useEffect } from 'react';
import { createQuestionnaire, getQuestionnaireById, updateQuestionnaire } from 'services/QuestionnaireService';

import Divider from '@mui/material/Divider';
import MainCard from '../../../../ui-component/cards/MainCard';
import { showMessage } from '../../../../utils/form/Communication';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import { useParams } from 'react-router-dom';

function QuestionnaireForm() {
    const { id } = useParams();
    const [quiz, setQuiz] = React.useState([{ label: 'How do you do?', answers: [{ label: '', definedTags: [] }] }]);
    const dispatch = useDispatch();

    useEffect(() => {
        getQuestionnaireById(id)
            .then((res) => res.data)
            .then((question) => {
                setQuiz(mapQuestionsForForm([question], []));
            })
            .catch((e) => {});
    }, []);

    const addQuestion = () => {
        setQuiz([...quiz, { label: '', answers: [{ label: '', definedTags: [] }] }]);
    };

    const updateQuestion = (questionIndex, value) => {
        const state = [...quiz];
        state[questionIndex].label = value;
        setQuiz(state);
    };

    const deleteQuestion = (index) => {
        setQuiz([...quiz].filter((_value, questionIndex) => index !== questionIndex));
    };

    const addAnswer = (index) => {
        const state = [...quiz];
        state[index].answers = [...state[index].answers, { label: '', definedTags: [] }];
        setQuiz(state);
    };

    const updateAnswer = (questionIndex, answerIndex, value) => {
        const state = [...quiz];
        state[questionIndex].answers[answerIndex].label = value;
        setQuiz(state);
    };

    const toggleCorrectAnswer = (questionIndex, answerIndex) => {
        const state = [...quiz];
        state[questionIndex].answers[answerIndex].correct = !state[questionIndex].answers[answerIndex].correct;
        setQuiz(state);
    };

    const deleteAnswer = (questionIndex, answerIndex) => {
        const state = [...quiz];
        state[questionIndex].answers = [...quiz][questionIndex].answers.filter((_value, index) => index !== answerIndex);
        setQuiz(state);
    };

    const addTag = (questionIndex, answerIndex) => {
        const state = [...quiz];
        state[questionIndex].answers[answerIndex].definedTags.push({ name: '' });
        setQuiz(state);
    };

    const updateTag = (questionIndex, answerIndex, tagIndex, value) => {
        const state = [...quiz];
        state[questionIndex].answers[answerIndex].definedTags[tagIndex] = { name: value };
        setQuiz(state);
    };

    const quizHasErrors = () => {
        let hasError = false;
        const state = [...quiz];

        state.forEach((question) => {
            question.error = false;
            if (!question.label || question.label.length < 5) {
                question.error = true;
                hasError = true;
            }
            question.answers.forEach((answer) => {
                answer.error = false;
                if (!answer.label || answer.label.length === 0) {
                    answer.error = true;
                    hasError = true;
                }
            });
        });

        setQuiz(state);

        return hasError;
    };

    const mapQuestionsForForm = (quizResponse) => {
        const formData = quizResponse.map((question) => ({
            label: question.question,
            id: question.id,
            answers: question.answerList.map((answer) => ({
                label: answer.answer,
                definedTags: answer.definedTags.map((tag) => ({ id: tag.id, name: tag.name }))
            }))
        }));
        
        return formData;
    };

    const mapQuestionsForSubmit = () => {
        debugger;
        const questions = quiz.map((question) => ({
            question: question.label,
            id: question.id,
            answerList: question.answers.map((answer) => ({
                answer: answer.label,
                // checked: false,
                checked: answer.correct, // backend always returns false even when i send true value
                definedTags: answer.definedTags.filter((tag) => !!tag.name)
            }))
        }));

        
        return questions[0];
    };

    const submitForm = (event) => {
        if (!quizHasErrors()) {
            formik.validateForm().then((errors) => {
                if (errors && Object.keys(errors).length > 0) {
                    dispatch(showMessage('Validation Failed!'));
                } else {
                    formik.handleSubmit(event);
                }
            });
        } else {
            dispatch(showMessage('Validation Failed!'));
        }
    };

    const formik = useFormik({
        initialValues: {},
        validtionSchema: {},
        onSubmit: () => {
            quiz.forEach((question, questionIndex) => {
                updateQuestionnaire(mapQuestionsForSubmit())
                    .then(() => dispatch(showMessage('Successfully submitted', 'success')))
                    .catch((e) => {
                        dispatch(showMessage(e?.response?.data?.message));
                    });
            });
        }
    });

    return (
        <Grid>
            <form onSubmit={submitForm}>
                <MainCard title="Questionnaire / Edit Question">
                    {quiz &&
                        quiz.length &&
                        quiz.map((question, questionIndex) => (
                            <div key={questionIndex}>
                                <Grid item sx={{ marginTop: '6px' }}>
                                    <TextField
                                        fullWidth
                                        id="question"
                                        label="question"
                                        value={question.label}
                                        onChange={(event) => updateQuestion(questionIndex, event.target.value)}
                                        error={question.error}
                                    />
                                </Grid>

                                <Grid sx={{ display: 'inline' }}>
                                    {question.answers.map((answer, answerIndex) => (
                                        <div key={answerIndex}>
                                            <Grid item xs={12} sx={{ margin: '12px 0' }}>
                                                <TextField
                                                    sx={{ width: '50%' }}
                                                    id="answer"
                                                    label="answer"
                                                    placeholder="enter an answer"
                                                    value={answer.label}
                                                    onChange={(event) => updateAnswer(questionIndex, answerIndex, event.target.value)}
                                                    error={answer.error}
                                                />

                                                <Button
                                                    variant="contained"
                                                    color={!answer.correct ? 'error' : 'primary'}
                                                    onClick={() => toggleCorrectAnswer(questionIndex, answerIndex)}
                                                    sx={{ margin: '5px', width: '130px' }}
                                                >
                                                    {!answer.correct ? 'Not correct' : 'Correct answer'}
                                                </Button>
                                                <Button
                                                    variant="contained"
                                                    color="error"
                                                    onClick={() => deleteAnswer(questionIndex, answerIndex)}
                                                    sx={{ margin: '5px', width: '130px' }}
                                                >
                                                    delete answer
                                                </Button>

                                                <Button
                                                    variant="contained"
                                                    onClick={() => addTag(questionIndex, answerIndex)}
                                                    sx={{ margin: '5px' }}
                                                >
                                                    + tag
                                                </Button>
                                            </Grid>

                                            <Grid item xs={12}>
                                                {((answer.definedTags && answer.definedTags.length && answer.definedTags) || []).map(
                                                    (tag, tagIndex) => (
                                                        <TextField
                                                            key={tagIndex}
                                                            id="tag"
                                                            label="tag"
                                                            placeholder="enter a tag"
                                                            value={tag.name}
                                                            sx={{ margin: '0 5px 10px 0' }}
                                                            onChange={(event) =>
                                                                updateTag(questionIndex, answerIndex, tagIndex, event.target.value)
                                                            }
                                                        />
                                                    )
                                                )}
                                            </Grid>
                                        </div>
                                    ))}

                                    <Button variant="contained" onClick={() => addAnswer(questionIndex)} sx={{ margin: '5px' }}>
                                        + answer
                                    </Button>

                                    <Divider sx={{ margin: '16px 0 20px' }} />
                                </Grid>
                            </div>
                        ))}
                </MainCard>

                <br />
                <Grid>
                    <Button variant="contained" onClick={submitForm}>
                        Submit
                    </Button>
                </Grid>
            </form>
        </Grid>
    );
}

export default QuestionnaireForm;
