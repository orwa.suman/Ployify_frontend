import 'react-datetime-picker/dist/DateTimePicker.css';
import 'react-calendar/dist/Calendar.css';

import { ArrowBackIos, ArrowBackIosNew, ArrowForward, ArrowForwardIos } from '@mui/icons-material';
import { Box, Button, Divider, Grid, TextField } from '@mui/material';
import { CardContent, LinearProgress, Slider, Typography } from '@material-ui/core';
import { getUserQuestionnaire, setUserQuestionnaire } from 'services/userService';
import { useEffect, useState } from 'react';

import Card from '@mui/material/Card';
import DateTimePicker from 'react-datetime-picker';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import MainCard from 'ui-component/cards/MainCard';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import Picker from 'react-scrollable-picker';
import React from 'react';
import { getQuestionnaire } from 'services/QuestionnaireService';
import { loggedInUser } from 'contexts/JWTContext';
import { makeStyles } from '@mui/styles';
import { openDrawer } from 'store/slices/menu';
import { showMessage } from '../../../../utils/form/Communication';
import { useDispatch } from 'store';
import { useTheme } from '@mui/material/styles';

const CustomShadowBox = ({ shadow, label, color }) => {
    const theme = useTheme();

    return (
        <Card sx={{ mb: 3, boxShadow: shadow }}>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    py: 3,
                    bgcolor: color,
                    fontWeight: 'bold',
                    color: theme.palette.background.default
                }}
            >
                {label && <Box sx={{ color: 'inherit' }}>{label}</Box>}
            </Box>
        </Card>
    );
};
const useStyles = makeStyles((theme) => ({
    glassCard: {
        position: 'relative',
        background:
            'linear-gradient(145deg, rgba(255,255,255,0.3), rgba(255,255,255,0) 70%), linear-gradient(45deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0) 70%)',
        backdropFilter: 'blur(10px)',
        borderRadius: '10px',
        boxShadow:
            'inset 2px 2px 4px rgba(255, 255, 255, 0.2), inset -2px -2px 4px rgba(255, 255, 255, 0.2), 2px 2px 6px rgba(0, 0, 0, 0.2), -2px -2px 6px rgba(0, 0, 0, 0.2)',
        padding: theme.spacing(3),
        minHeight: '450px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        transition: 'box-shadow 0.3s ease-in-out',
        '&:hover': {
            boxShadow:
                'inset 2px 2px 4px rgba(255, 255, 255, 0.4), inset -2px -2px 4px rgba(255, 255, 255, 0.4), 4px 4px 12px rgba(0, 0, 0, 0.3), -4px -4px 12px rgba(0, 0, 0, 0.3)'
        },
        width: '500px',
        margin: 'auto',
        marginTop: '50px'
    },
    title: {
        color: '#fff',
        fontWeight: 'bold',
        marginBottom: theme.spacing(2)
    },
    subtitle: {
        color: '#fff',
        textAlign: 'center'
    },
    CardContent: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        width: '420px'
    },
    question: {
        color: '#fff',
        fontWeight: '10px',
        fontSize: '20px'
    }
}));

export default function Questionnaire() {
    const theme = useTheme();
    const dispatch = useDispatch();
    const [data, setData] = useState({ questions: [] });
    const [currentQuestion, setCurrentQuestion] = React.useState(0);
    const [showScore, setShowScore] = React.useState(false);
    const [progress, setProgress] = useState(0);

    const progressBar = ((currentQuestion + 1) / data?.questions.length) * 100;

    const mapQuestionsForForm = (questionnaire, userQAs = []) =>
        questionnaire.map((question) => ({
            id: question.id,
            question: question.question,
            questionDesign: question.questionDesign,
            answers: question.answerList.map((answer, answerIndex) => ({
                label: answer.answer,
                checked: ((userQAs.find((userQA) => userQA.id === question.id) ?? { answerList: [] }).answerList[answerIndex] || {})
                    .checked,
                definedTags: answer.definedTags?.map((tag) => ({ id: tag.id, name: tag.name }))
            }))
        }));

    const mapDataForSubmit = () => {
        const questions = [...data.questions].map((question) => ({
            id: question.id,
            question: question.question,
            answerList: question.answers.map((answer) => ({
                answer: answer.label,
                checked: answer.checked,
                definedTags: answer.definedTags?.filter((tag) => !!tag.name)
            }))
        }));
        return { questionnaireQAList: questions };
    };

    const getUserAnswers = (questionnaire) => {
        getUserQuestionnaire(loggedInUser.id)
            .then((res) => res.data)
            .then((userAnswers) => {
                setData({
                    questions: mapQuestionsForForm(
                        questionnaire.questionnaireQAList,
                        userAnswers?.questionnaireQAList?.length ? userAnswers.questionnaireQAList : []
                    )
                });
            })
            .catch((e) => {
                if (e?.response?.status === 417) {
                    setData({
                        questions: mapQuestionsForForm(questionnaire.questionnaireQAList)
                    });
                } else {
                    dispatch(showMessage(e?.response?.data?.message));
                }
            });
    };

    useEffect(() => {
        dispatch(openDrawer(false));

        getQuestionnaire()
            .then((res) => getUserAnswers(res.data))
            .then((questionnaire) => {
                
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);
    const [parentState, setParentState] = useState({});

    const nextQuestion = () => {
        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < data.questions.length) {
            setCurrentQuestion(nextQuestion);
            setProgress(((nextQuestion + 1) / data.questions.length) * 100);
        } else {
            setShowScore(true);
            setParentState({ valueGroups, valueAgeGroups, inputDateValue, selectedOption });
            setUserQuestionnaire(loggedInUser.id, mapDataForSubmit());
        }
    };

    const previousQuestion = () => {
        const previousQuestion = currentQuestion - 1;
        if (previousQuestion >= 0) {
            setCurrentQuestion(previousQuestion);
            const newProgress = ((previousQuestion + 1) / data.questions.length) * 100;
            setProgress(newProgress);
        }
    };

    const unselectOption = (questionIndex, answerIndex) => {
        const updatedStateForQuiz = { ...data };
        updatedStateForQuiz.questions[questionIndex].answers[answerIndex].checked = false;
        setData(updatedStateForQuiz);
    };

    const [selectedOption, setSelectedOption] = useState(null);

    const selectOption = (questionIndex, answerIndex) => {
        if (!data.questions[questionIndex].answers[answerIndex].checked) {
            const updatedStateForQuiz = { ...data };
            updatedStateForQuiz.questions[questionIndex].answers[answerIndex].checked = true;
            setData(updatedStateForQuiz);
            setSelectedOption(answerIndex);
        } else {
            unselectOption(questionIndex, answerIndex);
        }
    };

    const [userResponses, setUserResponses] = useState({
        questionnaireQAList: [],
        userTagList: []
    });

    const handleTagChange = (tag) => {
        setUserResponses((prevUserResponses) => ({
            ...prevUserResponses,
            userTagList: [...prevUserResponses.userTagList, tag]
        }));
    };

    const classes = useStyles();
    const [startQuiz, setStartQuiz] = useState(false);
    const isFirstQuestion = currentQuestion === 0;

    const [valueGroups, setValueGroups] = useState({
        weight: '30'
    });
    const [optionGroups, setOptionGroups] = useState({
        weight: []
    });

    for (let i = 30; i <= 250; i++) {
        optionGroups?.weight?.push({ value: i.toString() + ' kg', label: i.toString() + ' kg' });
    }

    const [valueAgeGroups, setValueAgeGroups] = useState({
        age: '10'
    });
    const [optionAgeGroups, setOptionAgeGroups] = useState({
        age: []
    });

    for (let i = 10; i <= 100; i++) {
        optionAgeGroups?.age?.push({ value: i.toString() + 'yr', label: i.toString() + ' yr' });
    }

    const handleWeightChange = (name, value, questionIndex, answerIndex) => {
        setValueGroups((prevValueGroups) => ({
            ...prevValueGroups,
            [name]: value
        }));

        if (!data?.questions[questionIndex]?.answers[answerIndex]?.checked) {
            const updatedStateForQuiz = { ...data };
            updatedStateForQuiz.questions[questionIndex].answers[answerIndex].checked = true;
            updatedStateForQuiz.questions[questionIndex].answers[answerIndex].answer = valueGroups.weight;
            setData(updatedStateForQuiz);
        } else {
            const updatedStateForQuiz = { ...data };
            updatedStateForQuiz.questions[questionIndex].answers[answerIndex].checked = false;
            setData(updatedStateForQuiz);
        }
    };

    

    const handleAgeChange = (name, value, questionIndex, answerIndex) => {
        setValueAgeGroups((prevValueAgeGroups) => ({
            ...prevValueAgeGroups,
            [name]: value
        }));

        if (
            !data?.questions[questionIndex]?.answers?.[answerIndex]?.checked ||
            data?.questions[questionIndex]?.answers?.[answerIndex]?.checked === null
        ) {
            setData((prevData) => {
                const updatedStateForQuiz = { ...prevData };
                const answers = updatedStateForQuiz.questions[questionIndex]?.answers;
                if (answers) {
                    answers[answerIndex] = answers[answerIndex] || {};
                    answers[answerIndex].checked = true;
                    answers[answerIndex].answer = valueAgeGroups.age;
                }
                return updatedStateForQuiz;
            });
        } else {
            setData((prevData) => {
                const updatedStateForQuiz = { ...prevData };
                const answers = updatedStateForQuiz.questions[questionIndex]?.answers;
                if (answers) {
                    answers[answerIndex] = answers[answerIndex] || {};
                    answers[answerIndex].checked = false;
                }
                return updatedStateForQuiz;
            });
        }
    };

    const [inputDateValue, setInputDateValue] = useState('');
    const handleInputDateChange = (event, questionIndex, answerIndex) => {
        const inputValue = event?.target?.value;
        setInputDateValue(inputValue);

        if (
            !data ||
            !data.questions ||
            !data.questions[questionIndex] ||
            !data.questions[questionIndex].answers ||
            !data.questions[questionIndex].answers[answerIndex]
        ) {
            return;
        }

        const { questions } = data;
        const { answers } = questions[questionIndex];

        if (!answers[answerIndex].checked) {
            const updatedAnswers = [...answers];
            updatedAnswers[answerIndex] = { ...updatedAnswers[answerIndex], checked: true, answer: inputValue };

            const updatedQuestions = [...questions];
            updatedQuestions[questionIndex] = { ...questions[questionIndex], answers: updatedAnswers };

            const updatedData = { ...data, questions: updatedQuestions };
            setData(updatedData);
        } else {
            const updatedAnswers = [...answers];
            updatedAnswers[answerIndex] = { ...updatedAnswers[answerIndex], checked: false };

            const updatedQuestions = [...questions];
            updatedQuestions[questionIndex] = { ...questions[questionIndex], answers: updatedAnswers };

            const updatedData = { ...data, questions: updatedQuestions };
            setData(updatedData);
        }
    };

    

    const [sliderValue, setSliderValue] = useState(5);

    const handleSliderChange = (event, newValue, questionIndex, answerIndex) => {
        setSliderValue(newValue);
        if (!data?.questions[questionIndex]?.answers[answerIndex]?.checked) {
            const updatedStateForQuiz = { ...data };
            updatedStateForQuiz.questions[questionIndex].answers[answerIndex].checked = true;
            updatedStateForQuiz.questions[questionIndex].answers[answerIndex].answer = sliderValue;
            setData(updatedStateForQuiz);
        } else {
            const updatedStateForQuiz = { ...data };
            updatedStateForQuiz.questions[questionIndex].answers[answerIndex].checked = false;
            setData(updatedStateForQuiz);
        }
    };
    const weightIndex = data?.questions[currentQuestion]?.answers.findIndex((answer) => answer.label === 'weight');
    const ageIndex = data?.questions[currentQuestion]?.answers.findIndex((answer) => answer.label === 'age');
    const fitIndex = data?.questions[currentQuestion]?.answers.findIndex((answer) => answer.label === 'fit');
    const dob = data?.questions[currentQuestion]?.answers.findIndex((answer) => answer.label === 'birth');

    return (
        <div className="app">
            <>
                <MainCard title={data.title} sx={{ padding: 0, margin: '-40px -44px -40px -66px' }}>
                    <Card
                        sx={{
                            justifyContent: 'center',
                            padding: '0',
                            backgroundImage: 'url(/images/questionnaire.jpg)',
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                            minHeight: '800px',
                            position: 'relative',
                            '&:before': {
                                content: '""',
                                position: 'absolute',
                                width: '100%',
                                height: '100%',
                                background: 'rgba(0,0,0, 0.3)',
                                backdropFilter: 'blur(15px)' /* add a blur effect to the background image */,
                                zIndex: '-1' /* set the pseudo-element behind the main content */
                            }
                        }}
                    >
                        <div>
                            {showScore ? (
                                <Card className={classes.glassCard}>
                                    <CardContent className={classes.CardContent}>
                                        <Typography variant="h5" className={classes.title} style={{ textAlign: 'center' }}>
                                            Warm applause for filling out the questionnaire! 👋
                                        </Typography>
                                        <Typography variant="subtitle1" className={classes.subtitle} style={{ marginTop: '20px' }}>
                                            You can come anytime and change your answers.
                                        </Typography>
                                    </CardContent>
                                </Card>
                            ) : (
                                <Card className={classes.glassCard}>
                                    {!startQuiz && (
                                        <CardContent className={classes.CardContent}>
                                            <Typography variant="h5" className={classes.title}>
                                                Hi, {loggedInUser?.firstName}👋
                                            </Typography>
                                            <Typography variant="subtitle1" className={classes.subtitle}>
                                                We will take a short questionnaire to get to know you better.{' '}
                                                <span style={{ fontStyle: 'italic' }}>It will take few minutes.</span>
                                            </Typography>
                                            <Button
                                                variant="contained"
                                                style={{ backgroundColor: '#CBAB70', marginTop: '10px' }}
                                                onClick={() => setStartQuiz(true)}
                                            >
                                                Start Questionnaire
                                            </Button>
                                        </CardContent>
                                    )}
                                    {startQuiz && (
                                        <CardContent style={{ display: 'block', width: '420px' }}>
                                            {data.questions[currentQuestion] && (
                                                <Typography variant="h5" className={classes.question}>
                                                    {data.questions[currentQuestion]?.question}
                                                </Typography>
                                            )}
                                            <LinearProgress variant="determinate" value={progressBar} style={{ marginTop: '15px' }} />
                                            {data?.questions[currentQuestion]?.questionDesign === 'DESIGN1' && weightIndex >= 0 ? (
                                                <Picker
                                                    optionGroups={optionGroups}
                                                    valueGroups={valueGroups}
                                                    onChange={(name, value) =>
                                                        handleWeightChange(name, value, currentQuestion, weightIndex)
                                                    }
                                                    selectedColor="#ffffff"
                                                />
                                            ) : data?.questions[currentQuestion]?.questionDesign === 'DESIGN1' && ageIndex >= 0 ? (
                                                <Picker
                                                    optionGroups={optionAgeGroups}
                                                    valueGroups={valueAgeGroups}
                                                    onChange={(name, value) =>
                                                        handleAgeChange(name, value, currentQuestion, currentQuestion, ageIndex)
                                                    }
                                                    selectedColor="#ffffff"
                                                />
                                            ) : data?.questions[currentQuestion]?.questionDesign === 'DESIGN2' && dob >= 0 ? (
                                                <div style={{ marginTop: '20px', width: '100%', marginBottom: '20px' }}>
                                                    <TextField
                                                        label="Enter your Date of Birth"
                                                        variant="outlined"
                                                        value={inputDateValue}
                                                        onChange={(e) => handleInputDateChange(currentQuestion, dob)}
                                                        style={{ width: '100%' }}
                                                    />
                                                </div>
                                            ) : data?.questions[currentQuestion]?.questionDesign === 'DESIGN2' && fitIndex >= 0 ? (
                                                <div style={{ marginTop: '20px', width: '100%', marginBottom: '20px' }}>
                                                    <p style={{ color: 'white' }}>Selected value: {sliderValue}</p>
                                                    <Slider
                                                        value={sliderValue}
                                                        onChange={(event, newValue) =>
                                                            handleSliderChange(event, newValue, currentQuestion, fitIndex)
                                                        }
                                                        aria-labelledby="slider-from-1-to-10"
                                                        step={1}
                                                        marks
                                                        min={1}
                                                        max={10}
                                                    />
                                                </div>
                                            ) : (
                                                <div>
                                                    <List style={{ padding: '0 16px', margin: '20px 0' }}>
                                                        {data.questions[currentQuestion]?.answers.map((answer, answerIndex) => (
                                                            <ListItem disablePadding key={answerIndex}>
                                                                <ListItemButton
                                                                    sx={{
                                                                        background: answer.checked ? '#CBAB70' : 'rgba(255,255,255, 0.8)',
                                                                        marginBottom: '8px',
                                                                        '&:hover': {
                                                                            background: 'white'
                                                                        },
                                                                        borderRadius: '10px',
                                                                        width: '400px'
                                                                    }}
                                                                    onClick={() => selectOption(currentQuestion, answerIndex)}
                                                                >
                                                                    <p style={{ color: 'black', fontSize: '14px' }}>{answer.label}</p>
                                                                </ListItemButton>
                                                            </ListItem>
                                                        ))}
                                                    </List>
                                                </div>
                                            )}

                                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                                <Button
                                                    onClick={() => previousQuestion()}
                                                    variant="contained"
                                                    style={{
                                                        backgroundColor: 'transparent',
                                                        boxShadow: '0px 1px 3px rgba(0, 0, 0, 0.2)'
                                                    }}
                                                >
                                                    <ArrowBackIos />
                                                </Button>
                                                <Button
                                                    onClick={() => nextQuestion()}
                                                    variant="contained"
                                                    style={{
                                                        backgroundColor: 'transparent',
                                                        boxShadow: '0px 1px 3px rgba(0, 0, 0, 0.2)'
                                                    }}
                                                >
                                                    <ArrowForwardIos />
                                                </Button>
                                            </div>
                                        </CardContent>
                                    )}
                                </Card>
                            )}
                        </div>
                        {/*    {showScore ? (
                            <div style={{ position: 'relative' }}>
                                <h1 style={{ textAlign: 'center', margin: '150px auto 30px' }}>
                                    Warm applause for filling out the questionnaire!
                                </h1>
                                <p style={{ textAlign: 'center', margin: '0 auto 50px' }}>
                                    You can come back any time and change your answers.
                                </p>
                            </div>
                        ) : (
                            <Grid container sx={{ justifyContent: 'center', marginTop: '80px', position: 'relative' }}>
                                <Grid item xs={12} md={6} lg={5} sx={{ minHeight: window.innerHeight + 'px' }}>
                                    <Grid container spacing={2} sx={{ paddingTop: '30px', justifyContent: 'center' }}>
                                        <Grid item xs={12}>
                                            {data.questions[currentQuestion] ? (
                                                <Grid item xs={12}>
                                                    <CustomShadowBox
                                                        color={theme.palette.primary.main}
                                                        shadow={theme.customShadows.primary}
                                                        label={data.questions[currentQuestion]?.question}
                                                    />
                                                </Grid>
                                            ) : (
                                                ''
                                            )}
                                            <Divider></Divider>
                                            <nav aria-label="primary mailbox folders">
                                                <List style={{ padding: '0 16px', margin: '20px 0' }}>
                                                    {data.questions[currentQuestion]?.answers.map((answer, answerIndex) => (
                                                        <ListItem disablePadding key={answerIndex}>
                                                            <ListItemButton
                                                                sx={{
                                                                    background: answer.checked
                                                                        ? theme.palette.success.dark
                                                                        : 'rgba(255,255,255, 0.8)',
                                                                    marginBottom: '8px',
                                                                    '&:hover': {
                                                                        background: answer.checked ? theme.palette.success.dark : 'white'
                                                                    }
                                                                }}
                                                                onClick={() => selectOption(currentQuestion, answerIndex)}
                                                            >
                                                                <p style={{ color: 'black', fontSize: '14px' }}>{answer.label}</p>
                                                            </ListItemButton>
                                                        </ListItem>
                                                    ))}
                                                </List>
                                            </nav>

                                            <Divider></Divider>

                                            <Button
                                                sx={{
                                                    margin: '12px 0',
                                                    height: '60px',
                                                    borderRadius: '24px',
                                                    color:
                                                        theme.palette.mode === 'dark'
                                                            ? theme.palette.dark.light
                                                            : theme.palette.secondary.dark,
                                                    background:
                                                        theme.palette.mode === 'dark'
                                                            ? theme.palette.dark.main
                                                            : theme.palette.secondary.light,
                                                    '&:hover': {
                                                        background: '#ccad70',
                                                        color: 'black'
                                                    }
                                                }}
                                                color="secondary"
                                                onClick={() => nextQuestion()}
                                                fullWidth
                                                startIcon={<NavigateNextIcon />}
                                            >
                                                Next Question
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                                            )} */}
                    </Card>
                </MainCard>
            </>
        </div>
    );
}
