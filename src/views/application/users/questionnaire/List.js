import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../../assets/scss/style.css';
import MainCard from 'ui-component/cards/MainCard';

import { useTheme } from '@mui/material/styles';
import ChatBubbleTwoToneIcon from '@mui/icons-material/ChatBubbleTwoTone';
import BlockTwoToneIcon from '@mui/icons-material/BlockTwoTone';
import { gridSpacing } from 'store/constant';

// assets
import { IconSearch } from '@tabler/icons';

import {
    Grid,
    IconButton,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
    Typography,
    OutlinedInput,
    InputAdornment,
    Button,
    Link
} from '@mui/material';
import { HiPencil } from 'react-icons/hi';
import { deleteQuestionnaire, getQuestionnaire } from 'services/QuestionnaireService';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { showMessage } from 'utils/form/Communication';

function List() {
    const theme = useTheme();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    let [data, setData] = React.useState([]);

    React.useEffect(() => {
        getQuestionnaire()
            .then((res) => res.data)
            .then((data) => setData((data?.questionnaireQAList ?? []).reverse()))
            .catch((e) => {
                
            });
    }, []);

    const deleteQuestion = (id) => {
        deleteQuestionnaire(id)
            .then((res) => res.data)
            .then(() => dispatch(showMessage('Successfully deleted', 'success')) 
            )
            .catch((e) => {
                
            });
    }

    return (
        <MainCard
            title={
                <Grid container alignItems="center" spacing={gridSpacing}>
                    <Grid item>
                        <Typography variant="h3">List</Typography>
                    </Grid>
                    <Grid item>
                        <Button variant="contained" onClick={() => navigate('/dashboard/user/questionnaire/form')} size="20px">
                            Add new Question
                        </Button>
                    </Grid>
                </Grid>
            }
            content={false}
        >
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell scope="col">Question</TableCell>
                            <TableCell scope="col">number of answers</TableCell>
                            <TableCell align="center" sx={{ pr: 3 }}>
                                Actions
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data &&
                            data.map((row, index) => (
                                <TableRow hover key={index}>
                                    <TableCell>{row.question}</TableCell>
                                    <TableCell>{row.answerList.length}</TableCell>
                                    <TableCell align="center" sx={{ pr: 3 }}>
                                        <Stack direction="row" justifyContent="center" alignItems="center">
                                            <Tooltip placement="top" title="Edit">
                                                <IconButton
                                                    color="primary"
                                                    aria-label="edit"
                                                    size="large"
                                                    onClick={() => navigate('/dashboard/user/questionnaire/form/' + row.id)}
                                                >
                                                    <HiPencil sx={{ fontSize: '1.1rem' }} />
                                                </IconButton>
                                            </Tooltip>

                                            <Tooltip placement="top" title="Delete" onClick={() => {}}>
                                                <IconButton
                                                    color="primary"
                                                    sx={{
                                                        color: theme.palette.orange.dark,
                                                        borderColor: theme.palette.orange.main,
                                                        '&:hover ': { background: theme.palette.orange.light }
                                                    }}
                                                    size="large"
                                                    onClick={() => deleteQuestion(row.id) }
                                                >
                                                    <BlockTwoToneIcon sx={{ fontSize: '1.1rem' }} />
                                                </IconButton>
                                            </Tooltip>
                                        </Stack>
                                    </TableCell>
                                </TableRow>
                            ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </MainCard>
    );
}

export default List;
