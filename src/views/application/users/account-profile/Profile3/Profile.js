// material-ui
import { Avatar, Button, Grid, Stack, TextField, Typography } from '@mui/material';

// project imports
import SubCard from 'ui-component/cards/SubCard';
import AnimateButton from 'ui-component/extended/AnimateButton';
import { gridSpacing } from 'store/constant';

// assets
import Avatar1 from 'assets/images/users/avatar-1.png';
import { loggedInUser } from '../../../../../contexts/JWTContext';
import { FormattedMessage } from 'react-intl';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import FileUpload from 'views/forms/components/FileUpload';
import { useDispatch } from 'store';
import { showMessage } from 'utils/form/Communication';

// ==============================|| PROFILE 3 - PROFILE ||============================== //

const Profile = () => {
    const user = loggedInUser;
    
    const dispatch = useDispatch();
    const validationSchema = Yup.object({
        firstName: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(50, <FormattedMessage id="Input Max Length" values={{ length: 50 }} />)
            .required(<FormattedMessage id="Required" />),
        lastName: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(50, <FormattedMessage id="Input Max Length" values={{ length: 50 }} />)
            .required(<FormattedMessage id="Required" />),
        email: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(150, <FormattedMessage id="Input Max Length" values={{ length: 150 }} />)
            .required(<FormattedMessage id="Required" />)
    });
    const formik = useFormik({
        initialValues: {
            picture: undefined,
            firstName: loggedInUser.firstName,
            lastName: loggedInUser.lastName,
            email: loggedInUser.email
        },
        validationSchema,
        onSubmit: () => {
            /* 
            updateUser(formik.values, id)
                .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                })
            */
        }
    });

    const submitForm = (event) => {
        formik.validateForm().then((errors) => {
            if (errors && Object.keys(errors).length > 0) {
                dispatch(showMessage(<FormattedMessage id="Validation Fail" />));
            }
            formik.handleSubmit(event);
        });
    };

    const handlePicture = (data) => formik.setValues({ ...formik.values, picture: data });

    return (
        <form onSubmit={submitForm}>
            <Grid container spacing={gridSpacing}>
                <Grid item sm={6} md={4}>
                    <SubCard title={user.firstName + ' ' + user.lastName} contentSX={{ textAlign: 'center' }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <div style={{ justifyContent: 'center', display: 'flex' }}>
                                    {user.picture || formik.values.picture ? (
                                        <img
                                            style={{ width: '100%', margin: '0 auto' }}
                                            src={user.picture || formik.values.picture}
                                            alt={user.firstName + ' ' + user.lastName}
                                        ></img>
                                    ) : (
                                        <Avatar alt="User 1" src={Avatar1} />
                                    )}
                                </div>
                            </Grid>
                            <Grid item xs={12}>
                                <Grid item xs={12} className="careployee-form__block">
                                    <FileUpload
                                        allowDisplay={false}
                                        label={<FormattedMessage id="Upload/Change Your Profile Image" />}
                                        onChange={handlePicture}
                                    ></FileUpload>
                                </Grid>
                            </Grid>
                        </Grid>
                    </SubCard>
                </Grid>
                <Grid item sm={6} md={8}>
                    <SubCard title="Edit Account Details">
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12} md={6}>
                                <TextField
                                    id="firstName"
                                    fullWidth
                                    label={<FormattedMessage id="First Name" />}
                                    value={formik.values.firstName}
                                    error={formik.touched.firstName && Boolean(formik.errors.firstName)}
                                    helperText={formik.touched.firstName && formik.errors.firstName}
                                />
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <TextField
                                    id="lastName"
                                    fullWidth
                                    label={<FormattedMessage id="Last Name" />}
                                    onChange={formik.handleChange}
                                    value={formik.values.lastName}
                                    error={formik.touched.lastName && Boolean(formik.errors.lastName)}
                                    helperText={formik.touched.lastName && formik.errors.lastName}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="email"
                                    fullWidth
                                    label="Email address"
                                    value={formik.values.email ?? ''}
                                    onChange={formik.handleChange}
                                    error={formik.touched.email && Boolean(formik.errors.email)}
                                    helperText={formik.touched.email && formik.errors.email}
                                />
                            </Grid>
                            <Grid item md={6} xs={12}>
                                <TextField
                                    id="companyName"
                                    fullWidth
                                    disabled
                                    label={<FormattedMessage id="Company" />}
                                    defaultValue={!!user.company ? user.company.companyName ?? '' : ''}
                                />
                            </Grid>
                            <Grid item md={6} xs={12}>
                                <TextField
                                    id="companyAddress"
                                    fullWidth
                                    disabled
                                    label={<FormattedMessage id="Address" />}
                                    defaultValue={!!user.company ? user.company.address ?? '' : ''}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Stack direction="row">
                                    <AnimateButton>
                                        <Button onClick={submitForm} variant="contained">
                                            
                                            <FormattedMessage id="Change Details" />
                                        </Button>
                                    </AnimateButton>
                                </Stack>
                            </Grid>
                        </Grid>
                    </SubCard>
                </Grid>
            </Grid>
        </form>
    );
};

export default Profile;
