// material-ui
import {useTheme} from '@mui/material/styles';
import {
    Grid,
} from '@mui/material';

// project imports
import BillCard from 'ui-component/cards/BillCard';
import {gridSpacing} from 'store/constant';

// assets
import {loggedInUser} from "../../../../../contexts/JWTContext";
import Rewards from "../../../Rewards/Rewards";

// table data
function createData(tid, date, amount, badgeText, badgeType) {
    return {tid, date, amount, badgeText, badgeType};
}

// ==============================|| PROFILE 3 - BILLING ||============================== //

const Billing = () => {
    const theme = useTheme();
    const user = loggedInUser;
    const userInfo = user.userInfo ?? {};

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} sm={6} md={4}>
                <BillCard
                    // primary="Gesammelte Punkte"
                    secondary={userInfo.points ?? 0 + " Punkten"}
                    link="Ein Training Plan jetzt starten!"
                    color={theme.palette.orange.dark}
                    bg={theme.palette.orange.light}
                />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
                <BillCard
                    // primary="Benötigte Punkte bis deine ausgewählte Belohnung..."
                    secondary={Math.abs(userInfo.points || 0 - userInfo.rewardGoal?.rewardPoints || 0) + " Punkte bis deine ausgewählte Belohnung"}
                    link="Ein Training Plan jetzt starten!"
                    color={theme.palette.warning.dark}
                    bg={theme.palette.warning.light}
                />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
                <BillCard
                    primary="Gewünschte Belohnung"
                    secondary={userInfo.rewardGoal?.customReward?.title || ''}
                    link="Eine andere Belohnung als Ziel auswählen"
                    color={theme.palette.success.dark}
                    bg={theme.palette.success.light}
                />
            </Grid>

            <Rewards/>

            {/*<Grid item xs={12}>*/}
            {/*    <SubCard sx={{ overflowX: 'auto' }} title="Billing History" content={false}>*/}
            {/*        <TableContainer>*/}
            {/*            <Table>*/}
            {/*                <TableHead>*/}
            {/*                    <TableRow>*/}
            {/*                        <TableCell sx={{ pl: 3 }}>Order No.</TableCell>*/}
            {/*                        <TableCell>Date</TableCell>*/}
            {/*                        <TableCell>Price</TableCell>*/}
            {/*                        <TableCell sx={{ pr: 3 }}>Status</TableCell>*/}
            {/*                    </TableRow>*/}
            {/*                </TableHead>*/}
            {/*                <TableBody>*/}
            {/*                    {rows.map((row, index) => (*/}
            {/*                        <TableRow hover key={index}>*/}
            {/*                            <TableCell sx={{ pl: 3 }}>{row.tid}</TableCell>*/}
            {/*                            <TableCell>{row.date}</TableCell>*/}
            {/*                            <TableCell>{row.amount}</TableCell>*/}
            {/*                            <TableCell sx={{ pr: 3 }}>*/}
            {/*                                <Chip chipcolor={row.badgeType} label={row.badgeText} size="small" />*/}
            {/*                            </TableCell>*/}
            {/*                        </TableRow>*/}
            {/*                    ))}*/}
            {/*                </TableBody>*/}
            {/*            </Table>*/}
            {/*        </TableContainer>*/}
            {/*    </SubCard>*/}
            {/*</Grid>*/}
        </Grid>
    );
};

export default Billing;
