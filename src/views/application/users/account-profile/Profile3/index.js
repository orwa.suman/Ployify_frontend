import { Box, Tab, Tabs } from '@mui/material';

import Billing from './Billing';
import { FormattedMessage } from 'react-intl';
import History from './History';
import { Link } from 'react-router-dom';
import MainCard from 'ui-component/cards/MainCard';
import Notifications from './Notifications';
import Profile from './Profile';
import PropTypes from 'prop-types';
import React from 'react';
import Security from './Security';
import UserReward from 'views/application/Rewards/UserReward';

// material-ui

// project imports

// tabs
function TabPanel({ children, value, index, ...other }) {
    return (
        <div role="tabpanel" hidden={value !== index} id={`simple-tabpanel-${index}`} aria-labelledby={`simple-tab-${index}`} {...other}>
            {value === index && <Box sx={{ p: 0 }}>{children}</Box>}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`
    };
}

// ==============================|| PROFILE 3 ||============================== //

const Profile3 = () => {
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <MainCard title={<FormattedMessage id="Account" />}>
            <div>
                <Tabs
                    value={value}
                    indicatorColor="primary"
                    onChange={handleChange}
                    sx={{
                        mb: 3,
                        minHeight: 'auto',
                        '& button': {
                            minWidth: 100
                        },
                        '& a': {
                            minHeight: 'auto',
                            minWidth: 10,
                            py: 1.5,
                            px: 1,
                            mr: 2.25,
                            color: 'grey.600'
                        },
                        '& a.Mui-selected': {
                            color: 'primary.main'
                        }
                    }}
                    aria-label="simple tabs example"
                    variant="scrollable"
                >
                    <Tab component={Link} to="#" label={<FormattedMessage id="Profile" />} {...a11yProps(0)} />
                    {/* <Tab component={Link} to="#" label={<FormattedMessage id="History" />} {...a11yProps(1)} /> */}
                    <Tab component={Link} to="#" label={<FormattedMessage id="Rewards" />} {...a11yProps(2)} />

                    {/*
                    <Tab component={Link} to="#" label="Passwort ändern" {...a11yProps(3)} />

                    <Tab component={Link} to="#" label="Notifications" {...a11yProps(4)} />
                    */}
                </Tabs>
                <TabPanel value={value} index={0}>
                    <Profile />
                </TabPanel>

                <TabPanel value={value} index={1}>
                    <History />
                </TabPanel>

                <TabPanel value={value} index={2}>
                    <UserReward />
                </TabPanel>

                {/*
                <TabPanel value={value} index={2}>
                    <Security />
                </TabPanel>

                {/*
                <TabPanel value={value} index={3}>
                    <Notifications />
                </TabPanel>
                */}
            </div>
        </MainCard>
    );
};

export default Profile3;
