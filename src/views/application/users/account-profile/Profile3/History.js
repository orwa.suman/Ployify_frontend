import SubCard from '../../../../../ui-component/cards/SubCard';
import { Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import Chip from '../../../../../ui-component/extended/Chip';

import { Link as RouterLink } from 'react-router-dom';

// material-ui
import { useTheme } from '@mui/material/styles';
import { loggedInUser } from '../../../../../contexts/JWTContext';
import React from 'react';
import { loadTrainingPlans } from '../../../../../services/TrainingPlanService';
import { getActivitiesOfUser } from '../../../../../services/userService';
import { FormattedMessage } from 'react-intl';

// table data
function createData(tid, date, amount, badgeText, badgeType) {
    return { tid, date, amount, badgeText, badgeType };
}

const rows = [
    createData('12877227695', '26 Feb 2021 9:16 am', '$56.32', 'Awaiting', 'warning'),
    createData('12901477937', '30 Jan 2021 2:54 pm', '$75.56', 'Paid', 'success'),
    createData('12767886919', '22 Jan 2021 12:01 pm', '$34.23', 'Paid', 'success')
];

// ==============================|| PROFILE 3 - BILLING ||============================== //

const Billing = () => {
    const theme = useTheme();

    const user = loggedInUser;
    

    let [trainingPlans, setTrainingPlans] = React.useState([]);
    let [recipes, setRecipes] = React.useState([]);
    // let [seminars, setSeminars] = React.useState([]);
    // let [mentalHealths, setMentalHealths] = React.useState([]);

    React.useEffect(() => {
        getActivitiesOfUser(user.id)
            .then((res) => res.data)
            .then((r) => {
                
                setTrainingPlans(r.TrainingPlan);
                setRecipes(r.Recipe);
                // setSeminars(r.Seminar);
                // setMentalHealth(r.MentalHealth);
            })
            .catch((e) => {
                
            });
    }, []);

    const formatDate = (date) => {
        date = new Date(date);
        const year = date.getFullYear().toString().substring(2, 4);
        const month = (parseInt(date.getMonth()) + 1).toString().padStart(2, '0');
        const day = date.getDay().toString().padStart(2, '0');
        const hours = date.getHours().toString().padStart(2, '0');
        const minutes = date.getMinutes().toString().padStart(2, '0');

        return `${day}.${month}.${year} ${hours}:${minutes}`;
    };

    return (
        <Grid item xs={12}>
            <SubCard sx={{ overflowX: 'auto', margin: '0 0 24px' }} title={<FormattedMessage id="Training Plans" />} content={false}>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell sx={{ pl: 3 }}>
                                    <FormattedMessage id="Title" />
                                </TableCell>
                                <TableCell>
                                    <FormattedMessage id="Start Date" />
                                </TableCell>
                                <TableCell>
                                    <FormattedMessage id="End Date" />
                                </TableCell>
                                <TableCell sx={{ pr: 3 }}>
                                    <FormattedMessage id="Status" />
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {trainingPlans &&
                                trainingPlans.map((row, index) => (
                                    <TableRow hover key={index}>
                                        <TableCell sx={{ pl: 3 }}>{row.careployeeBaseEntity.title}</TableCell>
                                        <TableCell>{row.startDateTime ? formatDate(row.startDateTime) : ' - '}</TableCell>
                                        <TableCell>{row.endDateTime ? formatDate(row.endDateTime) : ' - '}</TableCell>
                                        <TableCell sx={{ pr: 3 }}>
                                            {row.progress > 0 ? (
                                                <Chip
                                                    chipcolor={row.progress === 100 ? 'success' : 'warning'}
                                                    label={row.progress + '%'}
                                                    size="small"
                                                />
                                            ) : (
                                                <></>
                                            )}
                                        </TableCell>
                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </SubCard>

            <SubCard sx={{ overflowX: 'auto' }} title={<FormattedMessage id="Recipes" />} content={false}>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell sx={{ pl: 3 }}>
                                    <FormattedMessage id="Title" />
                                </TableCell>
                                <TableCell>
                                    <FormattedMessage id="Start Date" />
                                </TableCell>
                                <TableCell>
                                    <FormattedMessage id="End Date" />
                                </TableCell>
                                <TableCell sx={{ pr: 3 }}>
                                    <FormattedMessage id="Status" />
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {recipes &&
                                recipes.map((row, index) => (
                                    <TableRow hover key={index}>
                                        <TableCell sx={{ pl: 3 }}>
                                            {row.careployeeBaseEntity.title || row.careployeeBaseEntity.name}
                                        </TableCell>
                                        <TableCell>{row.startDateTime ? formatDate(row.startDateTime) : ' - '}</TableCell>
                                        <TableCell>{row.endDateTime ? formatDate(row.endDateTime) : ' - '}</TableCell>
                                        <TableCell sx={{ pr: 3 }}>
                                            {row.progress > 0 ? (
                                                <Chip
                                                    chipcolor={row.progress === 100 ? 'success' : 'warning'}
                                                    label={row.progress + '%'}
                                                    size="small"
                                                />
                                            ) : (
                                                <></>
                                            )}
                                        </TableCell>
                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </SubCard>
        </Grid>
    );
};

export default Billing;
