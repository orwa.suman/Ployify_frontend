import React, { useEffect } from 'react';
import {
    addActivityToUserFavorites,
    addActivityToUserHistory,
    getUserFavorites,
    removeActivityFromUserFavorites
} from '../../../../services/userService';
import { useLocation, useNavigate } from 'react-router-dom';

import { Grid } from '@mui/material';
import MakeOverview from './TrainingPlanOverview';
import { dispatch } from 'store';
import { getTrainingPlan } from '../../../../services/TrainingPlanService';
import { loggedInUser } from '../../../../contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';
import { updateStartCountTrainingPlanTracking } from '../../../../services/trackingService';

function TrainingPlanPlayMainScreen() {
    let [data, setData] = React.useState([]);
    let [load, setLoad] = React.useState(false);
    let [open, setOpen] = React.useState(false);
    let [videoSrc, setVideoSrc] = React.useState('');
    const [favorites, setFavorites] = React.useState([]);
    const [makeMeFavourate, setMakeMeFavourate] = React.useState(false);
    const navigate = useNavigate();
    const location = useLocation();

    React.useEffect(() => {
        const trainingPlan = location.state;
        updateStartCountTrainingPlanTracking(trainingPlan.id);
        addActivityToUserHistory(loggedInUser.id, trainingPlan.id, 1);
        getTrainingPlan(trainingPlan.id)
            .then((res) => res.data)
            .then((r) => {
                
                setData(r);
                setLoad(true);
            })
            .catch((e) => {
                
            });
    }, [location.state, makeMeFavourate]);

    const objToSendToNextPage = { trainingPlan: data, groupIndex: 0, trainingIndex: 0 };
    const toComponentPlayVideo = () => {
        navigate('/activities/TrainingPlan/list/playVideo', { state: objToSendToNextPage });
    };

    const handleClickOpen = (videoSrc) => {
        setOpen(true);
        setVideoSrc(videoSrc);
    };

    const handleClose = (value) => {
        setOpen(false);
    };

    const loadFavorites = (onFavoriteTab = false) =>
        getUserFavorites(loggedInUser.id, 1)
            .then((res) => res.data)
            .then((data) => {
                let userFavorites = [];
                data.forEach((favorit) => {
                    if (
                        !userFavorites.find(
                            (intermediateFavorit) => intermediateFavorit.careployeeBaseEntity?.id === favorit.careployeeBaseEntity?.id
                        )
                    ) {
                        userFavorites = [...userFavorites, favorit];
                    }
                });
                setFavorites(userFavorites);
            })
            .catch((e) => {
                //  dispatch(showMessage(e.message));
            });

    useEffect(() => {
        loadFavorites();
    }, [makeMeFavourate]);

    const isFavorit = (id) => (!!favorites && favorites.length ? favorites : []).some((favorit) => favorit.careployeeBaseEntity.id === id);

    const setAsFavorite = (activityId) => {
        addActivityToUserFavorites({ userId: loggedInUser.id, activityId, activityNum: 1 })
            .then((res) => {
                setMakeMeFavourate(true);
            })
            .catch((e) => {
                dispatch(showMessage(e.message));
            });
        setMakeMeFavourate(false);
    };

    const RemoveAsFavorite = (activityId) => {
        removeActivityFromUserFavorites({ userId: loggedInUser.id, activityId, activityNum: 1 })
            .then((res) => {
                setMakeMeFavourate(true);
            })
            .catch((e) => {
                dispatch(showMessage(e.message));
            });
        setMakeMeFavourate(false);
    };

    return (
        <div>
            {load ? (
                <Grid>
                    {MakeOverview({
                        toComponentPlayVideo: () => toComponentPlayVideo(),
                        handleClickOpen: (videoSrc) => handleClickOpen(videoSrc),
                        handleClose,
                        open,
                        videoSrc,
                        setAsFavorite,
                        RemoveAsFavorite,
                        item: data,
                        isFavorit: isFavorit(data.id)
                    })}
                </Grid>
            ) : (
                ''
            )}
        </div>
    );
}

export default TrainingPlanPlayMainScreen;
