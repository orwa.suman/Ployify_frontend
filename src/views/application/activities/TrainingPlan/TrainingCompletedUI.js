import { Box, Button } from '@mui/material';

import { KeyboardArrowUp } from '@mui/icons-material';
import KeyboardDoubleArrowUpIcon from '@mui/icons-material/KeyboardDoubleArrowUp';
import { Typography } from '@material-ui/core';
import { useNavigate } from 'react-router-dom';

const TrainingCompletedUI = ({ handleComplete, trainingPlan, userInfo, PointsProgressBar, getProgressForGoal }) => {
    const navigate = useNavigate();
    return (
        <Box>
            <Typography onClick={() => navigate('/dashboard/default')} style={{ cursor: 'pointer' }}>
                Back to Dashboard
            </Typography>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    flexDirection: ['column', null, 'row']
                }}
            >
                <Box sx={{ display: 'flex', flexDirection: 'column', marginTop: '30px' }}>
                    <Typography variant="h5">Congratulations! You did it</Typography>
                    <Button
                        onClick={() => handleComplete()}
                        variant="contained"
                        style={{
                            backgroundColor: '#CBAB70',
                            padding: '10px',
                            borderRadius: '18px',
                            width: ['100%', null, '30em'],
                            marginTop: '40px'
                        }}
                    >
                        <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                            <p style={{ margin: 0, padding: '5px' }}>Finish your training</p>{' '}
                            <p style={{ margin: 0, padding: 0 }}>to get your desired points.</p>
                        </Box>
                    </Button>
                    <Box sx={{ display: 'flex', marginTop: '40px', marginLeft: ['-30px', null, '0'] }}>
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',
                                alignContent: 'center',
                                marginTop: '30px'
                            }}
                        >
                            <Typography variant="p" sx={{ fontSize: '20px' }}>
                                + {trainingPlan.points}
                            </Typography>
                            <Typography variant="p" style={{ fontWeight: 'bold' }}>
                                Points
                            </Typography>
                            <KeyboardDoubleArrowUpIcon style={{ marginBottom: 0, paddingBottom: 0, fontSize: '150px' }} />
                            <KeyboardArrowUp style={{ marginTop: '-80px', paddingTop: 0, fontSize: '150px' }} />
                        </div>
                        <div style={{ marginTop: '40px' }}>
                            <PointsProgressBar value={getProgressForGoal()} />
                            {userInfo?.rewardGoal === null && <p>Please select a goal to get points.</p>}
                        </div>
                        <img
                            src={userInfo.rewardGoal?.customReward?.picture || '/images/dish.jpg'}
                            alt="reward"
                            style={{
                                marginLeft: '20px',
                                marginTop: '40px',
                                borderRadius: '10px',
                                height: '200px',
                                width: '200px'
                            }}
                        />
                    </Box>
                </Box>

                <Box>
                    <img src="/images/congrats.jpg" alt="congrats" style={{ maxWidth: '800px', maxHeight: '600px', width: '100%' }} />
                </Box>
            </Box>
        </Box>
    );
};

export default TrainingCompletedUI;
