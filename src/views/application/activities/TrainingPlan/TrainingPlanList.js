import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../../assets/scss/style.css';

import {
    Grid,
    IconButton,
    InputAdornment,
    LinearProgress,
    OutlinedInput,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
    Typography
} from '@mui/material';
import React, { useState } from 'react';

import BlockTwoToneIcon from '@mui/icons-material/BlockTwoTone';
import { ContentCutOutlined } from '@mui/icons-material';
import { FormattedMessage } from 'react-intl';
import { HiPencil } from 'react-icons/hi';
import { IconSearch } from '@tabler/icons';
import MainCard from 'ui-component/cards/MainCard';
import { deleteTrainingPlan } from 'services/TrainingService';
import { getMostUsedTrainingPlans } from '../../../../services/AnalyzeToolService';
import { gridSpacing } from 'store/constant';
import { loadTrainingPlans } from '../../../../services/TrainingPlanService';
import { showMessage } from '../../../../utils/form/Communication';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';

// assets

const TrainingList = () => {
    const theme = useTheme();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [deleteTrainingsPlan, setDeleteTrainingsPlan] = useState(false);
    let [data, setData] = React.useState([]);
    let [trackingData, setTrackingData] = React.useState([]);
    let [loading, setLoading] = React.useState(true);

    // React.useEffect(() => {
    //     loadTrainingPlans()
    //         .then((res) => res.data)
    //         .then(setData)
    //         .catch((e) => {
    //             
    //             dispatch(showMessage(e?.response?.data?.message));
    //         });
    // }, []);

    React.useEffect(() => {
        loadTrainingPlans()
            .then((res) => res.data)
            .then(setData)
            .then(setLoading(false))
            .catch((e) => {
                setLoading(false);
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, [deleteTrainingsPlan]);

    // React.useEffect(() => {
    //     loadTrainingPlans()
    //         .then((res) => res.data)
    //         .then(setData)
    //         .catch((e) => {
    //             dispatch(showMessage(e?.response?.data?.message));
    //         });
    // }, []);

    React.useEffect(() => {
        const trainingPlanTrackingEntities = getMostUsedTrainingPlans();
        trainingPlanTrackingEntities
            .then((res) => res.data)
            .then((data) => {
                setTrackingData(data);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    const handleDelete = (id) => {
        deleteTrainingPlan(id)
            .then(() => {
                loadTrainingPlans();
                setDeleteTrainingsPlan(true);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
        setDeleteTrainingsPlan(false);
    };

    return (
        <MainCard
            title={
                <Grid container alignItems="center" justifyContent="space-between" spacing={gridSpacing}>
                    <Grid item>
                        <Typography variant="h3">
                            <FormattedMessage id="List" />
                        </Typography>
                    </Grid>
                    <Grid item>
                        <OutlinedInput
                            id="input-search-list-style1"
                            placeholder="Search"
                            startAdornment={
                                <InputAdornment position="start">
                                    <IconSearch stroke={1.5} size="16px" />
                                </InputAdornment>
                            }
                            size="small"
                        />
                    </Grid>
                </Grid>
            }
            content={false}
        >
            <TableContainer>
                {loading && (
                    <LinearProgress
                        variant="indeterminate"
                        sx={{
                            backgroundColor: '#cbab7075',
                            '& .MuiLinearProgress-bar': {
                                backgroundColor: '#cbab70'
                            }
                        }}
                    />
                )}
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell scope="col">
                                <FormattedMessage id="Picture" />
                            </TableCell>
                            <TableCell scope="col">
                                <FormattedMessage id="Title" />
                            </TableCell>
                            <TableCell scope="col">
                                <FormattedMessage id="Location" />
                            </TableCell>
                            <TableCell scope="col">
                                <FormattedMessage id="Level" />
                            </TableCell>

                            <TableCell scope="col">
                                <FormattedMessage id="Duration" />
                            </TableCell>

                            {/* <TableCell scope="col">
                                <FormattedMessage id="Number of trainings" />
                            </TableCell> */}
                            <TableCell scope="col" align="center" sx={{ pr: 3 }}>
                                <FormattedMessage id="Actions" />
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data &&
                            data.map((row, index) => (
                                <TableRow hover key={index}>
                                    <TableCell>
                                        {!!row.picture ? (
                                            <div className="careployee-training-plan__image">
                                                <img src={row.picture}></img>
                                            </div>
                                        ) : (
                                            ''
                                        )}
                                    </TableCell>
                                    <TableCell>
                                        <Grid container spacing={2} alignItems="center">
                                            <Grid item xs zeroMinWidth>
                                                <Typography align="left" component="div">
                                                    {row.title} {trackingData.includes(row.id) ? '*' : ''}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </TableCell>
                                    <TableCell>{row.trainingPlanLocation}</TableCell>
                                    <TableCell>{row.levelOfAdvance}</TableCell>
                                    <TableCell>{row.totalDuration}s</TableCell>

                                    {/* <TableCell>
                                        {row.trainingInfo !== undefined
                                            ? row.trainingGroups[0].trainingInfos !== null
                                                ? row.trainingGroups[0].trainingInfos[0].equipments.map((val) => val + '\n')
                                                : 'None'
                                            : 'None'}
                                    </TableCell>
                                    <TableCell>{row.trainingGroups.length}</TableCell> */}
                                    <TableCell align="center" sx={{ pr: 3 }}>
                                        <Stack direction="row" justifyContent="center" alignItems="center">
                                            <Tooltip placement="top" title="edit">
                                                <IconButton
                                                    color="primary"
                                                    aria-label="edit"
                                                    onClick={() => navigate('/dashboard/activities/training-plan/form/' + row.id)}
                                                    size="large"
                                                >
                                                    <HiPencil sx={{ fontSize: '1.1rem' }} />
                                                </IconButton>
                                            </Tooltip>

                                            <Tooltip placement="top" title="Delete" onClick={() => {}}>
                                                <IconButton
                                                    color="primary"
                                                    sx={{
                                                        color: theme.palette.orange.dark,
                                                        borderColor: theme.palette.orange.main,
                                                        '&:hover ': { background: theme.palette.orange.light }
                                                    }}
                                                    size="large"
                                                    onClick={() => {
                                                        handleDelete(row.id);
                                                    }}
                                                >
                                                    <BlockTwoToneIcon sx={{ fontSize: '1.1rem' }} />
                                                </IconButton>
                                            </Tooltip>
                                        </Stack>
                                    </TableCell>
                                </TableRow>
                            ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </MainCard>
    );
};

export default TrainingList;
