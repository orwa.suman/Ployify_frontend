import { Box, Button, Grid } from '@mui/material';
import { useLocation, useNavigate } from 'react-router-dom';

import FinishIcon from 'assets/images/finish-line-amico.svg';
import { FormattedMessage } from 'react-intl';
import MainCard from '../../../../ui-component/cards/MainCard';

export default function FinishPlan(props) {
    const navigate = useNavigate();
    const location = useLocation();
    const trainingPlan = location.state.trainingPlan;

    return (
        <MainCard sx={{ textAlign: 'left', height: '100vh' }}>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'column',
                    textAlign: 'center',
                    height: '480px'
                }}
            >
                {/* <div style={{ fontSize: '300px' }}>&#129395;</div> */}
                <img
                    src={FinishIcon}
                    alt="Berry"
                    style={{
                        width: 500,
                        maxWidth: '100%'
                    }}
                />
            </Box>
            {/* <Grid container>
                <Grid item xs={12}>
                    <img src="/images/finish-training-plan.png" alt="finish training plan" style={{ width: '100%', margin: '0' }} />
                </Grid>
              
            </Grid> */}
            <Grid item xs={12} sx={{ textAlign: 'center' }}>
                {/* <Grid>
                    <h1>{trainingPlan.endTitle} </h1>
                </Grid>
                <Grid>{trainingPlan.endText}</Grid> */}
                <Grid>
                    <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                        You've earned
                        <Box
                            sx={{
                                width: '40px',
                                height: '40px',
                                color: 'white',
                                backgroundColor: '#CBAB70',
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: '100%',
                                marginInline: '10px',
                                fontWeight: '700'
                            }}
                        >
                            {trainingPlan.points}
                        </Box>
                        points
                    </Box>
                </Grid>
                <Button color="secondary" sx={{ margin: '24px 0 0' }} variant="contained" onClick={() => navigate('/dashboard/default')}>
                    <FormattedMessage id="Back to dashboard" />
                </Button>
            </Grid>
        </MainCard>
    );
}
