// ==============================|| FORM VALIDATION - LOGIN FORMIK  ||============================== //

import * as Yup from 'yup';

import { Autocomplete, Button, FormControlLabel, Grid, InputLabel, Radio, RadioGroup, TextField } from '@mui/material';
import { FormattedMessage, useIntl } from 'react-intl';
import React, { useState } from 'react';
import { addTrainingPlan, getTrainingPlan, updateTrainingPlan } from '../../../../services/TrainingPlanService';
import { loadTrainingTags, loadTrainings } from '../../../../services/TrainingService';

import Dialog from '@mui/material/Dialog';
import Divider from '@mui/material/Divider';
import FileUpload from '../../../forms/components/FileUpload';
import MainCard from '../../../../ui-component/cards/MainCard';
import PropTypes from 'prop-types';
import TrainingForm from '../Training/TrainingForm';
import TrainingsTagsForm from './TrainingsTagsForm';
import { loadAuthors } from '../../../../services/AuthorService';
import { showMessage } from '../../../../utils/form/Communication';
import { uploadToCloudinary } from 'services/CloudinaryService';
import { useDispatch } from 'store';
import { useFormik } from 'formik';
import { useParams } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';

const emails = ['username@gmail.com', 'user02@gmail.com'];

function SimpleDialog(props) {
    const { onClose, selectedValue, open } = props;

    const handleClose = () => {
        onClose(selectedValue);
    };

    return (
        <Dialog onClose={handleClose} open={open} sx={{ minWidth: '500px' }}>
            <TrainingForm />
        </Dialog>
    );
}

SimpleDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    selectedValue: PropTypes.string.isRequired
};

export function SimpleTagDialog(props) {
    const { open, onClose, type } = props;

    return (
        <Dialog onClose={onClose} open={open} fullWidth>
            <TrainingsTagsForm />
            {/* {
                type === 'training' ? <TrainingsTagsForm /> 
                    : type === 'recipe'? <DishTagForm />
                        : type === 'mentalHealth'? <MentalHealthTagForm />
                        : <TrainingsTagsForm/>
            } */}
        </Dialog>
    );
}

SimpleTagDialog.propTypes = {
    open: PropTypes.bool.isRequired
};

function TrainingPlanForm() {
    const theme = useTheme();
    const [trainingPlanTrainings, setTrainingPlanTrainings] = React.useState([
        {
            name: 'Group 1',
            groupSequence: { value: 1 },
            trainings: [{ training: { value: null }, duration: { value: 0 }, sequence: { value: 1 } }],
            postPause: null
        }
    ]);
    const [open, setOpen] = React.useState(false);
    const [selectedValue, setSelectedValue] = React.useState(emails[1]);
    const dispatch = useDispatch();
    const [trainings, setTrainings] = React.useState([]);
    const { id } = useParams();
    const [tagForm, setTagForm] = useState(false);
    const [trainingPlanTags, setTrainingTags] = useState([]);

    const [picture, setPicture] = React.useState([]);
    const [pictureName, setPictureName] = React.useState([]);
    const [changed, setChanged] = React.useState(false);

    const [authors, setAuthors] = React.useState([]);
    const [author, setAuthor] = React.useState({});

    const intl = useIntl();

    React.useEffect(() => {
        requestAuthors();
        requestTrainings();
        loadTags();

        if (id) {
            getTrainingPlan(id)
                .then((res) => res.data)
                .then((data) => {
                    const savedTrainingPlanTraining = (data.trainingGroups || []).map((group, index) => ({
                        name: group.groupName,
                        groupSequence: { value: group.groupSequence },
                        postPause: group.postPause,
                        trainings: (group.trainingInfos || []).map((trainingInfo) => ({
                            ...trainingInfo,
                            duration: { value: trainingInfo.duration },
                            sequence: { value: trainingInfo.sequence },
                            training: { value: trainingInfo.training },
                            postPause: trainingInfo.postPause
                        }))
                    }));

                    setTrainingPlanTrainings(savedTrainingPlanTraining);

                    formik.setValues({
                        title: data.title,
                        information: data.information,
                        trainingPlanLocation: data.trainingPlanLocation,
                        levelOfAdvance: data.levelOfAdvance,
                        endTitle: data.endTitle,
                        endText: data.endText,
                        points: data.points,
                        author: data.author,
                        picture: data.picture,
                        trainingPlanTags: data.trainingPlanTags
                    });
                });
        }
    }, []);

    const requestAuthors = () => {
        const request = loadAuthors();
        request
            .then((res) => res.data)
            .then((data) => {
                setAuthors(data);
                if (data.length) {
                    formik.values.author = data[0];
                    setAuthor(data[0]);
                }
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const requestTrainings = () => {
        const trainings = loadTrainings();
        trainings
            .then((res) => res.data)
            .then((data) => {
                setTrainings(data);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const loadTags = () => {
        loadTrainingTags()
            .then((res) => res.data)
            .then((data) => {
                setTrainingTags(data);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const validationSchema = Yup.object({
        title: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(100, <FormattedMessage id="Input Max Length" values={{ length: 100 }} />)
            .required(<FormattedMessage id="Required" />),
        information: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(1000, <FormattedMessage id="Input Max Length" values={{ length: 1000 }} />),
        //.required(<FormattedMessage id="Required" />),
        endTitle: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(200, <FormattedMessage id="Input Max Length" values={{ length: 200 }} />),
        // .required(<FormattedMessage id="Required" />),
        endText: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(1000, <FormattedMessage id="Input Max Length" values={{ length: 1000 }} />),
        // .required(<FormattedMessage id="Required" />),
        points: Yup.number()
            .min(1, <FormattedMessage id="Input Min Length" values={{ length: 1 }} />)
            .max(100, <FormattedMessage id="Input Max Length" values={{ length: 100 }} />)
            .required(<FormattedMessage id="Required" />),
        author: Yup.object()
            .required(<FormattedMessage id="Required" />)
            .nullable()
    });

    const trainingPlanHasErrors = () => {
        let hasError = false;
        let newList = [...trainingPlanTrainings];

        trainingPlanTrainings.forEach((group, groupIndex) => {
            if (!group.groupSequence?.value) {
                newList[groupIndex].groupSequence = { value: '', error: <FormattedMessage id="Required" /> };
                hasError = true;
            }

            group.trainings.forEach((training, trainingIndex) => {
                if (!training.duration?.value) {
                    newList[groupIndex].trainings[trainingIndex].duration = { value: '', error: <FormattedMessage id="Required" /> };
                    hasError = true;
                }
                if (!training.sequence?.value) {
                    newList[groupIndex].trainings[trainingIndex].sequence = { value: '', error: <FormattedMessage id="Required" /> };
                    hasError = true;
                }
                if (!training.training.value) {
                    newList[groupIndex].trainings[trainingIndex].training = { value: null, error: <FormattedMessage id="Required" /> };
                    hasError = true;
                }
            });
        });

        if (hasError) {
            setTrainingPlanTrainings(newList);
        }

        return hasError;
    };

    const groupNamesHaveErrors = () => {
        let hasError = false;
        let list = [...trainingPlanTrainings];
        [...list].forEach((group) => {
            if (group.error) hasError = true;
        });
        setTrainingPlanTrainings(list);
        return hasError;
    };

    const mapTrainingPlanTrainingsForSubmit = () => {
        let trainingPlans = [];

        let result = [];

        trainingPlanTrainings.forEach((group) => {
            const trainingPlanGroup = {
                groupName: group.name,
                groupSequence: group.groupSequence.value,
                postPause: group.postPause
            };

            group.trainings.forEach((training) => {
                const trainingPlanTraining = {
                    ...training,
                    training: training.training.value,
                    duration: training.duration.value,
                    sequence: training.sequence.value,
                    postPause: training.postPause
                };
                trainingPlans = [...trainingPlans, trainingPlanTraining];
            });

            trainingPlanGroup.trainingInfos = trainingPlans;
            result = [...result, trainingPlanGroup];
            trainingPlans = [];
        });

        return result;
    };

    const submitForm = (event) => {
        if (!trainingPlanHasErrors() && !groupNamesHaveErrors()) {
            formik.values.trainingGroups = mapTrainingPlanTrainingsForSubmit();
            formik.validateForm().then((errors) => {
                if (errors && Object.keys(errors).length > 0) {
                    dispatch(showMessage(<FormattedMessage id="Validation Fail" />));
                }
                formik.handleSubmit(event);
            });
        } else {
            dispatch(showMessage(<FormattedMessage id="Validation Fail" />));
        }
    };

    const formik = useFormik({
        initialValues: {
            title: '',
            information: '',
            trainingPlanLocation: 'HOME',
            levelOfAdvance: 'STANDARD',
            endTitle: '',
            endText: '',
            points: 0,
            author: null,
            picture: undefined,
            trainingInfos: [],
            trainingGroups: [],
            trainingPlanTags: []
        },
        validationSchema,
        onSubmit: () => {
            

            uploadToCloudinary(picture, pictureName, 'trainingPlan_', '/Ployify/Activities/TrainingPlan')
                .then((data) => {
                    if (changed) {
                        
                        
                        formik.values.picture = data.url;
                        formik.values.trainingPlanLevel = 4; // TODO static to be changed later
                        formik.values.trainingPlanRange = 5; // TODO static to be changed later
                    }

                    if (id) {
                        updateTrainingPlan(id, formik.values)
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    } else {
                        addTrainingPlan(formik.values)
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    }
                })
                .catch((err) => {
                    
                });
        }
    });

    function handleTag(event, value) {
        formik.setValues({ ...formik.values, trainingPlanTags: value });
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = (value) => {
        setOpen(false);
        setSelectedValue(value);
        requestTrainings();
    };

    const openTagForm = () => {
        setTagForm(true);
    };

    const handleDialogClose = (value) => {
        setTagForm(false);
        loadTags();
    };

    const changeLocation = (event) => {
        formik.setValues({ ...formik.values, trainingPlanLocation: event.target.value });
    };

    const changeDifficulty = (event) => {
        formik.setValues({ ...formik.values, levelOfAdvance: event.target.value });
    };

    const addGroup = () => {
        const list = [...trainingPlanTrainings];
        setTrainingPlanTrainings([
            ...list,
            {
                name: 'Group ' + (trainingPlanTrainings.length + 1),
                groupSequence: { value: trainingPlanTrainings.length + 1 },
                trainings: [{ training: { value: null }, duration: { value: '' }, sequence: { value: '' } }]
            }
        ]);
    };

    const removeGroup = (groupIndex) => {
        setTrainingPlanTrainings([...trainingPlanTrainings].filter((value, index) => index !== groupIndex));
    };

    const addTraining = (groupIndex) => {
        const list = [...trainingPlanTrainings];
        const training = { training: { value: null }, duration: { value: '' }, sequence: { value: list[groupIndex].trainings.length + 1 } };

        list[groupIndex].trainings = [...list[groupIndex].trainings, training];
        setTrainingPlanTrainings(list);
    };

    const removeTraining = (groupIndex, trainingIndex) => {
        const list = [...trainingPlanTrainings];
        list[groupIndex].trainings = [...list][groupIndex].trainings.filter((value, index) => index !== trainingIndex);
        setTrainingPlanTrainings(list);
    };

    const changeTraining = (value, groupIndex, trainingIndex) => {
        const list = [...trainingPlanTrainings];

        if (!value) {
            list[groupIndex].trainings[trainingIndex].training = { value: null, error: <FormattedMessage id="Required" /> };
        }

        list[groupIndex].trainings[trainingIndex].training = { value };
        setTrainingPlanTrainings(list);
    };

    const changeDuration = (event, groupIndex, trainingIndex) => {
        const list = [...trainingPlanTrainings];
        const value = event.target.value;

        if (!value) {
            list[groupIndex].trainings[trainingIndex].duration = { value: '', error: <FormattedMessage id="Required" /> };
        }

        list[groupIndex].trainings[trainingIndex].duration = { value };
        setTrainingPlanTrainings(list);
    };

    const changeSequence = (event, groupIndex, trainingIndex) => {
        const list = [...trainingPlanTrainings];
        const value = event.target.value;

        if (!value) {
            list[groupIndex].trainings[trainingIndex].sequence = { value: '', error: <FormattedMessage id="Required" /> };
        }

        list[groupIndex].trainings[trainingIndex].sequence = { value };
        setTrainingPlanTrainings(list);
    };

    const changePostPause = (event, groupIndex, trainingIndex) => {
        const list = [...trainingPlanTrainings];
        const value = event.target.value;
        list[groupIndex].trainings[trainingIndex].postPause = value;
        setTrainingPlanTrainings(list);
    };

    const changeGroupName = (event, groupIndex) => {
        const list = [...trainingPlanTrainings];
        const value = event.target.value;

        if (!value) {
            list[groupIndex] = { ...list[groupIndex], name: '', error: <FormattedMessage id="Required" /> };
        } else {
            list[groupIndex] = { ...list[groupIndex], name: value };
        }

        setTrainingPlanTrainings(list);
    };

    const changeGroupSequence = (event, groupIndex) => {
        const list = [...trainingPlanTrainings];
        const value = event.target.value;

        if (!value) {
            list[groupIndex].groupSequence = { ...list[groupIndex], value: '', error: <FormattedMessage id="Required" /> };
        } else {
            list[groupIndex].groupSequence = { ...list[groupIndex], value: value };
        }

        setTrainingPlanTrainings(list);
    };

    const changeGroupPostPause = (event, groupIndex) => {
        const list = [...trainingPlanTrainings];
        const value = event.target.value;
        list[groupIndex].postPause = value;
        setTrainingPlanTrainings(list);
    };

    const handlePicture = (data, fileName) => {
        setPicture(data);
        setPictureName(fileName);
        setChanged(true);
    };

    return (
        <Grid>
            <form onSubmit={formik.handleSubmit}>
                <div className="careployee-form__group">
                    <MainCard title={<FormattedMessage id="First Page" />}>
                        <Grid container>
                            <Grid item xs={12} md={5} className="careployee-form__block">
                                <Grid item xs={12} className="careployee-form__block">
                                    <label htmlFor="title" className="careployee-form__label">
                                        <FormattedMessage id="Title" />
                                    </label>
                                    <TextField
                                        fullWidth
                                        id="title"
                                        label="Training Plan Title"
                                        value={formik.values.title}
                                        onChange={formik.handleChange}
                                        multiline
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.title && Boolean(formik.errors.title)}
                                        helperText={formik.touched.title && formik.errors.title}
                                    />
                                </Grid>

                                <Grid item xs={12} className="careployee-form__block">
                                    <label htmlFor="information" className="careployee-form__label">
                                        <FormattedMessage id="Description" />
                                    </label>
                                    <TextField
                                        fullWidth
                                        id="information"
                                        label="Information"
                                        multiline
                                        rows={9}
                                        value={formik.values.information}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.information && Boolean(formik.errors.information)}
                                        helperText={formik.touched.information && formik.errors.information}
                                    />
                                </Grid>
                            </Grid>
                            <Grid item xs={12} md={3} className="careployee-form__block">
                                <Grid item xs={12} className="careployee-form__block">
                                    <FileUpload defaultPicture={formik.values.picture} onChange={handlePicture}></FileUpload>
                                </Grid>
                            </Grid>

                            <Grid item xs={12} md={4} className="careployee-form__block">
                                <InputLabel>
                                    <FormattedMessage id="Location" />
                                </InputLabel>
                                <RadioGroup row value={formik.values.trainingPlanLocation} onChange={changeLocation}>
                                    <FormControlLabel label="Home" value="HOME" control={<Radio />} />
                                    <FormControlLabel label="Office" value="OFFICE" control={<Radio />} />
                                </RadioGroup>
                            </Grid>

                            <Grid item xs={12} md={4} className="careployee-form__block">
                                <InputLabel>
                                    <FormattedMessage id="Difficulty" />
                                </InputLabel>
                                <RadioGroup row value={formik.values.levelOfAdvance} onChange={changeDifficulty}>
                                    <FormControlLabel label="Beginner" value="BEGINNER" control={<Radio />} />
                                    <FormControlLabel label="Standard" value="STANDARD" control={<Radio />} />
                                    <FormControlLabel label="Advanced" value="ADVANCED" control={<Radio />} />
                                    <FormControlLabel label="Pro" value="PRO" control={<Radio />} />
                                </RadioGroup>
                            </Grid>
                        </Grid>
                    </MainCard>
                </div>
                <div className="careployee-form__group">
                    <MainCard title={<FormattedMessage id="Last Page" />}>
                        {/*  <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="endTitle" className="careployee-form__label">
                                <FormattedMessage id="End Title" />
                            </label>
                            <TextField
                                fullWidth
                                maxRows={4}
                                id="endTitle"
                                value={formik.values.endTitle}
                                onChange={formik.handleChange}
                                multiline
                                onBlur={formik.handleBlur}
                                error={formik.touched.endTitle && Boolean(formik.errors.endTitle)}
                                helperText={formik.touched.endTitle && formik.errors.endTitle}
                            />
    </Grid> */}

                        {/*  <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="endText" className="careployee-form__label">
                                <FormattedMessage id="End Text" />
                            </label>
                            <TextField
                                fullWidth
                                id="endText"
                                multiline
                                rows={3}
                                value={formik.values.endText}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.endText && Boolean(formik.errors.endText)}
                                helperText={formik.touched.endText && formik.errors.endText}
                            />
    </Grid> */}

                        <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="points" className="careployee-form__label">
                                <FormattedMessage id="Default Points" />
                            </label>
                            <TextField
                                fullWidth
                                id="points"
                                label="Default Points"
                                multiline
                                rows={3}
                                value={formik.values.points}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.points && Boolean(formik.errors.points)}
                                helperText={formik.touched.points && formik.errors.points}
                            />
                        </Grid>

                        <Grid item xs={12} md={5} className="careployee-form__block">
                            <label htmlFor="author" className="careployee-form__label">
                                <FormattedMessage id="Author" />
                            </label>
                            <Autocomplete
                                style={{ height: '44px' }}
                                disablePortal
                                options={authors}
                                getOptionLabel={(option) => option.firstName + ' ' + option.lastName}
                                value={author}
                                onChange={(event, value) => {
                                    formik.values.author = value;
                                    setAuthor(value);
                                }}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        error={formik.errors.author}
                                        helperText={!!formik.errors.author && formik.errors.author}
                                    />
                                )}
                            />
                        </Grid>
                    </MainCard>
                </div>
                <MainCard title={<FormattedMessage id="Trainings" />}>
                    <Button
                        variant="contained"
                        onClick={handleClickOpen}
                        sx={{
                            margin: '16px 0 12px',
                            borderRadius: '8px',
                            background: theme.palette.success.dark,
                            '&:hover': { background: theme.palette.success.main }
                        }}
                    >
                        <FormattedMessage id="Add New Training" />
                    </Button>
                    <Grid className="careployee-form__block careployee-form__multiline-group-wrapper">
                        {(trainingPlanTrainings || []).map((group, groupIndex) => (
                            <div key={groupIndex}>
                                <div className="careployee-form__multiline-group">
                                    <TextField
                                        id={`group-${groupIndex}`}
                                        label="Group"
                                        placeholder="Group"
                                        onChange={(event) => changeGroupName(event, groupIndex)}
                                        value={group.name}
                                        error={group.name.length === 0}
                                        helperText={!!group.error && group.error}
                                    />

                                    <TextField
                                        id={`group-${groupIndex}-sequence`}
                                        label="Group Sequence"
                                        placeholder="Group Sequence"
                                        onChange={(event) => changeGroupSequence(event, groupIndex)}
                                        value={group.groupSequence?.value}
                                        // error={group.groupSequence?.length === 0}
                                        // helperText={!!group.error && group.error}
                                        error={!!group.groupSequence.error}
                                        helperText={group.groupSequence?.error}
                                    />
                                    {/* tarek */}
                                    <TextField
                                        id={`group-${groupIndex}-postPause`}
                                        label="Group Post Pause"
                                        placeholder="Group Post Pause"
                                        onChange={(event) => changeGroupPostPause(event, groupIndex)}
                                        value={group.postPause}
                                    />

                                    {trainingPlanTrainings.length > 1 ? (
                                        <Button
                                            variant="contained"
                                            size="small"
                                            onClick={(event) => removeGroup(groupIndex)}
                                            sx={{
                                                position: 'relative',
                                                top: '10px',
                                                fontWeight: 'bold',
                                                height: '40px',
                                                padding: '8px 24px',
                                                marginLeft: '8px',
                                                borderRadius: '8px',
                                                background: theme.palette.error.main,
                                                '&:hover': { background: theme.palette.error.dark }
                                            }}
                                        >
                                            X
                                        </Button>
                                    ) : (
                                        <></>
                                    )}

                                    {(group.trainings || []).map((groupedTraining, trainingIndex) => (
                                        <div className="careployee-form__multiline-item" key={trainingIndex}>
                                            <Grid container>
                                                <Grid item xs={12} md={3} sx={{ marginTop: '6px' }}>
                                                    <Autocomplete
                                                        style={{ height: '44px' }}
                                                        disablePortal
                                                        options={trainings}
                                                        getOptionLabel={(option) => option.trainingName}
                                                        value={groupedTraining.training?.value || null}
                                                        onChange={(event, value) => {
                                                            changeTraining(value, groupIndex, trainingIndex);
                                                        }}
                                                        renderInput={(params) => (
                                                            <TextField
                                                                {...params}
                                                                label="TrainingName"
                                                                error={!!groupedTraining.training?.error}
                                                                helperText={groupedTraining.training?.error}
                                                            />
                                                        )}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} md={2} sx={{ margin: '5px' }}>
                                                    <TextField
                                                        fullWidth
                                                        id={`training-${trainingIndex}-duration`}
                                                        label="duration"
                                                        type="number"
                                                        placeholder="duration dieser Übung"
                                                        onChange={(event) => changeDuration(event, groupIndex, trainingIndex)}
                                                        value={groupedTraining.duration?.value}
                                                        error={!!groupedTraining.duration.error}
                                                        helperText={groupedTraining.training?.error}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} md={2} sx={{ margin: '5px' }}>
                                                    <TextField
                                                        fullWidth
                                                        id={`training-${trainingIndex}-sequence`}
                                                        label="sequence"
                                                        type="number"
                                                        placeholder="Sequenz dieser Übung"
                                                        onChange={(event) => changeSequence(event, groupIndex, trainingIndex)}
                                                        value={groupedTraining.sequence?.value}
                                                        error={!!groupedTraining.sequence.error}
                                                        helperText={groupedTraining.training?.error}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} md={2} sx={{ margin: '5px' }}>
                                                    <TextField
                                                        fullWidth
                                                        id={`training-${trainingIndex}-postPause`}
                                                        label="Post Pause"
                                                        type="number"
                                                        onChange={(event) => changePostPause(event, groupIndex, trainingIndex)}
                                                        value={groupedTraining.postPause}
                                                    />
                                                </Grid>
                                                {group.trainings.length > 1 && (
                                                    <Button
                                                        variant="contained"
                                                        size="small"
                                                        onClick={(event) => removeTraining(groupIndex, trainingIndex)}
                                                        sx={{
                                                            position: 'relative',
                                                            top: '10px',
                                                            fontWeight: 'bold',
                                                            height: '40px',
                                                            padding: '8px 24px',
                                                            marginLeft: '8px',
                                                            borderRadius: '8px',
                                                            background: theme.palette.error.main,
                                                            '&:hover': { background: theme.palette.error.dark }
                                                        }}
                                                    >
                                                        X
                                                    </Button>
                                                )}
                                            </Grid>
                                            <Divider />
                                        </div>
                                    ))}
                                    <Grid container>
                                        <Button
                                            variant="contained"
                                            onClick={() => addTraining(groupIndex)}
                                            sx={{
                                                margin: '16px 8px 16px 0',
                                                borderRadius: '8px',
                                                background: theme.palette.success.dark,
                                                '&:hover': { background: theme.palette.success.main }
                                            }}
                                        >
                                            +
                                        </Button>
                                    </Grid>
                                </div>
                                {groupIndex === trainingPlanTrainings.length - 1 ? (
                                    <Button variant="contained" size="small" onClick={addGroup}>
                                        New Group
                                    </Button>
                                ) : (
                                    <></>
                                )}
                            </div>
                        ))}

                        <Grid item>
                            <Button
                                variant="contained"
                                onClick={openTagForm}
                                sx={{
                                    margin: '16px 0 24px',
                                    borderRadius: '8px',
                                    background: theme.palette.success.dark,
                                    '&:hover': { background: theme.palette.success.main }
                                }}
                            >
                                <FormattedMessage id="Add New Tag" />
                            </Button>
                            <Autocomplete
                                multiple={true}
                                options={trainingPlanTags}
                                value={
                                    !!formik.values.trainingPlanTags && formik.values.trainingPlanTags.length
                                        ? [...formik.values.trainingPlanTags]
                                        : []
                                }
                                getOptionLabel={(option) => option.name}
                                onChange={(event, key) => handleTag(event, key)}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label={intl.formatMessage({ id: 'Multiple Tags' })}
                                        onChange={(value) => handleTag(value)}
                                    />
                                )}
                            />

                            <SimpleTagDialog open={tagForm} onClose={handleDialogClose} type="training" />
                        </Grid>

                        {/*<Grid item xs={12} sx={formBlockStyle}>*/}
                        {/*    <label htmlFor="picture" style={labelStyle}>*/}
                        {/*        Picture*/}
                        {/*    </label>*/}
                        {/*    <Button variant="contained" component="label">*/}
                        {/*        Upload*/}
                        {/*        <input type="file" name="picture" onChange={handlePicture} hidden accept="image/*"/>*/}
                        {/*    </Button>*/}
                        {/*    <IconButton color="primary" aria-label="upload picture" component="label">*/}
                        {/*        <input type="file" name="picture" onChange={handlePicture} hidden accept="image/*" />*/}
                        {/*        <PhotoCamera />*/}
                        {/*    </IconButton>*/}
                        {/*</Grid>*/}
                    </Grid>

                    <Grid item xs={12}>
                        <span className="careployee-form__submit-button">
                            <Button variant="contained" onClick={submitForm} size="large">
                                <FormattedMessage id="Submit" />
                            </Button>
                        </span>
                    </Grid>
                </MainCard>
            </form>
            <SimpleDialog selectedValue={selectedValue} open={open} onClose={handleClose} />
        </Grid>
    );
}

export default TrainingPlanForm;
