import { Box, CircularProgress, Divider, Typography } from '@mui/material';
import { Button, Card } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import ChairIcon from '@mui/icons-material/Chair';
import Dashboard1 from 'assets/images/widget/dashborad-1.jpg';
import Intensity from 'stories/Intensity/Intensity';
import { KeyboardArrowUp } from '@mui/icons-material';
import KeyboardDoubleArrowUpIcon from '@mui/icons-material/KeyboardDoubleArrowUp';
import LinearProgress from '@mui/material/LinearProgress';
import ProgressBar from './TrainingPlanProgressBar';
import Vimeo from '@u-wave/react-vimeo';
import { addTrainingPlanPointsToUser } from 'services/GamificationService';
import { getUserById } from 'services/userService';
import { loggedInUser } from 'contexts/JWTContext';
import { makeStyles } from '@mui/styles';
import { openDrawer } from 'store/slices/menu';
import { showMessage } from 'utils/form/Communication';
import { useDispatch } from 'react-redux';

// assets

// material-ui

// assets

const useStyles = makeStyles((theme) => ({
    LinearRoot: {
        borderRadius: '10px',
        backgroundColor: '#CBAB70'
    },
    videoContainer: {
        width: '100%',
        maxHeight: 600,
        minWidth: 300,
        minHeight: 150,
        [theme.breakpoints.up('lg')]: {
            width: 1200
        }
    },
    progressContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '20%'
    },
    responsiveDiv: {
        display: 'flex',
        justifyContent: 'space-between',
        [theme.breakpoints.down('sm')]: {
            flexDirection: 'column'
        }
    },
    progressBarContainer: {
        width: '120px',
        height: '80%',
        border: 'none',
        borderRadius: '40px',
        backgroundColor: '#E3D4C9'
    },
    remainingPointsContainer: {
        backgroundColor: '#E3D4C9',
        borderTopLeftRadius: '30px',
        borderTopRightRadius: '30px',
        borderBottomLeftRadius: '30px',
        borderBottomRightRadius: '30px',
        width: '120px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    progressPointsContainer: {
        backgroundColor: '#CBAB70',
        borderBottomLeftRadius: '30px',
        borderBottomRightRadius: '30px',
        borderTopLeftRadius: '30px',
        borderTopRightRadius: '30px',
        width: '120px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    pointsText: {
        fontSize: '20px',
        color: 'white'
    }
}));

export default function TrainingPlayPage() {
    const location = useLocation();
    const trainingPlan = location.state.trainingPlan;
    const trainingIndex = location.state.trainingIndex;
    const groupIndex = location.state.groupIndex;
    
    

    const defaultVideo = '';

    const getVimeoVideoSrc = () => {
        const trainingVideoLink = trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex]?.training?.linkToVideo;
        return trainingVideoLink ?? defaultVideo;
    };
    return DisplayInfo(trainingPlan, groupIndex, trainingIndex, getVimeoVideoSrc());
}

/**
 * Display certain information of the trainingInfo
 * @param trainingPlan training plan containing trainingInfos in the form: [id, duration, sequence, repetition, note, training]
 * @param trainingIndex the index of the current trainingInfo
 */

function DisplayInfo(trainingPlan, groupIndex, trainingIndex, videoSrc) {

    

    const [paused, setPaused] = React.useState(true);
    const [done, setDone] = React.useState(false);
    const [isVideoReady, setIsVideoReady] = useState(false);
    const [isVideoPlay, setVideoPlay] = useState(false);
    const [outerProgress, setOuterProgress] = useState(0);

    const isLastGroup = groupIndex === trainingPlan.trainingGroups.length - 1;
    const isLastTrainingWithinGroup = trainingIndex === trainingPlan.trainingGroups[groupIndex].trainingInfos.length - 1;
    const isLastVideo = isLastGroup && isLastGroup;
    const isLastVideoCompleted = isLastVideo && done;

    const [userInfo, setUserInfo] = useState([]);
    const dispatch = useDispatch();
    const classes = useStyles();
    const navigate = useNavigate();
    const equipmentIconsMapping = {
        CHAIR: <ChairIcon />
    };

    const getProgressForGoal = () => (userInfo.points / userInfo?.rewardGoal?.rewardPoints) * 100;
    const getRemainingPointsToGoal = () => {
        return (userInfo.rewardGoal?.rewardPoints - userInfo.points || 0) > 0 ? userInfo.rewardGoal?.rewardPoints - userInfo.points : 0;
    };

    const getWindowSize = () => {
        const { innerWidth, innerHeight } = window;
        return { innerWidth, innerHeight };
    };
    const [windowSize, setWindowSize] = React.useState(getWindowSize());
    React.useEffect(() => {
        const handleWindowResize = () => {
            setWindowSize(getWindowSize());
        };
        window.addEventListener('resize', handleWindowResize);
        return () => {
            window.removeEventListener('resize', handleWindowResize);
        };
    }, []);

    useEffect(() => {
        dispatch(openDrawer(false));
    }, []);

    useEffect(() => {
        getUserById(loggedInUser.id)
            .then((res) => res.data)
            .then((user) => {
                setUserInfo(user.userInfo);
            })
            .catch((e) => dispatch(showMessage(e.message)));
    }, []);

    const onVideoReady = () => {
        setPaused(false);
    };

    useEffect(() => {
        const timeout = setTimeout(() => {
            setIsVideoReady(true);
        }, 10000);

        return () => clearTimeout(timeout);
    }, []);

    const handleComplete = () => {
        addTrainingPlanPointsToUser(loggedInUser.id, trainingPlan.id);
    };

    const PointsProgressBar = ({ value }) => {
        const remainingPoints = getRemainingPointsToGoal();
        const progressForGoal = getProgressForGoal();
        const totalPoints = remainingPoints + progressForGoal;
        const remainingHeight = (remainingPoints / totalPoints) * 100;
        const progressHeight = (progressForGoal / totalPoints) * 100;

        return (
            <Box className={classes.progressBarContainer}>
                <Box className={classes.remainingPointsContainer} style={{ height: `${remainingHeight}%` }}>
                    {remainingPoints !== null && remainingPoints !== 0 && <span className={classes.pointsText}>{remainingPoints}</span>}
                </Box>
                <Box className={classes.progressPointsContainer} style={{ height: `${progressHeight}%` }}>
                    {progressForGoal !== null && progressForGoal !== 0 && <span className={classes.pointsText}>{progressForGoal}</span>}
                </Box>
            </Box>
        );
    };

    return (
        <div>
            {/* {isLastVideoCompleted ? ( */}
            {false ? (
                <div>
                    <Typography onClick={() => navigate('/dashboard/default')} style={{ cursor: 'pointer' }}>
                        Back to Dashboard
                    </Typography>
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between'
                        }}
                    >
                        <div style={{ display: 'flex', flexDirection: 'column', marginTop: '30px' }}>
                            <Typography variant="h1">Congratulations! You did it</Typography>
                            <Button
                                onClick={() => handleComplete()}
                                variant="contained"
                                style={{
                                    backgroundColor: '#CBAB70',
                                    padding: '10px',
                                    borderRadius: '18px',
                                    width: '30em',
                                    marginTop: '40px'
                                }}
                            >
                                <div style={{ display: 'flex', flexDirection: 'column' }}>
                                    <p style={{ margin: 0, padding: '5px' }}>Finish your training</p>{' '}
                                    <p style={{ margin: 0, padding: 0 }}>to get your desired points.</p>
                                </div>
                            </Button>
                            <div style={{ display: 'flex', marginTop: '40px', marginLeft: '-30px' }}>
                                <div
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginTop: '30px'
                                    }}
                                >
                                    <Typography variant="h3" sx={{ fontSize: '20px' }}>
                                        + {trainingPlan.points}
                                    </Typography>
                                    <Typography variant="h3">Points</Typography>
                                    <KeyboardDoubleArrowUpIcon style={{ marginBottom: 0, paddingBottom: 0, fontSize: '150px' }} />
                                    <KeyboardArrowUp style={{ marginTop: '-80px', paddingTop: 0, fontSize: '150px' }} />
                                </div>
                                <div style={{ marginTop: '40px' }}>
                                    <PointsProgressBar value={getProgressForGoal()} />
                                    {userInfo?.rewardGoal === null && <p>Please select a goal to get points.</p>}
                                </div>

                                <img
                                    src={
                                        userInfo.rewardGoal?.customReward?.picture
                                            ? userInfo.rewardGoal?.customReward.picture
                                            : '/images/dish.jpg'
                                    }
                                    alt="reward"
                                    style={{
                                        marginLeft: '20px',
                                        marginTop: '40px',
                                        borderRadius: '10px',
                                        height: '200px',
                                        width: '200px'
                                    }}
                                />
                            </div>
                        </div>

                        <div>
                            <img src="/images/congrats.jpg" alt="congrats" style={{ width: '800px', height: '600px' }} />
                        </div>
                    </div>
                </div>
            ) : (
                <div>
                    <div className={classes.responsiveDiv}>
                        <div style={{ flexGrow: 1 }}>
                            <Box
                                // className={classes.videoContainer}
                                sx={{
                                    height: '580px !important',
                                    width: '1000px !important',
                                    marginBottom: '170px',
                                    margin: 'auto'
                                }}
                            >
                                {isVideoReady ? (
                                    <Vimeo
                                        autoplay
                                        // video={'https://vimeo.com/723382089' || videoSrc}
                                        video={videoSrc}
                                        responsive
                                        // height={windowSize.innerHeight - 100}
                                        // width={windowSize.innerWidth - 50}
                                        onPlaying={(player) => onVideoReady()}
                                        onPlay={() => {
                                            setVideoPlay(true);
                                            setPaused(false);
                                        }}
                                        paused={paused}
                                        // onPause={() => setPaused(true)}
                                    />
                                ) : (
                                    <Box className={classes.progressContainer}>
                                        <CircularProgress size={100} style={{ color: '#cbab70' }} />
                                    </Box>
                                )}
                            </Box>
                            <Typography gutterBottom variant="subtitle1" component="div">
                                <h2 style={{ margin: 0, marginTop: 0 }}>
                                    {trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex]?.training.trainingName}{' '}
                                    {trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex]?.training.equipments.map(
                                        (equipment) => equipmentIconsMapping[equipment]
                                    )}
                                </h2>
                            </Typography>
                        </div>

                        <div
                            style={
                                {
                                    // WebkitScrollbar: {
                                    //     width: '10px'
                                    // },
                                    // WebkitScrollbarTrack: {
                                    //     borderRadius: '8px',
                                    //     backgroundColor: '#e7e7e7',
                                    //     border: '1px solid #cacaca',
                                    //     boxShadow: 'inset 0 0 6px rgba(0, 0, 0, .3)'
                                    // },
                                    // WebkitScrollbarThumb: {
                                    //     bordeRadius: '8px !important',
                                    //     backgroundColor: '#363636'
                                    // }
                                }
                            }
                        >
                            {/* <div>
                                <Typography variant="h2">Up Next: </Typography>
                                <Card style={{ marginTop: '20px', width: 230 }}>
                                    <img
                                        src={
                                            isLastTrainingWithinGroup
                                                ? isLastGroup
                                                    ? !trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex]?.training
                                                          ?.picture
                                                        ? Dashboard1
                                                        : trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex]?.training
                                                              ?.picture
                                                    : !trainingPlan.trainingGroups[groupIndex + 1].trainingInfos[trainingIndex]?.training
                                                          ?.picture
                                                    ? Dashboard1
                                                    : trainingPlan.trainingGroups[groupIndex + 1].trainingInfos[trainingIndex]?.training
                                                          ?.picture
                                                : !trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex + 1]?.training
                                                      ?.picture
                                                ? Dashboard1
                                                : trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex + 1]?.training
                                                      ?.picture
                                        }
                                        width="230"
                                        height="100"
                                        alt="training"
                                        style={{ objectFit: 'contain' }}
                                    />

                                    <Typography variant="h5" style={{ padding: '10px' }}>
                                        {isLastTrainingWithinGroup
                                            ? isLastGroup
                                                ? trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex]?.training
                                                      ?.trainingName
                                                : trainingPlan.trainingGroups[groupIndex + 1].trainingInfos[trainingIndex]?.training
                                                      ?.trainingName
                                            : trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex + 1]?.training
                                                  ?.trainingName}
                                    </Typography>
                                    <div style={{ display: 'flex', alignItems: 'center', padding: '10px' }}>
                                        <Intensity
                                            level={
                                                isLastTrainingWithinGroup
                                                    ? isLastGroup
                                                        ? trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex]?.training
                                                              ?.difficulty
                                                        : trainingPlan.trainingGroups[groupIndex + 1].trainingInfos[trainingIndex]?.training
                                                              ?.difficulty
                                                    : trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex + 1]?.training
                                                          ?.difficulty
                                            }
                                        ></Intensity>
                                    </div>
                                </Card>
                            </div> */}

                            <Box
                                sx={{
                                    maxHeight: '80vh',
                                    overflowY: 'scroll',
                                    paddingInline: '10px'
                                }}
                            >
                                {trainingPlan.trainingGroups.map((group, _groupIndex) => {
                                    const isActiveGroup = groupIndex === _groupIndex;
                                    return group.trainingInfos.map((trainingInfo, _trainingIndex) => {
                                        const isActiveTrainging = trainingIndex === _trainingIndex;
                                        const isActiveVideo = isActiveGroup && isActiveTrainging;
                                        return (
                                            <Box
                                                id={trainingInfo?.training.trainingName + '-' + _trainingIndex}
                                                key={trainingInfo?.training.trainingName + '-' + _trainingIndex}
                                            >
                                                {!_trainingIndex && (
                                                    <Box>
                                                        {group.groupName}
                                                        <Divider sx={{ marginTop: '4px' }} />
                                                    </Box>
                                                )}
                                                <Box
                                                    sx={{
                                                        display: 'flex',
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        backgroundColor: '#cbab70',
                                                        borderRadius: '6px',
                                                        width: '20px',
                                                        height: '20px',
                                                        padding: '4px',
                                                        position: 'relative',
                                                        top: '12px',
                                                        left: '-6px',
                                                        opacity: isActiveVideo ? 1 : 0.4
                                                    }}
                                                >
                                                    <Typography sx={{ fontSize: '16px', fontWeight: '400', color: 'white' }}>
                                                        {_trainingIndex + 1}
                                                    </Typography>
                                                </Box>
                                                <Card
                                                    style={{
                                                        marginBottom: '20px',
                                                        width: '220px',
                                                        opacity: isActiveVideo ? 1 : 0.4
                                                    }}
                                                >
                                                    <img
                                                        src={trainingInfo?.training?.picture}
                                                        width="220px"
                                                        height="140px"
                                                        alt="training"
                                                        style={{ objectFit: 'cover' }}
                                                    />
                                                    <Typography variant="h5" style={{ padding: '10px' }}>
                                                        {trainingInfo?.training?.trainingName}
                                                    </Typography>
                                                    {isActiveVideo && (
                                                        <LinearProgress
                                                            variant="determinate"
                                                            value={outerProgress}
                                                            sx={{
                                                                backgroundColor: '#cbab7075',
                                                                '& .MuiLinearProgress-bar': {
                                                                    backgroundColor: '#cbab70'
                                                                }
                                                            }}
                                                        />
                                                    )}
                                                </Card>
                                            </Box>
                                        );
                                    });
                                })}
                            </Box>

                            <div style={{ marginTop: '70px', marginLeft: '-30px', visibility: 'hidden' }}>
                                <ProgressBar
                                    trainingPlan={trainingPlan}
                                    trainingIndex={trainingIndex}
                                    groupIndex={groupIndex}
                                    paused={paused}
                                    setPaused={setPaused}
                                    done={done}
                                    setDone={setDone}
                                    isVideoReady={isVideoReady}
                                    isVideoPlay={isVideoPlay}
                                    setOuterProgress={setOuterProgress}
                                ></ProgressBar>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}
