import { Box, Button, CircularProgress, Grid, LinearProgress, Typography } from '@mui/material';

import BreathIcon from 'assets/images/breathe-session-icon.svg';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import FinishIcon from 'assets/images/finish-line-amico.svg';
import { FormattedMessage } from 'react-intl';
import React from 'react';
import Slide from '@mui/material/Slide';
import { addTrainingPlanPointsToUser } from 'services/GamificationService';
import { loggedInUser } from '../../../../contexts/JWTContext';
import { updateEndCountTrainingPlanTracking } from '../../../../services/trackingService';
import { updateTrainingPlanForUser } from '../../../../services/userService';
import { useNavigate } from 'react-router-dom';
import { withStyles } from '@mui/styles';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function ProgressBar(props) {
    const { trainingPlan, groupIndex, trainingIndex, paused, setOuterProgress } = props;
    const [progress, setProgress] = React.useState(0);
    const [progressTime, setProgressTime] = React.useState(0);
    const [lastTraining, setLastTraining] = React.useState(false);
    const [isLastGroup, setIsLastGroup] = React.useState(false);
    const progressRef = React.useRef(() => {});

    React.useEffect(() => {
        progressRef.current = () => {
            const _isLastTraining = trainingIndex >= trainingPlan.trainingGroups[groupIndex].trainingInfos.length - 1;
            const _isLastGroup = groupIndex >= trainingPlan.trainingGroups.length - 1;
            if (progress >= 100) {
                props.setDone(true);
                if (_isLastTraining) {
                    setLastTraining(true);
                    if (_isLastGroup) {
                        setIsLastGroup(true);
                        // props.setDone(true);
                    } else {
                        setProgress(0);
                        setProgressTime(0);
                        toComponentPlayVideo(_isLastGroup, _isLastTraining);
                        setOuterProgress(0);
                    }
                } else {
                    setProgress(0);
                    setProgressTime(0);
                    toComponentPlayVideo(_isLastGroup, _isLastTraining);
                    setOuterProgress(0);
                }
            } else {
                if (!paused) {
                    const _progress = progress + 100 / trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex]?.duration;
                    setProgress(_progress);
                    setProgressTime(progressTime + 1);
                    setOuterProgress(_progress);
                }
            }
        };
    });

    React.useEffect(() => {
        const timer = setInterval(() => {
            progressRef.current();
        }, 1000);

        return () => {
            clearInterval(timer);
        };
    }, []);

    const BorderLinearProgress = withStyles(() => ({
        root: {
            height: 230,
            width: 300,
            borderRadius: 5,
            transform: 'rotate(90deg)'
        },
        bar: {
            borderRadius: 5
        }
    }))(LinearProgress);

    const navigate = useNavigate();

    // Here is the fucking problem

    const toComponentPlayVideo = (_isLastGroup, _isLastTraining) => {
        const objToSendToNextPage = {
            trainingPlan,
            groupIndex: _isLastTraining ? groupIndex + 1 : groupIndex,
            trainingIndex: _isLastTraining ? 0 : trainingIndex + 1
        };

        props.setDone(false);
        updateTrainingPlanForUser(
            loggedInUser.id,
            trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex].id,
            100 * (trainingIndex / trainingPlan.trainingGroups[groupIndex].trainingInfos.length),
            null
        );
        setProgressTime(0);
        setProgress(0);

        // TODO adjust fake postpause
        // tarek remove this
        const trainingPostPause = trainingPlan.trainingGroups[groupIndex].trainingInfos[trainingIndex].postPause || 4000;
        const groupPostPause = trainingPlan.trainingGroups[groupIndex].postPause || 5000;

        // setTrainingPostPause(trainingPostPause);
        // setGroupPostPause(groupPostPause);
        if (_isLastTraining || lastTraining) {
            updateEndCountTrainingPlanTracking(trainingPlan.id);
            updateTrainingPlanForUser(loggedInUser.id, trainingPlan.id, null, true);
            addTrainingPlanPointsToUser(loggedInUser.id, trainingPlan.id);
            props.setPaused(true);
            setTimeout(() => {
                props.setPaused(false);
                if (_isLastGroup || isLastGroup) {
                    navigate('/activities/TrainingPlan/list/finish', { state: { trainingPlan } });
                } else {
                    // 
                    setTimeout(() => {
                        props.setPaused(false);
                        // 
                        // 
                    }, trainingPostPause);
                    navigate('/activities/TrainingPlan/list/playVideo', { state: objToSendToNextPage });
                }
            }, groupPostPause);

            /*
            return (
                <FinishPlan trainingInfoList={trainingInfoList} ind={ind} paused={paused}></FinishPlan>
            )
            */
        } else {
            props.setPaused(true);
            setTimeout(() => {
                props.setPaused(false);
                // 
                // 
                // navigate('/activities/TrainingPlan/list/playVideo', { state: objToSendToNextPage });
            }, trainingPostPause);
            navigate('/activities/TrainingPlan/list/playVideo', { state: objToSendToNextPage });
        }
    };

    const convertSecondsToTime = (seconds) => {
        var minutes = Math.floor(seconds / 60);
        var remainingSeconds = seconds % 60;

        var formattedMinutes = ('0' + minutes).slice(-2);
        var formattedSeconds = ('0' + remainingSeconds).slice(-2);

        return formattedMinutes + ':' + formattedSeconds;
    };

    return (
        <>
            <Grid container>
                <Grid item xs={12}>
                    <Grid container spacing={2} alignItems="center" justifyContent="center">
                        <Grid item xs hidden={props.done}>
                            {/* <BorderLinearProgress variant="determinate" color="secondary" value={progress} /> */}
                            <Box
                                sx={{
                                    position: 'relative',
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    marginBottom: '40px'
                                }}
                            >
                                <CircularProgress variant="determinate" value={progress} size={100} sx={{ color: '#cbab70' }} />
                                <Box
                                    sx={{
                                        top: 0,
                                        left: 0,
                                        bottom: 0,
                                        right: 0,
                                        position: 'absolute',
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}
                                >
                                    <Typography
                                        sx={{ fontSize: '20px' }}
                                        variant="caption"
                                        component="div"
                                        color="text.secondary"
                                    >{`${Math.round(progress)}%`}</Typography>
                                </Box>
                            </Box>
                            <Box sx={{ display: 'flex', justifyContent: 'center' }}>{convertSecondsToTime(progressTime)}</Box>
                        </Grid>
                        {/*    <Grid item hidden={done}>
                            <Typography variant="h6">{Math.round(progressTime)}s</Typography>
    </Grid> */}

                        <Grid item hidden={!props.done} sx={{ textAlign: 'center', padding: 0, paddingTop: 0, paddingLeft: 0 }}>
                            <h2 style={{ marginTop: 0 }}>
                                <FormattedMessage id="Well Done!" />
                            </h2>
                            <Button
                                variant="contained"
                                onClick={toComponentPlayVideo}
                                sx={{
                                    backgroundColor: '#cbab70',
                                    '&:hover': {
                                        backgroundColor: '#a78d5c'
                                    }
                                }}
                            >
                                <FormattedMessage id={lastTraining ? 'Finish Training Plan' : 'Next training!'} />
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>

                <Dialog open={!!props.isVideoReady && !!props.isVideoPlay && paused} TransitionComponent={Transition} keepMounted>
                    <DialogTitle>
                        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            Take a pause! <CircularProgress size={30} sx={{ marginInlineStart: '20px' }} style={{ color: '#cbab70' }} />
                        </Box>
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            <Box>
                                <img
                                    src={BreathIcon}
                                    alt="Berry"
                                    style={{
                                        width: 400,
                                        maxWidth: '100%'
                                    }}
                                />
                            </Box>
                        </DialogContentText>
                    </DialogContent>
                </Dialog>
                <Dialog open={props.done} TransitionComponent={Transition} keepMounted>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            <Box
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    flexDirection: 'column',
                                    height: '400px'
                                }}
                            >
                                {/* <img
                                    src={FinishIcon}
                                    alt="Berry"
                                    style={{
                                        width: 400,
                                        maxWidth: '100%'
                                    }}
                                /> */}
                                <div style={{ fontSize: '300px', marginTop: '200px' }}>&#129395;</div>
                                <Button
                                    variant="contained"
                                    onClick={toComponentPlayVideo}
                                    sx={{
                                        backgroundColor: '#cbab70',
                                        '&:hover': {
                                            backgroundColor: '#a78d5c'
                                        }
                                    }}
                                >
                                    <FormattedMessage id={lastTraining ? 'Finish Training Plan' : 'Next training!'} />
                                </Button>
                            </Box>
                        </DialogContentText>
                    </DialogContent>
                </Dialog>
            </Grid>
        </>
    );
}
