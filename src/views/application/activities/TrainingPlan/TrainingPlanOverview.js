// material-ui

import { Adjust, ExpandMoreSharp, Favorite, PlayArrow, PlayCircleRounded } from '@mui/icons-material';
import { Avatar, CardMedia, IconButton, Typography } from '@mui/material';
import { Box, Button, CardContent, Divider, Grid, Link } from '@mui/material';
import { Card, CardActions } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { styled, useTheme } from '@mui/styles';

import CloseIcon from '@mui/icons-material/Close';
import Dashboard1 from 'assets/images/widget/dashborad-1.jpg';
import Dialog from '@mui/material/Dialog';
import { FormattedMessage } from 'react-intl';
import Intensity from 'stories/Intensity/Intensity';
import MainCard from '../../../../ui-component/cards/MainCard';
import MilitaryTechIcon from '@mui/icons-material/MilitaryTech';
import PropTypes from 'prop-types';
import Vimeo from '@u-wave/react-vimeo';
import { getDuration } from '../../../../services/TrainingPlanService';
import { getUserFavorites } from 'services/userService';
import { gridSpacing } from '../../../../store/constant';
import { loggedInUser } from 'contexts/JWTContext';

// assets

const mediaSX = {
    width: 90,
    height: 80,
    borderRadius: '12px'
};

function SimpleDialog(props) {
    const getWindowSize = () => {
        const { innerWidth, innerHeight } = window;
        return { innerWidth, innerHeight };
    };

    const { onClose, open, videoSrc } = props;
    const [windowSize, setWindowSize] = React.useState(getWindowSize());
    const handleClose = () => {
        onClose();
    };

    React.useEffect(() => {
        const handleWindowResize = () => {
            setWindowSize(getWindowSize());
        };
        window.addEventListener('resize', handleWindowResize);
        return () => {
            window.removeEventListener('resize', handleWindowResize);
        };
    }, []);

    return (
        <Dialog className="careployee-training-plan__popup" fullScreen onClose={handleClose} open={open} sx={{ padding: '0 0 40px' }}>
            <Grid item>
                <Link onClick={handleClose} className="careployee-training-plan__popup__menubar" sx={{ cursor: 'pointer' }}>
                    <CloseIcon fontSize="large" sx={{ cursor: 'pointer', color: 'white' }}></CloseIcon>
                </Link>
                <Vimeo height={windowSize.innerHeight - 100} width={windowSize.innerWidth - 50} autoplay video={videoSrc} />
            </Grid>
        </Dialog>
    );
}

SimpleDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    videoSrc: PropTypes.string.isRequired
};

const ExpandMore = styled((props) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest
    })
}));

// ==============================|| UI TIMELINE - CUSTOMIZED ||============================== //

export default function MakeOverview(props) {
    const theme = useTheme();

    return (
        <div>
            <Grid container alignItems="center" justifyContent="center">
                <Grid item xs={12} lg={10} sx={{ margin: '0 0 32px' }}>
                    <div title={<FormattedMessage id="Übungsgruppen Übersicht" values={{ title: props.item.title }} />} content={false}>
                        <div
                            style={{
                                position: 'relative',
                                height: '500px'
                            }}
                        >
                            {/* Image */}
                            <div
                                style={{
                                    position: 'absolute',
                                    top: '0',
                                    left: '0',
                                    height: '100%',
                                    width: '100%',
                                    backgroundImage: `url(${props.item.picture})`,
                                    backgroundSize: 'cover',
                                    backgroundPosition: 'center',
                                    zIndex: '1',
                                    borderRadius: '10px'
                                }}
                            ></div>
                            {/* Shade */}
                            <div
                                style={{
                                    position: 'absolute',
                                    top: '0',
                                    left: '0',
                                    height: '100%',
                                    width: '100%',
                                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                                    zIndex: '2'
                                }}
                            ></div>
                            {/* Play button */}
                            {/* 
                            <PlayArrow
                                style={{
                                    position: 'absolute',
                                    top: '50%',
                                    left: '50%',
                                    transform: 'translate(-50%, -50%)',
                                    color: 'white',
                                    zIndex: '3',
                                    fontSize: '64px',
                                    cursor: 'pointer'
                                }}
                                onClick={props.toComponentPlayVideo}
                            /> */}
                        </div>

                        <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: '20px' }}>
                            <Intensity level={props.item.levelOfAdvance}></Intensity>
                            <Button
                                variant="contained"
                                onClick={props.toComponentPlayVideo}
                                sx={{
                                    backgroundColor: '#cbab70',
                                    '&:hover': {
                                        backgroundColor: '#a78d5c'
                                    },
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}
                            >
                                <PlayArrow />
                            </Button>
                            <IconButton
                                disabled={props.item.isFavorit}
                                onClick={(event) => {
                                    event.stopPropagation();
                                    props.isFavorit ? props.RemoveAsFavorite(props.item.id) : props.setAsFavorite(props.item.id);
                                }}
                                sx={{
                                    height: '42px',
                                    borderRadius: '4px',
                                    color: props.isFavorit ? 'red' : '#BDBDBD',
                                    '& .MuiSvgIcon-root': { fontSize: '1.5rem' },
                                    float: 'right'
                                }}
                                size="large"
                            >
                                <Favorite />
                            </IconButton>
                        </Box>
                        <div>
                            <Grid container onClick={props.toComponentPlayVideo} style={{ width: '100%' }}>
                                <div style={{ width: '100%', display: 'flex', justifyContent: 'space-between', padding: '20px' }}>
                                    <Grid item xs={12} md={4}>
                                        <Typography component="div" align="left" variant="subtitle1" sx={{ paddingTop: '8px' }}>
                                            {props.item.information}
                                        </Typography>
                                    </Grid>
                                </div>
                            </Grid>
                        </div>
                    </div>
                </Grid>

                <Grid item xs={12} lg={10}>
                    <div title={<FormattedMessage id="Details Übersicht" />} content={false}>
                        <Typography align="left" variant="h3" style={{ textTransform: 'capitalize', padding: '20px' }}>
                            <FormattedMessage id="Details Übersicht" />
                        </Typography>

                        <div style={{ padding: '20px' }}>
                            <Grid
                                item
                                xs={12}
                                md={3}
                                container
                                spacing={gridSpacing}
                                alignItems="center"
                                sx={{
                                    position: 'relative',
                                    '&>*': {
                                        position: 'relative',
                                        zIndex: '5'
                                    },
                                    '&:after': {
                                        content: '""',
                                        position: 'absolute',
                                        left: 112,
                                        width: 2,
                                        height: 'calc(100% - 20px)',
                                        background: '#cbab70',
                                        zIndex: '1'
                                    },
                                    margin: 'auto'
                                }}
                            >
                                {props.item.trainingGroups.map((item) => (
                                    <Grid item xs={12} key={item.group}>
                                        <Grid container spacing={2}>
                                            <Grid item>
                                                <Grid container spacing={2} alignItems="center" sx={{ width: '125px' }}>
                                                    <Grid item xs zeroMinWidth>
                                                        <Typography align="left" variant="caption">
                                                            {/* {getDuration(item.duration)} */}
                                                            {item.groupDuration}
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item>
                                                        <Avatar sx={{ background: '#F9FAFB', color: '#cbab70' }}>
                                                            <Adjust />
                                                        </Avatar>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                            <Grid item xs>
                                                <Grid container spacing={1}>
                                                    <Grid item xs={12}>
                                                        <Typography
                                                            component="div"
                                                            align="left"
                                                            variant="subtitle1"
                                                            sx={{ paddingTop: '8px' }}
                                                        >
                                                            {item.groupName}
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                ))}
                            </Grid>
                        </div>

                        {props.item.trainingGroups.map((groupObj) => (
                            <div content={false} key={groupObj.groupName}>
                                <Typography variant="h4" style={{ padding: '10px' }}>
                                    {groupObj.groupName}
                                </Typography>
                                <Grid container spacing={gridSpacing}>
                                    {groupObj?.trainingInfos?.map((trainingInfo, index) => (
                                        <Grid
                                            item
                                            // hidden={trainingInfo.groupName !== groupObj.groupName}
                                            md={4}
                                            style={{
                                                // marginBottom: '10px',
                                                boxShadow: 'none',
                                                transition: 'box-shadow 0.3s',
                                                ':hover': { boxShadow: '0 0 10px rgba(0, 0, 0, 0.2)' }
                                            }}
                                            // sm={4}
                                            key={trainingInfo.id}
                                        >
                                            <Grid container alignItems="center" spacing={2} sx={{ padding: '12px 24px' }}>
                                                <Grid item>
                                                    <Box
                                                        sx={{
                                                            display: 'flex',
                                                            justifyContent: 'center',
                                                            alignItems: 'center',
                                                            backgroundColor: '#cbab70',
                                                            borderRadius: '6px',
                                                            width: '20px',
                                                            height: '20px',
                                                            padding: '4px',
                                                            position: 'relative',
                                                            top: '14px',
                                                            left: '-8px'
                                                        }}
                                                    >
                                                        <Typography sx={{ fontSize: '16px', fontWeight: '400', color: 'white' }}>
                                                            {index + 1}
                                                        </Typography>
                                                    </Box>
                                                    <Link
                                                        onClick={() => props.handleClickOpen(trainingInfo.training.linkToVideo)}
                                                        sx={{ cursor: 'pointer' }}
                                                    >
                                                        {!!trainingInfo.training.picture ? (
                                                            <div className="careployee-training-plan__image">
                                                                <img
                                                                    src={trainingInfo.training.picture}
                                                                    alt={trainingInfo.training.trainingName}
                                                                ></img>
                                                            </div>
                                                        ) : (
                                                            <img
                                                                src={Dashboard1}
                                                                alt="dashboard"
                                                                style={{ width: '50px', height: '50px' }}
                                                            ></img>
                                                        )}
                                                    </Link>
                                                </Grid>
                                                <Grid item xs zeroMinWidth>
                                                    <Grid container spacing={1}>
                                                        <Grid item xs={12}>
                                                            <Link
                                                                onClick={() => props.handleClickOpen(trainingInfo.training.linkToVideo)}
                                                                sx={{ cursor: 'pointer', textDecoration: 'none' }}
                                                            >
                                                                <Typography
                                                                    align="left"
                                                                    component="div"
                                                                    variant="subtitle1"
                                                                    style={{ textTransform: 'capitalize' }}
                                                                >
                                                                    {trainingInfo.training.trainingName}
                                                                </Typography>
                                                                <Typography align="left" component="div" variant="caption">
                                                                    {trainingInfo.training.description}
                                                                </Typography>
                                                                <Typography align="left" component="div" variant="subtitle1">
                                                                    {trainingInfo.training.difficulty} | {trainingInfo.duration}
                                                                </Typography>
                                                            </Link>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    ))}
                                </Grid>
                                <Divider />
                            </div>
                        ))}
                    </div>
                </Grid>
            </Grid>
            <SimpleDialog videoSrc={props.videoSrc} open={props.open} onClose={props.handleClose} />
        </div>
    );
}
