import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { getAllTrainingPLanByLocation } from 'services/TrainingPlanService';
import { loadAllTrainingTags } from 'services/TrainingService';
import { showMessage } from 'utils/form/Communication';
import Slider from 'react-slick';
import { MealIcon, TagIconButton, tagsSetting } from '../../Recipes/RecipesCards';
import { Grid } from '@mui/material';
import { setting } from '.';
import TrainingPlanCard from './TrainingPlanCard';

const TagManu = () => {
    const [allTraining, setAllTraining] = useState([]);
    const [tags, setTags] = useState([]);
    const [selectedTags, setSelectedTags] = useState('');
    const [trainingByTag, setTrainingByTag] = useState([]);
    const dispatch = useDispatch();
    const { location, tagname } = useParams();
    const nagivate = useNavigate();

    useEffect(() => {
        getAllTrainingPLanByLocation(location)
            .then((res) => setAllTraining(res.data))
            .then((data) => [console.log(data)])
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    useEffect(() => {
        loadAllTrainingTags()
            .then((res) => setTags(res.data))
            .catch((e) => {
                dispatch(showMessage(e.message));
            });
    }, []);

    useEffect(() => {
        if (tagname && allTraining) {
            setSelectedTags(tagname);
            setTrainingByTag(getTrainingByTag(tagname));
        } else {
            
        }
    }, [tagname, allTraining]);

    function getTrainingByTag(tag) {
        if (!allTraining) {
            console.error('Recipes data not loaded yet');
            return [];
        }
        const trainingTag = allTraining?.filter((training) => training?.trainingPlanTags?.some((trainingtag) => trainingtag?.name === tag));
        return trainingTag;
    }

    const handleTagClick = (tagname) => {
        setSelectedTags(tagname);
        setTrainingByTag(getTrainingByTag(tagname));
        const location = 'HOME';
        nagivate(`/activities/TrainingPlan/tag-menu/${location}/${tagname}`);
    };
    return (
        <>
            <div style={{ margin: '20px' }}>
                <Slider {...tagsSetting} style={{ width: '100%', transform: 'none' }}>
                    {tags &&
                        tags?.map((tab, index) => (
                            <Grid item xs={2} key={tab.id}>
                                <TagIconButton
                                    name={tab?.name}
                                    IconComponent={MealIcon}
                                    onClick={() => handleTagClick(tab.name)}
                                    selected={selectedTags === tab.name} // check if the tag is selected
                                />
                            </Grid>
                        ))}
                </Slider>
            </div>
            {trainingByTag.length > 0 && (
                <div style={{ margin: '20px' }}>
                    <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                        {trainingByTag?.map((row) => {
                            row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                            return (
                                <TrainingPlanCard key={row.id} onSetAsFavorite={() => console.log('set as favorite')} row={{ ...row }} />
                            );
                        })}
                    </Slider>
                </div>
            )}
        </>
    );
};

export default TagManu;
