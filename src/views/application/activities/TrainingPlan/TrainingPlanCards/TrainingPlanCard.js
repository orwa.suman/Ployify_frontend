// material-ui

import 'bootstrap/dist/css/bootstrap.min.css';

import { addActivityToUserFavorites, removeActivityFromUserFavorites } from 'services/userService';

import { Grid } from '@mui/material';
import ProductCard from 'ui-component/cards/ProductCard';
import React from 'react';
import { getDuration } from '../../../../../services/TrainingPlanService';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from '../../../../../utils/form/Communication';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';

// third party

const TrainingPlanCard = ({ row, makeMeFavourate, setMakeMeFavourate }) => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    let [isFavorit, setIsFavorit] = React.useState(row.isFavorit);

    // legacy code
    const trainingPlanDurationSum = (trainingInfos = []) => {
        if (trainingInfos != null) {
            return getDuration(trainingInfos.map((info) => info?.duration).reduce((partialSum, a) => partialSum + a, 0));
        }
        return '0';
    };

    const setAsFavorite = (activityId) => {
        addActivityToUserFavorites({ userId: loggedInUser.id, activityId: activityId, activityNum: 1 })
            .then((res) => res?.data)
            .then(() => {
                setIsFavorit(true);
                setMakeMeFavourate(true);
            })
            .catch((e) => {
                dispatch(showMessage(e.message));
            });
        setMakeMeFavourate(false);
    };

    const removeFromFavourate = (activityId) => {
        removeActivityFromUserFavorites({ userId: loggedInUser.id, activityId: activityId, activityNum: 1 })
            .then((res) => res?.data)
            .then(() => {
                setIsFavorit(false);
                setMakeMeFavourate(true);
            })
            .catch((e) => {
                dispatch(showMessage(e.message));
            });
        setMakeMeFavourate(false);
    };

    return (
        <div
            onClick={() =>
                navigate('/activities/TrainingPlan/list/TrainingPlanMainScreen', {
                    state: { ...row, isFavorit }
                })
            }
        >
            <ProductCard
                id={row.id}
                onAddFavorite={setAsFavorite}
                removeFromFavourate={removeFromFavourate}
                image={row.icon}
                isFavorit={row.isFavorit}
                makeMeFavourate={makeMeFavourate}
                mostUsed={row.isMostUsed}
                picture={row.picture}
                title={row.title}
                information={row.information}
                levelOfAdvance={row.levelOfAdvance}
                levelOfAdvanceAdvance={row.levelOfAdvance}
                //duration={trainingPlanDurationSum(row.trainingInfos)}
                duration={row.totalDuration}
            />
        </div>
    );
};

export default TrainingPlanCard;
