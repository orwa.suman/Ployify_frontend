import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { ArrowBackIos, ArrowForwardIos } from '@mui/icons-material';
import { Box, CircularProgress, Divider, Grid, Typography } from '@mui/material';
import { FormattedMessage, useIntl } from 'react-intl';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { MealIcon, TagIconButton, tagsSetting } from '../../Recipes/RecipesCards';
import { getUserFavorites, getUserHistory } from 'services/userService';
import { useEffect, useState } from 'react';

import React from 'react';
import Slider from 'react-slick';
import TrainingPlanCard from './TrainingPlanCard';
import { getAllTrainingPLanByLocation } from '../../../../../services/TrainingPlanService';
import { getMostUsedTrainingPlans } from '../../../../../services/AnalyzeToolService';
import { loadAllTrainingTags } from 'services/TrainingService';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from '../../../../../utils/form/Communication';
import { useDispatch } from 'store';
import { useTheme } from '@mui/material/styles';

// material-ui

// third party

export const customPrevArrow = <ArrowBackIos />;
export const customNextArrow = <ArrowForwardIos />;

export const setting = {
    dots: false,
    arrow: false,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 2,
    prevArrow: customPrevArrow,
    nextArrow: customNextArrow,
    responsive: [
        {
            breakpoint: 1920,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 1440,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
};

const TrainingPlanCards = () => {
    const dispatch = useDispatch();
    let { location } = useParams();
    let [data, setData] = React.useState([]);
    let [favorites, setFavorites] = React.useState([]);
    let [trackingData, setTrackingData] = React.useState([]);
    let [trainingPlanLocation, setTrainingPlanLocation] = React.useState('');
    const [latest, setLatest] = React.useState([]);
    const [latestLoading, setLatestLoading] = React.useState(true);
    const [training, setTraining] = React.useState([]);
    const [mostUsed, setMostUsed] = React.useState([]);
    const [allTraining, setAllTraining] = React.useState([]);
    const [makeMeFavourate, setMakeMeFavourate] = React.useState(false);
    const nagivate = useNavigate();

    useEffect(() => {
        getAllTrainingPLanByLocation(location)
            .then((res) => setAllTraining(res.data))
            .then((data) => {
                setTraining(data);
            })
            .catch((e) => {
                // dispatch(showMessage(e?.response?.data?.message));
            });
    }, [makeMeFavourate, location]);

    useEffect(() => {
        setLatestLoading(true);
        getUserHistory(loggedInUser.id, 1)
            .then((res) => res.data)
            .then((data) => {
                
                setLatest(data);
                setLatestLoading(false);
            })
            .catch((e) => {
                //  dispatch(showMessage(e.message));
                
                setLatestLoading(false);
            });
    }, [location]);

    // useEffect(() => {
    //     getMostUsedTrainingPlans()
    //         .then((res) => res.data)
    //         .then((r) => {
    //             
    //             setTrackingData(r);
    //             setMostUsed(data);
    //             setData(data);
    //         })
    //         .catch((e) => {
    //             // dispatch(showMessage(e?.response?.data?.message));
    //         });
    // }, [location]);

    // const loadMostUsed = () =>
    //     getMostUsedTrainingPlans()
    //         .then((res) => res.data)
    //         .then((r) => {
    //             setTrackingData(r);
    //             setData(data);
    //         })
    //         .catch((e) => {
    //             // dispatch(showMessage(e?.response?.data?.message));
    //         });

    useEffect(() => {
        getUserFavorites(loggedInUser.id, 1)
            .then((res) => res.data)
            .then((data) => {
                // let userFavorites = [];
                // data.forEach((favorit) => {
                //     if (
                //         !userFavorites.find(
                //             (intermediateFavorit) => intermediateFavorit.careployeeBaseEntity?.id === favorit.careployeeBaseEntity?.id
                //         )
                //     ) {
                //         userFavorites = [...userFavorites, favorit];
                //     }
                // });
                setFavorites(data);
            })
            .catch((e) => {
                // dispatch(showMessage(e.message));
            });
    }, [location]);

    const loadFavorites = (onFavoriteTab = false) =>
        getUserFavorites(loggedInUser.id, 1)
            .then((res) => res.data)
            .then((data) => {
                let userFavorites = [];
                data.forEach((favorit) => {
                    if (
                        !userFavorites.find(
                            (intermediateFavorit) => intermediateFavorit.careployeeBaseEntity?.id === favorit.careployeeBaseEntity?.id
                        )
                    ) {
                        userFavorites = [...userFavorites, favorit];
                    }
                });
                setFavorites(userFavorites);
                setData(onFavoriteTab ? userFavorites : data);
            })
            .catch((e) => {
                //  dispatch(showMessage(e.message));
            });

    if (location !== trainingPlanLocation) {
        setTrainingPlanLocation(location);
        // loadMostUsed();
        loadFavorites();
    }
    useEffect(() => {
        loadFavorites();
    }, [makeMeFavourate, location]);

    const isFavorit = (id) => (!!favorites && favorites.length ? favorites : []).some((favorit) => favorit.careployeeBaseEntity.id === id);

    function getTrainingByTag(tag) {
        if (!allTraining) {
            console.error('Recipes data not loaded yet');
            return [];
        }
        const trainingTag = allTraining?.filter((training) => training?.trainingPlanTags?.some((trainingtag) => trainingtag?.name === tag));
        return trainingTag;
    }
    const [tags, setTags] = useState([]);
    const [trainingByTag, setTrainingByTag] = useState([]);
    const [selectedTags, setSelectedTags] = useState('');

    useEffect(() => {
        loadAllTrainingTags()
            .then((res) => setTags(res.data))
            .catch((e) => {
                dispatch(showMessage(e.message));
            });
    }, []);

    const handleTagClick = (tagname) => {
        nagivate(`/activities/TrainingPlan/tag-menu/${location}/${tagname}`);
        setSelectedTags(tagname);
        setTrainingByTag(getTrainingByTag(tagname));
    };

    const favouratesResult = allTraining?.filter((recipe) =>
        favorites?.find((favRecipe) => favRecipe?.careployeeBaseEntity?.id === recipe?.id)
    );

    const latestUsedResult = allTraining?.filter((recipe) =>
        latest?.find((favRecipe) => favRecipe?.careployeeBaseEntity?.id === recipe?.id)
    );

    return (
        <div>
            {latestLoading && (
                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                    <CircularProgress size={100} style={{ color: '#cbab70' }} />
                </Box>
            )}
            {latestUsedResult && latestUsedResult?.length > 0 && (
                <div>
                    <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                        Latest Used
                    </Typography>
                    <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                        {latestUsedResult?.map((row) => {
                            row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                            return (
                                <TrainingPlanCard
                                    key={row.id}
                                    row={{ ...row, isFavorit: isFavorit(row.id), isMostUsed: trackingData.includes(row.id) }}
                                    makeMeFavourate={makeMeFavourate}
                                    setMakeMeFavourate={setMakeMeFavourate}
                                />
                            );
                        })}
                    </Slider>
                </div>
            )}
            {tags && tags.length > 0 && <Divider sx={{ margin: '32px 0' }} />}
            {false && (
                <div style={{ margin: '20px' }}>
                    <Slider {...tagsSetting} style={{ width: '100%', transform: 'none' }}>
                        {tags &&
                            tags?.map((tab, index) => (
                                <Grid item xs={2} key={tab.id}>
                                    <TagIconButton
                                        name={tab?.name}
                                        IconComponent={MealIcon}
                                        onClick={() => handleTagClick(tab.name)}
                                        selected={selectedTags === tab.name} // check if the tag is selected
                                    />
                                </Grid>
                            ))}
                    </Slider>
                </div>
            )}
            {trainingByTag.length > 0 && (
                <div style={{ margin: '5px' }}>
                    <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                        {trainingByTag?.map((row) => {
                            row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                            return (
                                <TrainingPlanCard
                                    key={row.id}
                                    row={{ ...row, isFavorit: isFavorit(row.id), isMostUsed: trackingData.includes(row.id) }}
                                    makeMeFavourate={makeMeFavourate}
                                    setMakeMeFavourate={setMakeMeFavourate}
                                />
                            );
                        })}
                    </Slider>
                </div>
            )}
            {mostUsed && mostUsed.length > 0 && <Divider sx={{ margin: '32px 0' }} />}
            {mostUsed && mostUsed.length > 0 && (
                <div>
                    <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                        Most Used
                    </Typography>
                    <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                        {mostUsed.map((row) => {
                            row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                            return (
                                <TrainingPlanCard
                                    key={row.id}
                                    row={{ ...row, isFavorit: isFavorit(row.id), isMostUsed: trackingData.includes(row.id) }}
                                    makeMeFavourate={makeMeFavourate}
                                    setMakeMeFavourate={setMakeMeFavourate}
                                />
                            );
                        })}
                    </Slider>
                </div>
            )}
            {favouratesResult && favouratesResult.length > 0 && <Divider sx={{ margin: '32px 0' }} />}
            {favouratesResult && favouratesResult.length > 0 ? (
                <div>
                    <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                        Favorites
                    </Typography>
                    <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                        {favouratesResult?.map((row) => {
                            // row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                            return (
                                <TrainingPlanCard
                                    key={row.id}
                                    row={{ ...row, isFavorit: isFavorit(row.id), isMostUsed: trackingData.includes(row.id) }}
                                    makeMeFavourate={makeMeFavourate}
                                    setMakeMeFavourate={setMakeMeFavourate}
                                />
                            );
                        })}
                    </Slider>
                </div>
            ) : (
                <div style={{ width: '100%' }}>
                    <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                        Favorites
                    </Typography>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <img src="/images/favourate.svg" alt="first favourate" style={{ textAlign: 'center' }} />
                    </div>
                </div>
            )}
            {allTraining && allTraining.length > 0 ? (
                <div>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            All Trainings
                        </Typography>
                        <Typography
                            component={Link}
                            to="/dashboard/activities/training/all"
                            style={{ marginLeft: '10px', cursor: 'pointer', textDecoration: 'none', color: 'lightGray', marginTop: '4px' }}
                        >
                            Show More
                        </Typography>
                    </div>
                    <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                        {allTraining.map((row) => {
                            row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                            return (
                                <TrainingPlanCard
                                    key={row.id}
                                    row={{ ...row, isFavorit: isFavorit(row.id), isMostUsed: trackingData.includes(row.id) }}
                                    makeMeFavourate={makeMeFavourate}
                                    setMakeMeFavourate={setMakeMeFavourate}
                                />
                            );
                        })}
                    </Slider>
                </div>
            ) : (
                <div>
                    {location === 'OFFICE' ? (
                        <div>
                            <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                                Trainings
                            </Typography>
                            <div style={{ display: 'flex', justifyContent: 'center' }}>
                                <img width={400} src="/images/officeTraining.png" alt="first favourate" style={{ textAlign: 'center' }} />
                            </div>
                        </div>
                    ) : (
                        <div>
                            <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                                Trainings
                            </Typography>
                            <div style={{ display: 'flex', justifyContent: 'center' }}>
                                <img width={400} src="/images/homeTraining.png" alt="first favourate" style={{ textAlign: 'center' }} />
                            </div>
                        </div>
                    )}
                </div>
            )}
        </div>
        /*    <div>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div" sx={{ margin: '0 0 4px' }}>
                        <FormattedMessage id={'Training plans @' + location} />
                    </Typography>
                </Grid>

                <Grid item xs={2}>
                    <HoverSocialCard
                        primary=" "
                        secondary={intl.formatMessage({ id: 'Latest used' })}
                        color={selectedTab === 'latestUsed' ? theme.palette.secondary.dark : theme.palette.secondary.main}
                        iconPrimary={Update}
                        onClick={() => showTab('latestUsed')}
                    />
                </Grid>

                <Grid item xs={2}>
                    <HoverSocialCard
                        primary=" "
                        secondary={intl.formatMessage({ id: 'Most used' })}
                        color={selectedTab === 'mostUsed' ? theme.palette.secondary.dark : theme.palette.secondary.main}
                        iconPrimary={WhatshotIcon}
                        onClick={() => showTab('mostUsed')}
                        active={selectedTab === 'mostUsed'}
                    />
                </Grid>

                <Grid item xs={2}>
                    <HoverSocialCard
                        primary=" "
                        secondary={intl.formatMessage({ id: 'Favourite' })}
                        color={selectedTab === 'favorite' ? theme.palette.secondary.dark : theme.palette.secondary.main}
                        iconPrimary={Favorite}
                        onClick={() => showTab('favorite')}
                        active={selectedTab === 'favorite'}
                    />
                </Grid>

                <Grid item xs={2}>
                    <HoverSocialCard
                        primary=" "
                        secondary={intl.formatMessage({ id: 'For You' })}
                        color={selectedTab === 'forYou' ? theme.palette.secondary.dark : theme.palette.secondary.main}
                        iconPrimary={SensorOccupied}
                        onClick={() => showTab('forYou')}
                        active={selectedTab === 'forYou'}
                    />
                </Grid>

                <Grid item xs={2}>
                    <HoverSocialCard
                        primary=" "
                        secondary={intl.formatMessage({ id: 'All' })}
                        color={selectedTab === 'all' ? theme.palette.secondary.dark : theme.palette.secondary.main}
                        iconPrimary={Apps}
                        onClick={() => showTab('all')}
                        active={selectedTab === 'all'}
                    />
                </Grid>

                <Grid item xs={12}>
                    <Divider sx={{ borderColor: 'grey.400' }} />
                </Grid>
                {

                }
            </Grid>
            </div> */
    );
};

export default TrainingPlanCards;
