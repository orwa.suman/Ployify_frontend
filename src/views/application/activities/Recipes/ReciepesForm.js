// ==============================|| FORM VALIDATION - LOGIN FORMIK  ||============================== //
import {
    Autocomplete,
    Button,
    Checkbox,
    Dialog,
    FormControlLabel,
    Grid,
    Radio,
    RadioGroup,
    TextField,
    Typography,
    useTheme
} from '@mui/material';
import { useFormik } from 'formik';
import React, { useState } from 'react';
import { loadDishTags, loadExtraDishes } from '../../../../services/DishService';
import { addRecipe, getRecipe, updateRecipe } from '../../../../services/RecipeService';
import MainCard from '../../../../ui-component/cards/MainCard';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';
import { showMessage } from '../../../../utils/form/Communication';
import FileUpload from '../../../forms/components/FileUpload';
import { useParams } from 'react-router-dom';
import { FormattedMessage, useIntl } from 'react-intl';
import DishTagForm from './DishTagForm';
import PropTypes from 'prop-types';
import TrainingsTagsForm from '../TrainingPlan/TrainingsTagsForm';
import { TreeItem, TreeView } from '@mui/lab';
import { CheckBox, ChevronRight, ExpandMoreOutlined } from '@mui/icons-material';
import { uploadToCloudinary } from 'services/CloudinaryService';
import { loadAuthors } from '../../../../services/AuthorService';

export function SimpleTagDialog(props) {
    const { open, onClose, type } = props;

    return (
        <Dialog onClose={onClose} open={open} fullWidth>
            
            <DishTagForm />
            {/* {
                type === 'training' ? <TrainingsTagsForm /> 
                    : type === 'recipe'? <DishTagForm />
                        : type === 'mentalHealth'? <MentalHealthTagForm />
                        : <TrainingsTagsForm/>
            } */}
        </Dialog>
    );
}

SimpleTagDialog.propTypes = {
    open: PropTypes.bool.isRequired
};

function RecipeForm() {
    const [level, setLevel] = React.useState('NORMAL');
    const { id } = useParams();
    const [dishes, setDishes] = React.useState([]);
    const [dishTags, setDishTags] = React.useState([]);
    const [extraDishes, setExtraDishes] = React.useState([]);
    const allExtraDishes = extraDishes;
    const [open, setOpen] = React.useState(false);
    const theme = useTheme();
    const intl = useIntl();

    const [picture, setPicture] = React.useState([]);
    const [pictureName, setPictureName] = React.useState([]);
    const [changed, setChanged] = React.useState(false);

    const [authors, setAuthors] = React.useState([]);
    const [author, setAuthor] = React.useState({});

    const dispatch = useDispatch();

    const loadTags = () => {
        loadDishTags()
            .then((res) => res.data)
            .then((data) => {
                setDishTags(data);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const requestAuthors = () => {
        const request = loadAuthors();
        request
            .then((res) => res.data)
            .then((data) => {
                setAuthors(data);
                if (data.length) {
                    formik.values.author = data[0];
                    setAuthor(data[0]);
                }
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    React.useEffect(() => {
        loadExtraDishes(true)
            .then((res) => res.data)
            .then((data) => {
                setExtraDishes(data);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    React.useEffect(() => {
        loadExtraDishes(false)
            .then((res) => res.data)
            .then((data) => {
                setDishes(data);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });

        loadTags();
        requestAuthors();

        if (id) {
            getRecipe(id)
                .then((res) => res.data)
                .then((data) => {
                    
                    formik.setValues({
                        name: data.name,
                        level: data.level,
                        totalDuration: data.totalDuration,
                        portions: data.portions,
                        dishInfos: data.dishInfos,
                        dishTags: data.dishTags,
                        author: data.author,
                        picture: data.picture
                    });
                })
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
    }, []);

    const openTagForm = () => {
        setOpen(true);
    };

    const handleClose = (value) => {
        setOpen(false);
        loadTags();
    };

    const submitForm = (event) => {
        formik.validateForm().then((errors) => {
            if (errors && Object.keys(errors).length > 0) {
                const errorMessages = Object.entries(errors).map(([fieldName, fieldError]) => {
                    return `${fieldName}: ${fieldError.props.id}`;
                });

                dispatch(showMessage(<FormattedMessage id={errorMessages.join(', ')} />));
            }
            formik.handleSubmit(event);
        });
    };

    const validationSchema = Yup.object({
        name: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(50, <FormattedMessage id="Input Max Length" values={{ length: 50 }} />)
            .required(<FormattedMessage id="Required" />),
        totalDuration: Yup.number()
            .min(1, <FormattedMessage id="Input Min Length" values={{ length: 1 }} />)
            .max(250, <FormattedMessage id="Input Max Length" values={{ length: 250 }} />)
            .required(<FormattedMessage id="Required" />),
        author: Yup.object().required(<FormattedMessage id="Required" />).nullable(),
        portions: Yup.number()
            .min(1, <FormattedMessage id="Input Min Length" values={{ length: 1 }} />)
            .max(50, <FormattedMessage id="Input Max Length" values={{ length: 50 }} />)
            //.required(<FormattedMessage id="Required" />)
    });

    const [selectedExtraDishesByDish, setSelectedExtraDishesByDish] = useState({});

    const renderTree = (dish) => (
        <TreeItem key={dish.id} nodeId={dish.id} label={dish.name}>
            {allExtraDishes &&
                allExtraDishes.map((extraDish) => (
                    <FormControlLabel
                        key={extraDish.id}
                        control={
                            <Checkbox
                                checked={selectedExtraDishesByDish[dish.id] && selectedExtraDishesByDish[dish.id].includes(extraDish)}
                                onChange={(e) => handleExtraDishChange(e.target.checked, extraDish, dish)}
                            />
                        }
                        label={extraDish.name}
                    />
                ))}
        </TreeItem>
    );

    const handleAddDishes = () => {
        formik.setFieldValue('dishInfos', [...formik.values.dishInfos, { dish: '', extras: [] }]);
    };

    const handleDishesChange = (event, value, index) => {
        const updatedDishSelections = [...formik.values.dishInfos];
        
        updatedDishSelections[index].dish = { 'id': value.id, 'name': value.name }; // value;
        formik.setFieldValue('dishInfos', updatedDishSelections);
    };

    const handleExtraDishesChange = (event, value, index) => {
        const updatedDishSelections = [...formik.values.dishInfos];
        

        const newValue = value.map(obj => {
            return { id: obj.id, name: obj.name };
        });

        
        updatedDishSelections[index].extras = newValue;
        formik.setFieldValue('dishInfos', updatedDishSelections);
    };

    const formik = useFormik({
        initialValues: {
            name: '',
            level: 'NORMAL',
            totalDuration: '',
            portions: 1,
            dishInfos: [{ dish: '', extras: [] }],
            dishTags: [],
            author: null,
            picture: ''
        },
        validationSchema,
        onSubmit: () => {
            //  const selectedExtraDishes = getSelectedExtraDishes();

            uploadToCloudinary(picture, pictureName, 'recipe_', '/Ployify/Activities/Recipe')
                .then((data) => {

                    if (changed) {
                        
                        
                        formik.values.picture = data.url;
                    }

                    if (id) {
                        updateRecipe(id, { ...formik.values })
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    } else {
                        addRecipe({ ...formik.values })
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    }
                })
                .catch((err) => {
                    
                });
        }
    });

    function handleChange(event) {
        formik.values.level = event.target.value;
        setLevel(formik.values.level);
    }

    function handleDish(event, value) {
        formik.setValues({ ...formik.values, dishList: value });
    }

    const handleExtraDishes = (value) => {
        formik.setValues({ ...formik.values, extraDishesList: value });
    };

    function handleTag(event, value) {
        formik.setValues({ ...formik.values, dishTags: value });
    }

    const handlePicture = (data, fileName) => {
        setPicture(data);
        setPictureName(fileName);
        setChanged(true);
    };

    const [dishSelections, setDishSelections] = useState([{ dish: '', extras: [] }]);

    const handleAddDishSelection = () => {
        setDishSelections([...dishSelections, { dish: '', extras: [] }]);
    };

    const handleDishChange = (event, value, index) => {
        const updatedDishSelections = [...dishSelections];
        updatedDishSelections[index].dish = value;
        setDishSelections(updatedDishSelections);
    };

    const handleExtraDishChange = (event, value, index) => {
        const updatedDishSelections = [...dishSelections];
        updatedDishSelections[index].extras = value;
        setDishSelections(updatedDishSelections);
    };

    return (
        <Grid>
            <form onSubmit={submitForm}>
                <MainCard title={'Add new Receipt'}>
                    <Grid container>
                        <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="name" className="careployee-form__label">
                                <FormattedMessage id="Receipt Title" />
                            </label>
                            <TextField
                                fullWidth
                                maxRows={4}
                                id="name"
                                value={formik.values.name}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.name && Boolean(formik.errors.name)}
                                helperText={formik.touched.name && formik.errors.name}
                            />
                        </Grid>

                        <Grid item xs={12} className="careployee-form__block">
                            <Grid item xs={12} className="careployee-form__block">
                                <FileUpload
                                    defaultPicture={formik.values.picture || '/images/dish.jpg'}
                                    onChange={handlePicture}
                                ></FileUpload>
                            </Grid>
                        </Grid>

                        <Grid item xs={12}>
                            <Grid container className="careployee-form__block">
                                <Grid item xs={12} md={2}>
                                    <label htmlFor="totalDuration" className="careployee-form__label">
                                        <FormattedMessage id="Receipt duration (Min)" />
                                    </label>
                                    <TextField
                                        fullWidth
                                        maxRows={4}
                                        id="totalDuration"
                                        label="Receipt Dauer"
                                        value={formik.values.totalDuration}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={formik.touched.totalDuration && Boolean(formik.errors.totalDuration)}
                                        helperText={formik.touched.totalDuration && formik.errors.totalDuration}
                                    />
                                </Grid>
                                <Grid item xs={12} md={5} sx={{ paddingLeft: '24px' }}>
                                    <label htmlFor="level" className="careployee-form__label">
                                        <FormattedMessage id="Level" />
                                    </label>
                                    <RadioGroup row value={level} onChange={handleChange}>
                                        <FormControlLabel label="Simple" value="SIMPLE" control={<Radio />} />
                                        <FormControlLabel label="Normal" value="NORMAL" control={<Radio />} />
                                        <FormControlLabel label="Advanced" value="ADVANCED" control={<Radio />} />
                                        <FormControlLabel label="Pro" value="PRO" control={<Radio />} />
                                    </RadioGroup>
                                </Grid>
                            </Grid>
                        </Grid>

                        <Grid item xs={12} sx={{ paddingLeft: '4px' }}>
                            <label htmlFor="portions" className="careployee-form__label">
                                <FormattedMessage id="Portions" />
                            </label>
                            <TextField
                                maxRows={4}
                                id="portions"
                                value={formik.values.portions}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.portions && Boolean(formik.errors.portions)}
                                helperText={formik.touched.portions && formik.errors.portions}
                            />
                        </Grid>

                        <Grid item xs={12} md={5} className="careployee-form__block">
                            <label htmlFor="author" className="careployee-form__label">
                                <FormattedMessage id="Author" />
                            </label>
                            <Autocomplete
                                style={{ height: '44px' }}
                                disablePortal
                                options={authors}
                                getOptionLabel={(option) => option.firstName + ' ' + option.lastName}
                                value={author}
                                onChange={(event, value) => {
                                    formik.values.author = value;
                                    setAuthor(value);
                                }}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        error={formik.errors.author}
                                        helperText={!!formik.errors.author && formik.errors.author}
                                    />
                                )}
                            />
                        </Grid>

                    </Grid>
                </MainCard>

                {/*   <MainCard title={<FormattedMessage id="Add dishes" />}>
                    <div>
                        <TreeView defaultCollapseIcon={<ExpandMoreOutlined />} defaultExpandIcon={<ChevronRight />}>
                            {dishes && dishes.map((dish) => (dish ? renderTree(dish) : null))}
                        </TreeView>
                    </div>
                    <Grid item sx={{ margin: '5px' }}>
                        <Autocomplete
                            multiple
                            disablePortal
                            required
                            onBlur={formik.handleBlur}
                            value={!!formik.values.dishList && formik.values.dishList.length ? [...formik.values.dishList] : []}
                            error={formik.touched.Autocomplete && Boolean(formik.errors.Autocomplete)}
                            helperText={formik.touched.Autocomplete && formik.errors.Autocomplete}
                            options={dishes}
                            getOptionLabel={(option) => option.name}
                            renderInput={(params) => <TextField {...params} label={intl.formatMessage({ id: 'Dish Name' })} />}
                            onChange={(event, value) => handleDish(event, value)}
                        />
                    </Grid>
    </MainCard> */}

                {/* <MainCard title={<FormattedMessage id="Assign extra dish" />} sx={{ margin: '0 0 24px' }}>
                    <Grid item xs={12}>
                        <Autocomplete
                            disablePortal
                            name="extraDishes"
                            multiple
                            value={
                                !!formik.values.extraDishesList && formik.values.extraDishesList.length
                                    ? [...formik.values.extraDishesList]
                                    : []
                            }
                            options={allExtraDishes}
                            getOptionLabel={(option) => option.name}
                            renderInput={(params) => <TextField {...params} label="Extra Name" />}
                            onChange={(_event, value) => {
                                handleExtraDishes(value);
                            }}
                        />
                    </Grid>
                </MainCard> */}
                <MainCard title={<Typography>Select Dish and Extra dishes</Typography>}>
                    <form onSubmit={formik.handleSubmit}>
                        {formik.values.dishInfos.map((dishSelection, index) => (
                            <div key={index} style={{ display: 'flex', justifyContent: 'space-around' }}>
                                <Autocomplete
                                    style={{ width: '40%', marginBottom: '10px' }}
                                    options={dishes}
                                    getOptionLabel={(option) => (option ? option.name : '')}
                                    onBlur={formik.handleBlur}
                                    value={dishSelection.dish}
                                    onChange={(event, value) => handleDishesChange(event, value, index)}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="Dish Name"
                                            error={formik.touched.dishSelections && Boolean(formik.errors.dishSelections)}
                                            helperText={formik.touched.dishSelections && formik.errors.dishSelections}
                                        />
                                    )}
                                />
                                <Autocomplete
                                    style={{ width: '40%', marginBottom: '10px' }}
                                    multiple
                                    options={extraDishes}
                                    getOptionLabel={(option) => option.name}
                                    onBlur={formik.handleBlur}
                                    value={dishSelection.extras}
                                    onChange={(event, value) => handleExtraDishesChange(event, value, index)}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="Extra Dishes"
                                            error={formik.touched.dishSelections && Boolean(formik.errors.dishSelections)}
                                            helperText={formik.touched.dishSelections && formik.errors.dishSelections}
                                        />
                                    )}
                                />
                            </div>
                        ))}
                        <Button className="careployee-form__submit-button" variant="contained" onClick={handleAddDishes}>
                            Add Dish Selection
                        </Button>
                    </form>
                </MainCard>

                <MainCard title={<FormattedMessage id="Tags" />}>
                    <Grid item>
                        <Button
                            variant="contained"
                            onClick={openTagForm}
                            sx={{
                                margin: '16px 0 24px',
                                borderRadius: '8px',
                                background: theme.palette.success.dark,
                                '&:hover': { background: theme.palette.success.main }
                            }}
                        >
                            <FormattedMessage id="Add New Tag" />
                        </Button>
                        <Autocomplete
                            multiple={true}
                            options={dishTags}
                            value={!!formik.values.dishTags && formik.values.dishTags.length ? [...formik.values.dishTags] : []}
                            getOptionLabel={(option) => option.name}
                            onChange={(event, key) => handleTag(event, key)}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label={intl.formatMessage({ id: 'Multiple Tags' })}
                                    onChange={(value) => handleTag(value)}
                                />
                            )}
                        />
                    </Grid>
                </MainCard>

                <Grid>
                    <Button className="careployee-form__submit-button" variant="contained" size="large" onClick={submitForm}>
                        <FormattedMessage id="Submit" />
                    </Button>
                </Grid>
            </form>
            <SimpleTagDialog open={open} onClose={handleClose} type="recipe" />
        </Grid>
    );
}

export default RecipeForm;
