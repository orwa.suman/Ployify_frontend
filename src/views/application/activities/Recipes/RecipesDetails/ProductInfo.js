import PropTypes from 'prop-types';

// material-ui
import { useTheme } from '@mui/material/styles';
import {
    Button,
    ButtonBase,
    ButtonGroup,
    Card,
    Divider,
    Grid,
    Rating,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableRow,
    Tooltip,
    Typography
} from '@mui/material';

// project imports
import Chip from 'ui-component/extended/Chip';
import Avatar from 'ui-component/extended/Avatar';

// assets
import CircleIcon from '@mui/icons-material/Circle';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import { AccessTimeFilledOutlined, Favorite, OutdoorGrill, OutdoorGrillOutlined, PunchClock } from '@mui/icons-material';
import React, { useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { RecipeLevels } from 'services/RecipeService';
import { addActivityToUserFavorites, getUserFavorites, isFavorit, removeActivityFromUserFavorites } from 'services/userService';
import { loggedInUser } from 'contexts/JWTContext';
import useMediaQuery from '@mui/material/useMediaQuery';
import theme from 'theme';

// ==============================|| COLORS OPTION ||============================== //
const clock = <PunchClock />;
const Colors = ({ checked, colorsData }) => {
    const theme = useTheme();
    return (
        <Grid item>
            <Tooltip title={colorsData[0].label}>
                <ButtonBase sx={{ borderRadius: '50%' }}>
                    <Avatar
                        color="inherit"
                        size="badge"
                        sx={{
                            bgcolor: colorsData[0].bg,
                            color: theme.palette.mode === 'light' ? 'grey.50' : 'grey.800'
                        }}
                    >
                        {checked && (
                            <CircleIcon sx={{ color: theme.palette.mode === 'light' ? 'grey.50' : 'grey.800', fontSize: '0.75rem' }} />
                        )}
                        {!checked && <CircleIcon sx={{ color: colorsData[0].bg, fontSize: '0.75rem' }} />}
                    </Avatar>
                </ButtonBase>
            </Tooltip>
        </Grid>
    );
};

Colors.propTypes = {
    checked: PropTypes.bool,
    colorsData: PropTypes.array
};

const Increment = ({ quantity, updateQuantity }) => {
    const [value, setValue] = useState(quantity);

    const incrementHandler = () => {
        setValue(value - 1);
        updateQuantity(value - 1);
    };

    const decrementHandler = () => {
        setValue(value + 1);
        updateQuantity(value + 1);
    };

    return (
        <ButtonGroup size="large" variant="text" color="inherit" sx={{ border: '1px solid', borderColor: 'grey.400' }}>
            <Button key="three" disabled={value <= 1} onClick={incrementHandler} sx={{ pr: 0.75, pl: 0.75, minWidth: '0px !important' }}>
                <RemoveIcon fontSize="inherit" />
            </Button>
            <Button key="two" sx={{ pl: 0.5, pr: 0.5 }}>
                {value}
            </Button>
            <Button key="one" onClick={decrementHandler} sx={{ pl: 0.75, pr: 0.75, minWidth: '0px !important' }}>
                <AddIcon fontSize="inherit" />
            </Button>
        </ButtonGroup>
    );
};

Increment.propTypes = {
    itemId: PropTypes.number,
    quantity: PropTypes.number,
    updateQuantity: PropTypes.func
};

// ==============================|| PRODUCT DETAILS - INFORMATION ||============================== //

const ProductInfo = ({ product, portion, dishTags, updateQuantity }) => {
    let [fav, setFav] = React.useState();
    const mobile = useMediaQuery('(max-width:600px)');
    const tablet = useMediaQuery('(max-width:768px)');
    const [favourates, setFavorites] = useState([]);

    const removeFromFavourate = (activityId) => {
        removeActivityFromUserFavorites({ userId: loggedInUser.id, activityId: activityId, activityNum: 1 })
            .then((res) => res?.data)
            .then(() => {
                setFav(true);
            })
            .catch((e) => {
                
            });
        setFav(false);
    };

    const setAsFavorite = (activityId) => {
        addActivityToUserFavorites({ userId: loggedInUser.id, activityId: activityId, activityNum: 1 })
            .then((res) => res?.data)
            .then(() => {
                setFav(true);
            })
            .catch((e) => {
                
            });
        setFav(false);
    };

    useEffect(() => {
        getUserFavorites(loggedInUser.id, 2)
            .then((res) => res.data)
            .then((resource) => {
                let userFavorites = [];
                resource.forEach((favorit) => {
                    if (
                        !userFavorites.find(
                            (intermediateFavorit) => intermediateFavorit.careployeeBaseEntity?.id === favorit.careployeeBaseEntity?.id
                        )
                    ) {
                        userFavorites = [...userFavorites, favorit];
                    }
                });

                setFavorites(userFavorites);
            })
            .catch((e) => {
                //  dispatch(showMessage(e.message));
            });
    }, [fav]);

    const isFavorit = (id) =>
        !!(!!favourates && favourates?.length ? favourates : []).find((favorit) => (favorit.id || favorit.careployeeBaseEntity.id) === id);

    
    return (
        <Card
            variant="outlined"
            sx={{
                padding: '20px',
                marginTop: '35px',
                minWidth: '500px',
                marginLeft: ['-130px', '0', '0'],
                width: '100%',
                [theme.breakpoints.up('lg')]: {
                    marginLeft: '-130px'
                }
            }}
        >
            <Grid container spacing={2} sx={{ marginTop: '8px' }}>
                <Grid item xs={12}>
                    <Stack direction="row" alignItems="center" justifyContent="space-between">
                        <Grid container spacing={1}>
                            <Grid item xs={12}>
                                <Stack direction="row" alignItems="center" spacing={1}>
                                    <Typography variant="h3">{product.name}</Typography>
                                </Stack>
                            </Grid>
                        </Grid>
                        <Favorite
                            style={{ cursor: 'pointer', color: isFavorit(product.id) ? 'red' : '#BDBDBD' }}
                            onClick={() => (isFavorit(product.id) ? removeFromFavourate(product.id) : setAsFavorite(product.id))}
                        />
                    </Stack>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="body2">{product.description}</Typography>
                </Grid>
                <Grid item xs={12}>
                    <Stack direction="row" alignItems="center" spacing={1}>
                        <Rating
                            name="simple-controlled"
                            value={RecipeLevels[product.level]}
                            max={4}
                            icon={<OutdoorGrill fontSize="inherit" />}
                            emptyIcon={<OutdoorGrillOutlined fontSize="inherit" />}
                            precision={0.1}
                            readOnly
                        />
                    </Stack>
                </Grid>
                <Grid item xs={12}>
                    <Stack direction="row" alignItems="center" spacing={1}>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <AccessTimeFilledOutlined style={{ width: '20px', height: '20px', marginRight: '10px' }} color="black" />
                            <Typography variant="h4" color="black">
                                {product.totalDuration} Min
                            </Typography>
                        </div>
                    </Stack>
                </Grid>
                <Grid container spacing={1} sx={{ marginTop: '10px', marginLeft: '10px' }}>
                    {dishTags?.map((tag) => (
                        <Grid item key={tag.id}>
                            <Chip size="small" label={tag.name} color="primary" variant="outlined" />
                        </Grid>
                    ))}
                </Grid>
                <Grid item xs={12}>
                    <Divider />
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} md={6}>
                            <Table>
                                <TableBody sx={{ '& .MuiTableCell-root': { borderBottom: 'none' } }}>
                                    <TableRow>
                                        <TableCell>
                                            <Typography variant="body2">
                                                <FormattedMessage id="Portions" />:
                                            </Typography>
                                        </TableCell>
                                        <TableCell align="left">
                                            <Increment quantity={portion} updateQuantity={updateQuantity} />
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Card>
    );
};

ProductInfo.propTypes = {
    product: PropTypes.object
};

export default ProductInfo;
