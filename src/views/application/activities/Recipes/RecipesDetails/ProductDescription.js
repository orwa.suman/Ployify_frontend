import Accordion from 'ui-component/extended/Accordion';

// ==============================|| PRODUCT DETAILS - DESCRIPTION ||============================== //

const ProductDescription = ({ itemDescription, dishlist, selectExtraDish }) => (
    <Accordion itemDescription={itemDescription} dishlist={dishlist} selectExtraDish={selectExtraDish} />
);

export default ProductDescription;
