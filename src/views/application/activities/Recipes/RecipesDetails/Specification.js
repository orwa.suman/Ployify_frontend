import { Grid, Table, TableBody, TableCell, TableContainer, TableRow, Typography } from '@mui/material';

import { FormattedMessage } from 'react-intl';

const Specification = ({ data }) => (
    <Grid container spacing={2}>
        {console.log('daaa', data)}
        <Grid item xs={12} lg={6} sx={{ marginBottom: { xs: '16px', lg: '0' } }}>
            <Typography variant="h3" sx={{ fontWeight: 'bold', marginBottom: '16px' }}>
                <FormattedMessage id="Ingredients" />
            </Typography>
            <TableContainer>
                <Table sx={{ maxWidth: 380 }} size="small" aria-label="simple table">
                    <TableBody>
                        {data.ingredients.map((row) => (
                            <TableRow key={row.name} sx={{ '& td, & th': { border: 0 } }}>
                                <TableCell component="th" scope="row">
                                    <Typography variant="caption" sx={{ fontWeight: 500, fontSize: '14px' }}>
                                        {row.name}
                                    </Typography>
                                </TableCell>
                                <TableCell sx={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <span style={{ display: 'inline-block', textAlign: 'right', paddingRight: '8px' }}>{row.amount}</span>
                                    <span style={{ width: '50px', display: 'inline-block' }}>{row.unit}</span>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Grid>

        <Grid item xs={12} lg={6}>
            <Typography variant="h3" sx={{ fontWeight: 'bold', marginBottom: '16px' }}>
                <FormattedMessage id="Energie" />
            </Typography>
            <TableContainer>
                <Table sx={{ maxWidth: 280 }} size="small" aria-label="simple table">
                    <TableBody>
                        {Object.keys(data.addedValues).map((property, index) => (
                            <TableRow key={index} sx={{ '& td, & th': { border: 0 } }}>
                                <TableCell component="th" scope="row">
                                    <Typography variant="caption" sx={{ fontWeight: 500 }}>
                                        {property}
                                    </Typography>
                                </TableCell>
                                <TableCell>{data.addedValues[property].toFixed()}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Grid>
    </Grid>
);

export default Specification;
