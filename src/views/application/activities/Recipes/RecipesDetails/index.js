import { Box, CircularProgress, Grid, Tabs } from '@mui/material';
import { addToShoppingCart, getRecipe } from 'services/RecipeService';
import { useEffect, useState } from 'react';

import FloatingCart from 'ui-component/cards/FloatingCart';
import ProductDescription from './ProductDescription';
import ProductImages from './ProductImages';
import ProductInfo from './ProductInfo';
import PropTypes from 'prop-types';
import Specification from './Specification';
import { addActivityToUserHistory } from 'services/userService';
import { gridSpacing } from 'store/constant';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from '../../../../../utils/form/Communication';
import { updateStartCountRecipeTracking } from 'services/trackingService';
import { useDispatch } from 'store';
import useLocalStorage from 'hooks/useLocalStorage';
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';

function TabPanel({ children, value, index, ...other }) {
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`product-details-tabpanel-${index}`}
            aria-labelledby={`product-details-tab-${index}`}
            {...other}
        >
            {value === index && <Box>{children}</Box>}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired
};

const RecipesDetails = () => {
    const { id } = useParams();
    const dispatch = useDispatch();
    const [value, setValue] = useState(0);
    const [data, setData] = useState(0);
    const [portion, setPortion] = useState(1);
    const [itemDescription, setItemDescription] = useState([]);
    const [loading, setLoading] = useState(true);
    const navigate = useNavigate();

    const getCookingGuide = (dish = {}) => ({
        id: 'data' + dish.name,
        title: dish.name,
        content: dish.cookingGuide,
        hasExtraDish: dish.extraDishesList?.length
    });

    // tarek;
    const getItemDescription = (dishList = [], portion) => {
        let itemDescription = [];
        let ingredients = [];
        let energieValues = { kCal: 0, kJ: 0, protein: 0, fett: 0, kohlenHydrate: 0, kh: 0 };

        const addEnergieValues = (ingredientInfo) => {
            Object.keys(energieValues).forEach((property) => {
                if (ingredientInfo.ingredient && ingredientInfo.ingredient[property]) {
                    energieValues[property] += (parseInt(ingredientInfo.ingredient[property]) / 100) * ingredientInfo.gramms;
                }
            });

            Object.keys(energieValues).forEach((property) => {
                energieValues[property] = Number(energieValues[property].toFixed(4));
            });

            return energieValues;
        };

        const multiplyEnergyValues = () => {
            Object.keys(energieValues).forEach((property) => {
                energieValues[property] = Number((energieValues[property] * portion).toFixed(4));
            });

            return energieValues;
        };

        const compareAndSumIfRedundant = (ingredientList) => {
            // Combine the two ingredient lists
            var combinedIngredients = [];

            // Loop through the first ingredient list and add quantities to combinedIngredients
            for (var i = 0; i < ingredientList.length; i++) {
                var ingredient = ingredientList[i];
                var existingIngredient = combinedIngredients.find(function (item) {
                    return item.id === ingredient.id;
                });

                if (existingIngredient) {
                    existingIngredient.amount += ingredient.amount;
                } else {
                    combinedIngredients.push(ingredient);
                }
            }

            return combinedIngredients;
        };

        const getIngredients = (dish = {}) =>
            (dish.ingredientInfos || []).map((ingredientInfo) => {
                addEnergieValues(ingredientInfo);
                return {
                    id: ingredientInfo.ingredient.id,
                    name: ingredientInfo.ingredient.name,
                    amount: ingredientInfo.quantity * portion,
                    unit: ingredientInfo.unit,
                    necessary: ingredientInfo.necessary
                };
            });

        [...dishList].forEach((dish) => {
            ingredients = [...ingredients, ...getIngredients(dish)];
            itemDescription = [...itemDescription, getCookingGuide(dish)];
            debugger;
            if (dish.extraDishesList.length) {
                const extraDishes = dish.extraDishesList.filter((extraDish) => extraDish.selected);
                // const extraDishes = dish.extraDishesList;
                extraDishes.forEach((extraDish) => {
                    ingredients = [...ingredients, ...getIngredients(extraDish)];
                    itemDescription = [...itemDescription, getCookingGuide(extraDish)];
                });
            }
        });

        multiplyEnergyValues();

        const uniqueIngredients = compareAndSumIfRedundant(ingredients);

        const handleEnergyLabels = (key) => {
            if (key === 'kCal') {
                return 'kcal';
            }
            if (key === 'kJ') {
                return 'kJ';
            }
            if (key === 'protein') {
                return 'Eiweiß';
            }
            if (key === 'fett') {
                return 'Fett';
            }
            if (key === 'kohlenHydrate') {
                return 'Kohlenhydrate';
            }
            if (key === 'kh') {
                return 'davon Zucker';
            }
        };

        // change labels
        let _energieValues = {};
        Object.keys(energieValues).forEach((property) => {
            _energieValues[handleEnergyLabels(property)] = energieValues[property];
        });

        return [
            {
                id: 'basic1',
                defaultExpand: true,
                title: null,
                content: <Specification data={{ ingredients: uniqueIngredients, addedValues: _energieValues }} />
            },
            ...itemDescription
        ];
    };

    useEffect(() => {
        getRecipe(id)
            .then((res) => res.data)
            .then((data) => {
                setLoading(false);
                setPortion(data.portions);
                const dataWithExtraDishesSelected = {
                    ...data,
                    dishList: [...data.dishInfos].map((dish) => ({
                        ...dish?.dish,
                        extraDishesList: [...(dish.extras ?? [])].map((extraDish, extraDishIndex) => ({
                            ...extraDish,
                            selected: extraDishIndex === 0
                        }))
                    }))
                };

                setData(dataWithExtraDishesSelected);
                const x = getItemDescription(dataWithExtraDishesSelected.dishList, data.portions);
                setItemDescription(x);

                addActivityToUserHistory(loggedInUser.id, id, 2);
                updateStartCountRecipeTracking(id)
                    .then((res) => res.data)
                    .then((data) => {
                        
                    });
            })
            .catch((e) => {
                //dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const updateQuantity = (value) => {
        setItemDescription(getItemDescription(data.dishList, value));
        setPortion(value);
    };
    // const getDishPictures = (dishes) => {
    //     return dishes?.filter((dish) => !!dish).map((dish) => dish.picture);
    // };

    const selectExtraDish = (value, dishIndex) => {
        if (value && value.id) {
            let state = { ...data };
            const selectedExtraDishIndex = state.dishList[dishIndex].extraDishesList.findIndex((extraDish) => {
                return extraDish.id === value.id;
            });
            state.dishList[dishIndex].extraDishesList = state.dishList[dishIndex].extraDishesList.map((extraDish, extraDishIndex) => ({
                ...extraDish,
                selected: extraDishIndex === selectedExtraDishIndex
            }));
            setData(state);
            setItemDescription(getItemDescription([...state.dishList], state.portions)); // tarek
        } else {
            let state = { ...data };

            state.dishList[dishIndex].extraDishesList = state.dishList[dishIndex].extraDishesList.map((extraDish) => ({
                ...extraDish,
                selected: false
            }));
            setData(state);
            setItemDescription(getItemDescription([...state.dishList], state.portions)); // tarek
        }
    };

    // TODO complete or enhance
    const storeInLocalStorage = (key, value) => {
        localStorage.setItem(key, value);
    };

    const storeRecipesIngredients = () => {
        const ingredientList = data.dishInfos[0].dish.ingredientInfos.map((info) => ({
            quantity: info.quantity,
            name: info.ingredient.name
        }));
        const ls = JSON.parse(localStorage.getItem('shopping-cart-state')) || [];

        // Combine the two ingredient lists
        var combinedIngredients = [];

        // Loop through the first ingredient list and add quantities to combinedIngredients
        for (var i = 0; i < ls.length; i++) {
            var ingredient = ls[i];
            var existingIngredient = combinedIngredients.find(function (item) {
                return item.name === ingredient.name;
            });

            if (existingIngredient) {
                existingIngredient.quantity += ingredient.quantity;
            } else {
                combinedIngredients.push(ingredient);
            }
        }

        // Loop through the second ingredient list and add quantities to combinedIngredients
        for (var i = 0; i < ingredientList.length; i++) {
            var ingredient = ingredientList[i];
            var existingIngredient = combinedIngredients.find(function (item) {
                return item.name === ingredient.name;
            });

            if (existingIngredient) {
                existingIngredient.quantity += ingredient.quantity;
            } else {
                combinedIngredients.push(ingredient);
            }
        }

        // storeInLocalStorage('shopping-cart-state', JSON.stringify(combinedIngredients));
        dispatch(showMessage('ingredients has been added to your shopping cart', 'success'));
    };

    const addToCart = () => {
        // storeRecipesIngredients();
        addToShoppingCart(loggedInUser?.id, id, 2)
            .then((res) => {
                navigate('/activities/Reciepts/checkout');
            })
            .then((data) => console.log(data))
            .catch((e) => console.log(e));
    };

    return (
        <Grid container alignItems="center" justifyContent="center" spacing={gridSpacing}>
            <Grid item xs={12} lg={10}>
                <div>
                    {loading && (
                        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <CircularProgress size={100} style={{ color: '#cbab70' }} />
                        </Box>
                    )}
                    {data && data?.id === String(id) ? (
                        <Grid container spacing={gridSpacing}>
                            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                <Grid item xs={12} md={8}>
                                    <ProductImages picture={data.picture} product={data} />
                                </Grid>
                                <Grid item xs={12} md={4}>
                                    <ProductInfo
                                        product={data}
                                        dishTags={data?.dishTags}
                                        portion={portion}
                                        updateQuantity={updateQuantity}
                                    />
                                </Grid>
                            </div>

                            <Grid item xs={12}>
                                <Tabs
                                    value={value}
                                    indicatorColor="primary"
                                    onChange={handleChange}
                                    sx={{}}
                                    aria-label="product description tabs example"
                                    variant="scrollable"
                                ></Tabs>
                                <TabPanel value={value} index={0}>
                                    {console.log('itemDescription', itemDescription)}
                                    <ProductDescription
                                        itemDescription={itemDescription}
                                        dishlist={data?.dishList}
                                        selectExtraDish={selectExtraDish}
                                    />
                                </TabPanel>
                            </Grid>
                        </Grid>
                    ) : null}
                </div>
            </Grid>
            <div onClick={addToCart}>
                <FloatingCart fromMain={false} />
            </div>
        </Grid>
    );
};

export default RecipesDetails;
