import PropTypes from 'prop-types';
import { useState } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Grid, useMediaQuery } from '@mui/material';

// project import
import { gridSpacing } from 'store/constant';

// third-party
import useConfig from 'hooks/useConfig';
const prodImage = require.context('assets/images/e-commerce', true);

// ==============================|| PRODUCT DETAILS - IMAGES ||============================== //

const ProductImages = ({ product, picture }) => {
    const theme = useTheme();
    const { borderRadius } = useConfig();
    const mobile = useMediaQuery('(max-width:600px)');
    const matchDownLG = useMediaQuery(theme.breakpoints.up('lg'));
    const initialImage = product.image ? prodImage(`./${product.image}`).default : '';

    const [selected, setSelected] = useState(initialImage);
    const [modal, setModal] = useState(false);

    const lgNo = matchDownLG ? 4 : 3;

    const settings = {
        dots: false,
        centerMode: true,
        swipeToSlide: true,
        focusOnSelect: true,
        centerPadding: '0px'
        //  slidesToShow: pictures.length > 3 ? lgNo : pictures.length
    };

    return (
        <>
            <Grid container alignItems="center" justifyContent="center" spacing={gridSpacing}>
                <Grid item xs={12}>
                    <div content={false} style={{ m: '0 auto' }}>
                        <img
                            alt="product"
                            src={picture}
                            style={{
                                borderRadius: `${borderRadius}px`,
                                overflow: 'hidden',
                                width: mobile ? '300px' : '650px',
                                height: mobile ? '250px' : '400px',
                                marginLeft: mobile && '6px',
                                marginTop: !mobile && '20px'
                            }}
                        />
                    </div>
                </Grid>
                {/* pictures.length > 0 && (
                    <Grid item xs={11} sm={7} md={9} lg={10} xl={8}>
                        <Slider {...settings}>
                            {pictures.map((item, index) => (
                                <Box key={index} sx={{ p: 1 }}>
                                    <CardMedia
                                        component="img"
                                        image={item}
                                        sx={{
                                            borderRadius: `${borderRadius}px`,
                                            overflow: 'hidden',
                                            cursor: 'zoom-in',
                                            width: '80',
                                            height: '120px'
                                        }}
                                    />
                                </Box>
                            ))}
                        </Slider>
                    </Grid>
                                    ) */}
            </Grid>
            {/*    <ModalGateway>
                {modal && pictures && (
                    <Modal onClose={() => setModal(!modal)}>
                        <Carousel views={pictures} />
                    </Modal>
                )}
                </ModalGateway> */}
        </>
    );
};

ProductImages.propTypes = {
    product: PropTypes.object,
    pictures: PropTypes.array,
    picture: PropTypes.string
};

export default ProductImages;
