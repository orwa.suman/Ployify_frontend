import 'bootstrap/dist/css/bootstrap.min.css';

import { Box, Button, CardActions, CardContent, Divider, Drawer, Grid, Typography, useMediaQuery } from '@mui/material';
import { appDrawerWidth, gridSpacing } from 'store/constant';
import { deleteDishyById, loadDishes } from 'services/DishService';
import { styled, useTheme } from '@mui/material/styles';
import { useEffect, useState } from 'react';

import BlockTwoToneIcon from '@mui/icons-material/BlockTwoTone';
import { FormattedMessage } from 'react-intl';
import { HiPencil } from 'react-icons/hi';
import MainCard from 'ui-component/cards/MainCard';
import React from 'react';
import SkeletonProductPlaceholder from 'ui-component/cards/Skeleton/ProductPlaceholder';
import { showMessage } from '../../../../utils/form/Communication';
import useConfig from 'hooks/useConfig';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';

// material-ui

// third party

// product list container
const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(({ theme, open }) => ({
    flexGrow: 1,
    transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.shorter
    }),
    marginRight: -appDrawerWidth,
    [theme.breakpoints.down('xl')]: {
        paddingRight: 0,
        marginRight: 0
    },
    ...(open && {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.shorter
        }),
        marginRight: 0
    })
}));

const ListItems = () => {
    const theme = useTheme();

    const navigate = useNavigate();

    let [data, setData] = React.useState([]);
    let [trackingData, setTrackingData] = React.useState([]);

    const getDishes = () => {
        loadDishes()
            .then((res) => res.data.reverse())
            .then(setData)
            .then((data) => setLoading(false))
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    React.useEffect(() => {
        getDishes();
    }, []);

    const { borderRadius } = useConfig();
    const dispatch = useDispatch();

    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
    const matchDownMD = useMediaQuery(theme.breakpoints.down('lg'));
    const matchDownLG = useMediaQuery(theme.breakpoints.down('xl'));

    const [isLoading, setLoading] = useState(true);
    useEffect(() => {
        setLoading(true);
    }, []);

    // drawer
    const [open, setOpen] = useState(isLoading);

    const handleDrawerOpen = () => {
        setOpen((prevState) => !prevState);
    };

    const editItem = (id) => navigate('/dashboard/activities/recipes/dish/form/' + id);
    const deleteItem = (id) => {
        deleteDishyById(id)
            .then((data) => getDishes())
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    useEffect(() => {
        setOpen(!matchDownLG);
    }, [matchDownLG]);

    let productResult = <></>;
    if (data && data.length > 0) {
        

        productResult = data.map((row, index) => (
            <Grid key={index} item xs={12} sm={6} md={4}>
                <MainCard
                    className="careployee-cards"
                    content={false}
                    boxShadow
                    sx={{
                        '&:hover': {
                            transform: 'scale3d(1.02, 1.02, 1)',
                            transition: 'all .4s ease-in-out'
                        }
                    }}
                >
                    <div className="careployee-cards__image">
                        <img src={row.picture || '/images/dish.jpg'} alt={row.name}></img>
                    </div>
                    <CardContent sx={{ p: 2 }} className="careployee-cards__box">
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Typography variant="h4" sx={{ textDecoration: 'none' }}>
                                    {row.name}
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <CardActions sx={{ padding: '4px' }}>
                                    <Grid container>
                                        <Grid item xs={12} sx={{ margin: '0 0 12px' }}>
                                            <Button variant="outlined" fullWidth startIcon={<HiPencil />} onClick={() => editItem(row.id)}>
                                                <FormattedMessage id="Edit" />
                                            </Button>
                                        </Grid>

                                        <Grid item xs={12}>
                                            <Button
                                                variant="outlined"
                                                fullWidth
                                                startIcon={<BlockTwoToneIcon />}
                                                onClick={() => deleteItem(row.id)}
                                            >
                                                <FormattedMessage id="Delete" />
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </CardActions>
                            </Grid>
                        </Grid>
                    </CardContent>
                </MainCard>
            </Grid>
        ));
    } else {
        productResult = (
            <Grid item xs={12} sx={{ mt: 3 }}>
                <p>
                    <FormattedMessage id="Empty" />
                </p>
            </Grid>
        );
    }

    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <Divider sx={{ borderColor: 'grey.400' }} />
            </Grid>
            <Grid item xs={12}>
                <Box sx={{ display: 'flex', padding: '40px' }}>
                    <Main open={open}>
                        <Grid container spacing={gridSpacing}>
                            {isLoading
                                ? [1, 2, 3, 4, 5, 6, 7, 8].map((item) => (
                                      <Grid key={item} item xs={12} sm={6} md={4} lg={3}>
                                          <SkeletonProductPlaceholder />
                                      </Grid>
                                  ))
                                : productResult}
                        </Grid>
                    </Main>
                    <Drawer
                        sx={{
                            ml: open ? 3 : 0,
                            height: matchDownLG ? '100vh' : 'auto',
                            flexShrink: 0,
                            zIndex: { xs: 1200, lg: open ? 0 : -1 },
                            overflowX: 'hidden',
                            width: appDrawerWidth,
                            '& .MuiDrawer-paper': {
                                height: 'auto',
                                width: appDrawerWidth,
                                position: matchDownLG ? 'fixed' : 'relative',
                                border: 'none',
                                borderRadius: matchDownLG ? 0 : `${borderRadius}px`
                            }
                        }}
                        variant={matchDownLG ? 'temporary' : 'persistent'}
                        anchor="right"
                        open={open}
                        ModalProps={{ keepMounted: true }}
                        onClose={handleDrawerOpen}
                    ></Drawer>
                </Box>
            </Grid>
        </Grid>
    );
};

export default ListItems;
