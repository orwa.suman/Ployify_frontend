import React, { useEffect, useState } from 'react';
import { loadRecipes } from 'services/RecipeService';
import { useDispatch } from 'react-redux';
import { showMessage } from 'utils/form/Communication';
import { Divider, Grid, Typography } from '@mui/material';
import RecipeCard from '../RecipesCards/RecipeCard';

const AllRecipes = () => {
    const [data, setData] = useState([]);
    const dispatch = useDispatch();
    useEffect(() => {
        loadRecipes()
            .then((res) => res.data)
            .then((resource) => {
                setData({ ...data, all: resource });
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    return (
        <div>
            <Typography variant="h3">All Recipes</Typography>
            <Divider sx={{ margin: '32px 0' }} />
            <Grid container spacing={2}>
                {(data && data?.all?.length ? data?.all : []).map((row, index) => {
                    row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                    return (
                        <Grid item xs={12} sm={6} md={4} lg={3} key={row.id}>
                            <RecipeCard row={{ ...row }} />
                        </Grid>
                    );
                })}
            </Grid>
        </div>
    );
};

export default AllRecipes;
