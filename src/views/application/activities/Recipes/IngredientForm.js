import { Button, Dialog, Grid, TextField, useTheme, Autocomplete } from '@mui/material';
import { useFormik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { addIngredient, loadIngredients, loadIngredientTags, updateIngredient } from '../../../../services/IngredientService';
import MainCard from '../../../../ui-component/cards/MainCard';
import { useDispatch } from 'store';
import { showMessage } from '../../../../utils/form/Communication';
import { useParams } from 'react-router-dom';
import { FormattedMessage, useIntl } from 'react-intl';
import IngredientTagForm from './IngredientTagForm';
import PropTypes from 'prop-types';
import { uploadToCloudinary } from 'services/CloudinaryService';
import FileUpload from '../../../forms/components/FileUpload';

function SimpleDialog(props) {
    const { open, onClose } = props;

    return (
        <Dialog onClose={onClose} open={open} fullWidth>
            <IngredientTagForm />
        </Dialog>
    );
}

SimpleDialog.propTypes = {
    open: PropTypes.bool.isRequired
};

function IngredientForm() {
    const { id } = useParams();
    const [ingredientTags, setIngredientTags] = React.useState([]);
    const [open, setOpen] = React.useState(false);
    const allIngredientTags = ingredientTags;
    const intl = useIntl();
    const theme = useTheme();
    const dispatch = useDispatch();

    const [picture, setPicture] = React.useState([]);
    const [pictureName, setPictureName] = React.useState([]);
    const [changed, setChanged] = React.useState(false);

    const loadTags = () => {
        loadIngredientTags()
            .then((res) => res.data)
            .then((r) => {
                
                setIngredientTags(r);
            })
            .catch((e) => {
                
            });
    };

    React.useEffect(() => {
        loadTags();

        if (id) {
            loadIngredients()
                .then((res) => res.data)
                .then((data) => {
                    const row = data.find((ingredient) => ingredient.id === id);

                    formik.setValues({
                        name: row.name,
                        ingredientTags: row.ingredientTags,
                        kCal: row.kCal,
                        kJ: row.kJ,
                        protein: row.protein,
                        fett: row.fett,
                        kohlenHydrate: row.kohlenHydrate,
                        kh: row.kh,
                        picture: row.picture
                    });
                })
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
    }, []);

    const openTagForm = () => {
        setOpen(true);
    };

    const handleClose = (value) => {
        setOpen(false);
        loadTags();
    };

    const handlePicture = (data, fileName) => {
        setPicture(data);
        setPictureName(fileName);
        setChanged(true);
    };

    const submitForm = (event) => {
        formik.validateForm().then((errors) => {
            if (errors && Object.keys(errors).length > 0) {
                const errorMessages = Object.entries(errors).map(([fieldName, fieldError]) => {
                    return `${fieldName}: ${fieldError.props.id}`;
                });

                dispatch(showMessage(<FormattedMessage id={errorMessages.join(', ')} />));
            }
            formik.handleSubmit(event);
        });
    };

    const validationSchema = Yup.object({
        name: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(50, <FormattedMessage id="Input Max Length" values={{ length: 50 }} />)
            .required(<FormattedMessage id="Required" />)
    });

    const formik = useFormik({
        initialValues: {
            name: '',
            ingredientTags: [],
            kCal: 0,
            kJ: 0,
            protein: 0,
            fett: 0,
            kohlenHydrate: 0,
            kh: 0,
            picture: ''
        },
        validationSchema,
        onSubmit: () => {

            uploadToCloudinary(picture, pictureName, 'ingredient_', '/Ployify/Activities/Ingredient')
                .then((data) => {

                    if (changed) {
                        
                        
                        formik.values.picture = data.url;
                    }

                    if (id) {
                        updateIngredient(id, formik.values)
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    } else {
                        addIngredient(formik.values)
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    }

                })
                .catch((err) => {
                    
                });

        }
    });

    return (
        <Grid>
            <form onSubmit={formik.handleSubmit}>
                <MainCard title={<FormattedMessage id="Add new igredient" />}>
                    <Grid item xs={12} className="careployee-form__block">
                        <label htmlFor="name" className="careployee-form__label">
                            <FormattedMessage id="Ingredient Name" />
                        </label>
                        <TextField
                            fullWidth
                            maxRows={4}
                            id="name"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            error={formik.touched.name && Boolean(formik.errors.name)}
                            helperText={formik.touched.name && formik.errors.name}
                        />
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <Grid item xs={12} className="careployee-form__block">
                            <FileUpload defaultPicture={formik.values.picture || '/images/dish.jpg'} onChange={handlePicture}></FileUpload>
                        </Grid>
                    </Grid>

                    <Grid container>
                        <Grid item xs={12} md={2} className="careployee-form__block">
                            <label htmlFor="kcal" className="careployee-form__label">
                                kCal
                            </label>
                            <TextField
                                fullWidth
                                maxRows={1}
                                id="kCal"
                                type="number"
                                value={formik.values.kCal}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.kCal && Boolean(formik.errors.kCal)}
                                helperText={formik.touched.kCal && formik.errors.kCal}
                            />
                        </Grid>

                        <Grid item xs={12} md={2} className="careployee-form__block">
                            <label htmlFor="kj" className="careployee-form__label">
                                KJ
                            </label>
                            <TextField
                                fullWidth
                                maxRows={1}
                                id="kJ"
                                type="number"
                                value={formik.values.kJ}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.kJ && Boolean(formik.errors.kJ)}
                                helperText={formik.touched.kJ && formik.errors.kJ}
                            />
                        </Grid>

                        <Grid item xs={12} md={2} className="careployee-form__block">
                            <label htmlFor="protein" className="careployee-form__label">
                                Protein
                            </label>
                            <TextField
                                fullWidth
                                maxRows={1}
                                id="protein"
                                label="protein"
                                type="number"
                                value={formik.values.protein}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.protein && Boolean(formik.errors.protein)}
                                helperText={formik.touched.protein && formik.errors.protein}
                            />
                        </Grid>
                    </Grid>

                    <Grid container>
                        <Grid item xs={12} md={2} className="careployee-form__block">
                            <label htmlFor="fett" className="careployee-form__label">
                                <FormattedMessage id="Fat" />
                            </label>
                            <TextField
                                fullWidth
                                maxRows={1}
                                id="fett"
                                type="number"
                                value={formik.values.fett}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.fett && Boolean(formik.errors.fett)}
                                helperText={formik.touched.fett && formik.errors.fett}
                            />
                        </Grid>

                        <Grid item xs={12} md={2} className="careployee-form__block">
                            <label htmlFor="kohlenHydrate" className="careployee-form__label">
                                <FormattedMessage id="kohlenHydrate" />
                            </label>
                            <TextField
                                fullWidth
                                maxRows={1}
                                id="kohlenHydrate"
                                type="number"
                                value={formik.values.kohlenHydrate}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.kohlenHydrate && Boolean(formik.errors.kohlenHydrate)}
                                helperText={formik.touched.kohlenHydrate && formik.errors.kohlenHydrate}
                            />
                        </Grid>

                        <Grid item xs={12} md={2} className="careployee-form__block">
                            <label htmlFor="kh" className="careployee-form__label">
                                kh
                            </label>
                            <TextField
                                fullWidth
                                maxRows={1}
                                id="kh"
                                label="kh"
                                type="number"
                                value={formik.values.kh}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.kh && Boolean(formik.errors.kh)}
                                helperText={formik.touched.kh && formik.errors.kh}
                            />
                        </Grid>
                    </Grid>
                </MainCard>

                <MainCard title={<FormattedMessage id="Add ingredient tags" />}>
                    <Button
                        variant="contained"
                        onClick={openTagForm}
                        sx={{
                            margin: '16px 0 24px',
                            borderRadius: '8px',
                            background: theme.palette.success.dark,
                            '&:hover': { background: theme.palette.success.main }
                        }}
                    >
                        <FormattedMessage id="Add New Tag" />
                    </Button>
                    <Grid item>
                        <Autocomplete
                            required
                            multiple
                            options={allIngredientTags}
                            getOptionLabel={(option) => option.name}
                            value={formik.values.ingredientTags} // update the value prop here
                            onChange={(event, value) => formik.setFieldValue('ingredientTags', value)} // update the field value when ingredient is selected
                            renderInput={(params) => (
                                <TextField {...params} label={intl.formatMessage({ id: 'Ingredient Tag' })} onBlur={formik.handleBlur} />
                            )}
                        />
                    </Grid>
                </MainCard>

                <Grid item xs={12}>
                    <Button className="careployee-form__submit-button" variant="contained" size="large" onClick={submitForm}>
                        <FormattedMessage id="Submit" />
                    </Button>
                </Grid>
            </form>
            <SimpleDialog open={open} onClose={handleClose} />
        </Grid>
    );
}

export default IngredientForm;
