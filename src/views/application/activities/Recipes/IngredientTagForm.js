import { Button, Grid, TextField } from '@mui/material';
import { useFormik } from 'formik';
import React from 'react';
import * as Yup from 'yup';

import { addIngredientTags } from '../../../../services/IngredientService';
import MainCard from '../../../../ui-component/cards/MainCard';
import { useDispatch } from 'store';
import { showMessage } from '../../../../utils/form/Communication';
import { FormattedMessage } from 'react-intl';

function IngredientTagForm() {
    const dispatch = useDispatch();

    const submitForm = (event) => {
        formik.validateForm().then((errors) => {
            if (errors && Object.keys(errors).length > 0) {
                dispatch(showMessage(<FormattedMessage id="Validation Fail" />));
            }
            formik.handleSubmit(event);
        });
    };
    const validationSchema = Yup.object({
        name: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(50, <FormattedMessage id="Input Max Length" values={{ length: 50 }} />)
            .required(<FormattedMessage id="Required" />)
    });

    const formik = useFormik({
        initialValues: {
            name: ''
        },
        validationSchema,
        onSubmit: () =>
            addIngredientTags(formik.values)
                .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                })
    });

    return (
        <form onSubmit={formik.handleSubmit} style={{ width: '100%' }}>
            <MainCard title={<FormattedMessage id="Add New Tag" />}>
                <Grid item xs={12} className="careployee-form__block">
                    <label htmlFor="name" className="careployee-form__label">
                        <FormattedMessage id="Ingredient Tag" />
                    </label>
                    <TextField
                        fullWidth
                        maxRows={4}
                        id="name"
                        value={formik.values.name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.name && Boolean(formik.errors.name)}
                        helperText={formik.touched.name && formik.errors.name}
                    />
                </Grid>
            </MainCard>

            <Grid item xs={12}>
                <Button className="careployee-form__submit-button" variant="contained" size="large" onClick={submitForm}>
                    <FormattedMessage id="Submit" />
                </Button>
            </Grid>
        </form>
    );
}

export default IngredientTagForm;
