import { useEffect, useState } from 'react';

import CheckoutUI from '../CheckoutUI';
import { getShopppingCartData } from 'services/RecipeService';
import { loggedInUser } from 'contexts/JWTContext';

const RecipeIngredients = () => {
    const [ingredients, setIngredients] = useState([]);
    const [shoppingCartData, setShoppingCartData] = useState([]);

    const handleDeleteItem = (ingredient) => {
        // const ls = JSON.parse(localStorage.getItem('shopping-cart-state'));
        // const ingToDeleteIndex = ls.findIndex((ing) => ing.name === ingredient.name && ing.quantity && ingredient.quantity);
        // ls.splice(ingToDeleteIndex, 1);
        // localStorage.setItem('shopping-cart-state', JSON.stringify(ls));
        // setIngredients(ls);
    };

    const getShoppingData = () => {
        getShopppingCartData(loggedInUser?.id)
            .then((res) => {
                setShoppingCartData(res.data.recipeList);
                setIngredients(res.data.ingredientList);
            })
            .catch((e) => {
                
            });
    };

    useEffect(() => {
        getShoppingData();
    }, []);

    return (
        <>
            <CheckoutUI
                // handleDeleteItem={handleDeleteItem}
                ingredients={ingredients}
                shoppingCartData={shoppingCartData}
                refetchShoppingCart={() => getShoppingData()}
            />
        </>
    );
};

export default RecipeIngredients;
