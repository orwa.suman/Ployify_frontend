// material-ui
import { Button, Card, CardActions, CardContent, Grid, IconButton, Rating, Typography } from '@mui/material';
// third party

import { useDispatch } from 'store';
import ProductCard from 'ui-component/cards/ProductCard';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { showMessage } from '../../../../../utils/form/Communication';
import { loggedInUser } from 'contexts/JWTContext';
import { addActivityToUserFavorites, removeActivityFromUserFavorites } from 'services/userService';
import { useTheme } from '@mui/styles';
import { Stack } from '@mui/system';
import { Favorite, OutdoorGrill, OutdoorGrillOutlined } from '@mui/icons-material';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import { RecipeLevels } from 'services/RecipeService';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    main: {
        border: '1px solid rgba(58, 53, 65, 0.12)',
        backgroundColor: '#fff',
        borderRadius: theme.shape.borderRadius,
        transition: 'box-shadow 0.5s ease-in-out',
        width: '270px',
        height: '370px',
        margin: '10px'
    }
}));

const RecipeCard = ({ row, makeMeFavourate, setMakeMeFavourate }) => {
    const classes = useStyles();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    let [isFavorit, setIsFavorit] = React.useState(row.isFavorit);
    const theme = useTheme();

    const setAsFavorite = (activityId) => {
        addActivityToUserFavorites({ userId: loggedInUser.id, activityId, activityNum: 2 })
            .then((res) => res?.data)
            .then(() => {
                setIsFavorit(true);
                setMakeMeFavourate(true);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
        setMakeMeFavourate(false);
    };

    const removeFromFavorite = (activityId) => {
        removeActivityFromUserFavorites({ userId: loggedInUser.id, activityId, activityNum: 2 })
            .then((res) => res?.data)
            .then(() => {
                setIsFavorit(false);
                setMakeMeFavourate(true);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
        setMakeMeFavourate(false);
    };

    return (
        <>
            <Card className={classes.main}>
                <div className="careployee-cards__image">
                    <div style={{ position: 'relative' }}>
                        <IconButton
                            // disabled={row.isFavorit || isFavorit}
                            onClick={(e) => (row.isFavorit ? removeFromFavorite(row.id, e) : setAsFavorite(row.id, e))}
                            sx={{
                                position: 'absolute',
                                top: 0,
                                right: 0,
                                zIndex: 1,
                                backgroundColor: 'white',
                                '& .MuiSvgIcon-root': { fontSize: '1.5rem' },
                                color: row.isFavorit ? 'red' : '#BDBDBD'
                            }}
                            size="large"
                        >
                            <Favorite />
                        </IconButton>
                    </div>
                    <img
                        src={row.picture ? row.picture : '/images/dish.jpg'}
                        alt={row.title}
                        style={{ width: '100%' }}
                        onClick={() => navigate('/activities/Reciepts/reciptDetails/' + row.id)}
                    ></img>
                </div>
                <CardContent
                    sx={{ padding: '24px 12px' }}
                    className="careployee-cards__box"
                    onClick={() => navigate('/activities/Reciepts/reciptDetails/' + row.id)}
                >
                    <Grid container spacing={2}>
                        <Grid item xs={12} className="careployee-cards__most-used__wrapper">
                            {row.isMostUsed ? (
                                <div className="careployee-cards__box__most-used careployee-cards__box__most-used--recipes">
                                    <WhatshotIcon
                                        color="primary"
                                        sx={{
                                            color: theme.palette.orange.dark,
                                            borderColor: theme.palette.orange.main
                                        }}
                                        size="large"
                                    />
                                </div>
                            ) : (
                                <></>
                            )}
                            <h6 sx={{ textDecoration: 'none' }}>{row.name}</h6>
                        </Grid>

                        <Grid item xs={12}>
                            <Stack direction="row" justifyContent="space-between" alignItems="center" sx={{ marginBottom: '10px' }}>
                                <Stack direction="row" justifyContent="space-between" alignItems="center" sx={{ marginLeft: '10px' }}>
                                    <Grid container spacing={1}>
                                        <div>
                                            <Grid container spacing={1}>
                                                <Rating
                                                    name="simple-controlled"
                                                    value={RecipeLevels[row.level]}
                                                    max={5}
                                                    icon={<OutdoorGrill fontSize="inherit" />}
                                                    emptyIcon={<OutdoorGrillOutlined fontSize="inherit" />}
                                                    precision={0.1}
                                                    readOnly
                                                />
                                            </Grid>
                                        </div>
                                    </Grid>
                                </Stack>
                            </Stack>
                            <div style={{ display: 'flex', alignIems: 'center', justifyContent: 'space-between' }}>
                                <Grid container>
                                    <Grid item>
                                        <AccessTimeIcon style={{ width: '18px', height: '18px' }} />
                                    </Grid>
                                    <Grid item>
                                        <Typography
                                            variant="p"
                                            sx={{
                                                color: theme.palette.mode === 'dark' ? 'white' : 'black',
                                                paddingLeft: '7px',
                                                fontSize: '13px'
                                            }}
                                        >
                                            {row.totalDuration} Min
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </div>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </>
    );
};

export default RecipeCard;
