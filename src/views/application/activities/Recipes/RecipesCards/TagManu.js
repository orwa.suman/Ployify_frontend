import { Grid } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { loadRecipes } from 'services/RecipeService';
import { loadAllTags } from 'services/TrainingService';
import { showMessage } from 'utils/form/Communication';
import { MealIcon, TagIconButton, tagsSetting } from '.';
import { useNavigate, useParams } from 'react-router-dom';
import Slider from 'react-slick';
import { setting } from '../../TrainingPlan/TrainingPlanCards';
import RecipeCard from './RecipeCard';

const TagManu = () => {
    const [tags, setTags] = useState([]);
    const [recipesByTag, setRecipesByTag] = useState([]);
    const [selectedTags, setSelectedTags] = useState('');
    const [allRecipies, setAllRecipies] = useState([]);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { tagname } = useParams();

    const loadAllRecipes = () =>
        loadRecipes()
            .then((res) => res.data)
            .then((resource) => {
                setAllRecipies({ all: resource });
                //   setData({ ...data, all: resource });
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });

    useEffect(() => {
        loadRecipes()
            .then((res) => res.data)
            .then((resource) => {
                setAllRecipies({ all: resource });
                //   setData({ ...data, all: resource });
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    useEffect(() => {
        loadAllTags()
            .then((res) => {
                setTags(res.data);
            })
            .catch((e) => {
                
            });
    }, []);

    function getRecipesByTag(tag) {
        if (!allRecipies || !allRecipies.all) {
            console.error('Recipes data not loaded yet');
            return [];
        }
        const recipesWithTag = allRecipies?.all?.filter((recipe) => recipe?.dishTags?.some((dishTag) => dishTag?.name === tag));
        return recipesWithTag;
    }
    useEffect(() => {
        if (tagname && (allRecipies || allRecipies.all)) {
            setSelectedTags(tagname);
            setRecipesByTag(getRecipesByTag(tagname));
        } else {
            
        }
    }, [tagname, allRecipies]);

    const handleTagClick = (tagname) => {
        navigate(`/activities/Reciepts/cards/tag-menu/${tagname}`);
        setSelectedTags(tagname);
        setRecipesByTag(getRecipesByTag(tagname));
    };

    return (
        <div>
            <div style={{ margin: '20px' }}>
                <Slider {...tagsSetting} style={{ width: '100%', transform: 'none' }}>
                    {tags &&
                        tags?.map((tab, index) => (
                            <Grid item xs={2} key={tab.id}>
                                <TagIconButton
                                    name={tab?.name}
                                    IconComponent={MealIcon}
                                    onClick={() => handleTagClick(tab.name)}
                                    selected={selectedTags === tab.name} // check if the tag is selected
                                />
                            </Grid>
                        ))}
                </Slider>
            </div>
            {recipesByTag.length > 0 && (
                <div style={{ margin: '20px' }}>
                    <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                        {recipesByTag?.map((row) => {
                            //  row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                            return <RecipeCard onSetAsFavorite={console.log('set as favorite')} row={{ ...row }} />;
                        })}
                    </Slider>
                </div>
            )}
        </div>
    );
};

export default TagManu;
