// material-ui

import 'bootstrap/dist/css/bootstrap.min.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import {
    Apps,
    BreakfastDining,
    DinnerDining,
    Fastfood,
    FitnessCenter,
    LocalFlorist,
    LunchDining,
    Update,
    Visibility
} from '@mui/icons-material';
import { ArrowBackIos, ArrowForwardIos } from '@mui/icons-material';
import { Box, Button, CircularProgress, Divider, Grid, Typography } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import { getUserFavorites, getUserHistory } from 'services/userService';
import { getUserMatchingRecipes, loadRecipes } from '../../../../../services/RecipeService';

import FloatingCart from 'ui-component/cards/FloatingCart';
import { MdEco } from 'react-icons/md';
import RecipeCard from './RecipeCard';
import Slider from 'react-slick';
import SubjectIcon from '@mui/icons-material/Subject';
import { getMostUsedRecipes } from 'services/AnalyzeToolService';
import { loadAllTags } from 'services/TrainingService';
import { loggedInUser } from '../../../../../contexts/JWTContext';
import { setting } from '../../TrainingPlan/TrainingPlanCards';

export const MealIcon = ({ mealTypes }) => {
    const icons = mealTypes.map((mealType) => {
        switch (mealType.toLowerCase()) {
            case 'breakfast':
                return <BreakfastDining key="breakfast" />;
            case 'lunch':
                return <LunchDining key="lunch" />;
            case 'dinner':
                return <DinnerDining key="dinner" />;
            case 'snack':
                return <Fastfood key="snack" />;
            case 'vegetarian':
                return <MdEco key="vegetarian" />;
            case 'vegan':
                return <LocalFlorist key="vegan" />;
            case 'low calories':
                return <FitnessCenter key="low-calories" />;
            case 'no sugar':
                return <Visibility key="no-sugar" />;
            case 'fast':
                return <Update key="fast" />;
            case 'little ingredients':
                return <Apps key="little-ingredients" />;
            default:
                return <SubjectIcon />;
        }
    });

    return <>{icons}</>;
};

export const TagIconButton = ({ name, IconComponent, onClick, selected }) => {
    let displayname = name;
    if (name.length > 11) {
        displayname = name.slice(0, 10) + '..';
    }

    return (
        <Button
            variant="contained"
            sx={{
                color: 'white',
                '& .MuiButton-endIcon': {
                    marginLeft: '8px'
                },
                padding: '8px 16px',
                width: '150px',
                opacity: selected ? 0.8 : 1,
                backgroundColor: selected ? '#cbab50' : '#cbab70',
                '&:hover': {
                    backgroundColor: '#cbab70',
                    opacity: selected ? '0.8' : '1'
                }
            }}
            onClick={onClick}
        >
            {displayname}
            {IconComponent && (
                <span style={{ marginLeft: '8px' }}>
                    <MealIcon mealTypes={[name]} />
                </span>
            )}
        </Button>
    );
};

const customPrevArrow = <ArrowBackIos />;
const customNextArrow = <ArrowForwardIos />;

export const tagsSetting = {
    dots: false,
    arrow: true,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 2,
    prevArrow: customPrevArrow,
    nextArrow: customNextArrow,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
};

const RecipeCards = () => {
    let [trackingData, setTrackingData] = React.useState([]);
    let [favorites, setFavorites] = React.useState([]);
    let [data, setData] = React.useState({});
    const [mostUsed, setMostUsed] = React.useState([]);
    const [makeMeFavourate, setMakeMeFavourate] = React.useState(false);
    const [latestUsed, setLatestUsed] = React.useState({});
    const [userMatchingRecipes, setUserMatchingRecipes] = React.useState({});
    const [favouratesReciepes, setFavouratesReciepes] = React.useState([]);
    const [allRecipies, setAllRecipies] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const navigate = useNavigate();

    useEffect(() => {
        getUserHistory(loggedInUser.id, 2)
            .then((res) => res.data)
            .then((resource) => {
                // let items = [];
                // resource.forEach((item) => {
                //     if (!items.find((intermediateItem) => intermediateItem.careployeeBaseEntity.id === item.careployeeBaseEntity.id)) {
                //         items = [...items, item];
                //     }
                // });
                // 
                // setLatestUsed({ ...data, latestUsed: items });
                setLatestUsed({ latestUsed: resource });
            })
            .catch((e) => {
                // dispatch(showMessage(e?.response?.data?.message));
                
            });
    }, []);

    useEffect(() => {
        getUserMatchingRecipes(loggedInUser.id)
            .then((res) => res.data)
            .then((resource) => {
                let items = [];
                resource.forEach((item) => {
                    if (!items.find((intermediateItem) => intermediateItem.id === item.id)) {
                        items = [...items, item];
                    }
                });
                setUserMatchingRecipes({ ...data, forYou: items });
            })
            .catch((e) => {
                // dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    useEffect(() => {
        getMostUsedRecipes()
            .then((res) => res.data)
            .then((data) => setMostUsed(data));
    }, []);

    useEffect(() => {
        getUserFavorites(loggedInUser.id, 2)
            .then((res) => res.data)
            .then((resource) => {
                // let userFavorites = [];
                // resource.forEach((favorit) => {
                //     if (
                //         !userFavorites.find(
                //             (intermediateFavorit) => intermediateFavorit.careployeeBaseEntity?.id === favorit.careployeeBaseEntity?.id
                //         )
                //     ) {
                //         userFavorites = [...userFavorites, favorit];
                //     }
                // });
                setFavouratesReciepes(resource);
                setFavorites(resource);
                setLoading(false);
            })
            .catch((e) => {
                //  dispatch(showMessage(e.message));
                setLoading(false);
            });
    }, [makeMeFavourate]);

    const loadAllRecipes = () =>
        loadRecipes()
            .then((res) => setAllRecipies({ ...data, all: res.data }))
            .then((resource) => {
                // setAllRecipies({ ...data, all: resource });
                //setData({ ...data, all: resource });
            })
            .catch((e) => {
                // dispatch(showMessage(e?.response?.data?.message));
            });

    useEffect(() => {
        loadAllRecipes();
    }, []);

    const loadMostUsed = () =>
        getMostUsedRecipes()
            .then((res) => res.data)
            .then((data) => setTrackingData(data));

    React.useEffect(() => {
        loadMostUsed();
    }, []);

    const [tags, setTags] = useState([]);
    const [recipesByTag, setRecipesByTag] = useState([]);
    const [selectedTags, setSelectedTags] = useState('');

    useEffect(() => {
        loadAllTags()
            .then((res) => {
                setTags(res.data);
            })
            .catch((e) => {
                //  dispatch(showMessage(e.message));
                
            });
    }, []);

    function getRecipesByTag(tag) {
        if (!allRecipies || !allRecipies.all) {
            console.error('Recipes data not loaded yet');
            return [];
        }
        const recipesWithTag = allRecipies?.all?.filter((recipe) => recipe?.dishTags?.some((dishTag) => dishTag?.name === tag));
        return recipesWithTag;
    }

    const handleTagClick = (tagname) => {
        navigate(`/activities/Reciepts/cards/tag-menu/${tagname}`);
        setSelectedTags(tagname);
        setRecipesByTag(getRecipesByTag(tagname));
    };
    const isFavorit = (id) =>
        !!(!!favorites && favorites.length ? favorites : []).find((favorit) => (favorit.id || favorit.careployeeBaseEntity.id) === id);

    const addToCart = () => {
        
    };

    const favourateResult = allRecipies?.all?.filter((recipe) =>
        favouratesReciepes?.find((favRecipe) => favRecipe?.careployeeBaseEntity?.id === recipe?.id)
    );

    const latestUsedResult = allRecipies?.all?.filter((recipe) =>
        latestUsed?.latestUsed?.find((favRecipe) => favRecipe?.careployeeBaseEntity?.id === recipe?.id)
    );

    return (
        <div>
            <div style={{ margin: '20px' }}>
                {isLoading && (
                    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                        <CircularProgress size={100} style={{ color: '#cbab70' }} />
                    </Box>
                )}
                {favourateResult?.length > 0 ? (
                    <div>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Favorites
                        </Typography>
                        <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                            {favourateResult?.map((row) => {
                                // row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                return (
                                    <RecipeCard
                                        row={{ ...row, isFavorit: isFavorit(row.id), isMostUsed: (mostUsed || []).includes(row.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                ) : (
                    <div style={{ width: '100%' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Favorites
                        </Typography>
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <img src="/images/favourate.svg" alt="first favourate" style={{ textAlign: 'center' }} />
                        </div>
                    </div>
                )}
            </div>
            <Divider sx={{ margin: '32px 0' }} />

            {false && (
                <div style={{ margin: '20px' }}>
                    <Slider {...tagsSetting} style={{ width: '100%', transform: 'none' }}>
                        {tags &&
                            tags?.map((tab, index) => (
                                <Grid item xs={2} key={tab.id}>
                                    <TagIconButton
                                        name={tab?.name}
                                        IconComponent={MealIcon}
                                        onClick={() => handleTagClick(tab.name)}
                                        selected={selectedTags === tab.name} // check if the tag is selected
                                    />
                                </Grid>
                            ))}
                    </Slider>
                </div>
            )}

            {recipesByTag.length > 0 && (
                <div style={{ margin: '5px' }}>
                    <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                        {recipesByTag?.map((row) => {
                            //  row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                            return (
                                <RecipeCard
                                    row={{ ...row, isFavorit: isFavorit(row.id), isMostUsed: (mostUsed || []).includes(row.id) }}
                                    makeMeFavourate={makeMeFavourate}
                                    setMakeMeFavourate={setMakeMeFavourate}
                                />
                            );
                        })}
                    </Slider>
                </div>
            )}
            <div style={{ margin: '20px' }}>
                {latestUsedResult && (
                    <div>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Latest Used
                        </Typography>
                        <Slider {...setting} style={{ transform: 'none', width: '100%' }}>
                            {latestUsedResult?.map((row) => {
                                //  row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                return (
                                    <RecipeCard
                                        row={{ ...row, isFavorit: isFavorit(row.id), isMostUsed: (mostUsed || []).includes(row.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                )}
            </div>
            <div style={{ margin: '20px' }}>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            All Recipies
                        </Typography>
                        <Typography
                            component={Link}
                            to="/dashboard/activities/recipes/all"
                            style={{
                                marginLeft: '10px',
                                cursor: 'pointer',
                                textDecoration: 'none',
                                color: 'lightGray',
                                marginTop: '4px'
                            }}
                        >
                            Show More
                        </Typography>
                    </>
                </div>
                {allRecipies && allRecipies?.all?.length > 0 ? (
                    <div>
                        <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                            {allRecipies &&
                                allRecipies?.all?.slice(0, 10).map((row) => {
                                    // row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                    return (
                                        <RecipeCard
                                            row={{ ...row, isFavorit: isFavorit(row.id), isMostUsed: (mostUsed || []).includes(row.id) }}
                                            makeMeFavourate={makeMeFavourate}
                                            setMakeMeFavourate={setMakeMeFavourate}
                                        />
                                    );
                                })}
                        </Slider>
                    </div>
                ) : (
                    <div style={{ width: '100%' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Meal
                        </Typography>
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <img width={400} src="/images/firstMeal.png" alt="first meal" style={{ textAlign: 'center' }} />
                        </div>
                    </div>
                )}
            </div>
            <div onClick={addToCart}>
                <FloatingCart fromMain={true} />
            </div>
        </div>
    );
};

export default RecipeCards;
