import { Box, Button, CircularProgress, Grid, IconButton, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { deleteShoppingCartItem, getShoppingCartPdf } from 'services/RecipeService';

import DeleteIcon from '@mui/icons-material/Delete';
import { DeleteOutline } from '@mui/icons-material';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from '../../../../utils/form/Communication';
import { useDispatch } from 'store';

const CheckoutUI = ({ ingredients, shoppingCartData, refetchShoppingCart }) => {
    const dispatch = useDispatch();
    const [_ingredients, setIngredients] = useState([]);

    useEffect(() => {
        // storeRecipesIngredients(shoppingCartData);
    }, [shoppingCartData]);

    function deleteItemFromShoppingCart(recipeId) {
        deleteShoppingCartItem(loggedInUser?.id, recipeId)
            .then((res) => res)
            .then((res) => {
                refetchShoppingCart();
                dispatch(showMessage('recipe has been removed from your shopping cart', 'success'));
            })
            .catch((e) => {
                
            });
    }

    const downLoadShoppingCartPdf = () => {
        getShoppingCartPdf(loggedInUser?.id)
            .then((res) => {
                dispatch(showMessage('Downloading you shopping cart', 'success'));
                var link = document.createElement('a');
                link.href = res.data;
                link.download = 'ployify-shopping-cart.pdf';
                link.dispatchEvent(new MouseEvent('click'));
            })
            .catch((e) => {
                
            });
    };

    const groupIngredients = (ingList) => {
        let _ingList = [];

        const isFind = (id) =>
            _ingList.find((item) => {
                return item.id === id;
            });

        // const getIndex = (id) => _ingList.find((item) => {
        //     return item.id === id;
        // });

        ingList.forEach((item) => {
            if (isFind(item.id)) {
                const index = _ingList.findIndex((_item) => _item.id === item.id);
                _ingList[index].quantity += item.quantity;
            } else {
                _ingList.push(item);
            }
        });
        setIngredients(_ingList);
    };

    const handleDeleteItem = (ingredient) => {
        let modifiedIngList = [..._ingredients];
        const ingToDeleteIndex = modifiedIngList.findIndex((ing) => ing.id === ingredient.id);
        modifiedIngList.splice(ingToDeleteIndex, 1);
        setIngredients(modifiedIngList);
    };

    useEffect(() => {
        groupIngredients(ingredients);
    }, [ingredients]);

    const storeRecipesIngredients = (ingList) => {
        debugger;
        const ingredientList = ingList.map((info) => ({
            // quantity: info.quantity,
            // name: info.ingredient.name
        }));
        // const ls = JSON.parse(localStorage.getItem('shopping-cart-state')) || [];

        // Combine the two ingredient lists
        var combinedIngredients = [];

        // Loop through the first ingredient list and add quantities to combinedIngredients
        for (var i = 0; i < ingredientList.length; i++) {
            var ingredient = ingredientList[i];
            var existingIngredient = combinedIngredients.find(function (item) {
                return item.name === ingredient.name;
            });

            if (existingIngredient) {
                existingIngredient.quantity += ingredient.quantity;
            } else {
                combinedIngredients.push(ingredient);
            }
        }

        // Loop through the second ingredient list and add quantities to combinedIngredients
        for (var i = 0; i < ingredientList.length; i++) {
            var ingredient = ingredientList[i];
            var existingIngredient = combinedIngredients.find(function (item) {
                return item.name === ingredient.name;
            });

            if (existingIngredient) {
                existingIngredient.quantity += ingredient.quantity;
            } else {
                combinedIngredients.push(ingredient);
            }
        }

        // storeInLocalStorage('shopping-cart-state', JSON.stringify(combinedIngredients));
        // dispatch(showMessage('ingredients has been added to your shopping cart', 'success'));
        setIngredients(combinedIngredients);
    };

    return (
        <div>
            <Typography variant="h1" style={{ textAlign: 'center', color: '#cbab70' }}>
                Shopping List
            </Typography>

            <div style={{ display: 'flex', justifyContent: 'space-around', marginTop: '40px' }}>
                <Grid container spacing={1} style={{ width: '40%' }}>
                    <Grid item xs={2}></Grid>
                    <Grid item xs={6}>
                        <Typography variant="h5" style={{ fontWeight: 'bold', textDecoration: 'underline' }}>
                            Ingredients
                        </Typography>
                    </Grid>
                    <Grid item xs={4}>
                        <Typography variant="h5" style={{ fontWeight: 'bold', textDecoration: 'underline' }}>
                            Quantity
                        </Typography>
                    </Grid>
                    {(!_ingredients || _ingredients.length == 0) && <Typography sx={{ m: 'auto' }}>No Ingredients found</Typography>}
                    {_ingredients?.map((ingredient, index) => (
                        <React.Fragment key={index}>
                            <Grid item xs={1} sx={{ display: 'flex', alignItems: 'center' }}>
                                <IconButton aria-label="delete" color="error" onClick={() => handleDeleteItem(ingredient)}>
                                    <DeleteIcon />
                                </IconButton>
                            </Grid>
                            <Grid item xs={2} sx={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}>
                                <img src={ingredient.ingredient.picture} width={26} height={26} />
                            </Grid>
                            <Grid item xs={4} sx={{ display: 'flex', alignItems: 'center' }}>
                                <Typography>{ingredient.ingredient.name}</Typography>
                            </Grid>
                            <Grid item xs={5} sx={{ display: 'flex', alignItems: 'center' }}>
                                <Typography>
                                    {ingredient.quantity} {ingredient.unit}
                                </Typography>
                            </Grid>

                            <Grid item xs={12}>
                                <hr style={{ border: '1px solid #5f5f5f', margin: '4px 0' }} />
                            </Grid>
                        </React.Fragment>
                    ))}
                </Grid>
                <div>
                    <Typography variant="h5" style={{ fontWeight: 'bold', textDecoration: 'underline', marginLeft: '20px' }}>
                        Recipes
                    </Typography>

                    {!shoppingCartData && <CircularProgress size={100} sx={{ mt: 2 }} />}
                    {shoppingCartData.length == 0 && <Typography sx={{ m: 'auto' }}>No Recipes found</Typography>}
                    {shoppingCartData?.map((recipe) => (
                        <div key={recipe.id} style={{ display: 'flex', alignItems: 'center', marginBottom: '16px', marginTop: '30px' }}>
                            <IconButton onClick={() => deleteItemFromShoppingCart(recipe.id)}>
                                <DeleteOutline />
                            </IconButton>
                            <img
                                src={recipe.picture}
                                alt={recipe.name}
                                style={{ marginRight: '16px', width: '50px', height: '50px', objectFit: 'cover', borderRadius: '10%' }}
                            />
                            <div>
                                <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
                                    {recipe.name}
                                </Typography>
                                {/* <Typography variant="body2" style={{ color: '#666' }}>
                                    Quantity: {recipe.quantity}
                                </Typography> */}
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            {/* <Box sx={{ display: 'flex', justifyContent: 'flex-end', marginTop: '50px' }}>
                <Button
                    variant="contained"
                    onClick={() => downLoadShoppingCartPdf()}
                    sx={{
                        backgroundColor: '#cbab70',
                        '&:hover': {
                            backgroundColor: '#a78d5c'
                        }
                    }}
                >
                    Download recipes pdf
                </Button>
            </Box> */}
        </div>
    );
};

export default CheckoutUI;
