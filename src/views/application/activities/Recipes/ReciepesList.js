import 'bootstrap/dist/css/bootstrap.min.css';

import { Apps, Favorite, OutdoorGrill, OutdoorGrillOutlined, SensorOccupied, Update } from '@mui/icons-material';
import {
    Box,
    Button,
    Card,
    CardActions,
    CardContent,
    CircularProgress,
    Drawer,
    Grid,
    Rating,
    Stack,
    Typography,
    useMediaQuery
} from '@mui/material';
import { RecipeLevels, deleteRecipes, getUserMatchingRecipes, loadRecipes } from '../../../../services/RecipeService';
import { addActivityToUserFavorites, getUserFavorites, getUserHistory } from 'services/userService';
import { styled, useTheme } from '@mui/material/styles';
import { useEffect, useState } from 'react';

import AccessTimeIcon from '@mui/icons-material/AccessTime';
import BlockTwoToneIcon from '@mui/icons-material/BlockTwoTone';
import ColorTabs from './Tabs';
import { FormattedMessage } from 'react-intl';
import { HiPencil } from 'react-icons/hi';
import React from 'react';
import SubCard from 'ui-component/cards/SubCard';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import { appDrawerWidth } from 'store/constant';
import { getMostUsedRecipes } from 'services/AnalyzeToolService';
import { loggedInUser } from '../../../../contexts/JWTContext';
import { showMessage } from '../../../../utils/form/Communication';
import useConfig from 'hooks/useConfig';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';

// material-ui

// third party

// product list container
const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(({ theme, open }) => ({
    flexGrow: 1,
    transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.shorter
    }),
    marginRight: -appDrawerWidth,
    [theme.breakpoints.down('xl')]: {
        paddingRight: 0,
        marginRight: 0
    },
    ...(open && {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.shorter
        }),
        marginRight: 0
    })
}));

const ProductsList = () => {
    const theme = useTheme();
    const cardStyle = {
        cursor: 'pointer',
        background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.grey[50],
        border: '1px solid',
        borderColor: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.grey[100]
    };
    const navigate = useNavigate();

    const [loadAll, setLoadAll] = React.useState([]);

    const loadAllRecipes = () =>
        loadRecipes()
            .then((res) => res.data)
            .then((data) => {
                setLoadAll(data);
            })
            .catch((e) => {
                //  dispatch(showMessage(e?.response?.data?.message));
            });

    useEffect(() => {
        loadAllRecipes();
    }, []);

    const editRecipe = (id) => {
        navigate('/dashboard/activities/recipes/form/' + id);
    };
    const deleteRecipe = (id) => {
        deleteRecipes(id).then((res) => loadAllRecipes());
    };

    return (
        <>
            <Typography variant="h3" style={{ marginTop: '10px', marginBottom: '10px' }}>
                All Recipies
            </Typography>
            {!loadAll && <CircularProgress />}
            <Grid container spacing={2}>
                {loadAll &&
                    loadAll.map((row, index) => {
                        row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                        return (
                            <Grid key={index} item xs={12} sm={6} md={4}>
                                <SubCard>
                                    <Card sx={cardStyle}>
                                        <div
                                            className="careployee-cards__image"
                                            onClick={() => navigate('/activities/Reciepts/reciptDetails/' + row.id)}
                                        >
                                            <img src={row.picture ? row.picture : '/images/dish.jpg'} alt={row.title}></img>
                                        </div>
                                        <CardContent
                                            sx={{ padding: '24px 12px' }}
                                            onClick={() => navigate('/activities/Reciepts/reciptDetails/' + row.id)}
                                        >
                                            <Grid container spacing={1}>
                                                <Typography variant="4" sx={{ paddingTop: '4px', paddingLeft: '10px' }}>
                                                    {row?.name}
                                                </Typography>
                                                <Grid item xs={12}>
                                                    <Stack
                                                        direction="row"
                                                        justifyContent="space-between"
                                                        alignItems="center"
                                                        sx={{ paddingBottom: '12px', marginBottom: '12px' }}
                                                    >
                                                        <Grid container>
                                                            <Grid item>
                                                                <AccessTimeIcon />
                                                            </Grid>
                                                            <Grid item>
                                                                <Typography variant="h4" sx={{ paddingTop: '4px', paddingLeft: '10px' }}>
                                                                    {row.totalDuration} Min
                                                                </Typography>
                                                            </Grid>
                                                        </Grid>
                                                    </Stack>
                                                    <Stack
                                                        direction="row"
                                                        justifyContent="space-between"
                                                        alignItems="center"
                                                        sx={{ paddingBottom: '5px' }}
                                                    >
                                                        <Grid container spacing={1}>
                                                            <Rating
                                                                name="simple-controlled"
                                                                value={RecipeLevels[row.level]}
                                                                max={4}
                                                                icon={<OutdoorGrill fontSize="inherit" />}
                                                                emptyIcon={<OutdoorGrillOutlined fontSize="inherit" />}
                                                                precision={0.1}
                                                                readOnly
                                                            />
                                                        </Grid>
                                                    </Stack>
                                                </Grid>
                                            </Grid>
                                        </CardContent>

                                        <CardActions sx={{ padding: '4px' }}>
                                            <Grid container>
                                                <Grid item xs={12} sx={{ margin: '0 0 12px' }}>
                                                    <Button
                                                        variant="outlined"
                                                        fullWidth
                                                        startIcon={<HiPencil />}
                                                        onClick={() => editRecipe(row.id)}
                                                    >
                                                        <FormattedMessage id="Edit" />
                                                    </Button>
                                                </Grid>

                                                <Grid item xs={12}>
                                                    <Button
                                                        variant="outlined"
                                                        fullWidth
                                                        startIcon={<BlockTwoToneIcon />}
                                                        onClick={() => deleteRecipe(row.id)}
                                                    >
                                                        <FormattedMessage id="Delete" />
                                                    </Button>
                                                </Grid>
                                            </Grid>
                                        </CardActions>
                                    </Card>
                                </SubCard>
                            </Grid>
                        );
                    })}
            </Grid>
        </>
    );
};

export default ProductsList;
