import { Autocomplete, Button, Checkbox, FormControlLabel, Grid, Radio, RadioGroup, TextField } from '@mui/material';
import { useFormik } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { addDish, listAllIngredientUnits, loadDishes, loadExtraDishes, updateDish } from '../../../../services/DishService';
import { loadIngredients } from '../../../../services/IngredientService';
import MainCard from '../../../../ui-component/cards/MainCard';
import { useDispatch } from 'store';
import { showMessage } from '../../../../utils/form/Communication';
import FileUpload from '../../../forms/components/FileUpload';
import { useParams } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { useIntl } from 'react-intl';
import { uploadToCloudinary } from 'services/CloudinaryService';
import MDEditor from '@uiw/react-md-editor';

function DishForm() {
    const [levelOfAdvance, setLevelOfAdvance] = React.useState('NORMAL');
    const [isExtraTag, setIsExtraTag] = React.useState(false);
    const dispatch = useDispatch();
    // const [extraDishes, setExtraDishes] = React.useState([]);
    // const allExtraDishes = extraDishes;
    const emptyIngredient = {
        ingredient: { info: null },
        gramms: { label: '' },
        quantity: { label: '' },
        unit: { label: '' },
        necessary: false
    };
    const [ingredients, setIngredients] = React.useState([emptyIngredient]);
    const [systemIngredients, setSystemIngredients] = React.useState([]);
    const [units, setUnits] = React.useState([]);
    const { id } = useParams();
    const intl = useIntl();

    const [picture, setPicture] = React.useState([]);
    const [pictureName, setPictureName] = React.useState([]);
    const [changed, setChanged] = React.useState(false);

    React.useEffect(() => {
        // const extraDishes = loadExtraDishes(true);
        // extraDishes
        //     .then((res) => res.data)
        //     .then((r) => {
        //         setExtraDishes(r);
        //     })
        //     .catch((e) => {
        //         dispatch(showMessage(e?.response?.data?.message));
        //     });

        listAllIngredientUnits()
            .then((res) => res.data)
            .then((data) => {
                
                setUnits(data);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });

        if (id) {
            loadDishes()
                .then((res) => res.data)
                .then((data) => {
                    const row = data.find((item) => item.id === id);
                    setIngredients(
                        row.ingredientInfos.map((ingredientInfo) => ({
                            quantity: {
                                label: ingredientInfo.quantity
                            },
                            gramms: {
                                label: ingredientInfo.gramms
                            },
                            unit: {
                                label: ingredientInfo.unit
                            },
                            ingredient: { info: ingredientInfo.ingredient },
                            necessary: ingredientInfo.necessary,
                            id: ingredientInfo.id
                        }))
                    );
                    setIsExtraTag(row.extraTag ? row.extraTag : false);
                    formik.setValues({
                        name: row.name,
                        ingredientInfos: row.ingredientInfos,
                        minPortion: row.minPortion,
                        cookingGuide: row.cookingGuide,
                        duration: row.duration,
                        levelOfAdvance: row.levelOfAdvance,
                        totalFoodValues: row.totalFoodValues,
                        extraDishesList: row.extraDishesList,
                        extraTag: row.extraTag,
                        picture: row.picture
                    });
                })
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
    }, []);

    React.useEffect(() => {
        const trainings = loadIngredients();
        trainings
            .then((res) => res.data)
            .then((r) => {
                setSystemIngredients(r);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    const validationSchema = Yup.object({
        name: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(50, <FormattedMessage id="Input Max Length" values={{ length: 50 }} />)
            .required(<FormattedMessage id="Required" />),
        cookingGuide: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .required(<FormattedMessage id="Required" />),
        minPortion: Yup.number()
            .min(1, <FormattedMessage id="Input Min Length" values={{ length: 1 }} />)
            .max(100, <FormattedMessage id="Input Max Length" values={{ length: 100 }} />),
            //.required(<FormattedMessage id="Required" />),
        duration: Yup.number()
            .min(1, <FormattedMessage id="Input Min Length" values={{ length: 1 }} />)
            .max(300, <FormattedMessage id="Input Max Length" values={{ length: 300 }} />)
            //.required(<FormattedMessage id="Required" />)
    });

    const formik = useFormik({
        initialValues: {
            name: '',
            ingredientInfos: [],
            minPortion: 1,
            cookingGuide: '',
            duration: '',
            levelOfAdvance: 'NORMAL',
            totalFoodValues: '',
            extraDishesList: [],
            extraTag: false,
            picture: ''
        },
        validationSchema,
        onSubmit: () => {
            uploadToCloudinary(picture, pictureName, 'dish_', '/Ployify/Activities/Dish')
                .then((data) => {

                    if (changed) {
                        
                        
                        formik.values.picture = data.url;
                    }

                    if (id) {
                        
                        updateDish(id, formik.values)
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    } else {
                        addDish(formik.values)
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    }
                })
                .catch((err) => {
                    
                });
        }
    });

    const submitForm = (event) => {
        formik.values.ingredientInfos = ingredients.map((ingredientInfo) => ({
            ...ingredientInfo,
            ingredient: ingredientInfo.ingredient.info,
            gramms: ingredientInfo.gramms.label,
            quantity: ingredientInfo.quantity.label,
            unit: ingredientInfo.unit.label === '' ? undefined : ingredientInfo.unit.label
        }));
        formik.validateForm().then((errors) => {
            if (errors && Object.keys(errors).length > 0) {
                const errorMessages = Object.entries(errors).map(([fieldName, fieldError]) => {
                    return `${fieldName}: ${fieldError.props.id}`;
                });

                dispatch(showMessage(<FormattedMessage id={errorMessages.join(', ')} />));
            }
            formik.handleSubmit(event);
        });
    };

    function handleChange(event) {
        formik.setValues({ ...formik.values, levelOfAdvance: event.target.value });
        setLevelOfAdvance(event.target.value);
    }

    // const handleDishes = (value) => {
    //     formik.setValues({ ...formik.values, extraDishesList: value });
    // };

    function handleIsExtraChecked(event) {
        formik.setValues({ ...formik.values, extraTag: event.target.checked ? true : false });
        setIsExtraTag(event.target.checked);
    }

    const handlePicture = (data, fileName) => {
        setPicture(data);
        setPictureName(fileName);
        setChanged(true);
    };

    const isFieldValid = (value) => {
        if ((!!value && value.length > 0) || value?.id) return value;
        return false;
    };

    const handleIngredientUnit = (index, value) => {
        const state = [...ingredients];
        state[index].unit.label = value;
        setIngredients(state);
    };

    const handleIngredientNecessary = (index, value) => {
        const state = [...ingredients];
        state[index].necessary = value;
        setIngredients(state);
    };

    const handleIngredientQuantity = (index, value) => {
        const state = [...ingredients];
        state[index].quantity.label = value;
        setIngredients(state);
    };

    const handleIngredientGramms = (index, value) => {
        const state = [...ingredients];
        state[index].gramms.label = value;
        setIngredients(state);
    };

    const handleIngredient = (index, value) => {
        const state = [...ingredients];
        state[index].ingredient.info = value;
        setIngredients(state);
    };

    const deleteIngredient = (index) => {
        setIngredients([...ingredients].filter((value, itemIndex) => index !== itemIndex));
    };
    const addIngredient = () => {
        setIngredients([...ingredients, emptyIngredient]);
    };

    function handleCookingGuideChange(value) {
        formik.setFieldValue('cookingGuide', value);
    }

    return (
        <Grid>
            <form onSubmit={submitForm}>
                <MainCard title={<FormattedMessage id="Add new dish" />} sx={{ margin: '0 0 24px' }}>
                    <Grid item xs={12} className="careployee-form__block">
                        <label htmlFor="name" className="careployee-form__label">
                            <FormattedMessage id="Dish name" />
                        </label>
                        <TextField
                            fullWidth
                            maxRows={4}
                            id="name"
                            type="text"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            error={formik.touched.name && Boolean(formik.errors.name)}
                            helperText={formik.touched.name && formik.errors.name}
                        />
                        {/* {errors.name && touched.name ? <div>{errors.name}</div> : null} */}
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <Grid item xs={12} className="careployee-form__block">
                            <FileUpload defaultPicture={formik.values.picture || '/images/dish.jpg'} onChange={handlePicture}></FileUpload>
                        </Grid>
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <label htmlFor="cookingGuide" className="careployee-form__label">
                            <FormattedMessage id="Cooking guide" />
                        </label>

                        <div data-color-mode="light">
                            <MDEditor value={formik.values.cookingGuide} onChange={handleCookingGuideChange} />
                            {/*<MDEditor.Markdown source={formik.values.cookingGuide} style={{ whiteSpace: 'pre-wrap' }} />*/}
                        </div>
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <label htmlFor="levelOfAdvance" className="careployee-form__label">
                            <FormattedMessage id="Level" />
                        </label>
                        <RadioGroup row value={levelOfAdvance} onChange={handleChange}>
                            <FormControlLabel label="Simple" value="SIMPLE" control={<Radio />} />
                            <FormControlLabel label="Normal" value="NORMAL" control={<Radio />} />
                            <FormControlLabel label="Advanced" value="ADVANCED" control={<Radio />} />
                            <FormControlLabel label="Pro" value="PRO" control={<Radio />} />
                        </RadioGroup>
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <label htmlFor="duration" className="careployee-form__label">
                            <FormattedMessage id="Dish preparation time" />
                        </label>
                        <TextField
                            fullWidth
                            maxRows={4}
                            id="duration"
                            value={formik.values.duration}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            error={formik.touched.duration && Boolean(formik.errors.duration)}
                            helperText={formik.touched.duration && formik.errors.duration}
                        />
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <label htmlFor="minPortion" className="careployee-form__label">
                            <FormattedMessage id="Min Portions" />
                        </label>
                        <TextField
                            fullWidth
                            maxRows={4}
                            id="minPortion"
                            value={formik.values.minPortion}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            error={formik.touched.minPortion && Boolean(formik.errors.minPortion)}
                            helperText={formik.touched.minPortion && formik.errors.minPortion}
                        />
                    </Grid>

                    {
                        <Grid item className="careployee-form__block">
                            <label htmlFor="isExtraChecked" className="careployee-form__label" style={{ marginBottom: '0' }}>
                                <FormattedMessage id="Extra dish" />
                            </label>
                            <FormControlLabel control={<Checkbox checked={isExtraTag ? true : false} />} onChange={handleIsExtraChecked} />
                        </Grid>
                    }
                </MainCard>

                {/* <MainCard title={<FormattedMessage id="Assign extra dish" />} sx={{margin: '0 0 24px'}}>
                    <Grid item xs={12}>
                        <Autocomplete
                            disablePortal
                            name="extraDishes"
                            multiple
                            value={
                                !!formik.values.extraDishesList && formik.values.extraDishesList.length
                                    ? [...formik.values.extraDishesList]
                                    : []
                            }
                            options={allExtraDishes}
                            getOptionLabel={(option) => option.name}
                            renderInput={(params) => <TextField {...params} label="Extra Name" />}
                            onChange={(_event, value) => {
                                handleDishes(value);
                            }}
                        />
                    </Grid>
                </MainCard> */}

                <MainCard title={<FormattedMessage id="Add ingredients" />}>
                    {ingredients.map((ingredient, index) => (
                        <Grid container key={index}>
                            <Grid item xs={12} md={2} className="careployee-form__block">
                                <Autocomplete
                                    disablePortal
                                    name="ingredientInfos"
                                    options={systemIngredients}
                                    placeholder
                                    value={ingredient.ingredient.info}
                                    getOptionLabel={(option) => option.name}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label={<FormattedMessage id="Ingredient Name" />}
                                            error={!!ingredient.ingredient.error}
                                            helperText={!!ingredient.ingredient.error && ingredient.ingredient.error}
                                        />
                                    )}
                                    onChange={(_event, value) => handleIngredient(index, value)}
                                />
                            </Grid>

                            <Grid item xs={12} md={1} className="careployee-form__block">
                                <TextField
                                    fullWidth
                                    maxRows={4}
                                    id="gramms"
                                    placeholder={intl.formatMessage({ id: 'Gramms' })}
                                    value={ingredient.gramms.label}
                                    onChange={(event) => handleIngredientGramms(index, event.target.value)}
                                    onBlur={formik.handleBlur}
                                    error={!!ingredient.gramms.error}
                                    helperText={!!ingredient.gramms.error && ingredient.gramms.error}
                                />
                            </Grid>

                            <Grid item xs={12} md={1} className="careployee-form__block">
                                <TextField
                                    fullWidth
                                    maxRows={4}
                                    id="quantity"
                                    placeholder={intl.formatMessage({ id: 'Quantity' })}
                                    value={ingredient.quantity.label}
                                    onChange={(event) => handleIngredientQuantity(index, event.target.value)}
                                    onBlur={formik.handleBlur}
                                    error={!!ingredient.quantity.error}
                                    helperText={!!ingredient.quantity.error && ingredient.quantity.error}
                                />
                            </Grid>

                            <Grid item xs={12} md={2} className="careployee-form__block">
                                <Autocomplete
                                    disablePortal
                                    id="unit"
                                    options={units}
                                    value={ingredient.unit.label}
                                    getOptionLabel={(option) => option}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label={intl.formatMessage({ id: 'Unit' })}
                                            error={!!ingredient.unit.error}
                                            helperText={!!ingredient.unit.error && ingredient.unit.error}
                                        />
                                    )}
                                    onChange={(_event, value) => handleIngredientUnit(index, value)}
                                />
                            </Grid>

                            <Grid item xs={12} md={2} className="careployee-form__block">
                                <FormControlLabel
                                    control={<Checkbox checked={ingredient.necessary} />}
                                    onChange={(_event, value) => handleIngredientNecessary(index, value)}
                                />

                                <Button
                                    variant="contained"
                                    size="small"
                                    color="error"
                                    onClick={() => deleteIngredient(index)}
                                    sx={{ margin: '5px' }}
                                >
                                    <FormattedMessage id="Delete ingredient" />
                                </Button>
                            </Grid>
                        </Grid>
                    ))}

                    <Button variant="contained" onClick={() => addIngredient()} sx={{ margin: '5px' }}>
                        +
                    </Button>
                </MainCard>

                <Grid item xs={12}>
                    <Button className="careployee-form__submit-button" variant="contained" size="large" onClick={submitForm}>
                        <FormattedMessage id="Submit" />
                    </Button>
                </Grid>
            </form>
        </Grid>
    );
}

export default DishForm;
