import * as Yup from 'yup';

import {
    Autocomplete,
    Box,
    Button,
    CircularProgress,
    Collapse,
    Divider,
    Grid,
    IconButton,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Tooltip,
    Typography
} from '@mui/material';
import { FormattedMessage, useIntl } from 'react-intl';
import {
    addCompany,
    addCompanyOrganisation,
    getCompanyById,
    getCompanyOrganisation,
    loadCompanies
} from '../../../../services/CompanyService';

import BlockTwoToneIcon from '@mui/icons-material/BlockTwoTone';
import FileUpload from '../../../forms/components/FileUpload';
import { HiPencil } from 'react-icons/hi';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import LinearProgress from '@mui/material/LinearProgress';
import MainCard from '../../../../ui-component/cards/MainCard';
import React from 'react';
import SubCard from 'ui-component/cards/SubCard';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from '../../../../utils/form/Communication';
import { useDispatch } from 'store';
import { useFormik } from 'formik';
import { useParams } from 'react-router-dom';
import { useTheme } from '@mui/styles';

function CollapseRow({ row, openEditForm, deleteItem }) {
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const typeTranslations = {
        department: { name: 'Unit' },
        unit: { name: 'Team' }
    };

    return (
        <>
            <TableRow hover sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell align="right">
                    {row.children && row.children.length ? (
                        <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                        </IconButton>
                    ) : (
                        <></>
                    )}
                </TableCell>

                <TableCell>{row.unitName}</TableCell>
                <TableCell>{row.shortName}</TableCell>
                <TableCell>{row.parentUnit ? row.parentUnit.unitName : ''}</TableCell>
                <TableCell>{row.notes}</TableCell>
                <TableCell align="center" sx={{ pr: 3 }}>
                    <Stack direction="row" justifyContent="center" alignItems="center">
                        <Tooltip placement="top" title="edit">
                            <IconButton color="primary" aria-label="edit" onClick={() => openEditForm(row.unitName)} size="large">
                                <HiPencil sx={{ fontSize: '1.1rem' }} />
                            </IconButton>
                        </Tooltip>

                        <Tooltip placement="top" title="Delete" onClick={() => deleteItem(row.unitName)}>
                            <IconButton
                                color="primary"
                                sx={{
                                    color: theme.palette.orange.dark,
                                    borderColor: theme.palette.orange.main,
                                    '&:hover ': { background: theme.palette.orange.light }
                                }}
                                size="large"
                            >
                                <BlockTwoToneIcon sx={{ fontSize: '1.1rem' }} />
                            </IconButton>
                        </Tooltip>
                    </Stack>
                </TableCell>
            </TableRow>
            {row.children && row.children.length ? (
                <TableRow>
                    <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                        <Collapse in={open} timeout="auto" unmountOnExit>
                            {open && (
                                <Box sx={{ margin: 1 }}>
                                    <TableContainer>
                                        <SubCard
                                            sx={{ bgcolor: theme.palette.mode === 'dark' ? 'dark.800' : 'grey.50', mb: 2 }}
                                            content={false}
                                        >
                                            <Table size="small" aria-label="purchases">
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell scope="col"></TableCell>
                                                        <TableCell scope="col">
                                                            <FormattedMessage id={typeTranslations[row.level].name} />
                                                        </TableCell>
                                                        <TableCell scope="col">
                                                            <FormattedMessage id="Short name" />
                                                        </TableCell>
                                                        <TableCell scope="col">
                                                            <FormattedMessage id="Parent unit" />
                                                        </TableCell>
                                                        <TableCell scope="col">
                                                            <FormattedMessage id="Notes" />
                                                        </TableCell>
                                                        <TableCell align="center" sx={{ pr: 3 }}>
                                                            <FormattedMessage id="Actions" />
                                                        </TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {(row.children || []).map((child, index) => (
                                                        <CollapseRow
                                                            openEditForm={openEditForm}
                                                            key={index}
                                                            deleteItem={deleteItem}
                                                            row={child}
                                                        ></CollapseRow>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </SubCard>
                                    </TableContainer>
                                </Box>
                            )}
                        </Collapse>
                    </TableCell>
                </TableRow>
            ) : (
                <></>
            )}
        </>
    );
}

function CompanyHierarchyForm() {
    const formTypes = [
        { label: 'Set Department', id: 'department' },
        { label: 'Set Unit', id: 'unit' },
        { label: 'Set Team', id: 'team' }
    ];
    const initialValues = {
        unitName: '',
        shortName: '',
        parentUnitShortName: null,
        leaders: [],
        notes: ''
    };

    const dispatch = useDispatch();
    const theme = useTheme();
    const intl = useIntl();

    const validationSchema = Yup.object({
        unitName: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(100, <FormattedMessage id="Input Max Length" values={{ length: 100 }} />)
            .required(<FormattedMessage id="Required" />)
    });

    const [company, setCompany] = React.useState({});
    const [companyUnits, setCompanyUnits] = React.useState({});
    const [leader, setLeader] = React.useState('');
    const [leaders, setLeaders] = React.useState([]);
    const [formType, setFormType] = React.useState({});

    React.useEffect(() => {
        getCompanyOrganisation(loggedInUser.company.id)
            .then((res) => res.data)
            .then((data) => {
                
                
                setCompanyUnits(data ?? []);
                
                setCompany(loggedInUser.company);
            })
            .catch((e) => {
                dispatch(showMessage(e.response?.data?.message));
            });
    }, []);

    const formik = useFormik({
        initialValues: { ...initialValues },
        validationSchema,
        onSubmit: () => {
            if (formik.values.parentUnitShortName && formik.values.parentUnitShortName.shortName) {
                formik.values.parentUnitShortName = formik.values.parentUnitShortName.shortName;
            }

            addCompanyOrganisation(loggedInUser.company.id, formik.values)
                .then(() => {
                    formik.setValues({ ...initialValues });
                    dispatch(showMessage('Successfully submitted', 'success'));
                    getCompanyById(loggedInUser.company.id) // i think here is a problem: company name must be in paramter
                        .then((res) => res.data)
                        .then((companyData) => {
                            setCompany(companyData);
                        })
                        .catch((e) => {
                            dispatch(showMessage(e?.response?.data?.message));
                        });
                })
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
    });

    const submitForm = (event) => {
        formik.validateForm().then((errors) => {
            if (errors && Object.keys(errors).length > 0) {
                dispatch(showMessage(<FormattedMessage id="Validation Fail" />));
            }
            formik.handleSubmit(event);
        });
    };

    const openEditForm = (unitName) => {
        const unit = companyUnits.find((unit) => unit.unitName === unitName);

        
        if (unit) {
            formik.setValues({
                unitName: unit.unitName,
                shortName: unit.shortName,
                parentUnitShortName: unit.parentUnit ?? null,
                leaders: unit.leaders ?? null,
                notes: unit.notes
            });

            setLeaders(unit.leaders.map((leader) => leader.email));
            setFormType(!!unit.parentUnit ? formType : formTypes.find((type) => type.id === 'department'));
        }
    };

    const getUnits = () => {
        
        const units = companyUnits && companyUnits.length ? companyUnits : [];
        let unitsWithChildren = [];
        
        unitsWithChildren = units.filter((unit) => !unit.parentUnit);
        unitsWithChildren = unitsWithChildren.map((unit) => ({
            ...unit,
            level: 'department',
            children: units
                .filter((anyUnit) => {
                    return anyUnit.parentUnitShortName && anyUnit.parentUnitShortName === unit.shortName;
                })
                .map((anyUnit) => ({
                    ...anyUnit,
                    level: 'unit',
                    children: units
                        .filter((originalUnit) => {
                            return originalUnit.parentUnitShortName && originalUnit.parentUnitShortName === anyUnit.shortName;
                        })
                        .map((unit) => ({ ...unit, level: 'team' }))
                }))
        }));
        //
        unitsWithChildren = unitsWithChildren.filter((unit) => !unit.parentUnitShortName);
        
        return unitsWithChildren;
    };

    const deleteItem = (unitName) => {};

    return (
        <Grid>
            <MainCard sx={{ marginBottom: '24px' }} title={<FormattedMessage id="Company units" />}>
                <TableContainer>
                    {!getUnits() ||
                        (getUnits().length === 0 && (
                            <LinearProgress
                                variant="indeterminate"
                                sx={{
                                    backgroundColor: '#cbab7075',
                                    '& .MuiLinearProgress-bar': {
                                        backgroundColor: '#cbab70'
                                    }
                                }}
                            />
                        ))}
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell scope="col"> </TableCell>
                                <TableCell scope="col">
                                    <FormattedMessage id="Department" />
                                </TableCell>
                                <TableCell scope="col">
                                    <FormattedMessage id="Department short name" />
                                </TableCell>
                                <TableCell scope="col">
                                    <FormattedMessage id="Parent unit" />
                                </TableCell>
                                <TableCell scope="col">
                                    <FormattedMessage id="Notes" />
                                </TableCell>
                                <TableCell align="center" sx={{ pr: 3 }}>
                                    <FormattedMessage id="Actions" />
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {getUnits().map((row, index) => (
                                <CollapseRow row={row} key={index} openEditForm={openEditForm} deleteItem={deleteItem}></CollapseRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </MainCard>

            <Grid item xs={12} md={5} className="careployee-form__block">
                <label htmlFor="leaders" className="careployee-form__label">
                    <FormattedMessage id="Actions" />
                </label>
                <Autocomplete
                    disablePortal
                    id="options"
                    options={formTypes}
                    defaultValue={{ label: 'New Unit', id: 'unit' }}
                    getOptionLabel={(option) => option.label}
                    renderInput={(params) => <TextField {...params} />}
                    onChange={(_event, value) => {
                        setFormType(value);
                        formik.setValues({ ...initialValues });
                        // setCompanyUnits(row.companyUnits ?? []);
                    }}
                />
            </Grid>

            <form onSubmit={submitForm}>
                <MainCard title={<FormattedMessage id={formType.label || 'New Unit'} />}>
                    <Grid container>
                        <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="unitName" className="careployee-form__label">
                                <FormattedMessage id="Unit name" />
                            </label>
                            <TextField
                                id="unitName"
                                style={{ width: '100%' }}
                                value={formik.values.unitName}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.unitName && Boolean(formik.errors.unitName)}
                                helperText={formik.touched.unitName && formik.errors.unitName}
                            />
                        </Grid>

                        <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="shortName" className="careployee-form__label">
                                <FormattedMessage id="Unit short name" />
                            </label>
                            <TextField
                                id="shortName"
                                style={{ width: '100%' }}
                                value={formik.values.shortName}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.shortName && Boolean(formik.errors.shortName)}
                                helperText={formik.touched.shortName && formik.errors.shortName}
                            />
                        </Grid>

                        {formType.id !== 'department' && (
                            <Grid item xs={12} className="careployee-form__block">
                                <label htmlFor="parentUnitShortName" className="careployee-form__label">
                                    <FormattedMessage id="Parent Unit" />
                                </label>

                                <Autocomplete
                                    disablePortal
                                    id="parentUnitShortName"
                                    options={companyUnits}
                                    getOptionLabel={(option) => option.unitName}
                                    value={formik.values.parentUnitShortName}
                                    renderInput={(params) => <TextField {...params} />}
                                    onChange={(_event, value) => {
                                        formik.setValues({ ...formik.values, parentUnitShortName: value });
                                    }}
                                />
                            </Grid>
                        )}

                        <Grid container className="careployee-form__block">
                            <Grid item xs={12} md={5} className="careployee-form__block">
                                <label htmlFor="leaders" className="careployee-form__label">
                                    <FormattedMessage id="Leaders" />
                                </label>
                                <Autocomplete
                                    disablePortal
                                    id="leaders"
                                    options={leaders && leaders.length ? leaders : []}
                                    multiple
                                    value={formik.values.leaders}
                                    getOptionLabel={(option) => option}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label={intl.formatMessage({ id: 'leaders' })}
                                            error={!!formik.values.leaders.error}
                                            helperText={!!formik.values.leaders.error && formik.values.leaders.error}
                                        />
                                    )}
                                    onChange={(_event, value) => formik.setValues({ ...formik.values, leaders: value })}
                                />
                            </Grid>

                            <Grid item xs={12} md={5} className="careployee-form__block">
                                <label htmlFor="leaders" className="careployee-form__label">
                                    new leader name
                                </label>
                                <TextField
                                    id="leaders"
                                    style={{ width: '100%' }}
                                    value={leader}
                                    onChange={(event) => setLeader(event.target.value)}
                                    onBlur={(event) => {
                                        setLeader('');
                                        if (event.target.value && !leaders.includes(event.target.value))
                                            setLeaders([...leaders, event.target.value]);
                                    }}
                                    error={formik.touched.leaders && Boolean(formik.errors.leaders)}
                                    helperText={formik.touched.leaders && formik.errors.leaders}
                                />
                            </Grid>
                        </Grid>

                        <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="notes" className="careployee-form__label">
                                <FormattedMessage id="Notes" />
                            </label>
                            <TextField
                                id="notes"
                                fullWidth
                                multiline
                                rows={5}
                                value={formik.values.notes}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.notes && Boolean(formik.errors.notes)}
                                helperText={formik.touched.notes && formik.errors.notes}
                            />

                            <Divider sx={{ marginTop: '36px' }}></Divider>
                        </Grid>
                    </Grid>

                    <Grid container>
                        {/*
                        <Grid item xs={12} sx={{ marginBottom: '48px' }}>
                            <Button
                                variant="contained"
                                sx={{
                                    background: theme.palette.success.dark,
                                    '&:hover': { background: theme.palette.success.main }
                                }}
                                size="large"
                                onClick={addNewUnit}
                            >
                                <FormattedMessage id="Add new Company unit" />
                            </Button>
                        </Grid>
                            */}
                        <Grid item xs={12}>
                            <Button className="careployee-form__submit-btn" variant="contained" size="large" onClick={submitForm}>
                                <FormattedMessage id="Submit" />
                            </Button>
                        </Grid>
                    </Grid>
                </MainCard>
            </form>
        </Grid>
    );
}

export default CompanyHierarchyForm;
