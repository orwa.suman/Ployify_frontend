import * as Yup from 'yup';

import { Autocomplete, Button, Grid, TextField } from '@mui/material';
import { FormattedMessage, useIntl } from 'react-intl';
import { addCompany, loadCompanies, updateCompany } from '../../../../services/CompanyService';

import FileUpload from '../../../forms/components/FileUpload';
import MainCard from '../../../../ui-component/cards/MainCard';
import React from 'react';
import { showMessage } from '../../../../utils/form/Communication';
import { uploadToCloudinary } from 'services/CloudinaryService';
import { useDispatch } from 'store';
import { useFormik } from 'formik';
import { useParams } from 'react-router-dom';

function CompanyForm() {
    const dispatch = useDispatch();
    const { id } = useParams();
    const intl = useIntl();

    const [picture, setPicture] = React.useState([]);
    const [pictureName, setPictureName] = React.useState([]);
    const [pictureChanged, setPictureChanged] = React.useState(false);

    const [domain, setDomain] = React.useState('');
    const [domains, setDomains] = React.useState([]);

    const validationSchema = Yup.object({
        companyName: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(100, <FormattedMessage id="Input Max Length" values={{ length: 100 }} />)
            .required(<FormattedMessage id="Required" />),
        address: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(300, <FormattedMessage id="Input Max Length" values={{ length: 300 }} />),
        //.required(<FormattedMessage id="Required" />),
        superUserEmail: Yup.string()
            .email(<FormattedMessage id="Must be an email" />)
            .min(10, <FormattedMessage id="Input Min Length" values={{ length: 10 }} />)
            .max(200, <FormattedMessage id="Input Max Length" values={{ length: 200 }} />),
        //.required(<FormattedMessage id="Required" />),
        orderedAccounts: Yup.number(<FormattedMessage id="Must be a number" />)
            .min(1)
            .required(<FormattedMessage id="Required" />)
    });

    const [rewardInfoDtos, setRewardInfoDtos] = React.useState([]);

    const formik = useFormik({
        initialValues: {
            companyName: '',
            address: '',
            superUserEmail: '',
            rewardInfoDtos: [],
            picture: undefined,
            orderedAccounts: 1,
            notes: '',
            emailDomains: domains
        },
        validationSchema,
        onSubmit: () => {
            
            uploadToCloudinary(picture, pictureName, 'company_', '/Ployify/Company')
                .then((data) => {
                    if (pictureChanged) {
                        
                        
                        formik.values.picture = data.url;
                    }

                    if (id) {
                        
                        updateCompany(id, formik.values);
                    } else {
                        addCompany(formik.values)
                            .then(() => dispatch(showMessage('Successfully submitted', 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    }
                })
                .catch((err) => {
                    
                });
        }
    });

    React.useEffect(() => {
        if (id) {
            loadCompanies()
                .then((res) => res.data)
                .then((data) => {
                    const row = data.find((item) => item.id === id);
                    if (!row) {
                        return;
                    }
                    formik.setValues({
                        companyName: row.companyName,
                        address: row.address,
                        superUserEmail: row.superUserEmail,
                        rewardInfoDtos: row.rewardInfoDtos,
                        picture: row.picture,
                        orderedAccounts: row.orderedAccounts,
                        notes: row.notes,
                        emailDomains: row.emailDomains
                    });
                    setDomains(row.emailDomains || []);
                })
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
    }, []);

    
    
    const submitForm = (event) => {
        formik.validateForm().then((errors) => {
            if (errors && Object.keys(errors)?.length > 0) {
                const errorMessages = Object.entries(errors).map(([fieldName, fieldError]) => {
                    return `${fieldName}: ${fieldError?.props?.id}`;
                });

                dispatch(showMessage(<FormattedMessage id={errorMessages.join(', ')} />));
            }
            formik.handleSubmit(event);
        });
    };

    const handlePicture = (data, fileName) => {
        setPicture(data);
        setPictureName(fileName);
        setPictureChanged(true);
    };

    const handleRewardInfoDtos = (data) => {
        setRewardInfoDtos(data);
    };

    return (
        <Grid>
            <form onSubmit={submitForm}>
                <MainCard title="Erste Seite">
                    <Grid container>
                        <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="companyName" className="careployee-form__label">
                                Company name
                            </label>
                            <TextField
                                id="companyName"
                                style={{ width: '100%' }}
                                value={formik.values.companyName}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.companyName && Boolean(formik.errors.companyName)}
                                helperText={formik.touched.companyName && formik.errors.companyName}
                            />
                        </Grid>

                        <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="address" className="careployee-form__label">
                                Company Address
                            </label>
                            <TextField
                                id="address"
                                style={{ width: '100%' }}
                                value={formik.values.address}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.address && Boolean(formik.errors.address)}
                                helperText={formik.touched.address && formik.errors.address}
                            />
                        </Grid>

                        <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="superUserEmail" className="careployee-form__label">
                                SuperUser Email
                            </label>
                            <TextField
                                id="superUserEmail"
                                style={{ width: '100%' }}
                                value={formik.values.superUserEmail}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.superUserEmail && Boolean(formik.errors.superUserEmail)}
                                helperText={formik.touched.superUserEmail && formik.errors.superUserEmail}
                            />
                        </Grid>
                        <Grid container>
                            <Grid item xs={12} md={5} className="careployee-form__block">
                                <label htmlFor="domain" className="careployee-form__label">
                                    <FormattedMessage id="Email Domain" />
                                </label>
                                <Autocomplete
                                    disablePortal
                                    id="domain"
                                    options={domains && domains?.length ? domains : []}
                                    multiple
                                    getOptionLabel={(option) => option}
                                    value={formik.values.emailDomains || ''}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label={intl.formatMessage({ id: 'domain' })}
                                            error={!!formik.values.emailDomains?.error}
                                            helperText={!!formik.values.emailDomains?.error}
                                        />
                                    )}
                                    onChange={(_event, value) => formik.setValues({ ...formik.values, emailDomains: value })}
                                />
                            </Grid>

                            <Grid item xs={12} md={5} className="careployee-form__block">
                                <label htmlFor="domains" className="careployee-form__label">
                                    <FormattedMessage id="new domain" />
                                </label>
                                <TextField
                                    id="domains"
                                    style={{ width: '100%' }}
                                    value={domain}
                                    type="email"
                                    onChange={(event) => setDomain(event.target.value)}
                                    onBlur={(event) => {
                                        setDomain('');

                                        if (event.target.value && !domains.includes(event.target.value))
                                            setDomains([...domains, event.target.value]);
                                    }}
                                />
                            </Grid>
                        </Grid>

                        <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="orderedAccounts" className="careployee-form__label">
                                Number of Ordered Accounts
                            </label>
                            <TextField
                                id="orderedAccounts"
                                style={{ width: '100%' }}
                                value={formik.values.orderedAccounts}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                type="number"
                                error={formik.touched.orderedAccounts && Boolean(formik.errors.orderedAccounts)}
                                helperText={formik.touched.orderedAccounts && formik.errors.orderedAccounts}
                            />
                        </Grid>

                        <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="notes" className="careployee-form__label">
                                <FormattedMessage id="Notes" />
                            </label>
                            <TextField
                                id="notes"
                                fullWidth
                                multiline
                                rows={5}
                                value={formik.values.notes}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.notes && Boolean(formik.errors.notes)}
                                helperText={formik.touched.notes && formik.errors.notes}
                            />
                        </Grid>

                        <Grid item xs={12} className="careployee-form__block">
                            <FileUpload defaultPicture={formik.values.picture} onChange={handlePicture}></FileUpload>
                        </Grid>
                        <Grid item xs={12}>
                            <Button className="careployee-form__submit-btn" variant="contained" size="large" onClick={submitForm}>
                                <FormattedMessage id="Submit" />
                            </Button>
                        </Grid>
                    </Grid>
                </MainCard>
            </form>
        </Grid>
    );
}

export default CompanyForm;
