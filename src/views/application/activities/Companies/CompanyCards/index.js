import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../../../assets/scss/style.css';

import {
    IconButton,
    LinearProgress,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip
} from '@mui/material';
import { deleteCompany, loadCompanies } from 'services/CompanyService';

import AddIcon from '@mui/icons-material/Add';
import BlockTwoToneIcon from '@mui/icons-material/BlockTwoTone';
import { FormattedMessage } from 'react-intl';
import { HiPencil } from 'react-icons/hi';
import React from 'react';
import { addRegistrationCodesToCompany } from 'services/RegisterService';
import generate from 'namor/dist/generate';
import { showMessage } from 'utils/form/Communication';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';

function List() {
    const theme = useTheme();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    let [data, setData] = React.useState([]);
    let [loading, setLoading] = React.useState(true);

    const loadItems = () => {
        loadCompanies()
            .then((res) => res.data)
            .then(setData)
            .then(setLoading(false))
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
                setLoading(false);
            });
    };

    const deleteItem = (id) => {
        deleteCompany(id)
            .then(() => loadItems())
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const generateRegCodes = (companyId) => {
        addRegistrationCodesToCompany(companyId)
            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    React.useEffect(() => {
        loadItems();
    }, []);

    return (
        <TableContainer>
            {loading && (
                <LinearProgress
                    variant="indeterminate"
                    sx={{
                        backgroundColor: '#cbab7075',
                        '& .MuiLinearProgress-bar': {
                            backgroundColor: '#cbab70'
                        }
                    }}
                />
            )}
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell scope="col">
                            <FormattedMessage id="Name" />
                        </TableCell>

                        <TableCell scope="col">
                            <FormattedMessage id="Address" />
                        </TableCell>
                        <TableCell scope="col">
                            <FormattedMessage id="Picture" />
                        </TableCell>

                        <TableCell align="center" sx={{ pr: 3 }}>
                            <FormattedMessage id="Actions" />
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data &&
                        data.map((row, index) => (
                            <TableRow hover key={index}>
                                <TableCell>{row.companyName}</TableCell>
                                <TableCell>{row.address}</TableCell>
                                <TableCell>
                                    {!!row.picture ? (
                                        <div className="careployee-training-plan__image">
                                            <img src={row.picture} alt={row.companyName}></img>
                                        </div>
                                    ) : (
                                        <></>
                                    )}
                                </TableCell>
                                <TableCell align="center" sx={{ pr: 3 }}>
                                    <Stack direction="row" justifyContent="center" alignItems="center">
                                        <Tooltip placement="top" title="edit">
                                            <IconButton
                                                color="primary"
                                                aria-label="edit"
                                                onClick={() => navigate('/dashboard/company/form/' + row.id)}
                                                size="large"
                                            >
                                                <HiPencil sx={{ fontSize: '1.1rem' }} />
                                            </IconButton>
                                        </Tooltip>

                                        <Tooltip placement="top" title="Delete" onClick={() => deleteItem(row.id)}>
                                            <IconButton
                                                color="primary"
                                                sx={{
                                                    color: theme.palette.orange.dark,
                                                    borderColor: theme.palette.orange.main,
                                                    '&:hover ': { background: theme.palette.orange.light }
                                                }}
                                                size="large"
                                            >
                                                <BlockTwoToneIcon sx={{ fontSize: '1.1rem' }} />
                                            </IconButton>
                                        </Tooltip>

                                        <Tooltip placement="top" title="Add Registration Codes" onClick={() => generateRegCodes(row.id)}>
                                            <IconButton
                                                color="primary"
                                                sx={{
                                                    color: theme.palette.orange.dark,
                                                    borderColor: theme.palette.orange.main,
                                                    '&:hover ': { background: theme.palette.orange.light }
                                                }}
                                                size="large"
                                            >
                                                <AddIcon sx={{ fontSize: '1.1rem' }} />
                                            </IconButton>
                                        </Tooltip>
                                    </Stack>
                                </TableCell>
                            </TableRow>
                        ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default List;
