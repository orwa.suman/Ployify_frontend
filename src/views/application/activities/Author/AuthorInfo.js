import React from 'react';
import MainCard from '../../../../ui-component/cards/MainCard';
import { useDispatch } from 'store';
import { Grid } from '@mui/material';
import { useParams } from 'react-router-dom';
import { getAllCreatedContentOfAuthor, getAuthorByName } from 'services/AuthorService';

function AuthorInfo() {
    const { id, firstName, lastName } = useParams();
    const dispatch = useDispatch();
    let [data, setData] = React.useState([]);

    React.useEffect(() => {
        getAllCreatedContentOfAuthor(id)
            .then((res) => res.data)
            .then(setData)
            .catch((e) => {
                
            });

        getAuthorByName(firstName, lastName)
            .then((res) => res.data)
            .then(setData)
            .catch((e) => {
                
            });
    }, []);

    return (
        <MainCard title="Author Info">
            <Grid container>
                <Grid item xs={12} className="careployee-form__block">
                    <h3>{`${firstName} ${lastName}`}</h3>
                </Grid>
            </Grid>
        </MainCard>
    );
}

export default AuthorInfo;
