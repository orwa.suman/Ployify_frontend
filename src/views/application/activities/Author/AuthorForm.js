import React from 'react';
import { useFormik } from 'formik';
import MainCard from '../../../../ui-component/cards/MainCard';
import FileUpload from '../../../forms/components/FileUpload';
import { addAuthor, loadAuthors, updateAuthor } from '../../../../services/AuthorService';
import * as Yup from 'yup';
import { useDispatch } from 'store';
import { Button, Grid, TextField } from '@mui/material';
import { showMessage } from '../../../../utils/form/Communication';
import 'react-quill/dist/quill.snow.css';
import { FormattedMessage } from 'react-intl';
import { useParams } from 'react-router-dom';
import { uploadToCloudinary } from 'services/CloudinaryService';

function AuthorForm() {
    const dispatch = useDispatch();
    const { id } = useParams();

    const [picture, setPicture] = React.useState([]);
    const [pictureName, setPictureName] = React.useState([]);
    const [changed, setChanged] = React.useState(false);

    React.useEffect(() => {
        if (id) {
            loadAuthors()
                .then((res) => res.data)
                .then((data) => {
                    const row = data.find((item) => item.id === id);
                    formik.setValues({
                        firstName: row.firstName,
                        lastName: row.lastName,
                        picture: row.picture,
                        description: row.description,
                        notes: row.notes,
                        url: row.url,
                        createdContent: row.createdContent
                    });
                })
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
    }, []);

    const validationSchema = Yup.object({
        firstName: Yup.string()
            .max(50, <FormattedMessage id="Input Max Length" values={{ length: 50 }} />)
            .required(<FormattedMessage id="Required" />),
        lastName: Yup.string()
            .max(50, <FormattedMessage id="Input Max Length" values={{ length: 50 }} />)
            .required(<FormattedMessage id="Required" />)
    });

    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            picture: '',
            description: '',
            notes: '',
            url: undefined,
            createdContent: []
        },
        validationSchema,
        onSubmit: () => {

            uploadToCloudinary(picture, pictureName, "author_", '/Ployify/Author')
                .then((data) => {

                    if (changed) {
                        
                        
                        formik.values.picture = data.url;
                    }

                    if (id) {
                        updateAuthor(id, formik.values)
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    } else {
                        addAuthor(formik.values)
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    }
                }).catch((err) => {
                    
                });
        }
    });

    const submitForm = (event) => {
        formik.validateForm().then((errors) => {
            if (errors && Object.keys(errors).length > 0) {

                const errorMessages = Object.entries(errors).map(([fieldName, fieldError]) => {
                    return `${fieldName}: ${fieldError.props.id}`;
                });

                dispatch(showMessage(<FormattedMessage id={errorMessages.join(', ')} />));
            }
            formik.handleSubmit(event);
        });
    };

    const handleFileUpload = (data, fileName) => {
        setPicture(data);
        setPictureName(fileName);
        setChanged(true);
    };

    return (
        <Grid>
            <form onSubmit={submitForm}>
                <MainCard title={<FormattedMessage id="Authors" />}>
                    <Grid container>
                        <Grid container xs={12} lg={10} className="careployee-form__block">
                            <Grid item xs={12} md={5} className="careployee-form__block">
                                <label htmlFor="title" className="careployee-form__label">
                                    <FormattedMessage id="First Name" />
                                </label>
                                <TextField
                                    fullWidth
                                    id="firstName"
                                    value={formik.values.firstName}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    error={formik.touched.firstName && Boolean(formik.errors.firstName)}
                                    helperText={formik.touched.firstName && formik.errors.firstName}
                                />
                            </Grid>

                            <Grid item xs={12} md={5} className="careployee-form__block">
                                <label htmlFor="lastName" className="careployee-form__label">
                                    <FormattedMessage id="Last Name" />
                                </label>
                                <TextField
                                    fullWidth
                                    id="lastName"
                                    value={formik.values.lastName}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    error={formik.touched.lastName && Boolean(formik.errors.lastName)}
                                    helperText={formik.touched.lastName && formik.errors.lastName}
                                />
                            </Grid>

                            <Grid item xs={12} md={10} className="careployee-form__block">
                                <label htmlFor="url" className="careployee-form__label">
                                    <FormattedMessage id="Url" />
                                </label>
                                <TextField
                                    fullWidth
                                    id="url"
                                    value={formik.values.url}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    error={formik.touched.url && Boolean(formik.errors.url)}
                                    helperText={formik.touched.url && formik.errors.url}
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} className="careployee-form__block">
                            <label htmlFor="description" className="careployee-form__label">
                                <FormattedMessage id="Description" />
                            </label>
                            <TextField
                                fullWidth
                                id="description"
                                multiline
                                rows={4}
                                value={formik.values.description}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.description && Boolean(formik.errors.description)}
                            />
                        </Grid>
                        <Grid item xs={12} md={5} className="careployee-form__block">
                            <label htmlFor="notes" className="careployee-form__label">
                                <FormattedMessage id="Notes" />
                            </label>
                            <TextField
                                fullWidth
                                id="notes"
                                multiline
                                rows={13}
                                value={formik.values.notes}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.notes && Boolean(formik.errors.notes)}
                            />
                        </Grid>

                        <Grid item xs={12} md={5} className="careployee-form__block">
                            <FileUpload defaultPicture={formik.values.picture} label={<FormattedMessage id="Picture" />} onChange={handleFileUpload}></FileUpload>
                        </Grid>
                        <Grid item xs={12}>
                            <Button className="careployee-form__submit-btn" variant="contained" size="large" onClick={submitForm}>
                                <FormattedMessage id="Submit" />
                            </Button>
                        </Grid>
                    </Grid>
                </MainCard>
            </form>
        </Grid>
    );
}

export default AuthorForm;
