import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    authorRoot: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        marginBottom: theme.spacing(2)
    },
    authorAvatar: {
        marginRight: theme.spacing(2)
    },
    authorImg: {
        width: 30,
        height: 30,
        borderRadius: '50%',
        marginLeft: '10px',
        marginRight: theme.spacing(2)
    }
}));

function AuthorBasic({ firstName, lastName, description, avatarUrl }) {
    const classes = useStyles();

    return (
        <div
            className={classes.authorRoot}
            style={{ flexDirection: 'row', justifyContent: 'start', alignItems: 'center', marginTop: '10px' }}
        >
            <img src={avatarUrl} alt={firstName} className={classes.authorImg} />
            <div>
                <h6>
                    {firstName} {lastName}
                </h6>
            </div>
        </div>
    );
}

export default AuthorBasic;
