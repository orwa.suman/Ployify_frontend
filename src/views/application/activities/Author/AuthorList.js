import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../../assets/scss/style.css';

import {
    Grid,
    IconButton,
    InputAdornment,
    LinearProgress,
    OutlinedInput,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
    Typography
} from '@mui/material';
import { deleteAuthor, loadAuthors } from 'services/AuthorService';

import BlockTwoToneIcon from '@mui/icons-material/BlockTwoTone';
import ChatBubbleTwoToneIcon from '@mui/icons-material/ChatBubbleTwoTone';
import { FormattedMessage } from 'react-intl';
import { HiPencil } from 'react-icons/hi';
import { IconSearch } from '@tabler/icons';
import MainCard from 'ui-component/cards/MainCard';
import React from 'react';
import { dispatch } from 'store';
import { gridSpacing } from 'store/constant';
import { showMessage } from 'utils/form/Communication';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';

// assets

function AuthorList() {
    const theme = useTheme();
    const navigate = useNavigate();

    let [data, setData] = React.useState([]);
    let [isLoading, setLoading] = React.useState(true);

    const loadAuthorsList = () => {
        loadAuthors()
            .then((res) => res.data)
            .then(setData)
            .then(setLoading(false))
            .catch((e) => {
                
                setLoading(false);
            });
    };

    React.useEffect(() => {
        loadAuthorsList();
    }, []);

    const deleteItem = (id) => {
        deleteAuthor(id)
            .then(() => {
                dispatch(showMessage(<FormattedMessage id="Deleted Successfully" />, 'success'));
                loadAuthorsList();
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    return (
        <MainCard
            title={
                <Grid container alignItems="center" justifyContent="space-between" spacing={gridSpacing}>
                    <Grid item>
                        <Typography variant="h3">
                            <FormattedMessage id="List" />
                        </Typography>
                    </Grid>
                    <Grid item>
                        <OutlinedInput
                            id="input-search-list-style1"
                            placeholder="search"
                            startAdornment={
                                <InputAdornment position="start">
                                    <IconSearch stroke={1.5} size="16px" />
                                </InputAdornment>
                            }
                            size="small"
                        />
                    </Grid>
                </Grid>
            }
            content={false}
        >
            <TableContainer>
                {isLoading && (
                    <LinearProgress
                        variant="indeterminate"
                        sx={{
                            backgroundColor: '#cbab7075',
                            '& .MuiLinearProgress-bar': {
                                backgroundColor: '#cbab70'
                            }
                        }}
                    />
                )}
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell scope="col">
                                <FormattedMessage id="Picture" />
                            </TableCell>
                            <TableCell scope="col">
                                <FormattedMessage id="Name" />
                            </TableCell>
                            <TableCell scope="col">
                                <FormattedMessage id="Url" />
                            </TableCell>
                            <TableCell align="center" sx={{ pr: 3 }}>
                                <FormattedMessage id="Actions" />
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data &&
                            data.map((row, index) => (
                                <TableRow hover key={index}>
                                    <TableCell>
                                        {!!row.picture ? (
                                            <div className="careployee-training-plan__image">
                                                <img src={row.picture}></img>
                                            </div>
                                        ) : (
                                            ''
                                        )}
                                    </TableCell>
                                    <TableCell>{`${row.firstName} ${row.lastName}`}</TableCell>
                                    <TableCell>{row.url}</TableCell>
                                    <TableCell align="center" sx={{ pr: 3 }}>
                                        <Stack direction="row" justifyContent="center" alignItems="center">
                                            <Tooltip placement="top" title="edit">
                                                <IconButton
                                                    color="primary"
                                                    aria-label="edit"
                                                    onClick={() => navigate('/dashboard/author/form/' + row.id)}
                                                    size="large"
                                                >
                                                    <HiPencil sx={{ fontSize: '1.1rem' }} />
                                                </IconButton>
                                            </Tooltip>

                                            <Tooltip placement="top" title="Delete">
                                                <IconButton
                                                    color="primary"
                                                    sx={{
                                                        color: theme.palette.orange.dark,
                                                        borderColor: theme.palette.orange.main,
                                                        '&:hover ': { background: theme.palette.orange.light }
                                                    }}
                                                    size="large"
                                                    onClick={() => deleteItem(row.id)}
                                                >
                                                    <BlockTwoToneIcon sx={{ fontSize: '1.1rem' }} />
                                                </IconButton>
                                            </Tooltip>
                                        </Stack>
                                    </TableCell>
                                </TableRow>
                            ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </MainCard>
    );
}

export default AuthorList;
