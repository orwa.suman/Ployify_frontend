import * as React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';
import OptionsSelector from './OptionSelector';
import { Box, Rating, TextField, TextareaAutosize } from '@mui/material';
import { uniqueId } from 'lodash';
import { useDispatch } from 'react-redux';
import { showMessage } from 'utils/form/Communication';
import { FormattedMessage } from 'react-intl';
import { createNewFeedback } from 'services/FeedbackService';
import SendIcon from '@mui/icons-material/Send';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2)
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1)
    }
}));

function BootstrapDialogTitle(props) {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500]
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
}

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired
};

export default function FeedBackDialog({ open, setOpen }) {
    const handleClose = () => {
        setOpen(false);
    };
    const [actName, setActName] = React.useState('');

    const handleChange = (event) => {
        setActName(event.target.value);
    };
    const [value, setValue] = React.useState(1);
    const [text, setText] = React.useState('');
    const dispatch = useDispatch();
    const handleTextareaChange = (event) => {
        setText(event.target.value);
    };

    const options = [
        'Training at Home',
        'Training at Office',
        'Recipes',
        'Mental Health',
        'Seminare',
        'Rewards',
        'Dashboard(Home)',
        'Managment Tool',
        'Quiz',
        'Account',
        'Change Password',
        'Rights'
    ];

    function createFeedback() {
        const id = uniqueId();
        const data = {
            text,
            section: actName,
            rating: value
        };

        createNewFeedback(data)
            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
        setOpen(true);
    }
    return (
        <BootstrapDialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
            <div style={{ width: '500px' }}>
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    Feedback
                </BootstrapDialogTitle>
                <DialogContent dividers>
                    <div>
                        <OptionsSelector options={options} actName={actName} handleChange={handleChange} />
                    </div>
                    <div style={{ marginTop: '20px' }}>
                        <Typography gutterBottom>How would you rate your section:</Typography>

                        <Box
                            sx={{
                                '& > legend': { mt: 2 }
                            }}
                        >
                            <Rating
                                name="simple-controlled"
                                value={value}
                                onChange={(event, newValue) => {
                                    setValue(newValue);
                                }}
                            />
                        </Box>
                    </div>
                    <div>
                        <TextField
                            multiline
                            style={{ width: '100%', marginTop: '10px' }}
                            rows={7}
                            placeholder="Please give us feedback here and tell us where to get better - Thankyou!"
                            value={text}
                            onChange={handleTextareaChange}
                        />
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button
                        autoFocus
                        onClick={createFeedback}
                        disabled={text.length === 0}
                        variant="contained"
                        style={{ backgroundColor: '#cbab70' }}
                    >
                        Submit <SendIcon style={{ marginLeft: '4px', width: '16px', height: '16px' }} />
                    </Button>
                </DialogActions>
            </div>
        </BootstrapDialog>
    );
}
