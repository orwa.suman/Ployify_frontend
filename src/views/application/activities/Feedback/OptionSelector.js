import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function BasicSelect({ options, actName, handleChange }) {
    return (
        <Box sx={{ minWidth: 160, marginTop: '20px' }}>
            <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Select Section</InputLabel>
                <Select labelId="demo-simple-select-label" id="demo-simple-select" value={actName} label="act" onChange={handleChange}>
                    {options.map((option) => {
                        return <MenuItem value={option}>{option}</MenuItem>;
                    })}
                </Select>
            </FormControl>
        </Box>
    );
}
