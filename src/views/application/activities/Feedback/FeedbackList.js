import {
    Button,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Typography
} from '@mui/material';
import React, { useEffect, useState } from 'react';

import OptionsSelector from './OptionSelector';
import { getAllFeedback, getAllFeedbackByChecked } from 'services/FeedbackService';

const FeedbackList = () => {
    const [feedbackList, setFeedbackList] = React.useState([]);
    const options = [
        'Training at Home',
        'Training at Office',
        'Recipes',
        'Mental Health',
        'Seminare',
        'Rewards',
        'Dashboard(Home)',
        'Managment Tool',
        'Quiz',
        'Account',
        'Change Password',
        'Rights'
    ];
    const [actName, setActName] = React.useState('');
    const [isChecked, setIsChecked] = useState(false);
    const [isCheckedProcessed, setIsCheckedProcessed] = useState(false);
    const [overridProcessed, setOverridProcessed] = useState(false);

    const [selectedRow, setSelectedRow] = useState(null);

    const handleRowClick = (row) => {
        setSelectedRow(row);
    };

    const handleCloseDialog = () => {
        setSelectedRow(null);
    };

    const handleChange = (event) => {
        setActName(event.target.value);
    };

    function handleCheckBoxChange(event) {
        setIsChecked(!isChecked);
        setOverridProcessed(true);
    }

    function handleCheckProcessedBoxChange(event) {
        setIsCheckedProcessed(!isCheckedProcessed);
        setOverridProcessed(false);
    }

    useEffect(() => {
        getAllFeedback(actName, isChecked, isCheckedProcessed, overridProcessed)
            .then((res) => {
                setFeedbackList(res.data);
            })
            .catch((e) => {
                
            });
    }, [actName, isChecked, isCheckedProcessed, overridProcessed]);

    return (
        <div>
            <Typography variant="h3" style={{ margin: '20px' }}>
                User Feedback
            </Typography>

            <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'flex-start' }}>
                <OptionsSelector options={options} actName={actName} handleChange={handleChange} />
                <div style={{ display: 'flex', alignContent: 'center', alignItems: 'center', marginTop: '10px' }}>
                    <Typography>All: </Typography>
                    <Checkbox checked={isChecked} onChange={handleCheckBoxChange} inputProps={{ 'aria-label': 'primary checkbox' }} />
                </div>

                <div style={{ display: 'flex', alignContent: 'center', alignItems: 'center', marginTop: '10px' }}>
                    <Typography>Processed: </Typography>
                    <Checkbox
                        checked={isCheckedProcessed}
                        onChange={handleCheckProcessedBoxChange}
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                </div>
            </div>

            <TableContainer component={Paper} style={{ marginTop: '20px' }}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Processed</TableCell>
                            <TableCell align="center">Section</TableCell>
                            <TableCell align="center">Rating</TableCell>
                            <TableCell align="center">Text</TableCell>
                        </TableRow>
                    </TableHead>
                    {feedbackList?.length === 0 ? (
                        <Typography variant="h5" style={{ margin: '20px', padding: '20px' }}>
                            No Data Found.
                        </Typography>
                    ) : (
                        <TableBody>
                            {feedbackList?.map((row) => (
                                <TableRow
                                    key={row?.id}
                                    onClick={() => handleRowClick(row)}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {row?.checked ? 'Processed' : 'Not Processed'}
                                    </TableCell>
                                    <TableCell align="center">{row?.section}</TableCell>
                                    <TableCell align="center">{row?.rating}</TableCell>
                                    <TableCell align="center" style={{ maxWidth: 200, overflowY: 'auto' }}>
                                        {row?.text}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    )}
                </Table>
            </TableContainer>
            <Dialog open={Boolean(selectedRow)} onClose={handleCloseDialog}>
                <div style={{ minWidth: '500px' }}>
                    <DialogTitle>Feedback Details</DialogTitle>
                    <DialogContent>
                        <TableContainer>
                            <Table>
                                <TableBody>
                                    <TableRow>
                                        <TableCell>Status</TableCell>
                                        <TableCell>{selectedRow?.checked ? 'Processed' : 'Not Processed'}</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>Section</TableCell>
                                        <TableCell>{selectedRow?.section}</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>Rating</TableCell>
                                        <TableCell>{selectedRow?.rating}</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell>Text</TableCell>
                                        <TableCell>{selectedRow?.text}</TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleCloseDialog}>Close</Button>
                    </DialogActions>
                </div>
            </Dialog>
        </div>
    );
};

export default FeedbackList;
