import { Divider, Typography, Grid, Button, Box, CircularProgress } from '@mui/material';
import Card from '@mui/material/Card';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import React from 'react';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import MainCard from 'ui-component/cards/MainCard';
import { useEffect, useState } from 'react';
import { finishCurrentSeminar, getSeminare, updateCurrentSeminar } from 'services/SeminareService';
import { showMessage } from '../../../../utils/form/Communication';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'store';
import Vimeo from '@u-wave/react-vimeo';
import { PlayArrowOutlined } from '@mui/icons-material';
import { useTheme } from '@mui/material/styles';
import Author from 'ui-component/Author';
import SeminareQuizResult from './SeminareQuizResult';
import { addSeminarPointsToUser } from 'services/GamificationService';
import { loggedInUser } from 'contexts/JWTContext';
import { addActivityToUserHistory } from 'services/userService';
import { updateStartCountSeminarTracking } from 'services/trackingService';
import { FormattedMessage } from 'react-intl';
import AuthorBasic from '../Author/AuthorBasic';
import UserDialog from '../MentalHealth/MentalHealthCards/AuthorDialog';
import { openDrawer } from 'store/slices/menu';
import SeminarPoints from './SeminarPoints';

const CustomShadowBox = ({ shadow, label, color }) => {
    const theme = useTheme();

    return (
        <Card sx={{ mb: 3, boxShadow: shadow }}>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    py: 3,
                    bgcolor: color,
                    fontWeight: 'bold',
                    color: theme.palette.background.default
                }}
            >
                {!label && <Box sx={{ color: 'inherit' }}>boxShadow: {shadow}</Box>}
                {label && <Box sx={{ color: 'inherit' }}>{label}</Box>}
            </Box>
        </Card>
    );
};

export default function SeminanreQuiz() {
    const { id } = useParams();
    const theme = useTheme();
    const dispatch = useDispatch();
    const [open, setOpen] = useState(false);
    const [data, setData] = useState({ questions: [] });
    const [currentQuestion, setCurrentQuestion] = React.useState(0);
    const [showScore, setShowScore] = React.useState(false);
    const [score, setScore] = React.useState(0);
    const [maxScore, setMaxScore] = React.useState(0);
    const [showQuiz, setShowQuiz] = React.useState(false);
    const [videoCompleted, setVideoCompleted] = useState(false);
    const [isVideoReady, setIsVideoReady] = useState(false);
    const [lastQuestion, setLastQuestion] = useState(false);
    const [seminarData, setSeminarData] = useState([]);
    const handleVideoEnd = () => {
        // Set the videoCompleted state to true
        setVideoCompleted(true);
        finishCurrentSeminar(loggedInUser.id);
    };

    const handleVideoPause = () => {
        updateCurrentSeminar(loggedInUser.id);
    };

    const getMaxScore = (questions) => {
        let maxScore = 0;

        questions.forEach((question) => {
            maxScore += (question.answers || []).filter((answer) => answer.correct).length;
        });

        return maxScore;
    };

    const mapQuestions = (questions) => {
        return questions.map((question) => ({
            question: question.question,
            answers: question.answers.map((answer) => ({ label: answer.answer, correct: answer.correct }))
        }));
    };
    const handleClickOpen = () => {
        dispatch(openDrawer(false));
        setOpen(true);
    };

    useEffect(() => {
        getSeminare(id)
            .then((res) => {
                setSeminarData(res.data);
                const defaultVideo = 'https://vimeo.com/771124484';
                const questions = [
                    {
                        question: 'How do you do?',
                        answers: [{ label: 'A - Correct', correct: true }, { label: 'Not correct' }]
                    },
                    {
                        question: 'What do you do?',
                        answers: [{ label: 'Fishing - Not correct' }, { label: 'Jogging - Correct', correct: true }]
                    }
                ];

                const quiz = {
                    ...res.data,
                    videolink: res.data.videolink || defaultVideo,
                    questions:
                        !!res.data.seminarActivityDtoList && res.data.seminarActivityDtoList.length
                            ? mapQuestions(res.data.seminarActivityDtoList)
                            : questions
                };

                setData(quiz);
                setMaxScore(getMaxScore(quiz.questions));
                addActivityToUserHistory(loggedInUser.id, id, 3);
                updateStartCountSeminarTracking(id)
                    .then((res) => res.data)
                    .then((data) => {
                        
                    });
            })
            .then((data) => {
                
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    const nextQuestion = () => {
        let questionScore = 0;
        data?.questions[currentQuestion]?.answers.forEach((answer) => {
            if (answer.correct && answer.checked) {
                questionScore++;
            }
        });

        setScore(score + questionScore);

        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < data.questions.length) {
            setCurrentQuestion(nextQuestion);
        } else {
            setShowScore(true);
            addSeminarPointsToUser(loggedInUser.id, data.id);
        }
        if (currentQuestion === data.questions.length - 1) {
            setLastQuestion(true);
        }
    };

    const unselectOption = (questionIndex, answerIndex) => {
        const updatedStateForQuiz = { ...data };
        updatedStateForQuiz.questions[questionIndex].answers[answerIndex].checked = false;
        setData(updatedStateForQuiz);
    };

    const selectOption = (questionIndex, answerIndex) => {
        if (!data.questions[questionIndex].answers[answerIndex].checked) {
            const updatedStateForQuiz = { ...data };
            updatedStateForQuiz.questions[questionIndex].answers.forEach((answer, index) => {
                answer.checked = index === answerIndex;
            });
            setData(updatedStateForQuiz);
        } else {
            unselectOption(questionIndex, answerIndex);
        }
    };

    useEffect(() => {
        const timeout = setTimeout(() => {
            setIsVideoReady(true);
        }, 10000);

        return () => clearTimeout(timeout);
    }, []);

    const containVideoId = seminarData?.video?.match(/\/(\d+)$/)?.[1];

    return (
        <div className="app">
            {lastQuestion ? (
                <SeminarPoints id={data.id} score={maxScore}></SeminarPoints>
            ) : (
                <>
                    <div>
                        <Typography
                            variant="h6"
                            sx={{ textAlign: videoCompleted ? 'center' : 'left', margin: '0 0 24px', fontSize: '26px' }}
                        >
                            {seminarData?.title}
                        </Typography>
                        {videoCompleted && !showQuiz && (
                            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                                <img src="/images/goQuiz.png" alt="quiz" style={{ maxWidth: '70%', height: 'auto' }} />
                            </div>
                        )}
                        {videoCompleted && (
                            <div
                                style={{
                                    margin: 'auto',
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: '50%'
                                }}
                            >
                                <Button
                                    sx={{
                                        margin: '12px',
                                        borderRadius: '30px',
                                        opacity: showQuiz ? 0 : 1,
                                        backgroundColor: '#CCAD70',
                                        borderColor: '#CCAD70',
                                        color: 'black',
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                    variant="outlined"
                                    onClick={() => setShowQuiz(true)}
                                    fullWidth
                                    // startIcon={<PlayArrowOutlined />}
                                >
                                    <FormattedMessage id="Take the Quiz and Get your Points!" />
                                </Button>
                            </div>
                        )}

                        {videoCompleted ? (
                            <Grid item xs={12} md={8}>
                                <div hidden={showQuiz}></div>
                                <Card hidden={!showQuiz} vsx={{ marginBottom: '500px', maxWidth: '60%' }}>
                                    <Grid container spacing={2} sx={{ paddingTop: '30px' }}>
                                        <Grid item xs={12}>
                                            <div style={{ width: '100%', display: 'flex', justifyContent: 'center', marginBottom: '20px' }}>
                                                <div
                                                    style={{
                                                        padding: '20px',
                                                        width: '50%',
                                                        backgroundColor: '#CCAD70',
                                                        borderRadius: '40px',
                                                        display: 'flex',
                                                        justifyContent: 'center',
                                                        marginBottom: '20px'
                                                    }}
                                                >
                                                    <Typography variant="h3" style={{ textAlign: 'center', color: 'white' }}>
                                                        {data.questions[currentQuestion]?.question}
                                                    </Typography>
                                                </div>
                                            </div>
                                            <Divider></Divider>

                                            <nav aria-label="secondary mailbox folders">
                                                <List
                                                    style={{
                                                        width: '40%',
                                                        display: 'flex',
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        marginBottom: '20px',
                                                        margin: 'auto'
                                                    }}
                                                >
                                                    {data.questions[currentQuestion]?.answers.map((answer, answerIndex) => (
                                                        <ListItem>
                                                            <ListItemButton
                                                                primary
                                                                component="a"
                                                                sx={{
                                                                    background: answer.checked ? '#80BD9A' : '',
                                                                    marginBottom: '8px',

                                                                    borderRadius: '30px',
                                                                    padding: '10px',
                                                                    color: 'black',
                                                                    border: '1px solid lightgray',
                                                                    textAlign: 'center'
                                                                }}
                                                                href="#simple-list"
                                                                onClick={() => selectOption(currentQuestion, answerIndex)}
                                                            >
                                                                <ListItemText primary={answer.label} />
                                                            </ListItemButton>
                                                        </ListItem>
                                                    ))}
                                                </List>
                                            </nav>
                                            <div style={{ width: '100%', display: 'flex', justifyContent: 'flex-end' }}>
                                                <Button
                                                    sx={{
                                                        margin: '12px 0',
                                                        padding: '10px',
                                                        width: '5%',
                                                        backgroundColor: '#CCAD70',
                                                        borderRadius: '30px',
                                                        border: '1px solid lightGray',
                                                        marginLeft: 'auto'
                                                    }}
                                                    variant="outlined"
                                                    onClick={() => nextQuestion()}
                                                    fullWidth
                                                    startIcon={<NavigateNextIcon style={{ marginLeft: '15px', color: 'black' }} />}
                                                ></Button>
                                            </div>
                                        </Grid>
                                    </Grid>
                                </Card>
                            </Grid>
                        ) : (
                            <div style={{ width: '100%' }}>
                                {isVideoReady ? (
                                    <Vimeo
                                        width="100%"
                                        responsive
                                        video={
                                            !seminarData?.videolink || !containVideoId
                                                ? 'https://vimeo.com/771124484'
                                                : seminarData?.videolink
                                        }
                                        onEnd={handleVideoEnd}
                                        onPause={handleVideoPause}
                                    />
                                ) : (
                                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '20%' }}>
                                        <CircularProgress />
                                    </div>
                                )}
                            </div>
                        )}

                        <div>
                            <Grid container>
                                <Grid item xs={12} md={4} sx={{ padding: '16px 24px 16px' }}>
                                    {!videoCompleted && (
                                        <div onClick={handleClickOpen} style={{ cursor: 'pointer' }}>
                                            <AuthorBasic
                                                firstName={seminarData?.authorDto?.firstName || 'John'}
                                                lastName={seminarData?.authorDto?.lastName || 'Doe'}
                                                avatarUrl={seminarData?.authorDto?.picture || 'https://i.pravatar.cc/300'}
                                            />
                                        </div>
                                    )}
                                </Grid>
                                <UserDialog user={seminarData?.authorDto} open={open} setOpen={setOpen} />
                            </Grid>
                        </div>
                    </div>
                </>
            )}
        </div>
    );
}
