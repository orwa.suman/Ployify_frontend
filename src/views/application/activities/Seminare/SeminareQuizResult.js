import { Typography, Grid, Button } from '@mui/material';
import React from 'react';
import { PlayArrowOutlined } from '@mui/icons-material';
import { useNavigate } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

export default function SeminareQuizResult({ score, maxScore }) {
    const navigate = useNavigate();

    return (
        <Grid>
            <Typography sx={{ textAlign: 'center', margin: '110px 0 40px', padding: '16px' }} variant="h2" fontWeight="bold" gutterBottom>
                <FormattedMessage id="You scored" values={{ score, maxScore }} />
            </Typography>

            <div style={{ textAlign: 'center' }}>
                <span className="emoji">🏆</span>

                <Typography
                    sx={{ textAlign: 'center', margin: '20px 0 40px', padding: '16px' }}
                    variant="h2"
                    fontWeight="bold"
                    gutterBottom
                >
                    <FormattedMessage id="Congratulation!" />
                </Typography>

                <Button
                    sx={{ margin: '0 auto 12px' }}
                    variant="outlined"
                    onClick={() => navigate('/activities/seminare/cards/')}
                    startIcon={<PlayArrowOutlined />}
                >
                    <FormattedMessage id="Take other Quiz!" />
                </Button>
            </div>
        </Grid>
    );
}
