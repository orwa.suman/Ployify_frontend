import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import '../../../../assets/scss/style.css';

import BlockTwoToneIcon from '@mui/icons-material/BlockTwoTone';
import ChatBubbleTwoToneIcon from '@mui/icons-material/ChatBubbleTwoTone';
import { useTheme } from '@mui/material/styles';
import { deleteSeminare, loadSeminare } from '../../../../services/SeminareService';
import { HiPencil } from 'react-icons/hi';

import {
    Grid,
    IconButton,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
    Typography
} from '@mui/material';
import { FormattedMessage } from 'react-intl';
import { useNavigate } from 'react-router-dom';
import { deleteItem } from 'store/slices/kanban';
import { deleteMentalHealth } from 'services/MentalHealthService';

function SeminareList() {
    const theme = useTheme();
    const navigate = useNavigate();
    let [data, setData] = React.useState([]);

    React.useEffect(() => {
        loadSeminare()
            .then((res) => res.data)
            .then(setData)
            .catch((e) => {
                
            });
    }, []);

    const deleteSeminar = (id) => {
        deleteSeminare(id);
    };

    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell scope="col">
                            <FormattedMessage id="Title" />
                        </TableCell>
                        <TableCell scope="col">
                            <FormattedMessage id="Author" />
                        </TableCell>
                        <TableCell scope="col">
                            <FormattedMessage id="Sequence" />
                        </TableCell>
                        <TableCell scope="col">
                            <FormattedMessage id="Video link" />
                        </TableCell>
                        <TableCell align="center" sx={{ pr: 3 }}>
                            <FormattedMessage id="Actions" />
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data &&
                        data.map((row, index) => (
                            <TableRow hover key={index}>
                                <TableCell>
                                    <Grid container spacing={2} alignItems="center">
                                        {/*<Grid item>*/}
                                        {/*    <Avatar alt="User 1" src={avatarImage(`./${row.avatar}`).default} />*/}
                                        {/*</Grid>*/}
                                        <Grid item xs zeroMinWidth>
                                            <Typography align="left" component="div">
                                                {row.title}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </TableCell>

                                <TableCell>{row.authorDto?.firstName} {row.authorDto?.lastName}</TableCell>
                                <TableCell>{row.sequence}</TableCell>
                                <TableCell>{row.videolink}</TableCell>
                                <TableCell align="center" sx={{ pr: 3 }}>
                                    <Stack direction="row" justifyContent="center" alignItems="center">
                                        <Tooltip placement="top" title="edit">
                                            <IconButton
                                                color="primary"
                                                aria-label="edit"
                                                onClick={() => navigate('/dashboard/activities/seminar/form/' + row.id)}
                                                size="large"
                                            >
                                                <HiPencil sx={{ fontSize: '1.1rem' }} />
                                            </IconButton>
                                        </Tooltip>

                                        <Tooltip placement="top" title="Delete" onClick={() => deleteSeminar(row.id)}>
                                            <IconButton
                                                color="primary"
                                                sx={{
                                                    color: theme.palette.orange.dark,
                                                    borderColor: theme.palette.orange.main,
                                                    '&:hover ': { background: theme.palette.orange.light }
                                                }}
                                                size="large"
                                            >
                                                <BlockTwoToneIcon sx={{ fontSize: '1.1rem' }} />
                                            </IconButton>
                                        </Tooltip>
                                    </Stack>
                                </TableCell>
                            </TableRow>
                        ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default SeminareList;
