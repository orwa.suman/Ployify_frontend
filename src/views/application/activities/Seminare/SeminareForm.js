import * as Yup from 'yup';

import { Autocomplete, Button, Dialog, Grid, TextField } from '@mui/material';
import { FormattedMessage, useIntl } from 'react-intl';
import React, { useState } from 'react';
import { addSeminare, getSeminare, loadSeminare, updateSeminar } from '../../../../services/SeminareService';

import Divider from '@mui/material/Divider';
import FileUpload from 'views/forms/components/FileUpload';
import MainCard from '../../../../ui-component/cards/MainCard';
import PropTypes from 'prop-types';
import SeminarTagsForm from './SeminarTagForm';
import { loadAuthors } from 'services/AuthorService';
import { loadSeminarTags } from 'services/TagService';
import { makeStyles } from '@mui/styles';
import { showMessage } from '../../../../utils/form/Communication';
import theme from 'theme';
import { uploadToCloudinary } from 'services/CloudinaryService';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import { useParams } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    input: {
        marginTop: '4px',
        padding: '14px 16px',
        borderRadius: '6px',
        backgroundColor: '#FAFAFA',
        border: '1px solid ' + theme.palette.grey[300],
        '&:focus': {
            outline: 'none',
            borderColor: theme.palette.primary.main,
            boxShadow: '0 0 0 2px ' + theme.palette.primary.light
        }
    }
}));

export function SimpleTagDialog(props) {
    const { open, onClose, type } = props;

    return (
        <Dialog onClose={onClose} open={open} fullWidth>
            <SeminarTagsForm />
        </Dialog>
    );
}

SimpleTagDialog.propTypes = {
    open: PropTypes.bool.isRequired
};

function SeminareForm() {
    const [quiz, setQuiz] = React.useState([{ label: '', answers: [{ label: '' }] }]);
    const [authors, setAuthors] = React.useState([]);
    const [author, setAuthor] = React.useState({});
    const { id } = useParams();
    const classes = useStyles();

    const [picture, setPicture] = React.useState([]);
    const [pictureName, setPictureName] = React.useState([]);
    const [changedPicture, setChangedPicture] = React.useState(false);

    const [pdf, setPdf] = React.useState([]);
    const [pdfName, setPdfName] = React.useState([]);
    const [changedPdf, setChangedPdf] = React.useState(false);

    const [tagForm, setTagForm] = useState(false);
    const [seminarTags, setSeminarTags] = useState([]);

    const intl = useIntl();
    const dispatch = useDispatch();

    const addQuestion = () => {
        setQuiz([...quiz, { label: '', answers: [{ label: '' }] }]);
    };

    const updateQuestion = (questionIndex, value) => {
        const state = [...quiz];
        state[questionIndex].label = value;
        setQuiz(state);
    };

    const deleteQuestion = (index) => {
        setQuiz([...quiz].filter((value, questionIndex) => index !== questionIndex));
    };

    const addAnswer = (index) => {
        const state = [...quiz];
        state[index].answers = [...state[index].answers, { label: '' }];
        setQuiz(state);
    };

    const updateAnswer = (questionIndex, answerIndex, value) => {
        const state = [...quiz];
        state[questionIndex].answers[answerIndex].label = value;
        setQuiz(state);
    };

    const toggleCorrectAnswer = (questionIndex, answerIndex) => {
        const state = [...quiz];
        state[questionIndex].answers[answerIndex].correct = !state[questionIndex].answers[answerIndex].correct;
        setQuiz(state);
    };

    const deleteAnswer = (questionIndex, answerIndex) => {
        const state = [...quiz];
        state[questionIndex].answers = [...quiz][questionIndex].answers.filter((value, index) => index !== answerIndex);
        setQuiz(state);
    };

    const validationSchema = Yup.object({
        title: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(150, <FormattedMessage id="Input Max Length" values={{ length: 150 }} />)
            .required(<FormattedMessage id="Required" />),
        authorDto: Yup.object().required(<FormattedMessage id="Required" />),
        videolink: Yup.string()
            .min(10, <FormattedMessage id="Input Min Length" values={{ length: 10 }} />)
            .required(<FormattedMessage id="Required" />)
    });

    const quizHasErrors = () => {
        let hasError = false;

        quiz.forEach((question) => {
            question.error = false;
            if (!question.label || question.label.length < 5) {
                question.error = true;
                hasError = true;
            }
            question.answers.forEach((answer) => {
                answer.error = false;
                if (!answer.label || answer.label.length === 0) {
                    answer.error = true;
                    hasError = true;
                }
            });
        });
        return hasError;
    };

    const submitForm = (event) => {
        if (!quizHasErrors()) {
            formik.values.seminarActivityDtoList = quiz.map((question) => ({
                question: question.label,
                answers: question.answers.map((answer) => ({
                    answer: answer.label,
                    correct: !!answer.correct ? answer.correct : false,
                    checked: false
                }))
            }));

            formik.validateForm().then((errors) => {
                if (errors && Object.keys(errors).length > 0) {
                    const errorMessages = Object.entries(errors).map(([fieldName, fieldError]) => {
                        return `${fieldName}: ${fieldError.props.id}`;
                    });

                    dispatch(showMessage(<FormattedMessage id={errorMessages.join(', ')} />));
                } else {
                    formik.handleSubmit(event);
                }
            });
        } else {
            dispatch(showMessage(<FormattedMessage id="Validation Failed" />));
        }
    };

    const formik = useFormik({
        initialValues: {
            title: '',
            description: '',
            authorDto: null,
            sequence: 0,
            startDateTime: new Date(),
            videolink: '',
            pdf: '',
            picture: '',
            points: 0,
            seminarActivityDtoList: []
        },
        validationSchema,
        onSubmit: () => {
            uploadToCloudinary(picture, pictureName, 'seminar_', '/Ployify/Activities/Seminar')
                .then((data) => {
                    if (changedPicture) {
                        
                        
                        formik.values.picture = data.url;
                    }

                    uploadToCloudinary(pdf, pdfName, 'seminarPDF_', '/Ployify/Activities/Seminar/PDF')
                        .then((data) => {
                            if (changedPdf) {
                                
                                
                                formik.values.pdf = data.url;
                            }

                            if (id) {
                                updateSeminar(id, formik.values)
                                    .then((response) => {
                                        dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success'));
                                    })
                                    .catch((e) => {
                                        dispatch(showMessage(e?.response?.data?.message));
                                    });
                            } else {
                                addSeminare(formik.values)
                                    .then((response) => {
                                        dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success'));
                                    })
                                    .catch((e) => {
                                        dispatch(showMessage(e?.response?.data?.message));
                                    });
                            }
                        })
                        .catch((err) => {
                            
                        });
                })
                .catch((err) => {
                    
                });
        }
    });

    React.useEffect(() => {
        requestAuthors();
        loadTags();

        if (id) {
            //loadSeminare()
            getSeminare(id)
                .then((res) => res.data)
                .then((data) => {
                    //const row = data.find((item) => item.id === id);
                    const row = data;
                    
                    formik.setValues({
                        title: row.title,
                        description: row.description,
                        authorDto: row.authorDto,
                        sequence: row.sequence,
                        startDateTime: row.startDateTime,
                        videolink: row.videolink,
                        pdf: row.pdf,
                        picture: row.picture,
                        points: row.points,
                        seminarActivityDtoList: row.seminarActivityDtoList
                    });

                    setQuiz(
                        (row.seminarActivityDtoList && row.seminarActivityDtoList.length ? row.seminarActivityDtoList : []).map(
                            (question) => ({
                                question: { label: question.question },
                                answers: question.answers.map((answer) => ({
                                    label: answer.answer,
                                    correct: !!answer.correct ? answer.correct : false
                                }))
                            })
                        )
                    );
                })
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
    }, []);

    const requestAuthors = () => {
        const request = loadAuthors();
        request
            .then((res) => res.data)
            .then((data) => {
                setAuthors(data);
                if (data.length) {
                    formik.values.authorDto = data[0];
                    setAuthor(data[0]);
                }
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const loadTags = () => {
        loadSeminarTags()
            .then((res) => res.data)
            .then((data) => {
                setSeminarTags(data);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const handleFileUpload = (data, fileName) => {
        setPdf(data);
        setPdfName(fileName);
        setChangedPdf(true);
    };

    const handlePictureUpload = (data, fileName) => {
        setPicture(data);
        setPictureName(fileName);
        setChangedPicture(true);
    };

    const openTagForm = () => {
        setTagForm(true);
    };

    const handleDialogClose = (value) => {
        setTagForm(false);
        loadTags();
    };

    function handleTag(event, value) {
        formik.setValues({ ...formik.values, seminarTags: value });
    }

    return (
        <Grid>
            <form onSubmit={submitForm}>
                <MainCard title={'Add new seminar'}>
                    <Grid item xs={12} className="careployee-form__block">
                        <label htmlFor="title" className="careployee-form__label">
                            <FormattedMessage id="Title" />
                        </label>
                        <TextField
                            fullWidth
                            maxRows={4}
                            id="title"
                            placeholder="Bitte einen Speise Name hinzufügen.. Es sollte Unique sein!"
                            value={formik.values.title}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            error={formik.touched.title && Boolean(formik.errors.title)}
                            helperText={formik.touched.title && formik.errors.title}
                        />
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <label htmlFor="description" className="careployee-form__label">
                            <FormattedMessage id="Description" />
                        </label>
                        <TextField
                            fullWidth
                            id="description"
                            label="Description"
                            multiline
                            rows={8}
                            value={formik.values.description}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            error={formik.touched.description && Boolean(formik.errors.description)}
                        />
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <label htmlFor="author" className="careployee-form__label">
                            <FormattedMessage id="Author" />
                        </label>
                        <Autocomplete
                            style={{ height: '44px' }}
                            disablePortal
                            options={authors}
                            getOptionLabel={(option) => option.firstName + ' ' + option.lastName}
                            value={author}
                            onChange={(event, value) => {
                                formik.values.authorDto = value;
                                setAuthor(value);
                            }}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    error={formik.errors.authorDto}
                                    helperText={!!formik.errors.authorDto && formik.errors.authorDto}
                                />
                            )}
                        />
                    </Grid>

                    <Grid container>
                        <Grid item xs={6} sm={2} className="careployee-form__block">
                            <label htmlFor="points" className="careployee-form__label">
                                <FormattedMessage id="Points" />
                            </label>
                            <TextField
                                fullWidth
                                id="points"
                                type="number"
                                value={formik.values.points}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.points && Boolean(formik.errors.points)}
                                helperText={formik.touched.points && formik.errors.points}
                            />
                        </Grid>

                        <Grid item xs={6} sm={2} className="careployee-form__block">
                            <label htmlFor="sequence" className="careployee-form__label">
                                <FormattedMessage id="sequence" />
                            </label>
                            <TextField
                                fullWidth
                                id="sequence"
                                type="number"
                                value={formik.values.sequence}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.sequence && Boolean(formik.errors.sequence)}
                                helperText={formik.touched.sequence && formik.errors.sequence}
                            />
                        </Grid>

                        <Grid item xs={6} sm={2}>
                            <label htmlFor="startDateTime" className="careployee-form__label">
                                <FormattedMessage id="startDateTime" />
                            </label>
                            <input
                                style={{ marginTop: '4px' }}
                                type="datetime-local"
                                value={formik.values.startDateTime}
                                onChange={(event) => {
                                    formik.setFieldValue('startDateTime', event.target.value);
                                }}
                                className={classes.input}
                            />
                        </Grid>
                    </Grid>

                    <Grid item xs={12} md={4} className="careployee-form__block">
                        <FileUpload label="Pdf" onChange={handleFileUpload} picture={formik.values.pdf}></FileUpload>
                    </Grid>

                    <Grid item xs={12} md={4} className="careployee-form__block">
                        <FileUpload label="Picture" onChange={handlePictureUpload} picture={formik.values.picture}></FileUpload>
                    </Grid>

                    <Grid item xs={12} className="careployee-form__block">
                        <label htmlFor="videolink" className="careployee-form__label">
                            <FormattedMessage id="Video link" />
                        </label>
                        <TextField
                            fullWidth
                            id="videolink"
                            placeholder="Link"
                            value={formik.values.videolink}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            error={formik.touched.videolink && Boolean(formik.errors.videolink)}
                            helperText={formik.touched.videolink && formik.errors.videolink}
                        />
                    </Grid>
                </MainCard>

                <MainCard title={<FormattedMessage id="Question" />}>
                    {quiz.map((question, questionIndex) => (
                        <div key={questionIndex}>
                            <Grid item sx={{ marginTop: '6px' }}>
                                <TextField
                                    fullWidth
                                    id="qustion"
                                    label="question"
                                    value={question.label}
                                    onChange={(event) => updateQuestion(questionIndex, event.target.value)}
                                />
                            </Grid>

                            <Grid sx={{ display: 'inline' }}>
                                {question.answers.map((answer, answerIndex) => (
                                    <div key={answerIndex}>
                                        <Grid item xs={12} sx={{ margin: '5px' }}>
                                            <TextField
                                                sx={{ width: '50%' }}
                                                id="answer"
                                                label="answer"
                                                placeholder="enter an answer"
                                                value={answer.label}
                                                onChange={(event) => updateAnswer(questionIndex, answerIndex, event.target.value)}
                                            />

                                            <Button
                                                variant="contained"
                                                color={!answer.correct ? 'error' : 'primary'}
                                                onClick={() => toggleCorrectAnswer(questionIndex, answerIndex)}
                                                sx={{ margin: '5px', width: '130px' }}
                                            >
                                                <FormattedMessage id={!answer.correct ? 'Not correct' : 'Correct'} />
                                            </Button>
                                            <Button
                                                variant="contained"
                                                color="error"
                                                onClick={() => deleteAnswer(questionIndex, answerIndex)}
                                                sx={{ margin: '5px', width: '130px' }}
                                            >
                                                x
                                            </Button>
                                        </Grid>
                                    </div>
                                ))}
                                <Button variant="contained" onClick={() => addAnswer(questionIndex)} sx={{ margin: '5px' }}>
                                    +
                                </Button>

                                <Button
                                    variant="contained"
                                    color="error"
                                    onClick={() => deleteQuestion(questionIndex)}
                                    sx={{ margin: '5px' }}
                                >
                                    <FormattedMessage id="delete Question" />
                                </Button>
                                <Divider />
                            </Grid>
                        </div>
                    ))}

                    <Grid>
                        <Button variant="contained" onClick={addQuestion} sx={{ margin: '5px' }}>
                            <FormattedMessage id="add new Question" />
                        </Button>
                    </Grid>

                    <Grid item>
                        <Button
                            variant="contained"
                            onClick={openTagForm}
                            sx={{
                                margin: '16px 0 24px',
                                borderRadius: '8px',
                                background: theme.palette.success.dark,
                                '&:hover': { background: theme.palette.success.main }
                            }}
                        >
                            <FormattedMessage id="Add New Tag" />
                        </Button>
                        <Autocomplete
                            multiple={true}
                            options={seminarTags}
                            value={!!formik.values.seminarTags && formik.values.seminarTags.length ? [...formik.values.seminarTags] : []}
                            getOptionLabel={(option) => option.name}
                            onChange={(event, key) => handleTag(event, key)}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label={intl.formatMessage({ id: 'Multiple Tags' })}
                                    onChange={(value) => handleTag(value)}
                                />
                            )}
                        />

                        <SimpleTagDialog open={tagForm} onClose={handleDialogClose} type="seminar" />
                    </Grid>
                </MainCard>

                <br />
                <Grid>
                    <Button variant="contained" onClick={submitForm}>
                        <FormattedMessage id="Submit" />
                    </Button>
                </Grid>
            </form>
        </Grid>
    );
}

export default SeminareForm;
