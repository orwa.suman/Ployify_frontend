import { KeyboardArrowUp, KeyboardDoubleArrowUp } from '@mui/icons-material';
import { Button, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Box } from '@mui/system';
import { loggedInUser } from 'contexts/JWTContext';
import { userInfo } from 'os';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { addSeminarPointsToUser } from 'services/GamificationService';
import { getUserById } from 'services/userService';

const useStyles = makeStyles((theme) => ({
    progressContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '20%'
    },
    responsiveDiv: {
        display: 'flex',
        justifyContent: 'space-between',
        [theme.breakpoints.down('sm')]: {
            flexDirection: 'column'
        }
    },
    progressBarContainer: {
        width: '120px',
        height: '80%',
        border: 'none',
        borderRadius: '40px',
        backgroundColor: '#E3D4C9'
    },
    remainingPointsContainer: {
        backgroundColor: '#E3D4C9',
        borderTopLeftRadius: '30px',
        borderTopRightRadius: '30px',
        borderBottomLeftRadius: '30px',
        borderBottomRightRadius: '30px',
        width: '120px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    progressPointsContainer: {
        backgroundColor: '#CBAB70',
        borderBottomLeftRadius: '30px',
        borderBottomRightRadius: '30px',
        borderTopLeftRadius: '30px',
        borderTopRightRadius: '30px',
        width: '120px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    pointsText: {
        fontSize: '20px',
        color: 'white'
    }
}));

const SeminarPoints = ({ id, score }) => {
    const navigate = useNavigate();
    const classes = useStyles();
    const [userInfo, setUserInfo] = useState([]);
    const getProgressForGoal = () => (userInfo?.points / userInfo?.rewardGoal?.rewardPoints) * 100;
    const getRemainingPointsToGoal = () => {
        return (userInfo?.rewardGoal?.rewardPoints - userInfo?.points || 0) > 0 ? userInfo?.rewardGoal?.rewardPoints - userInfo?.points : 0;
    };
    const PointsProgressBar = ({ value }) => {
        const remainingPoints = getRemainingPointsToGoal();
        const progressForGoal = getProgressForGoal();
        const totalPoints = remainingPoints + progressForGoal;
        const remainingHeight = (remainingPoints / totalPoints) * 100;
        const progressHeight = (progressForGoal / totalPoints) * 100;

        return (
            <Box className={classes.progressBarContainer}>
                <Box className={classes.remainingPointsContainer} style={{ height: `${remainingHeight}%` }}>
                    {remainingPoints !== null && remainingPoints !== 0 && <span className={classes.pointsText}>{remainingPoints}</span>}
                </Box>
                <Box className={classes.progressPointsContainer} style={{ height: `${progressHeight}%` }}>
                    {progressForGoal !== null && progressForGoal !== 0 && <span className={classes.pointsText}>{progressForGoal}</span>}
                </Box>
            </Box>
        );
    };

    useEffect(() => {
        getUserById(loggedInUser.id)
            .then((res) => res.data)
            .then((user) => {
                setUserInfo(user.userInfo);
            })
            .catch((e) => console.log(e));
    }, []);

    const handleComplete = () => {
        addSeminarPointsToUser(loggedInUser.id, id);
    };

    
    return (
        <div>
            <div
                style={{
                    display: 'flex',
                    justifyContent: 'space-between'
                }}
            >
                <div style={{ display: 'flex', flexDirection: 'column', marginTop: '30px' }}>
                    <Typography variant="h1">Congratulations! You did it</Typography>
                    <Button
                        onClick={() => handleComplete()}
                        variant="contained"
                        style={{
                            backgroundColor: '#CBAB70',
                            padding: '10px',
                            borderRadius: '18px',
                            width: '30em',
                            marginTop: '40px'
                        }}
                    >
                        <div style={{ display: 'flex', flexDirection: 'column' }}>
                            <p style={{ margin: 0, padding: '5px' }}>Finish your Seminar</p>{' '}
                            <p style={{ margin: 0, padding: 0 }}>to get your desired points.</p>
                        </div>
                    </Button>
                    <div style={{ display: 'flex', marginTop: '40px', marginLeft: '-30px' }}>
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',
                                alignContent: 'center',
                                marginTop: '30px'
                            }}
                        >
                            <Typography variant="h3" sx={{ fontSize: '20px' }}>
                                +{score}
                            </Typography>
                            <Typography variant="h3">Points</Typography>
                            <KeyboardDoubleArrowUp style={{ marginBottom: 0, paddingBottom: 0, fontSize: '150px' }} />
                            <KeyboardArrowUp style={{ marginTop: '-80px', paddingTop: 0, fontSize: '150px' }} />
                        </div>
                        <div style={{ marginTop: '40px' }}>
                            <PointsProgressBar value={getProgressForGoal()} />
                            {userInfo?.rewardGoal === null && <p>Please select a goal to get points.</p>}
                        </div>

                        <img
                            src={
                                userInfo?.rewardGoal?.customReward?.picture
                                    ? userInfo?.rewardGoal?.customReward.picture
                                    : '/images/dish.jpg'
                            }
                            alt="reward"
                            style={{
                                marginLeft: '20px',
                                marginTop: '40px',
                                borderRadius: '10px',
                                height: '200px',
                                width: '200px'
                            }}
                        />
                    </div>
                </div>

                <div>
                    <img src="/images/congrats.jpg" alt="congrats" style={{ width: '800px', height: '600px' }} />
                </div>
            </div>
        </div>
    );
};

export default SeminarPoints;
