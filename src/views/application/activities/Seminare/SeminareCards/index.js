// material-ui

import 'bootstrap/dist/css/bootstrap.min.css';

import { Box, Divider, Grid, Typography } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import React, { useEffect } from 'react';

import { FormattedMessage } from 'react-intl';
import { RoomTwoTone } from '@mui/icons-material';
import SeminarCard from 'stories/SeminarCard/SeminarCard';
import SeminareCard from './SeminareCard';
import Slider from 'react-slick';
import { getMostUsedSeminars } from 'services/AnalyzeToolService';
import { getUserFavorites } from 'services/userService';
import { loadSeminare } from 'services/SeminareService';
import { loggedInUser } from 'contexts/JWTContext';
import { setting } from '../../TrainingPlan/TrainingPlanCards';
import { showMessage } from '../../../../../utils/form/Communication';
import { useDispatch } from 'store';

const SeminareCards = () => {
    const dispatch = useDispatch();
    let [data, setData] = React.useState([]);
    let [trackingData, setTrackingData] = React.useState([]);
    let [favorites, setFavorites] = React.useState([]);
    const [makeMeFavourate, setMakeMeFavourate] = React.useState(false);
    React.useEffect(() => {
        loadSeminare()
            .then((res) => res.data)
            .then(setData)
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });

        getMostUsedSeminars()
            .then((res) => res.data)
            .then((r) => {
                setTrackingData(r);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    useEffect(() => {
        getUserFavorites(loggedInUser.id, 3)
            .then((res) => res.data)
            .then((data) => {
                let userFavorites = [];
                data.forEach((favorit) => {
                    if (
                        !userFavorites.find(
                            (intermediateFavorit) => intermediateFavorit.careployeeBaseEntity?.id === favorit.careployeeBaseEntity?.id
                        )
                    ) {
                        userFavorites = [...userFavorites, favorit];
                    }
                });

                setFavorites(userFavorites);
            })
            .catch((e) => {
                
            });
    }, [makeMeFavourate]);

    const isFavorit = (id) => (!!favorites && favorites.length ? favorites : []).some((favorit) => favorit.careployeeBaseEntity.id === id);
    const [allSeminar, setAllSeminar] = React.useState(false);
    const favouratesResult = data?.filter((recipe) => favorites?.find((favRecipe) => favRecipe?.careployeeBaseEntity?.id === recipe?.id));
    return (
        <div>
            {allSeminar ? (
                <div>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            All Seminar
                        </Typography>
                        <Typography
                            component={Link}
                            style={{ marginLeft: '10px', cursor: 'pointer', textDecoration: 'none', color: '#D9D3D3' }}
                            onClick={() => setAllSeminar(false)}
                        >
                            Back
                        </Typography>
                    </div>
                    <Grid item xs={12}>
                        <Grid container spacing={3}>
                            {(data && data.length ? data : []).map((row, index) => (
                                <Grid item xs={12} sm={4} md={3} key={row.id}>
                                    <SeminarCard
                                        image={row?.picture || '/images/seminare.jpg'}
                                        description={
                                            row?.description
                                                ? row?.description?.length > 120
                                                    ? row?.description.substring(0, 120) + '...'
                                                    : row?.description
                                                : ''
                                        }
                                        row={row}
                                        title={row?.title || 'Test'}
                                        autherImage={row?.authorDto?.picture}
                                        checkFavorit={{ isFavorit: isFavorit(row?.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    ></SeminarCard>
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>
                </div>
            ) : (
                <div>
                    <Grid item xs={12}>
                        <h3 style={{ margin: '8px 0' }}>
                            <FormattedMessage id={'Seminare'} />
                        </h3>
                    </Grid>

                    {favouratesResult && favouratesResult?.length > 0 ? (
                        <div>
                            <Typography variant="h3" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                                Favourates
                            </Typography>
                            <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                                {favouratesResult?.map((row) => {
                                    // row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                                    return (
                                        <SeminarCard
                                            image={row?.picture || '/images/seminare.jpg'}
                                            description={
                                                row?.description
                                                    ? row?.description?.length > 120
                                                        ? row?.description.substring(0, 120) + '...'
                                                        : row?.description
                                                    : ''
                                            }
                                            row={row}
                                            title={row?.title || 'Test'}
                                            autherImage={row?.authorDto?.picture}
                                            checkFavorit={{ isFavorit: isFavorit(row?.id) }}
                                            makeMeFavourate={makeMeFavourate}
                                            setMakeMeFavourate={setMakeMeFavourate}
                                        ></SeminarCard>
                                    );
                                })}
                            </Slider>
                        </div>
                    ) : (
                        <div style={{ width: '100%' }}>
                            <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                                Favorites
                            </Typography>
                            <div style={{ display: 'flex', justifyContent: 'center' }}>
                                <img src="/images/favourate.svg" alt="first favourate" style={{ textAlign: 'center' }} />
                            </div>
                        </div>
                    )}

                    <Grid item xs={12} style={{ marginTop: '20px' }}>
                        <Divider sx={{ borderColor: 'grey.200' }} />
                    </Grid>

                    <Grid item xs={12}>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                                All Seminar
                            </Typography>
                            <Typography
                                component={Link}
                                style={{
                                    marginLeft: '10px',
                                    cursor: 'pointer',
                                    textDecoration: 'none',
                                    color: 'lightGray',
                                    marginTop: '4px'
                                }}
                                onClick={() => setAllSeminar(true)}
                            >
                                Show More
                            </Typography>
                        </div>
                    </Grid>
                    <>
                        <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                            {(data && data.length ? data : []).map((row, index) => (
                                <Grid item xs={8} sm={6} md={4} key={row.id}>
                                    <SeminarCard
                                        image={row?.picture || '/images/seminare.jpg'}
                                        description={
                                            row?.description
                                                ? row?.description?.length > 120
                                                    ? row?.description.substring(0, 120) + '...'
                                                    : row?.description
                                                : ''
                                        }
                                        row={row}
                                        title={row?.title || 'Test'}
                                        autherImage={row?.authorDto?.picture}
                                        checkFavorit={{ isFavorit: isFavorit(row?.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    ></SeminarCard>
                                </Grid>
                            ))}
                        </Slider>
                    </>
                </div>
            )}
        </div>
    );
};

export default SeminareCards;
