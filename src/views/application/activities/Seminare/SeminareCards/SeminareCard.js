// material-ui
import { Card, CardActions, CardContent, Grid, Typography } from '@mui/material';
// third party

import WhatshotIcon from '@mui/icons-material/Whatshot';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import SubCard from 'ui-component/cards/SubCard';
import { useTheme } from '@mui/styles';
import Author from 'ui-component/Author';
import { addActivityToUserFavorites } from 'services/userService';
import { loggedInUser } from 'contexts/JWTContext';
import { useDispatch } from 'store';
import { showMessage } from 'utils/form/Communication';

const SeminareCard = ({ row, onSetAsFavorite }) => {
    const theme = useTheme();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    let [isFavorit, setIsFavorit] = React.useState(row.isFavorit);

    const cardStyle = {
        cursor: 'pointer',
        background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.grey[50],
        border: '1px solid',
        borderColor: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.grey[100]
    };

    const setAsFavorite = (activityId) => {
        addActivityToUserFavorites({ userId: loggedInUser.id, activityId, activityNum: 4 })
            .then((res) => res?.data)
            .then(() => {
                onSetAsFavorite();
                setIsFavorit(true);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    return (
        <SubCard title={row.title}>
            <Card sx={cardStyle}>
                <div
                    className="careployee__elements__image-backdrop"
                    onClick={() => navigate('/activities/seminare/quiz/' + row.id)}
                    style={{
                        backgroundImage: `url(${row.picture || '/images/seminare.jpg)'}`
                    }}
                >
                    <CardContent
                        onClick={() => navigate('/activities/seminare/quiz/' + row.id)}
                        sx={{ minHeight: 240, color: theme.palette.common.white }}
                    >
                        <Grid container spacing={1}>
                            <Grid item className="careployee-cards__most-used__wrapper">
                                {row.isMostUsed ? (
                                    <div class="careployee-cards__box__most-used careployee-cards__box__most-used--seminare">
                                        <WhatshotIcon
                                            color="primary"
                                            sx={{
                                                color: theme.palette.orange.dark,
                                                borderColor: theme.palette.orange.main
                                            }}
                                            size="large"
                                        />
                                    </div>
                                ) : (
                                    <></>
                                )}
                                <Typography variant="subtitle1" color="inherit">
                                    {row.description
                                        ? row.description.length > 120
                                            ? row.description.substring(0, 120) + '...'
                                            : row.description
                                        : ''}
                                </Typography>
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardActions>
                        <Grid container>
                            <Author author={row.authorDto} color="white"></Author>
                        </Grid>
                    </CardActions>
                </div>
            </Card>
        </SubCard>
    );
};

export default SeminareCard;
