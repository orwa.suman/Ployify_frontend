import { Avatar, Box, Button, Divider, Grid, Typography } from '@mui/material';
import React, { useRef } from 'react';
import { useEffect, useState } from 'react';

import AssignmentIcon from '@mui/icons-material/AssignmentIndTwoTone';
import AuthorBasic from '../../Author/AuthorBasic';
import Card from '@mui/material/Card';
import CategoryIcon from '@mui/icons-material/Category';
import MainCard from 'ui-component/cards/MainCard';
import ThreePIcon from '@mui/icons-material/ThreeP';
import UserDialog from '../MentalHealthCards/AuthorDialog';
import Vimeo from '@u-wave/react-vimeo';
import VimeoPlayer from '@vimeo/player';
import { addActivityToUserHistory } from 'services/userService';
import { getMentalHealth } from 'services/MentalHealthService';
import { loggedInUser } from 'contexts/JWTContext';
import { openDrawer } from 'store/slices/menu';
import parse from 'html-react-parser';
import { showMessage } from '../../../../../utils/form/Communication';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';

const CustomShadowBox = ({ shadow, label, color }) => {
    const theme = useTheme();

    return (
        <Card sx={{ mb: 3, boxShadow: shadow }}>
            <Box
                sx={{
                    display: 'flex',
                    alignItems: 'center',
                    py: 3,
                    bgcolor: color,
                    fontWeight: 'bold',
                    color: 'white',
                    paddingLeft: '24px',
                    fontSize: '22px'
                }}
            >
                {label && <Box sx={{ color: 'inherit' }}>{label}</Box>}
            </Box>
        </Card>
    );
};

export default function MentalHealthDetails() {
    const { id } = useParams();
    const [mentalHealthData, setMentalHealthData] = useState(null);
    const [data, setData] = useState({
        title: 'Mental Health Title',
        description: '',
        category: 'Self awareness',
        autor: 'Admin',
        linkToVideo: 'https://vimeo.com/771124484',
        picture: '/images/seminar.jpg'
    });
    const navigate = useNavigate();
    const theme = useTheme();

    useEffect(() => {
        getMentalHealth(id)
            .then((res) => res.data)
            .then((data) => {
                setData(data);
                
                addActivityToUserHistory(loggedInUser.id, id, 4);
                setMentalHealthData(data);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);
    
    const dispatch = useDispatch();
    const videoUrl = mentalHealthData?.videoLink || 'https://vimeo.com/771124484';
    const videoId = videoUrl?.match(/vimeo\.com\/(\d+)/)[1];

    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        dispatch(openDrawer(false));
        setOpen(true);
    };

    const playerRef = useRef(null);
    const iframeRef = useRef(null);

    useEffect(() => {
        const player = new VimeoPlayer(iframeRef.current);

        // player.requestFullscreen().catch((error) => {
        //     
        //     // Handle the error appropriately, such as displaying a message to the user
        // });

        // player.on('fullscreenchange', handleFullscreenChange);

        // return () => {
        //     player.off('fullscreenchange', handleFullscreenChange);
        // };
    }, []);

    function handleFullscreenChange() {
        const player = playerRef.current;
        if (player) {
            if (!document.fullscreenElement && !document.webkitFullscreenElement) {
                navigate('/activities/MentalHealth/cards');
            }
        }
    }

    return (
        <>
            <div>
                <div>
                    <div style={{ marginLeft: '100px' }}>
                        <iframe
                            title="vimeo-player"
                            width={1000}
                            height={500}
                            ref={iframeRef}
                            src={`https://player.vimeo.com/video/${videoId}`}
                            frameBorder="0"
                            allowFullScreen
                        />
                    </div>
                </div>
                <div>
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            marginTop: '10px',
                            justifyContent: 'space-between',
                            paddingInlineEnd: '40px'
                        }}
                    >
                        <Typography variant="h3">{data.title}</Typography>
                        <Typography>
                            <Avatar
                                sx={{
                                    marginRight: '12px',
                                    width: '26px',
                                    height: '26px',
                                    display: 'inline-flex',
                                    position: 'relative',
                                    top: '6px'
                                }}
                                variant="rounded"
                                color="success"
                            >
                                <CategoryIcon size="12px" />
                            </Avatar>
                            <span style={{ fontWeight: 'bold', color: theme.palette.primary.main }}>{data.category}</span>
                        </Typography>
                    </div>
                    {/* <div>
                        <CustomShadowBox color="#CBAB70" label={data.title} />
                    </div> */}

                    <div onClick={handleClickOpen} style={{ cursor: 'pointer' }}>
                        <AuthorBasic
                            firstName={data.author?.firstName}
                            lastName={data?.author?.lastName}
                            description={data?.author?.description}
                            avatarUrl={data?.author?.picture}
                        />
                    </div>
                    <UserDialog user={mentalHealthData?.author} open={open} setOpen={setOpen} />

                    <div sx={{ margin: '0 0 12px', fontSize: '18px', marginLeft: '20px' }}>{parse(data.description ?? '')}</div>

                    {/* <Grid item xs={12} md={5} sx={{ padding: '16px' }}> */}

                    {/* </Grid> */}
                </div>
            </div>
        </>
    );
}
