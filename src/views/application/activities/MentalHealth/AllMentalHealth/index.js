import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { showMessage } from 'utils/form/Communication';
import { Divider, Grid, Typography } from '@mui/material';
import { loadMentalHealth } from 'services/MentalHealthService';
import MentalHealthCard from '../MentalHealthCards/MentalHealthCard';

const AllMentalHealth = () => {
    const [data, setData] = useState([]);
    const dispatch = useDispatch();
    useEffect(() => {
        loadMentalHealth()
            .then((res) => res.data)
            .then((data) => {
                setData(data);
            })
            .catch((e) => {
                dispatch(showMessage(e.message));
            });
    }, []);

    
    return (
        <div>
            <Typography variant="h3">All Training</Typography>
            <Divider sx={{ margin: '32px 0' }} />
            <Grid container spacing={2}>
                {(data && data?.length ? data : []).map((row, index) => {
                    row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                    return (
                        <Grid item xs={12} sm={6} md={4} key={row.id}>
                            <MentalHealthCard
                                key={row.id}
                                row={{
                                    ...row
                                }}
                            />
                        </Grid>
                    );
                })}
            </Grid>
        </div>
    );
};

export default AllMentalHealth;
