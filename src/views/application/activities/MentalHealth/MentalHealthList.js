import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../../assets/scss/style.css';

import {
    Grid,
    IconButton,
    InputAdornment,
    OutlinedInput,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
    Typography
} from '@mui/material';
import { deleteMentalHealth, loadMentalHealth } from 'services/MentalHealthService';

import Author from 'ui-component/Author';
import BlockTwoToneIcon from '@mui/icons-material/BlockTwoTone';
import ChatBubbleTwoToneIcon from '@mui/icons-material/ChatBubbleTwoTone';
import { FormattedMessage } from 'react-intl';
import { HiPencil } from 'react-icons/hi';
import { IconSearch } from '@tabler/icons';
import MainCard from 'ui-component/cards/MainCard';
import React from 'react';
import { dispatch } from 'store';
import { gridSpacing } from 'store/constant';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';

// assets

function MentalHealthList() {
    const theme = useTheme();
    const navigate = useNavigate();
    let [data, setData] = React.useState([]);

    const loadAllMEntalHealth = () => {
        loadMentalHealth()
            .then((res) => res.data)
            .then((data) => setData(data.reverse()))
            .catch((e) => {
                
            });
    };

    React.useEffect(() => {
        loadAllMEntalHealth();
    }, []);

    const deleteItem = (id) => {
        deleteMentalHealth(id).then(() => loadAllMEntalHealth());
    };

    return (
        <MainCard
            title={
                <Grid container alignItems="center" justifyContent="space-between" spacing={gridSpacing}>
                    <Grid item>
                        <Typography variant="h3">
                            <FormattedMessage id="List" />
                        </Typography>
                    </Grid>
                    <Grid item>
                        <OutlinedInput
                            id="input-search-list-style1"
                            placeholder="Search"
                            startAdornment={
                                <InputAdornment position="start">
                                    <IconSearch stroke={1.5} size="16px" />
                                </InputAdornment>
                            }
                            size="small"
                        />
                    </Grid>
                </Grid>
            }
            content={false}
        >
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell scope="col">
                                <FormattedMessage id="Picture" />
                            </TableCell>
                            <TableCell scope="col">
                                <FormattedMessage id="Title" />
                            </TableCell>
                            <TableCell scope="col">
                                <FormattedMessage id="Category" />
                            </TableCell>
                            <TableCell scope="col">
                                <FormattedMessage id="Author" />
                            </TableCell>
                            <TableCell align="center" sx={{ pr: 3 }}>
                                <FormattedMessage id="Actions" />
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data &&
                            data.map((row, index) => (
                                <TableRow hover key={index}>
                                    <TableCell>
                                        {!!row.picture ? (
                                            <div className="careployee-training-plan__image">
                                                <img src={row.picture}></img>
                                            </div>
                                        ) : (
                                            ''
                                        )}
                                    </TableCell>
                                    <TableCell>{row.title}</TableCell>
                                    <TableCell>{row.category}</TableCell>
                                    <TableCell>
                                        <Author author={row.authorDto}></Author>
                                    </TableCell>
                                    <TableCell align="center" sx={{ pr: 3 }}>
                                        <Stack direction="row" justifyContent="center" alignItems="center">
                                            <Tooltip placement="top" title="edit">
                                                <IconButton
                                                    color="primary"
                                                    aria-label="edit"
                                                    onClick={() => navigate('/dashboard/activities/mental-health/form/' + row.id)}
                                                    size="large"
                                                >
                                                    <HiPencil sx={{ fontSize: '1.1rem' }} />
                                                </IconButton>
                                            </Tooltip>

                                            <Tooltip placement="top" title="Delete" onClick={() => deleteItem(row.id)}>
                                                <IconButton
                                                    color="primary"
                                                    sx={{
                                                        color: theme.palette.orange.dark,
                                                        borderColor: theme.palette.orange.main,
                                                        '&:hover ': { background: theme.palette.orange.light }
                                                    }}
                                                    size="large"
                                                >
                                                    <BlockTwoToneIcon sx={{ fontSize: '1.1rem' }} />
                                                </IconButton>
                                            </Tooltip>
                                        </Stack>
                                    </TableCell>
                                </TableRow>
                            ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </MainCard>
    );
}

export default MentalHealthList;
