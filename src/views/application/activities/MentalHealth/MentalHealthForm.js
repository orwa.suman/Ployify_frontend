import React, { useEffect, useState } from 'react';
import { useFormik } from 'formik';
import MainCard from '../../../../ui-component/cards/MainCard';
import FileUpload from '../../../forms/components/FileUpload';
import { addMentalHealth, loadCategories, loadMentalHealth, updateMentalHealth } from '../../../../services/MentalHealthService';
import { loadAuthors } from '../../../../services/AuthorService';
import * as Yup from 'yup';
import { useDispatch } from 'store';
import { Button, Grid, TextField, Autocomplete, Dialog } from '@mui/material';
import { showMessage } from '../../../../utils/form/Communication';
import 'react-quill/dist/quill.snow.css';
import { useParams } from 'react-router-dom';
import { FormattedMessage, useIntl } from 'react-intl';
import { uploadToCloudinary } from 'services/CloudinaryService';
import theme from 'theme';
import { loadMentalHealthTags } from 'services/TagService';
import MentalHealthTagForm from './MentalHealthTagForm';
import PropTypes from 'prop-types';

export function SimpleTagDialog(props) {
    const { open, onClose, type } = props;

    return (
        <Dialog onClose={onClose} open={open} fullWidth>
            
            <MentalHealthTagForm />
        </Dialog>
    );
}

SimpleTagDialog.propTypes = {
    open: PropTypes.bool.isRequired
};

function MentalHealthForm() {
    const [categories, setCategories] = React.useState([]);
    const [category, setCategory] = React.useState('');
    const [authors, setAuthors] = React.useState([]);
    const [author, setAuthor] = React.useState({});
    const { id } = useParams();
    const [categoriesUpdated, setCategoriesUpdated] = React.useState(false);
    const [picture, setPicture] = React.useState([]);
    const [pictureName, setPictureName] = React.useState([]);
    const [changed, setChanged] = React.useState(false);

    const [tagForm, setTagForm] = useState(false);
    const [mentalHealthTags, setMentalHealthTags] = useState([]);

    const dispatch = useDispatch();
    const intl = useIntl();

    React.useEffect(() => {
        requestCategories();
    }, []);

    React.useEffect(() => {
        requestAuthors();
        loadTags();

        if (id) {
            loadMentalHealth()
                .then((res) => res.data)
                .then((data) => {
                    const row = data.find((item) => item.id === id);
                    formik.setValues({
                        title: row.title,
                        description: row.description,
                        categoryMH: row.category,
                        author: row.author,
                        videoLink: row.videoLink,
                        audioLink: row.audioLink,
                        picture: row.picture,
                        mentalHealthTags: data.mentalHealthTags
                    });
                })
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
    }, [categories, id, category]);

    const requestAuthors = () => {
        const request = loadAuthors();
        request
            .then((res) => res.data)
            .then((data) => {
                setAuthors(data);
                if (data.length) {
                    formik.values.author = data[0];
                    setAuthor(data[0]);
                }
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const requestCategories = () => {
        const request = loadCategories();
        request
            .then((res) => res.data)
            .then((data) => {
                setCategories(data);
                if (data.length) {
                    if (formik.values.categoryMH && data.includes(formik.values.categoryMH)) {
                        setCategory(formik.values.categoryMH);
                        
                    } else {
                        formik.values.categoryMH = data[0];
                        setCategory(data[0]);
                    }
                }
                // Categories request completed, update the trigger
                setCategoriesUpdated(!categoriesUpdated);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };
    
    const validationSchema = Yup.object({
        title: Yup.string()
            .max(50, <FormattedMessage id="Input Max Length" values={{ length: 50 }} />),
        //.required(<FormattedMessage id="Required" />),
        description: Yup.string().max(500, <FormattedMessage id="Input Max Length" values={{ length: 500 }} />),
        //.required(<FormattedMessage id="Required" />),
        categoryMH: Yup.string().required(<FormattedMessage id="Required" />),
        author: Yup.object().required(<FormattedMessage id="Required" />).nullable(),
        videoLink: Yup.string().required(<FormattedMessage id="Required" />)
    });

    const formik = useFormik({
        initialValues: {
            title: '',
            description: '',
            categoryMH: '',
            author: null,
            videoLink: '',
            audioLink: '',
            picture: '',
            mentalHealthTags: []
        },
        validationSchema,
        onSubmit: () => {
            uploadToCloudinary(picture, pictureName, 'mh_', '/Ployify/Activities/MentalHealth')
                .then((data) => {
                    if (changed) {
                        
                        
                        formik.values.picture = data.url;
                    }

                    if (id) {
                        updateMentalHealth(id, formik.values)
                            .then((response) => {
                                dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success'));
                            })
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    } else {
                        addMentalHealth(formik.values)
                            .then((response) => {
                                dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success'));
                            })
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    }
                })
                .catch((err) => {
                    
                });
        }
    });

    const submitForm = (event) => {
        formik.validateForm().then((errors) => {
            if (errors && Object.keys(errors).length > 0) {
                
                const errorMessages = Object.entries(errors).map(([fieldName, fieldError]) => {
                    return `${fieldName}: ${fieldError.props.id}`;
                });

                dispatch(showMessage(<FormattedMessage id={errorMessages.join(', ')} />));
            }
            formik.handleSubmit(event);
        });
    };

    const loadTags = () => {
        loadMentalHealthTags()
            .then((res) => res.data)
            .then((data) => {
                setMentalHealthTags(data);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const handleFileUpload = (data, fileName) => {
        setPicture(data);
        setPictureName(fileName);
        setChanged(true);
    };

    const openTagForm = () => {
        setTagForm(true);
    };

    const handleDialogClose = (value) => {
        setTagForm(false);
        loadTags();
    };

    function handleTag(event, value) {
        formik.setValues({ ...formik.values, mentalHealthTags: value });
    }

    return (
        <Grid>
            <form onSubmit={submitForm}>
                <MainCard title={<FormattedMessage id="First Page" />}>
                    <Grid container>
                        <Grid item xs={12} md={5} className="careployee-form__block">
                            <Grid item xs={12} className="careployee-form__block">
                                <label htmlFor="title" className="careployee-form__label">
                                    <FormattedMessage id="Title" />
                                </label>
                                <TextField
                                    fullWidth
                                    id="title"
                                    value={formik.values.title}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    error={formik.touched.title && Boolean(formik.errors.title)}
                                    helperText={formik.touched.title && formik.errors.title}
                                />
                            </Grid>
                            <Grid item xs={12} className="careployee-form__block">
                                <label htmlFor="description" className="careployee-form__label">
                                    <FormattedMessage id="Description" />
                                </label>
                                <TextField
                                    fullWidth
                                    id="description"
                                    multiline
                                    rows={8}
                                    value={formik.values.description}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    error={formik.touched.description && Boolean(formik.errors.description)}
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={4} className="careployee-form__block">
                            <FileUpload label="Picture" defaultPicture={formik.values.picture} onChange={handleFileUpload}></FileUpload>
                        </Grid>
                    </Grid>

                    <Grid container spacing={1} className="careployee-form__block">
                        <Grid item xs={12} md={5} className="careployee-form__block">
                            <label htmlFor="author" className="careployee-form__label">
                                <FormattedMessage id="Author" />
                            </label>
                            <Autocomplete
                                style={{ height: '44px' }}
                                disablePortal
                                options={authors}
                                getOptionLabel={(option) => option.firstName + ' ' + option.lastName}
                                value={author}
                                onChange={(event, value) => {
                                    formik.values.author = value;
                                    setAuthor(value);
                                }}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        error={formik.errors.author}
                                        helperText={!!formik.errors.author && formik.errors.author}
                                    />
                                )}
                            />
                        </Grid>

                        <Grid item xs={12} md={5} className="careployee-form__block">
                            <label htmlFor="categoryMH" className="careployee-form__label">
                                <FormattedMessage id="Category" />
                            </label>
                            <Autocomplete
                                style={{ height: '44px' }}
                                disablePortal
                                options={categories}
                                getOptionLabel={(option) => option}
                                value={formik.values.categoryMH === '' ? category : formik.values.categoryMH}
                                onChange={(event, value) => {
                                    formik.values.categoryMH = value;
                                    setCategory(value);
                                }}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        error={formik.errors.categoryMH}
                                        helperText={!!formik.errors.categoryMH && formik.errors.categoryMH}
                                    />
                                )}
                            />
                        </Grid>

                        <Grid item xs={10} className="careployee-form__block">
                            <label htmlFor="audioLink" className="careployee-form__label">
                                <FormattedMessage id="Audio link" />
                            </label>
                            <TextField
                                id="audioLink"
                                type={'url'}
                                style={{ width: '100%' }}
                                value={formik.values.audioLink}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.audioLink && Boolean(formik.errors.audioLink)}
                                helperText={formik.touched.audioLink && formik.errors.audioLink}
                            />
                        </Grid>

                        <Grid item xs={10} className="careployee-form__block">
                            <label htmlFor="videoLink" className="careployee-form__label">
                                <FormattedMessage id="Video link" />
                            </label>
                            <TextField
                                id="videoLink"
                                type={'url'}
                                style={{ width: '100%' }}
                                value={formik.values.videoLink}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.videoLink && Boolean(formik.errors.videoLink)}
                                helperText={formik.touched.videoLink && formik.errors.videoLink}
                            />
                        </Grid>
                    </Grid>

                    <Grid item>
                        <Button
                            variant="contained"
                            onClick={openTagForm}
                            sx={{
                                margin: '16px 0 24px',
                                borderRadius: '8px',
                                background: theme.palette.success.dark,
                                '&:hover': { background: theme.palette.success.main }
                            }}
                        >
                            <FormattedMessage id="Add New Tag" />
                        </Button>
                        <Autocomplete
                            multiple={true}
                            options={mentalHealthTags}
                            value={
                                !!formik.values.mentalHealthTags && formik.values.mentalHealthTags.length
                                    ? [...formik.values.mentalHealthTags]
                                    : []
                            }
                            getOptionLabel={(option) => option.name}
                            onChange={(event, key) => handleTag(event, key)}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label={intl.formatMessage({ id: 'Multiple Tags' })}
                                    onChange={(value) => handleTag(value)}
                                />
                            )}
                        />

                        <SimpleTagDialog open={tagForm} onClose={handleDialogClose} type="mentalHealth" />
                    </Grid>

                    <Grid>
                        <Grid item xs={12}>
                            <span className="careployee-form__submit-button">
                                <Button variant="contained" onClick={submitForm} size="large">
                                    <FormattedMessage id="Submit" />
                                </Button>
                            </span>
                        </Grid>
                    </Grid>
                </MainCard>
            </form>
        </Grid >
    );
}

export default MentalHealthForm;
