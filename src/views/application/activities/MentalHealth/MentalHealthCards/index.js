// material-ui

import 'bootstrap/dist/css/bootstrap.min.css';

import { Box, CircularProgress, Grid, Typography } from '@mui/material';
import { MealIcon, TagIconButton, tagsSetting } from '../../Recipes/RecipesCards';
import React, { useEffect, useState } from 'react';
import { getUserFavorites, getUserHistory } from 'services/userService';
import { loadCategories, loadMentalHealth } from 'services/MentalHealthService';

import MentalHealthCard from './MentalHealthCard';
import Slider from 'react-slick';
import { getMostUsedMentalHealth } from 'services/AnalyzeToolService';
import { loggedInUser } from 'contexts/JWTContext';
import { setting } from '../../TrainingPlan/TrainingPlanCards';
import { truncate } from 'lodash';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';

// third party

const Cards = () => {
    const navigate = useNavigate();
    const mockData = { id: 'XYZ', title: 'Title', description: 'Short description', category: 'Category name', autor: 'Gergo Mando' };
    let [data, setData] = React.useState([mockData]);
    let [favorites, setFavorites] = React.useState([]);
    let [trackingData, setTrackingData] = React.useState([]);
    const [allMentalHealth, setAllMentalHealth] = React.useState([]);
    let [selectedTab, setSelectedTab] = React.useState('MEDITATION');
    const [latestUsed, setLatestUsed] = React.useState([]);
    let [categories, setCategories] = React.useState([]);
    const [tags, setTags] = useState([]);
    const [mentalHealthbyTag, setmentalHealthbyTag] = useState([]);
    const [selectedTags, setSelectedTags] = useState('');
    const [makeMeFavourate, setMakeMeFavourate] = useState(false);
    const [loading, setLoading] = useState(true);

    React.useEffect(() => {
        showTab('latestUsed');
        loadCategories()
            .then()
            .then((res) => res.data)
            .then((r) => {
                setCategories(r);
                setTags(r);
            })
            .catch((e) => {
                //  dispatch(showMessage(e?.response?.data?.message));
            });

        getMostUsedMentalHealth()
            .then((res) => res.data)
            .then((r) => {
                setTrackingData(r);
            })
            .catch((e) => {
                // dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    const loadLatestUsed = () => {
        getUserHistory(loggedInUser.id, 4)
            .then((res) => res.data)
            .then((data) => {
                setData(data);
                setLatestUsed(data);
            })
            .catch((e) => {
                // dispatch(showMessage(e.message));
                
            });
    };

    useEffect(() => {
        loadLatestUsed();
    }, [makeMeFavourate]);

    useEffect(() => {
        getUserFavorites(loggedInUser.id, 4)
            .then((res) => res.data)
            .then((data) => {
                // let userFavorites = [];
                // data.forEach((favorit) => {
                //     if (
                //         !userFavorites.find(
                //             (intermediateFavorit) => intermediateFavorit.careployeeBaseEntity?.id === favorit.careployeeBaseEntity?.id
                //         )
                //     ) {
                //         userFavorites = [...userFavorites, favorit];
                //     }
                // });

                setFavorites(data);
            });
    }, [makeMeFavourate]);

    const showTab = (tab) => {
        if (tab !== selectedTab) {
            setSelectedTab(tab);
        }
        getUserFavorites(loggedInUser.id, 4)
            .then((res) => res.data)
            .then((data) => {
                let userFavorites = [];
                data.forEach((favorit) => {
                    if (
                        !userFavorites.find(
                            (intermediateFavorit) => intermediateFavorit.careployeeBaseEntity?.id === favorit.careployeeBaseEntity?.id
                        )
                    ) {
                        userFavorites = [...userFavorites, favorit];
                    }
                });
                setLoading(false);

                setFavorites(userFavorites);

                if (tab === 'favorite') {
                    setData(userFavorites);
                } else if (tab === 'latestUsed') {
                    loadLatestUsed();
                } else {
                    loadMentalHealth()
                        .then((res) => res.data)
                        .then((data) => {
                            setData(data.filter((item) => item.categoryMH === tab));
                        })
                        .catch((e) => {
                            // dispatch(showMessage(e.message));
                        });
                }
            })
            .catch((e) => {
                //  dispatch(showMessage(e.message));
            });
    };
    useEffect(() => {
        loadMentalHealth()
            .then((res) => setAllMentalHealth(res.data))
            .then((data) => {
                
            })
            .catch((e) => {
                //  dispatch(showMessage(e.message));
            });
    }, []);

    const isFavorit = (id) => (!!favorites && favorites.length ? favorites : []).find((favorit) => favorit.careployeeBaseEntity.id === id);

    function getmentalHealthbyTag(tag) {
        if (!allMentalHealth) {
            console.error('Recipes data not loaded yet');
            return [];
        }
        const recipesWithTag = allMentalHealth?.filter((mh) => mh?.category === tag);
        return recipesWithTag;
    }

    const handleTagClick = (tagname) => {
        navigate(`/activities/MentalHealth/cards/tag-menu/${tagname}`);
        setSelectedTags(tagname);
        setmentalHealthbyTag(getmentalHealthbyTag(tagname));
    };

    const favouratesResult = allMentalHealth?.filter((recipe) =>
        favorites?.find((favRecipe) => favRecipe?.careployeeBaseEntity?.id === recipe?.id)
    );

    const latestUsedResult = allMentalHealth?.filter((recipe) =>
        latestUsed?.find((favRecipe) => favRecipe?.careployeeBaseEntity?.id === recipe?.id)
    );

    const meditation = allMentalHealth.filter((item) => item.categoryMH === 'MEDITATION');
    const visualization = allMentalHealth.filter((item) => item.categoryMH === 'VISUALIZATION');
    const breath = allMentalHealth.filter((item) => item.categoryMH === 'BREATHE');
    const sounds = allMentalHealth.filter((item) => item.categoryMH === 'SOUNDS');
    const calmingVideos = allMentalHealth.filter((item) => item.categoryMH === 'CALMING_VIDEOS');
    const stress = allMentalHealth.filter((item) => item.categoryMH === 'STRESS');

    return (
        <>
            <div style={{ marginBottom: '70px' }}>
                {/*    <Grid item xs={12}>
            <Typography variant="h3" component="div" sx={{ margin: '0' }}>
                <FormattedMessage id={'Mental Health'} />
            </Typography>
</Grid> */}
                {loading && (
                    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                        <CircularProgress size={50} style={{ color: '#cbab70' }} />
                    </Box>
                )}

                {favouratesResult?.length > 0 ? (
                    <div style={{ marginBottom: '20px' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Favorites
                        </Typography>
                        <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                            {favouratesResult?.map((row) => {
                                // row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                return (
                                    <MentalHealthCard
                                        row={{ ...row, isMostUsed: true, isFavorit: isFavorit(row.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                ) : (
                    <div style={{ width: '100%' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Favorites
                        </Typography>
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <img src="/images/favourate.svg" alt="first favourate" style={{ textAlign: 'center' }} />
                        </div>
                    </div>
                )}

                {false && (
                    <div style={{ margin: '5px' }}>
                        <Slider {...tagsSetting} style={{ width: '100%', transform: 'none' }}>
                            {categories &&
                                categories?.map((tab, index) => (
                                    <Grid item xs={2} key={index}>
                                        <TagIconButton
                                            name={tab}
                                            IconComponent={MealIcon}
                                            onClick={() => handleTagClick(tab)}
                                            selected={selectedTags === tab} // check if the tag is selected
                                        />
                                    </Grid>
                                ))}
                        </Slider>
                    </div>
                )}

                {allMentalHealth.length === 0 && (
                    <div style={{ width: '100%' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Mental Health
                        </Typography>
                        <div style={{ display: 'flex', justifyContent: 'center' }}>
                            <img width={400} src="/images/mentalHealth.png" alt="first favourite" style={{ textAlign: 'center' }} />
                        </div>
                    </div>
                )}

                {mentalHealthbyTag.length > 0 && (
                    <div style={{ margin: '20px' }}>
                        <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                            {mentalHealthbyTag?.map((row) => {
                                row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                return (
                                    <MentalHealthCard
                                        row={{ ...row, isMostUsed: true, isFavorit: isFavorit(row.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                )}
                <div>
                    {/*  <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                {categories.map((category, index) => (
                    <Grid item xs={2} key={category}>
                        <HoverSocialCard
                            primary=" "
                            secondary={intl.formatMessage({ id: category })}
                            color={selectedTab === category ? '#cbab50' : '#cbab70'}
                            iconPrimary={List}
                            onClick={() => showTab(category)}
                        />
                    </Grid>
                ))}
                </div> */}
                </div>

                {latestUsedResult?.length > 0 && (
                    <div style={{ marginTop: '40px' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Latest Used
                        </Typography>
                        <Slider {...setting} style={{ width: '100%', transform: 'none', marginTop: '20px' }}>
                            {latestUsedResult?.map((row) => {
                                // row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                return (
                                    <MentalHealthCard
                                        row={{ ...row, isMostUsed: true, isFavorit: isFavorit(row.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                )}

                {meditation.length > 0 && (
                    <div style={{ marginTop: '40px' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Meditation
                        </Typography>
                        <Slider {...setting} style={{ width: '100%', transform: 'none', marginTop: '20px' }}>
                            {meditation?.map((row) => {
                                row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                return (
                                    <MentalHealthCard
                                        row={{ ...row, isMostUsed: true, isFavorit: isFavorit(row.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                )}

                {visualization.length > 0 && (
                    <div style={{ marginTop: '40px' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            visualization
                        </Typography>
                        <Slider {...setting} style={{ width: '100%', transform: 'none', marginTop: '20px' }}>
                            {visualization?.map((row) => {
                                row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                                return (
                                    <MentalHealthCard
                                        row={{ ...row, isMostUsed: true, isFavorit: isFavorit(row.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                )}

                {breath.length > 0 && (
                    <div style={{ marginTop: '40px' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Breathe
                        </Typography>
                        <Slider {...setting} style={{ width: '100%', transform: 'none', marginTop: '20px' }}>
                            {breath?.map((row) => {
                                row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                return (
                                    <MentalHealthCard
                                        row={{ ...row, isMostUsed: true, isFavorit: isFavorit(row.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                )}

                {sounds.length > 0 && (
                    <div style={{ marginTop: '40px' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Sounds
                        </Typography>
                        <Slider {...setting} style={{ width: '100%', transform: 'none', marginTop: '20px' }}>
                            {sounds?.map((row) => {
                                row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                return (
                                    <MentalHealthCard
                                        row={{ ...row, isMostUsed: true, isFavorit: isFavorit(row.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                )}
                {calmingVideos.length > 0 && (
                    <div style={{ marginTop: '40px' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Calming Videos
                        </Typography>
                        <Slider {...setting} style={{ width: '100%', transform: 'none', marginTop: '20px' }}>
                            {calmingVideos?.map((row) => {
                                row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                return (
                                    <MentalHealthCard
                                        row={{ ...row, isMostUsed: true, isFavorit: isFavorit(row.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                )}

                {stress.length > 0 && (
                    <div style={{ marginTop: '40px' }}>
                        <Typography variant="h2" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                            Stress
                        </Typography>
                        <Slider {...setting} style={{ width: '100%', transform: 'none', marginTop: '20px' }}>
                            {stress?.map((row) => {
                                row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                return (
                                    <MentalHealthCard
                                        row={{ ...row, isMostUsed: true, isFavorit: isFavorit(row.id) }}
                                        makeMeFavourate={makeMeFavourate}
                                        setMakeMeFavourate={setMakeMeFavourate}
                                    />
                                );
                            })}
                        </Slider>
                    </div>
                )}
            </div>

            <div
                style={{
                    backgroundColor: 'lightgrey',
                    padding: '16px',
                    borderRadius: '4px',
                    marginBottom: '16px',
                    color: '#424242',
                    marginTop: '80px',
                    width: '50%',
                    margin: 'auto'
                }}
            >
                <p style={{ margin: 0, textAlign: 'center', fontSize: '14px' }}>This is a disclaimar text.</p>
            </div>
        </>
    );
};

export default Cards;
