// material-ui

import 'bootstrap/dist/css/bootstrap.min.css';

import { Card, CardActionArea, CardActions, CardContent, Grid, IconButton, Typography } from '@mui/material';
import { addActivityToUserFavorites, removeActivityFromUserFavorites } from 'services/userService';

import Author from 'ui-component/Author';
import { CardMedia } from '@material-ui/core';
import { Favorite } from '@mui/icons-material';
import React from 'react';
import SubCard from 'ui-component/cards/SubCard';
import WhatshotIcon from '@mui/icons-material/Whatshot';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from '../../../../../utils/form/Communication';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/styles';

// third party

const MentalHealthCard = ({ row, onSetAsFavorite, makeMeFavourate, setMakeMeFavourate }) => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    let [isFavorit, setIsFavorit] = React.useState(row.isFavorit);
    const theme = useTheme();
    const cardStyle = {
        cursor: 'pointer',
        background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.grey[50],
        border: '1px solid',
        borderColor: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.grey[100]
    };

    const setAsFavorite = (activityId) => {
        addActivityToUserFavorites({ userId: loggedInUser.id, activityId, activityNum: 4 })
            .then((res) => res?.data)
            .then(() => {
                setIsFavorit(true);
                setMakeMeFavourate(true);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
        setMakeMeFavourate(false);
    };

    const removeFromFavorite = (activityId) => {
        removeActivityFromUserFavorites({ userId: loggedInUser.id, activityId, activityNum: 4 })
            .then((res) => res?.data)
            .then(() => {
                setIsFavorit(false);
                setMakeMeFavourate(true);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
        setMakeMeFavourate(false);
    };

    return (
        <Card
            sx={{
                border: '1px solid rgba(58, 53, 65, 0.12)',
                backgroundColor: '#fff',
                borderRadius: theme.shape.borderRadius,
                transition: 'box-shadow 0.5s ease-in-out',
                width: '290px',
                height: '276px',
                '&:hover': {
                    boxShadow: ' 0 0 30px rgba(0, 0, 0, 0.2)'
                },
                margin: '10px'
            }}
        >
            <div className="careployee-cards__image">
                <divs style={{ position: 'relative' }}>
                    <IconButton
                        // disabled={row.isFavorit || isFavorit}
                        onClick={(e) => (row.isFavorit ? removeFromFavorite(row.id, e) : setAsFavorite(row.id, e))}
                        sx={{
                            position: 'absolute',
                            top: 4,
                            right: 4,
                            zIndex: 1,
                            backgroundColor: 'white',
                            '& .MuiSvgIcon-root': { fontSize: '1.5rem' },
                            color: row.isFavorit ? 'red' : '#BDBDBD'
                        }}
                        size="large"
                    >
                        <Favorite />
                    </IconButton>
                    <img
                        src={row.picture ? row.picture : '/images/seminare.jpg'}
                        alt={row.title}
                        style={{ width: '100%' }}
                        onClick={() => navigate('/activities/MentalHealth/details/' + row.id)}
                    ></img>
                </divs>
            </div>
            <CardContent sx={{ padding: 0 }} className="careployee-cards__box">
                <div style={{ display: 'flex' }}>
                    <Typography sx={{ p: 2 }}>{row.title} </Typography>
                    {/* <img
                        src={row?.author?.picture}
                        alt={row?.author?.title}
                        height="50"
                        width="50"
                        style={{ marginTop: '-50px', borderRadius: '50%' }}
                    /> */}
                    {/* <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <div style={{ marginLeft: '20px', marginTop: '-18px' }}>
                            <h6>{row?.title}</h6>
                            <Typography variant="body2" color="text.secondary">
                                {row.description
                                    ? row.description.length > 60
                                        ? row.description.substring(0, 60) + '...'
                                        : row.description
                                    : ''}{' '}
                            </Typography>
                        </div>
                    </div> */}
                </div>
            </CardContent>
        </Card>
    );
};

export default MentalHealthCard;
