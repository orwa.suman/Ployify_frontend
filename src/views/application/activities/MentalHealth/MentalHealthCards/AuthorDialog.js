import React, { useState } from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Paper } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { openDrawer } from 'store/slices/menu';
import { Typography } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

const UserDialog = ({ user, setOpen, open }) => {
    const dispatch = useDispatch();
    const handleClose = () => {
        dispatch(openDrawer(true));
        setOpen(false);
    };

    const handleOpenInfo = () => {
        let url = user.url;
        if (!/^https?:\/\//i.test(url)) {
            url = 'http://' + url;
        }
        window.open(url, '_blank');
    };

    const styles = {
        dialogPaper: {
            backgroundColor: 'lightgray'
        }
    };

    return (
        <>
            <Dialog open={open} onClose={() => handleClose()} PaperComponent={(props) => <Paper {...props} />}>
                <DialogTitle>
                    <CloseIcon onClick={() => handleClose()} style={{ float: 'right', cursor: 'pointer' }} />
                </DialogTitle>
                <Paper
                    style={{
                        padding: '20px',
                        minWidth: '400px',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'column'
                    }}
                >
                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <img src={user?.picture} alt="user profile" style={{ borderRadius: '50px', width: '100px', height: '100px' }} />
                        <div style={{ marginLeft: '20px' }}>
                            <Typography variant="h5" style={{ fontSize: '20px' }}>
                                {user?.firstName}
                            </Typography>
                            <Typography variant="h5" style={{ fontSize: '20px' }}>
                                {user?.lastName}
                            </Typography>
                        </div>
                    </div>
                    <Typography variant="p" sx={{ marginTop: '20px', fontSize: '17px' }}>
                        {user?.description}
                    </Typography>
                    <Button
                        onClick={() => handleOpenInfo()}
                        variant="contained"
                        style={{ backgroundColor: '#cbab70', color: 'white', padding: '10px', marginTop: '30px' }}
                    >
                        See What Author is Doing
                    </Button>
                </Paper>
            </Dialog>
        </>
    );
};

export default UserDialog;
