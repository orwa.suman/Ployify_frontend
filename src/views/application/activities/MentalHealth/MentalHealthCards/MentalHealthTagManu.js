import { MealIcon, TagIconButton, tagsSetting } from '../../Recipes/RecipesCards';
import { loadCategories, loadMentalHealth } from 'services/MentalHealthService';
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import { Grid } from '@mui/material';
import MentalHealthCard from './MentalHealthCard';
import Slider from 'react-slick';
import { setting } from '../../TrainingPlan/TrainingPlanCards';

const MentalHealthTagManu = () => {
    const [allMentalHealth, setAllMentalHealth] = useState([]);
    const [mentalHealthbyTag, setmentalHealthbyTag] = useState([]);
    const [selectedTags, setSelectedTags] = useState('');
    const [categories, setCategories] = useState([]);
    const { tagname } = useParams();
    const nagivate = useNavigate();

    useEffect(() => {
        loadMentalHealth()
            .then((res) => res.data)
            .then((data) => {
                setAllMentalHealth(data);
            })
            .catch((e) => {
                
            });
    }, []);

    useEffect(() => {
        loadCategories()
            .then()
            .then((res) => res.data)
            .then((r) => {
                setCategories(r);
            })
            .catch((e) => {
                
            });
    }, []);

    function getmentalHealthbyTag(tag) {
        if (!allMentalHealth) {
            console.error('Recipes data not loaded yet');
            return [];
        }
        const recipesWithTag = allMentalHealth?.filter((mh) => mh?.category === tag);
        return recipesWithTag;
    }

    useEffect(() => {
        if (tagname && allMentalHealth) {
            setSelectedTags(tagname);
            setmentalHealthbyTag(getmentalHealthbyTag(tagname));
        } else {
            
        }
    }, [tagname, allMentalHealth]);

    const handleTagClick = (tagname) => {
        nagivate(`/activities/MentalHealth/cards/tag-menu/${tagname}`);
        setSelectedTags(tagname);
        setmentalHealthbyTag(getmentalHealthbyTag(tagname));
    };

    return (
        <div>
            <div style={{ margin: '20px' }}>
                <Slider {...tagsSetting} style={{ width: '100%', transform: 'none' }}>
                    {categories &&
                        categories?.map((tab, index) => (
                            <Grid item xs={2} key={index}>
                                <TagIconButton
                                    name={tab}
                                    IconComponent={MealIcon}
                                    onClick={() => handleTagClick(tab)}
                                    selected={selectedTags === tab} // check if the tag is selected
                                />
                            </Grid>
                        ))}
                </Slider>
            </div>
            <div>
                {mentalHealthbyTag.length > 0 && (
                    <div style={{ margin: '20px' }}>
                        <Slider {...setting} style={{ width: '100%', transform: 'none' }}>
                            {mentalHealthbyTag?.map((row) => {
                                row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;

                                return <MentalHealthCard row={{ ...row, isMostUsed: true }} />;
                            })}
                        </Slider>
                    </div>
                )}
            </div>
        </div>
    );
};

export default MentalHealthTagManu;
