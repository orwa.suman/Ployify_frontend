import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../../assets/scss/style.css';

import {
    Grid,
    IconButton,
    InputAdornment,
    LinearProgress,
    OutlinedInput,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
    Typography
} from '@mui/material';
import { deleteTraining, loadTrainings } from '../../../../services/TrainingService';

import BlockTwoToneIcon from '@mui/icons-material/BlockTwoTone';
import { FormattedMessage } from 'react-intl';
import { HiPencil } from 'react-icons/hi';
import { IconSearch } from '@tabler/icons';
import MainCard from 'ui-component/cards/MainCard';
import React from 'react';
import { gridSpacing } from 'store/constant';
import { useNavigate } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';

// assets

// Example of a data array that

// todo muskelgruppe liste ?
function TrainingList() {
    const theme = useTheme();
    const navigate = useNavigate();
    let [data, setData] = React.useState([]);
    let [loading, setLoading] = React.useState(true);

    const loadItems = () => {
        loadTrainings()
            .then((res) => {
                setData(res.data);
                setLoading(false);
            })
            .catch((e) => {
                
                setLoading(false);
            });
    };

    const deleteItem = (id) => {
        deleteTraining(id)
            .then(() => {
                loadItems();
            })
            .catch((e) => {
                
            });
    };

    React.useEffect(() => {
        loadItems();
    }, []);

    return (
        <MainCard
            title={
                <Grid container alignItems="center" justifyContent="space-between" spacing={gridSpacing}>
                    <Grid item>
                        <Typography variant="h3">List</Typography>
                    </Grid>
                    <Grid item>
                        <OutlinedInput
                            id="input-search-list-style1"
                            placeholder="Search"
                            startAdornment={
                                <InputAdornment position="start">
                                    <IconSearch stroke={1.5} size="16px" />
                                </InputAdornment>
                            }
                            size="small"
                        />
                    </Grid>
                </Grid>
            }
            content={false}
        >
            <TableContainer>
                {loading && (
                    <LinearProgress
                        variant="indeterminate"
                        sx={{
                            backgroundColor: '#cbab7075',
                            '& .MuiLinearProgress-bar': {
                                backgroundColor: '#cbab70'
                            }
                        }}
                    />
                )}
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell scope="col">
                                <FormattedMessage id="Picture" />
                            </TableCell>
                            <TableCell scope="col">
                                <FormattedMessage id="Name" />
                            </TableCell>

                            <TableCell scope="col">
                                <FormattedMessage id="Muscle group" />
                            </TableCell>
                            <TableCell scope="col">
                                <FormattedMessage id="Video link" />
                            </TableCell>

                            <TableCell scope="col">
                                <FormattedMessage id="Equipments" />
                            </TableCell>
                            <TableCell scope="col">
                                <FormattedMessage id="Difficulty" />
                            </TableCell>
                            <TableCell align="center" sx={{ pr: 3 }}>
                                <FormattedMessage id="Actions" />
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data &&
                            data.map((row, index) => (
                                <TableRow hover key={index}>
                                    <TableCell>
                                        {!!row.picture ? (
                                            <div className="careployee-training-plan__image">
                                                <img src={row.picture} alt={row.trainingName}></img>
                                            </div>
                                        ) : (
                                            ''
                                        )}
                                    </TableCell>
                                    <TableCell>
                                        <Grid container spacing={2} alignItems="center">
                                            {/*<Grid item>*/}
                                            {/*    <Avatar alt="User 1" src={avatarImage(`./${row.avatar}`).default} />*/}
                                            {/*</Grid>*/}
                                            <Grid item xs zeroMinWidth>
                                                <Typography align="left" component="div">
                                                    {row.trainingName}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </TableCell>

                                    <TableCell>
                                        {row.muscleGroups.map((line, index) => (
                                            <React.Fragment key={index}>
                                                {line.replace('_', ' ')}
                                                <br />
                                            </React.Fragment>
                                        ))}
                                    </TableCell>

                                    <TableCell>{row.linkToVideo}</TableCell>

                                    <TableCell>
                                        {row.equipments.map((line, index) => (
                                            <React.Fragment key={index}>
                                                {line.replace('_', ' ')}
                                                <br />
                                            </React.Fragment>
                                        ))}
                                    </TableCell>

                                    <TableCell>{row.difficulty} </TableCell>

                                    <TableCell align="center" sx={{ pr: 3 }}>
                                        <Stack direction="row" justifyContent="center" alignItems="center">
                                            <Tooltip placement="top" title="edit">
                                                <IconButton
                                                    color="primary"
                                                    aria-label="edit"
                                                    onClick={() => navigate('/dashboard/activities/training/form/' + row.id)}
                                                    size="large"
                                                >
                                                    <HiPencil sx={{ fontSize: '1.1rem' }} />
                                                </IconButton>
                                            </Tooltip>

                                            <Tooltip placement="top" title="Delete">
                                                <IconButton
                                                    color="primary"
                                                    sx={{
                                                        color: theme.palette.orange.dark,
                                                        borderColor: theme.palette.orange.main,
                                                        '&:hover ': { background: theme.palette.orange.light }
                                                    }}
                                                    size="large"
                                                    onClick={() => deleteItem(row.id)}
                                                >
                                                    <BlockTwoToneIcon sx={{ fontSize: '1.1rem' }} />
                                                </IconButton>
                                            </Tooltip>
                                        </Stack>
                                    </TableCell>
                                </TableRow>
                            ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </MainCard>
    );
}

export default TrainingList;
