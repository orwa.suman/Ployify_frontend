import React from 'react';
import { useFormik } from 'formik';
import MainCard from '../../../../ui-component/cards/MainCard';
import FileUpload from '../../../forms/components/FileUpload';
import { addTraining, loadEquipments, loadMuscleGroups, loadTrainings, updateTraining } from '../../../../services/TrainingService';
import * as Yup from 'yup';
import { useDispatch } from 'store';
import { Button, FormControlLabel, Grid, Radio, RadioGroup, TextField, Autocomplete } from '@mui/material';
import { showMessage } from '../../../../utils/form/Communication';
import { FormattedMessage } from 'react-intl';
import { useParams } from 'react-router-dom';
import { uploadToCloudinary } from 'services/CloudinaryService';

function TrainingForm() {
    const [difficulty, setDifficulty] = React.useState('STANDARD');
    const [preparation, setPreparation] = React.useState('false');
    const dispatch = useDispatch();
    const validationSchema = Yup.object({
        trainingName: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(100, <FormattedMessage id="Input Max Length" values={{ length: 100 }} />)
            .required(<FormattedMessage id="Required" />),
        description: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(1000, <FormattedMessage id="Input Max Length" values={{ length: 1000 }} />),
        linkToVideo: Yup.string()
            .min(2, <FormattedMessage id="Input Min Length" values={{ length: 2 }} />)
            .max(1000, <FormattedMessage id="Input Max Length" values={{ length: 1000 }} />)
            .required(<FormattedMessage id="Required" />),
        difficulty: Yup.string()
            .required(<FormattedMessage id="Required" />)
    });
    const labelStyle = {
        display: 'block',
        fontSize: '14px',
        fontWeight: 'bold',
        margin: '0 0 16px'
    };
    const formBlockStyle = { margin: '4px 4px 24px 4px' };
    const submitBtnStyle = {
        width: '240px',
        background: '#cbab70'
    };

    const [muscleGroups, setMuscleGroups] = React.useState([]);
    const [equipments, setEquipments] = React.useState([]);
    const [formErrors, setFormErrors] = React.useState({});
    const { id } = useParams();

    const [picture, setPicture] = React.useState([]);
    const [pictureName, setPictureName] = React.useState([]);
    const [changed, setChanged] = React.useState(false);

    React.useEffect(() => {
        requestMuscleGroups();
        requestEquipments();

        if (id) {
            loadTrainings()
                .then((res) => res.data)
                .then((data) => {
                    const row = data.find((item) => item.id === id);
                    formik.setValues({
                        trainingName: row.trainingName,
                        description: row.description,
                        difficulty: row.difficulty,
                        equipments: row.equipments,
                        muscleGroups: row.muscleGroups,
                        preparation: row.preparation,
                        linkToVideo: row.linkToVideo,
                        picture: row.picture
                    });

                    setDifficulty(row.difficulty);
                    setPreparation(row.preparation);
                })
                .catch((e) => {
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
    }, []);

    const requestMuscleGroups = () => {
        const muscleGroups = loadMuscleGroups();
        muscleGroups
            .then((res) => res.data)
            .then((data) => {
                var newStuff = data.map(function (item, index) {
                    return item.replace('_', ' ');
                });

                setMuscleGroups(newStuff);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const requestEquipments = () => {
        const equipments = loadEquipments();
        equipments
            .then((res) => res.data)
            .then((data) => {
                var newStuff = data.map(function (item, index) {
                    return item.replace('_', ' ');
                });

                setEquipments(newStuff);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const formik = useFormik({
        initialValues: {
            trainingName: '',
            description: '',
            difficulty: 'STANDARD',
            equipments: [],
            muscleGroups: [],
            preparation: 'false',
            linkToVideo: '',
            picture: undefined
        },
        validationSchema,
        onSubmit: () => {

            formik.values.equipments = formik.values.equipments.map(function (item, index) {
                return item.replace(' ', '_');
            });

            formik.values.muscleGroups = formik.values.muscleGroups.map(function (item, index) {
                return item.replace(' ', '_');
            });

            uploadToCloudinary(picture, pictureName, "training_", '/Ployify/Activities/Training')
                .then((data) => {

                    if (changed) {
                        
                        
                        formik.values.picture = data.url;
                    }

                    if (id) {
                        updateTraining(id, formik.values)
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    } else {
                        addTraining(formik.values)
                            .then(() => dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success')))
                            .catch((e) => {
                                dispatch(showMessage(e?.response?.data?.message));
                            });
                    }

                }).catch((err) => {
                    
                });
        }
    });

    const submitForm = (event) => {
        formik.validateForm().then((errors) => {
            if (errors && Object.keys(errors).length > 0) {

                const errorMessages = Object.entries(errors).map(([fieldName, fieldError]) => {
                    return `${fieldName}: ${fieldError.props.id}`;
                });

                dispatch(showMessage(<FormattedMessage id={errorMessages.join(', ')} />));
            }
            formik.handleSubmit(event);
        });
    };

    const handleMuscleGroups = (value) => {
        const errors = { ...formErrors };
        if (!value) {
            errors.muscleGroups = <FormattedMessage id="Required" />;
        } else {
            delete errors.muscleGroups;
        }
        setFormErrors(errors);

        formik.setValues({ ...formik.values, muscleGroups: value });
    };

    const handleEquipments = (value) => {
        const errors = { ...formErrors };
        if (!value) {
            errors.equipments = <FormattedMessage id="Required" />;
        } else {
            delete errors.equipments;
        }
        setFormErrors(errors);

        formik.setValues({ ...formik.values, equipments: value });
    };

    const handleChangeDifficulty = (event) => {
        formik.setValues({ ...formik.values, difficulty: event.target.value });
        setDifficulty(event.target.value);
    };

    const handleChangePreparation = (event) => {
        formik.setValues({ ...formik.values, preparation: event.target.value });
        setPreparation(event.target.value);
    };

    const handlePicture = (data, fileName) => {
        setPicture(data);
        setPictureName(fileName);
        setChanged(true);
    };

    return (
        <Grid>
            <form onSubmit={submitForm}>
                <MainCard title={<FormattedMessage id="First Page" />}>
                    <Grid container>
                        <Grid item xs={12} md={5} className="careployee-form__block">
                            <Grid item xs={12} className="careployee-form__block">
                                <label htmlFor="trainingName" style={labelStyle}>
                                    <FormattedMessage id="Name" />
                                </label>
                                <TextField
                                    fullWidth
                                    id="trainingName"
                                    value={formik.values.trainingName}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    error={formik.touched.trainingName && Boolean(formik.errors.trainingName)}
                                    helperText={formik.touched.trainingName && formik.errors.trainingName}
                                />
                            </Grid>
                            <Grid item xs={12} className="careployee-form__block">
                                <label htmlFor="description" style={labelStyle}>
                                    <FormattedMessage id="Description" />
                                </label>
                                <TextField
                                    fullWidth
                                    id="description"
                                    label="Description"
                                    multiline
                                    rows={8}
                                    value={formik.values.description}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    error={formik.touched.description && Boolean(formik.errors.description)}
                                />
                            </Grid>
                        </Grid>
                        <Grid item xs={12} md={4} className="careployee-form__block">
                            <FileUpload defaultPicture={formik.values.picture} onChange={handlePicture}></FileUpload>
                        </Grid>
                    </Grid>

                    <Grid container spacing={2} justifyContent="space-between" className="careployee-form__block">
                        <Grid item xs={10} md={5}>
                            <label htmlFor="difficulty" style={labelStyle}>
                                <FormattedMessage id="Difficulty" />
                            </label>
                            <RadioGroup value={difficulty} onChange={handleChangeDifficulty} row stlye={{ margin: '0 0 20px' }}>
                                <FormControlLabel label="Beginner" value="BEGINNER" control={<Radio />} />
                                <FormControlLabel label="Standard" value="STANDARD" control={<Radio />} />
                                <FormControlLabel label="Advanced" value="ADVANCED" control={<Radio />} />
                                <FormControlLabel label="Pro" value="PRO" control={<Radio />} />
                            </RadioGroup>
                        </Grid>

                        <Grid item xs={10} md={4}>
                            <label htmlFor="perpartion" style={labelStyle}>
                                <FormattedMessage id="Preparation" />
                            </label>
                            <RadioGroup row value={preparation} onChange={handleChangePreparation}>
                                <FormControlLabel label="Yes" value="true" control={<Radio />} />
                                <FormControlLabel label="No" value="false" control={<Radio />} />
                            </RadioGroup>
                        </Grid>
                    </Grid>

                    <Grid container spacing={2} className="careployee-form__block">
                        <Grid item xs={12} md={5}>
                            <label htmlFor="equipments" style={labelStyle}>
                                <FormattedMessage id="Equipments" />
                            </label>
                            <Autocomplete
                                style={{ height: '44px' }}
                                disablePortal
                                multiple
                                options={equipments}
                                getOptionLabel={(option) => option}
                                value={formik.values.equipments || []}
                                onChange={(event, value) => {
                                    handleEquipments(value);
                                }}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        error={!!formErrors.equipments}
                                        helperText={!!formErrors.equipments && formErrors.equipments}
                                    />
                                )}
                            />
                        </Grid>

                        <Grid item xs={12} md={5}>
                            <label htmlFor="muscleGroups" style={labelStyle}>
                                <FormattedMessage id="Muscle group" />
                            </label>

                            <Autocomplete
                                style={{ height: '44px' }}
                                disablePortal
                                multiple
                                options={muscleGroups}
                                getOptionLabel={(option) => option}
                                value={formik.values.muscleGroups || []}
                                onChange={(event, value) => {
                                    handleMuscleGroups(value);
                                }}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        error={!!formErrors.muscleGroup}
                                        helperText={!!formErrors.muscleGroup && formErrors.muscleGroup}
                                    />
                                )}
                            />
                        </Grid>
                    </Grid>

                    <Grid spacing={2} container sx={formBlockStyle}>
                        <Grid item xs={10} sx={formBlockStyle}>
                            <label htmlFor="linkToVideo" style={labelStyle}>
                                <FormattedMessage id="Video link" />
                            </label>
                            <TextField
                                id="linkToVideo"
                                type={'url'}
                                style={{ width: '100%' }}
                                value={formik.values.linkToVideo}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={formik.touched.linkToVideo && Boolean(formik.errors.linkToVideo)}
                                helperText={formik.touched.linkToVideo && formik.errors.linkToVideo}
                            />
                        </Grid>

                        <Grid item xs={12}>
                            <Button style={submitBtnStyle} variant="contained" size="large" onClick={submitForm}>
                                <FormattedMessage id="Submit" />
                            </Button>
                        </Grid>
                    </Grid>
                </MainCard>
            </form>
        </Grid>
    );
}

export default TrainingForm;
