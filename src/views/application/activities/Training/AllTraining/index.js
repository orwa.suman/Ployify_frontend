import { Typography } from '@material-ui/core';
import { Divider, Grid } from '@mui/material';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { getAllTrainingPLanByLocation } from 'services/TrainingPlanService';
import { showMessage } from 'utils/form/Communication';
import RecipeCard from '../../Recipes/RecipesCards/RecipeCard';
import TrainingPlanCard from '../../TrainingPlan/TrainingPlanCards/TrainingPlanCard';
import { getUserFavorites } from 'services/userService';
import { loggedInUser } from 'contexts/JWTContext';

const AllTraining = () => {
    const dispatch = useDispatch();
    const [data, setData] = useState([]);
    let [favorites, setFavorites] = useState([]);

    useEffect(() => {
        getAllTrainingPLanByLocation('OFFICE')
            .then((res) => res.data)
            .then((data) => {
                setData(data);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    const loadFavorites = (onFavoriteTab = false) =>
        getUserFavorites(loggedInUser.id, 1)
            .then((res) => res.data)
            .then((data) => {
                let userFavorites = [];
                data.forEach((favorit) => {
                    if (
                        !userFavorites.find(
                            (intermediateFavorit) => intermediateFavorit.careployeeBaseEntity?.id === favorit.careployeeBaseEntity?.id
                        )
                    ) {
                        userFavorites = [...userFavorites, favorit];
                    }
                });
                setFavorites(userFavorites);
            })
            .catch((e) => {
                //  dispatch(showMessage(e.message));
            });

    useEffect(() => {
        loadFavorites();
    }, []);
    const isFavorit = (id) => (!!favorites && favorites.length ? favorites : []).some((favorit) => favorit.careployeeBaseEntity.id === id);

    
    return (
        <div>
            <Typography variant="h5">All Training</Typography>
            <Divider sx={{ margin: '32px 0' }} />
            <Grid container spacing={2}>
                {(data && data.length ? data : []).map((row, index) => {
                    row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                    return (
                        <Grid item xs={12} sm={6} md={4} lg={3} key={row.id}>
                            <TrainingPlanCard key={row.id} row={{ ...row, isFavorit: isFavorit(row.id) }} />
                        </Grid>
                    );
                })}
            </Grid>
        </div>
    );
};

export default AllTraining;
