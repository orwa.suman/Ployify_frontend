import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import { ArrowBackIos, ArrowForwardIos, Quiz } from '@mui/icons-material';
import { CardContent, Divider, Grid, LinearProgress, Link, Stack, Typography, darken } from '@mui/material';
import { IconButton, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { getActivitiesOfUser, getUserById, getUserFavorites, getUserHistory } from 'services/userService';
import { useTheme, withStyles } from '@mui/styles';

import EmojiEventsIcon from '@mui/icons-material/EmojiEvents';
import { FaCouch } from 'react-icons/fa';
import { FiTarget } from 'react-icons/fi';
import { FormattedMessage } from 'react-intl';
import { GiOfficeChair } from 'react-icons/gi';
import GoalCard from 'stories/Card/Card';
import { IconUserCheck } from '@tabler/icons';
import { IoNutrition } from 'react-icons/io5';
import MainCard from 'ui-component/cards/MainCard';
import MentalHealthCard from 'views/application/activities/MentalHealth/MentalHealthCards/MentalHealthCard';
import RecipeCard from 'views/application/activities/Recipes/RecipesCards/RecipeCard';
import { RiMentalHealthFill } from 'react-icons/ri';
import SeminarCard from 'stories/SeminarCard/SeminarCard';
import SeminareCard from 'views/application/activities/Seminare/SeminareCards/SeminareCard';
import Slider from 'react-slick';
import TrainingPlanCard from 'views/application/activities/TrainingPlan/TrainingPlanCards/TrainingPlanCard';
import UserCountCard from 'ui-component/cards/UserCountCard';
import { getMostUsedTrainingPlans } from 'services/AnalyzeToolService';
import { gridSpacing } from 'store/constant';
import { loadSeminare } from 'services/SeminareService';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';
import useAuth from 'hooks/useAuth';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';

// material-ui

// assets

// ==============================|| DEFAULT DASHBOARD ||============================== //

// style constant
const BorderLinearProgress = withStyles(() => ({
    root: {
        height: 20,
        borderRadius: 5
    },
    bar: {
        borderRadius: 5
    }
}))(LinearProgress);

const menuItems = [
    {
        label: 'Training @home',
        link: '/activities/TrainingPlan/cards/HOME',
        icon: FaCouch
    },
    {
        label: 'Training @office',
        link: '/activities/TrainingPlan/cards/OFFICE',
        icon: GiOfficeChair
    },
    {
        label: 'Recipes',
        link: '/activities/Reciepts/cards',
        icon: IoNutrition
    },
    {
        label: 'Mental Health',
        link: '/activities/MentalHealth/cards',
        icon: RiMentalHealthFill
    },
    {
        label: 'Seminare',
        link: '/activities/seminare/cards',
        icon: IconUserCheck
    },
    {
        label: 'Update Your Quiz',
        link: '/user/questionnaire',
        icon: Quiz
    }
];

const useStyles = makeStyles((theme) => ({}));

const Dashboard = () => {
    const classes = useStyles();
    const [trainingPlans, setTrainingPlans] = useState([]);
    const [recipes, setRecipes] = useState([]);
    const [seminare, setSeminare] = useState([]);
    const [mentalhealth, setMentalhealth] = useState([]);
    const [userInfo, setUserInfo] = useState([]);
    const theme = useTheme();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    let [favorites, setFavorites] = React.useState([]);
    let [favoriteRecipes, setFavoriteRecipes] = React.useState([]);
    let [favoriteMentalHealth, setFavoriteMentalHealth] = React.useState([]);
    let [trackingData, setTrackingData] = React.useState([]);
    // const { getUserInfoByEmail } = useAuth();

    const activityNums = {
        trainingPlans: 1,
        recipes: 2,
        seminare: 3,
        mentalHealth: 4
    };

    localStorage.setItem('loggedinUserRole', loggedInUser.role);

    useEffect(() => {
        loadUserFavorites('trainingPlans');
        loadUserFavorites('recipes');
        loadUserFavorites('mentalHealth');
        loadLatestSeminare();

        getUserById(loggedInUser.id)
            .then((res) => res.data)
            .then((user) => {
                setUserInfo(user.userInfo);
            })
            .catch((e) => {
                // 
            });
    }, []);

    const getProgressForGoal = () => (userInfo.points / userInfo.rewardGoal?.rewardPoints) * 100;
    const getRemainingPointsToGoal = () => {
        return (userInfo.rewardGoal?.rewardPoints - userInfo.points || 0) > 0 ? userInfo.rewardGoal?.rewardPoints - userInfo.points : 0;
    };

    const isFavorit = (id, favorites) =>
        !!(!!favorites && favorites.length ? favorites : []).find((favorit) => favorit.careployeeBaseEntity.id === id);

    const isMostUsed = (id, type) => trackingData[type]?.includes(id);

    const loadUserFavorites = (type) => {
        getUserFavorites(loggedInUser.id, activityNums[type])
            .then((res) => res.data)
            .then((data) => {
                let userFavorites = [];
                data.forEach((favorit) => {
                    if (
                        !userFavorites.find(
                            (intermediateFavorit) => intermediateFavorit.careployeeBaseEntity?.id === favorit.careployeeBaseEntity?.id
                        )
                    ) {
                        userFavorites = [...userFavorites, favorit];
                    }
                });

                setFavorites({ ...favorites, [type]: userFavorites });
                if (type === 'trainingPlans') {
                    loadLatestUsedTrainingPlans();
                }

                if (type === 'recipes') {
                    setFavoriteRecipes([...userFavorites]);
                    loadLatestUsedRecipes();
                }

                if (type === 'mentalHealth') {
                    setFavoriteMentalHealth([...userFavorites]);
                    loadLatestUsedMentalHealth();
                }
            })
            .catch((e) => {
                //  dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const loadLatestUsedTrainingPlans = () => {
        getUserHistory(loggedInUser.id, 1)
            .then((res) => res.data)
            .then((data) => {
                setTrainingPlans(data);
            })
            .catch((e) => {
                // dispatch(showMessage(e.message));
            });
    };

    const loadLatestUsedRecipes = () => {
        getUserHistory(loggedInUser.id, 2)
            .then((res) => res.data)
            .then((data) => {
                setRecipes(data);  
            })
            .catch((e) => {
                // dispatch(showMessage(e.message));
            });
    };

    const loadLatestSeminare = () => {
        loadSeminare()
            .then((res) => res.data)
            .then((data) => {
                setSeminare(data);
            })
            .catch((e) => {
                //  dispatch(showMessage(e.message));
            });
    };

    const loadLatestUsedMentalHealth = () => {
        getUserHistory(loggedInUser.id, 4)
            .then((res) => res.data)
            .then((data) => {
                setMentalhealth(data);
            })
            .catch((e) => {
                // dispatch(showMessage(e.message));
            });
    };

    const customPrevArrow = <ArrowBackIos />;
    const customNextArrow = <ArrowForwardIos />;

    const setting = {
        dots: false,
        arrow: true,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 2,
        prevArrow: customPrevArrow,
        nextArrow: customNextArrow,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    const [showMore, setShowMore] = useState(false);
    const handleMouseEnter = () => {
        setShowMore(true);
    };

    const handleMouseLeave = () => {
        setShowMore(false);
    };

    return (
        <Grid container spacing={gridSpacing}>
            <div style={{ margin: '20px', backgroundColor: '#CBAB70', padding: '20px', borderRadius: '8px' }}>
                <Typography variant="h5" sx={{ margin: '50px', fontFamily: 'fantasy', color: 'white' }}>
                    Good to see you 👋
                    <span style={{ fontStyle: 'italic', textTransform: 'capitalize' }}> {loggedInUser.firstName}</span>
                </Typography>
            </div>
            {/* <button onClick={() => getUserInfoByEmail()}>getUserInfoByEmail() </button> */}
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                    <Grid item md={8} xs={12}>
                        <GoalCard
                            mainTitle="My Goal"
                            width={400}
                            imageSrc={
                                userInfo.rewardGoal?.customReward?.picture
                                    ? userInfo.rewardGoal?.customReward.picture
                                    : '/images/rewards.png'
                            }
                            title={userInfo.rewardGoal?.customReward.title || 'Choose your first reward goal'}
                            description={getRemainingPointsToGoal()}
                            progress={getProgressForGoal()}
                        ></GoalCard>
                        <Grid container spacing={gridSpacing} style={{ marginTop: '30px' }}>
                            {menuItems.map((item) => (
                                <Grid item xs={12} lg={4} sm={6} key={item.link}>
                                    <Link
                                        onClick={() => navigate(item.link)}
                                        style={{
                                            cursor: 'pointer',
                                            textDecoration: 'none',
                                            '&:hover': { background: darken(theme.palette.primary.dark) }
                                        }}
                                    >
                                        <UserCountCard secondary={item.label} iconPrimary={item.icon} color={item.color || '#cbab70'} />
                                    </Link>
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>

                    <Grid item md={4} xs={12}>
                        <Typography variant="h4" sx={{ margin: '0 0 24px' }}>
                            <FormattedMessage id="Last Seminar" /> <br />
                        </Typography>

                        <SeminarCard
                            image={seminare[seminare.length - 1]?.picture || '/images/seminare.jpg'}
                            description={
                                seminare[seminare.length - 1]?.description
                                    ? seminare[seminare.length - 1]?.description?.length > 120
                                        ? seminare[seminare.length - 1]?.description.substring(0, 120) + '...'
                                        : seminare[seminare.length - 1]?.description
                                    : ''
                            }
                            row={seminare[seminare.length - 1]}
                            checkFavorit={{ isFavorit: isFavorit(seminare[seminare.length - 1]?.id) }}
                            title={seminare[seminare?.length - 1]?.title || 'No Current Seminar'}
                            autherImage={seminare[seminare.length - 1]?.authorDto?.picture}
                            authorName={
                                seminare[seminare.length - 1]?.authorDto?.firstName +
                                ' ' +
                                seminare[seminare.length - 1]?.authorDto?.lastName
                            }
                            onClick={() => navigate('/activities/seminare/quiz/' + seminare[seminare.length - 1]?.id)}
                        ></SeminarCard>
                    </Grid>

                    <Grid xs={12} item sx={{ padding: '0 32px' }}>
                        <Typography variant="h4" sx={{ margin: '32px auto' }}>
                            <FormattedMessage id="Latest used" /> <br />
                        </Typography>

                        {trainingPlans && trainingPlans.length > 0 ? (
                            <div style={{ marginTop: '20px' }}>
                                <div
                                    onMouseEnter={handleMouseEnter}
                                    onMouseLeave={handleMouseLeave}
                                    style={{ display: 'flex', alignItems: 'center' }}
                                    onClick={() => navigate('/dashboard/activities/training/all')}
                                >
                                    <Typography variant="h6" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                                        Training Plan
                                    </Typography>
                                    {showMore && (
                                        <div>
                                            <Typography
                                                component={Link}
                                                to="/dashboard/activities/training/list"
                                                style={{
                                                    marginLeft: '10px',
                                                    cursor: 'pointer',
                                                    textDecoration: 'none',
                                                    color: 'lightGray',
                                                    marginTop: '4px'
                                                }}
                                            >
                                                Show More
                                            </Typography>
                                        </div>
                                    )}
                                </div>
                                <Slider {...setting}>
                                    {(trainingPlans && trainingPlans.length ? trainingPlans : []).slice(0, 10).map((row, index) => {
                                        row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                                        return (
                                            <TrainingPlanCard
                                                key={row.id}
                                                onSetAsFavorite={() => {}}
                                                row={{
                                                    ...row,
                                                    isFavorit: isFavorit(row.id, favorites.trainingPlans),
                                                    isMostUsed: isMostUsed(row.id, 'trainingPlans')
                                                }}
                                            />
                                        );
                                    })}
                                </Slider>
                            </div>
                        ) : (
                            <div style={{ marginTop: '10px' }}>
                                <Typography variant="h5" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                                    Trainings
                                </Typography>
                                <div style={{ display: 'flex', justifyContent: 'center' }}>
                                    <img width={400} src="/images/homeTraining.png" alt="first favourate" style={{ textAlign: 'center' }} />
                                </div>
                            </div>
                        )}

                        <Divider sx={{ margin: '32px 0' }} />
                        {recipes && recipes.length > 0 && (
                            <div
                                onMouseEnter={handleMouseEnter}
                                onMouseLeave={handleMouseLeave}
                                style={{ display: 'flex', alignItems: 'center' }}
                                onClick={() => navigate('/dashboard/activities/recipes/all')}
                            >
                                <Typography variant="h6" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                                    Recipes
                                </Typography>
                                {showMore && (
                                    <div>
                                        <Typography
                                            component={Link}
                                            to="/dashboard/activities/recipes/all"
                                            style={{
                                                marginLeft: '10px',
                                                cursor: 'pointer',
                                                textDecoration: 'none',
                                                color: 'lightGray',
                                                marginTop: '4px'
                                            }}
                                        >
                                            Show More
                                        </Typography>
                                    </div>
                                )}
                            </div>
                        )}
                        {recipes && recipes.length > 0 ? (
                            <div style={{ marginLeft: '20px' }}>
                                <Slider {...setting}>
                                    {(recipes && recipes.length ? recipes : []).slice(0, 10).map((row, index) => {
                                        row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                                        return (
                                            <RecipeCard
                                                key={row.id}
                                                onSetAsFavorite={() => {}}
                                                row={{
                                                    ...row,
                                                    isFavorit: isFavorit(row.id, favoriteRecipes),
                                                    isMostUsed: isMostUsed(row.id, 'recipes')
                                                }}
                                            />
                                        );
                                    })}
                                </Slider>
                            </div>
                        ) : (
                            <div style={{ width: '100%', marginTop: '10px' }}>
                                <Typography variant="h5" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                                    Meal
                                </Typography>
                                <div style={{ display: 'flex', justifyContent: 'center' }}>
                                    <img width={400} src="/images/firstMeal.png" alt="first meal" style={{ textAlign: 'center' }} />
                                </div>
                            </div>
                        )}

                        <Divider sx={{ margin: '32px 0' }} />
                        {mentalhealth && mentalhealth.length > 0 && (
                            <div
                                onMouseEnter={handleMouseEnter}
                                onMouseLeave={handleMouseLeave}
                                style={{ display: 'flex', alignItems: 'center' }}
                                onClick={() => navigate('/dashboard/activities/mental-health/all')}
                            >
                                <Typography variant="h6" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                                    Mental Health
                                </Typography>
                                {showMore && (
                                    <div>
                                        <Typography
                                            component={Link}
                                            to="/dashboard/activities/mental-health/all"
                                            style={{
                                                marginLeft: '10px',
                                                cursor: 'pointer',
                                                textDecoration: 'none',
                                                color: 'lightGray',
                                                marginTop: '4px'
                                            }}
                                        >
                                            Show More
                                        </Typography>
                                    </div>
                                )}
                            </div>
                        )}
                        {mentalhealth && mentalhealth.length > 0 ? (
                            <div>
                                <Slider {...setting}>
                                    {(mentalhealth && mentalhealth.length ? mentalhealth : []).slice(0, 10).map((row, index) => {
                                        row = row.careployeeBaseEntity ? row.careployeeBaseEntity : row;
                                        return (
                                            <MentalHealthCard
                                                key={row.id}
                                                onSetAsFavorite={() => {}}
                                                row={{
                                                    ...row,
                                                    isFavorit: isFavorit(row.id, favoriteMentalHealth),
                                                    isMostUsed: isMostUsed(row.id, 'mentalHealth')
                                                }}
                                            />
                                        );
                                    })}
                                </Slider>
                            </div>
                        ) : (
                            <div style={{ width: '100%' }}>
                                <Typography variant="h5" sx={{ marginTop: '14px', marginBottom: '10px' }}>
                                    Mental Health
                                </Typography>
                                <div style={{ display: 'flex', justifyContent: 'center' }}>
                                    <img width={400} src="/images/mentalHealth.png" alt="first favourite" style={{ textAlign: 'center' }} />
                                </div>
                            </div>
                        )}
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Dashboard;
