import {
    Box,
    Chip,
    Collapse,
    Grid,
    IconButton,
    LinearProgress,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Tooltip,
    Typography
} from '@mui/material';

import { FormattedMessage } from 'react-intl';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import React from 'react';
import SubCard from 'ui-component/cards/SubCard';
import { getAllRegistrationCodesOfCompany } from '../../../services/CompanyService';
import { getUsersListStyle1 } from 'store/slices/user';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';
import { useDispatch } from 'store';
import { useTheme } from '@mui/material/styles';

// material-ui

// project imports

// assets

// assets

// ==============================|| USER LIST 1 ||============================== //

function CollapseRow({ row }) {
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);

    return (
        <>
            <TableRow hover sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell align="right">
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell>
                    <Typography align="left" component="div">
                        {row.companyName}
                    </Typography>
                </TableCell>
                <TableCell>{row.numAllRegistrationCodes || 0}</TableCell>
                <TableCell>{row.numUsedRegistrationCodes || 0}</TableCell>
                <TableCell>{row.numActiveRegistrationCodes || 0}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        {open && (
                            <Box sx={{ margin: 1 }}>
                                <TableContainer>
                                    <SubCard
                                        sx={{ bgcolor: theme.palette.mode === 'dark' ? 'dark.800' : 'grey.50', mb: 2 }}
                                        content={false}
                                    ></SubCard>
                                </TableContainer>
                            </Box>
                        )}
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
}

const UserList = () => {
    const theme = useTheme();
    const dispatch = useDispatch();

    let [data, setData] = React.useState([]);
    let [company, setCompany] = React.useState(loggedInUser.company);

    let [registrationCodeList, setRegistrationCodeList] = React.useState([]);

    React.useEffect(() => {
        // getCompanyById(loggedInUser.company.id)
        getAllRegistrationCodesOfCompany(loggedInUser.company.id)
            .then((res) => res.data)
            .then((regCodelist) => {
                setRegistrationCodeList(regCodelist);
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }, []);

    return (
        <TableContainer>
            {!registrationCodeList.length ? (
                <LinearProgress
                    variant="indeterminate"
                    sx={{
                        backgroundColor: '#cbab7075',
                        '& .MuiLinearProgress-bar': {
                            backgroundColor: '#cbab70'
                        }
                    }}
                />
            ) : null}
            <Table size="small" aria-label="purchases">
                <TableHead>
                    <TableRow>
                        <TableCell></TableCell>
                        <TableCell>
                            <FormattedMessage id="Code" />
                        </TableCell>
                        <TableCell>
                            <FormattedMessage id="Used" />
                        </TableCell>
                        <TableCell>
                            <FormattedMessage id="User is active" />
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {registrationCodeList.length
                        ? registrationCodeList?.map((code) => (
                              <TableRow hover key={code.registrationCode.registrationCode}>
                                  <TableCell component="th" scope="row"></TableCell>
                                  <TableCell component="th" scope="row">
                                      {code.registrationCode.registrationCode}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                      {code.registrationCode.used ? (
                                          <Chip
                                              label={<FormattedMessage id="Used" />}
                                              size="small"
                                              sx={{
                                                  background:
                                                      theme.palette.mode === 'dark'
                                                          ? theme.palette.dark.main
                                                          : theme.palette.success.light + 60,
                                                  color: theme.palette.success.dark
                                              }}
                                          />
                                      ) : (
                                          <Chip
                                              label={<FormattedMessage id="Not Used" />}
                                              size="small"
                                              sx={{
                                                  background:
                                                      theme.palette.mode === 'dark'
                                                          ? theme.palette.dark.main
                                                          : theme.palette.orange.light + 80,
                                                  color: theme.palette.orange.dark
                                              }}
                                          />
                                      )}
                                  </TableCell>
                                  <TableCell component="th" scope="row">
                                      {code.registrationCode.assignedUser?.active ? (
                                          <Chip
                                              label={<FormattedMessage id="Active" />}
                                              size="small"
                                              sx={{
                                                  background:
                                                      theme.palette.mode === 'dark'
                                                          ? theme.palette.dark.main
                                                          : theme.palette.success.light + 60,
                                                  color: theme.palette.success.dark
                                              }}
                                          />
                                      ) : (
                                          <Chip
                                              label={<FormattedMessage id="Not Active" />}
                                              size="small"
                                              sx={{
                                                  background:
                                                      theme.palette.mode === 'dark'
                                                          ? theme.palette.dark.main
                                                          : theme.palette.orange.light + 80,
                                                  color: theme.palette.orange.dark
                                              }}
                                          />
                                      )}
                                  </TableCell>
                              </TableRow>
                          ))
                        : null}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default UserList;
