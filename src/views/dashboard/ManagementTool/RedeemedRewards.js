import * as React from 'react';

import {
    Box,
    Button,
    CardContent,
    Checkbox,
    Fab,
    Grid,
    IconButton,
    InputAdornment,
    LinearProgress,
    Menu,
    MenuItem,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel,
    TextField,
    Toolbar,
    Tooltip,
    Typography
} from '@mui/material';
import { changeRewardStatusAPI, markRewardAsFinisihedAPI } from 'services/GamificationService';
import { format, setDate } from 'date-fns';

import AddIcon from '@mui/icons-material/AddTwoTone';
import Avatar from 'ui-component/extended/Avatar';
import Chip from 'ui-component/extended/Chip';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import MainCard from 'ui-component/cards/MainCard';
import MoreHorizOutlinedIcon from '@mui/icons-material/MoreHorizOutlined';
import PrintIcon from '@mui/icons-material/PrintTwoTone';
import PropTypes from 'prop-types';
import SearchIcon from '@mui/icons-material/Search';
import { getAllRedeemRewardOfCompany } from 'services/CompanyService';
import { getProducts } from 'store/slices/product';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';
import { useDispatch } from 'store';
import { useTheme } from '@mui/material/styles';

// material-ui

// third-party

// project imports

// assets

const prodImage = require.context('assets/images/e-commerce', true);

// table sort
function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

const getComparator = (order, orderBy) =>
    order === 'desc' ? (a, b) => descendingComparator(a, b, orderBy) : (a, b) => -descendingComparator(a, b, orderBy);

function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

// table header options
const headCells = [
    // {
    //     id: 'id',
    //     numeric: true,
    //     label: ' ',
    //     align: 'center'
    // },
    {
        id: 'name',
        numeric: false,
        label: <FormattedMessage id="Name" />,
        align: 'left'
    },
    {
        id: 'code',
        numeric: false,
        label: <FormattedMessage id="Code" />,
        align: 'left'
    },
    {
        id: 'date',
        numeric: true,
        label: <FormattedMessage id="Redeem Date" />,
        align: 'left'
    },
    {
        id: 'note',
        numeric: true,
        label: <FormattedMessage id="Note" />,
        align: 'left'
    },
    {
        id: 'status',
        numeric: true,
        label: <FormattedMessage id="Status" />,
        align: 'left'
    }
];

// ==============================|| TABLE HEADER ||============================== //

function EnhancedTableHead({ onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort, theme, selected }) {
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.align}
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <Typography component="span" sx={{ display: 'none' }}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </Typography>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    theme: PropTypes.object,
    selected: PropTypes.array,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired
};

// ==============================|| TABLE HEADER TOOLBAR ||============================== //

const EnhancedTableToolbar = ({ numSelected }) => (
    <Toolbar
        sx={{
            p: 0,
            pl: 2,
            pr: 1,
            color: numSelected > 0 ? 'secondary.main' : 'inherit'
        }}
    >
        {numSelected > 0 ? (
            <Typography sx={{ flex: '1 1 100%' }} color="inherit" variant="h4" component="div">
                {numSelected} Selected
            </Typography>
        ) : (
            <></>
        )}
    </Toolbar>
);

EnhancedTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired
};

// ==============================|| PRODUCT LIST ||============================== //

const ProductList = () => {
    const theme = useTheme();
    const dispatch = useDispatch();
    const [selectProgress, setSelectProgress] = React.useState(true);
    const [selectDone, setSelectDone] = React.useState(false);
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('calories');
    const [selected, setSelected] = React.useState([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [search, setSearch] = React.useState('');
    const [rows, setRows] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    // const [rows, setRows] = React.useState([
    //     { id: 'xy', name: 'test reward', code: 'XYZ', date: '11.01.23 19:20', status: 'New' },
    //     { id: 'ab', name: 'Super test reward 2', code: 'ABC', date: '11.01.23 19:30', status: 'Progress' },
    //     { id: 'cd', name: 'test reward', code: 'XYZ', date: '11.01.23 19:20', status: 'Purchased' },
    //     { id: 'ef', name: 'Super test reward 2', code: 'ABC', date: '11.01.23 19:30', status: 'Done' }
    // ]);

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleMenuClick = (event) => {
        setAnchorEl(event?.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const loadReedemedRewards = () => {
        getAllRedeemRewardOfCompany(loggedInUser.company.id)
            .then((rewards) => {
                
                setRows(rewards.data);
                setLoading(false);
            })
            .catch((e) => {
                
                setLoading(false);
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    React.useEffect(() => {
        loadReedemedRewards();
    }, []);

    const handleSearch = (event) => {
        const newString = event?.target.value;
        setSearch(newString || '');

        if (newString) {
            const newRows = rows?.filter((row) => {
                let matches = true;

                const properties = ['name', 'code', 'status'];
                let containsQuery = false;

                properties.forEach((property) => {
                    if (row[property]?.toString().toLowerCase().includes(newString?.toString()?.toLowerCase())) {
                        containsQuery = true;
                    }
                });

                if (!containsQuery) {
                    matches = false;
                }

                return matches;
            });
            setRows(newRows && newRows.length ? newRows : rows);
        } else {
            getProducts();
        }
    };

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelectedId = rows?.map((n) => n.rewardTitle);
            setSelected(newSelectedId);
            return;
        }
        setSelected([]);
    };

    const handleClick = (event, id) => {
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
        }

        setSelected(newSelected);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event?.target.value, 10));
        setPage(0);
    };

    const isSelected = (name) => selected.indexOf(name) !== -1;
    const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

    const handleStatusChange = (redeemedReward) => {
        if (redeemedReward.status === 'NEW') {
            redeemedReward.status = 'PROGRESS';
        } else if (redeemedReward.status === 'PROGRESS') {
            redeemedReward.status = 'PURCHASED';
        } else if (redeemedReward.status === 'PURCHASED') {
            redeemedReward.status = 'DONE';
        } else if (redeemedReward.status === 'DONE') {
            redeemedReward.status = 'NEW';
        }

        changeRewardStatusAPI(loggedInUser.company.id, loggedInUser.id, redeemedReward)
            .then((res) => loadReedemedRewards())
            .catch((e) => {
                //  dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const markRewardsAsFinisihed = (data) => {
        
        
        let x = [];
        data.forEach((element, index) => {
            x.push(rows.find((item) => item.rewardTitle === element));
            
        });

        markRewardAsFinisihedAPI(loggedInUser.company.id, loggedInUser.id, x)
            .then((res) => res.data)
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    };

    const rewardStatusTypes = {
        NEW: { label: <FormattedMessage id="NEW" />, color: 'error' },
        PROGRESS: { label: <FormattedMessage id="PROGRESS" />, color: 'secondary' },
        PURCHASED: { label: <FormattedMessage id="PURCHASED" />, color: 'primary' },
        DONE: { label: <FormattedMessage id="DONE" />, color: 'success' }
    };

    const formatDate = (date) => {
        if (!date) {
            return ''; // or any default value you want to display when the date is undefined
        }
        
        date = new Date(date[0], date[1] - 1, date[2], date[3], date[4]);

        const year = date.getFullYear().toString().substring(2, 4);
        const month = (date.getMonth() + 1).toString().padStart(2, '0');
        const day = date.getDate().toString().padStart(2, '0');
        const hours = date.getHours().toString().padStart(2, '0');
        const minutes = date.getMinutes().toString().padStart(2, '0');

        return `${day}.${month}.${year} ${hours}:${minutes}`;
    };

    const handleDone = () => {
        setSelectDone(true);
        setSelectProgress(false);
    };

    const handleProgress = () => {
        setSelectDone(false);
        setSelectProgress(true);
    };

    return (
        <MainCard title={<FormattedMessage id="Redeemed Rewards" />} content={false}>
            <CardContent>
                <Grid container spacing={2}>
                    <Button
                        onClick={handleProgress}
                        variant="contained"
                        style={{ backgroundColor: '#F4DEC0', color: 'black', width: '250px', marginRight: '30px' }}
                    >
                        In Progress
                    </Button>
                    <Button onClick={handleDone} variant="contained" style={{ backgroundColor: '#B2C698', color: 'black', width: '250px' }}>
                        Done
                    </Button>
                </Grid>
            </CardContent>

            {/* table */}
            <TableContainer>
                {loading && (
                    <LinearProgress
                        variant="indeterminate"
                        sx={{
                            backgroundColor: '#cbab7075',
                            '& .MuiLinearProgress-bar': {
                                backgroundColor: '#cbab70'
                            }
                        }}
                    />
                )}
                <Table sx={{ minWidth: 750 }} aria-labelledby="tableTitle">
                    <EnhancedTableHead
                        numSelected={selected.length}
                        order={order}
                        orderBy={orderBy}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={rows.length}
                        theme={theme}
                        selected={selected}
                    />

                    <TableBody>
                        {rows.length > 0 &&
                            stableSort(rows, getComparator(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .filter((row) => {
                                    if (selectProgress) {
                                        return row.status !== 'DONE';
                                    } else if (selectDone) {
                                        return row.status === 'DONE';
                                    } else {
                                        return true; // Show all rows if neither selectProgress nor selectDone is true
                                    }
                                })
                                .map((row, index) => {
                                    if (typeof row === 'number') return null;
                                    const isItemSelected = isSelected(row.rewardTitle);
                                    const labelId = `enhanced-table-checkbox-${index}`;

                                    return (
                                        <TableRow
                                            hover
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={index}
                                            selected={isItemSelected}
                                        >
                                            {/* <TableCell
                                                align="center"
                                                component="th"
                                                id={labelId}
                                                scope="row"
                                                onClick={(event) => handleClick(event, row.rewardTitle)}
                                                sx={{ cursor: 'pointer' }}
                                            >
                                                <Avatar
                                                    sx={{ margin: '0 auto', textAlign: 'center' }}
                                                    src={row.picture}
                                                    size="md"
                                                    variant="rounded"
                                                />
                                            </TableCell> */}
                                            <TableCell component="th" id={labelId} scope="row" sx={{ cursor: 'pointer' }}>
                                                <Typography
                                                    component={Link}
                                                    to={`/e-commerce/product-details/${row.rewardTitle}`}
                                                    variant="subtitle1"
                                                    sx={{
                                                        color: theme.palette.mode === 'dark' ? theme.palette.grey[600] : 'grey.900',
                                                        textDecoration: 'none'
                                                    }}
                                                >
                                                    {row.rewardTitle}
                                                </Typography>
                                            </TableCell>
                                            <TableCell>{row.code}</TableCell>
                                            <TableCell>{formatDate(row.redeemDate)}</TableCell>
                                            <TableCell>{row.note}</TableCell>
                                            <TableCell>
                                                <Chip
                                                    size="medium"
                                                    label={rewardStatusTypes[row.status]?.label}
                                                    chipcolor={rewardStatusTypes[row.status]?.color}
                                                    sx={{ borderRadius: '4px', textTransform: 'capitalize', fontWeight: 'bold' }}
                                                    onClick={() => handleStatusChange(row)}
                                                />
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                        {emptyRows > 0 && (
                            <TableRow
                                style={{
                                    height: 53 * emptyRows
                                }}
                            >
                                <TableCell colSpan={6} />
                            </TableRow>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>

            {/* table pagination */}
            <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
            />
        </MainCard>
    );
};

export default ProductList;
