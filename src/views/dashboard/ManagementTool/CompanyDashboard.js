import { Grid } from '@mui/material';
import LoadBaseRewards from './BaseRewards';
import ModifiedRewards from './ModifiedRewards';
import React from 'react';
import { gridSpacing } from 'store/constant';
import { loadAllBaseRewards } from '../../../services/BaseRewardService';
import { loadAllCustomRewardsOfCompany } from '../../../services/CompanyService';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';
import { useDispatch } from 'store';

// import {BaseRewards} from "./BaseRewards";

function CompanyDashboard() {
    let [baseRewards, setBaseRewards] = React.useState({});
    let [companyRewards, setCompanyRewards] = React.useState({});
    let [loadingCustomRewards, setLoadingCustomRewards] = React.useState(true);
    let [loadingBaseRewards, setLoadingBaseRewards] = React.useState(true);
    const dispatch = useDispatch();

    const loadCustomRewards = () => {
        loadAllCustomRewardsOfCompany(loggedInUser.company?.id)
            .then((res) => res.data)
            .then((r) => {
                setCompanyRewards(r);
                setLoadingCustomRewards(false);
            })
            .catch((e) => {
                dispatch(showMessage(e.message));
                setLoadingCustomRewards(false);
            });
    };

    const loadBaseRewards = () => {
        loadAllBaseRewards()
            .then((res) => res.data)
            .then((r) => {
                setBaseRewards(r);
                setLoadingBaseRewards(false);
            })
            .catch((e) => {
                dispatch(showMessage(e.message));
                setLoadingBaseRewards(false);
            });
    };

    const loadLists = () => {
        loadCustomRewards();
        loadBaseRewards();
    };

    React.useEffect(() => {
        loadLists();
    }, []);

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} md={4}>
                {LoadBaseRewards(baseRewards, loadLists, loadingBaseRewards)}
            </Grid>
            <Grid item xs={12} md={8}>
                {ModifiedRewards(companyRewards, loadLists, loadingCustomRewards)}
            </Grid>
        </Grid>
    );
}

export default CompanyDashboard;
