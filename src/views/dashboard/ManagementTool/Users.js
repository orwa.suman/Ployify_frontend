import * as Yup from 'yup';

import { Autocomplete, Button, Grid, TextField } from '@mui/material';
import { FormattedMessage, useIntl } from 'react-intl';

import { LoadingButton } from '@mui/lab';
import MainCard from '../../../ui-component/cards/MainCard';
import React from 'react';
import { addSuperUserToCompany } from 'services/CompanyService';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from '../../../utils/form/Communication';
import { useDispatch } from 'store';
import { useFormik } from 'formik';

function Users() {
    const dispatch = useDispatch();
    const [leader, setLeader] = React.useState('');
    const [isLoading, setLoading] = React.useState(false);
    const intl = useIntl();

    const submitForm = (event) => {
        formik.validateForm().then((errors) => {
            if (errors && Object.keys(errors).length > 0) {
                dispatch(showMessage(<FormattedMessage id="Validation Fail" />));
            }
            formik.handleSubmit(event);
        });
    };

    const validationSchema = Yup.object({});

    const formik = useFormik({
        initialValues: {
            email: ''
        },
        validationSchema,
        onSubmit: () => {
            setLoading(true);
            const { email } = formik.values;
            addSuperUserToCompany(loggedInUser.company.id, email)
                .then(() => {
                    setLoading(false);
                    dispatch(showMessage(<FormattedMessage id="Submit Success" />, 'success'));
                })
                .catch((e) => {
                    setLoading(false);
                    dispatch(showMessage(e?.response?.data?.message));
                });
        }
    });

    return (
        <form onSubmit={formik.handleSubmit} style={{ width: '100%' }}>
            <MainCard title={<FormattedMessage id="Add New Users" />}>
                <Grid container>
                    {/* <Grid item xs={12} md={5} className="careployee-form__block">
                        <label htmlFor="email" className="careployee-form__label">
                            <FormattedMessage id="Email address" />
                        </label>
                        <Autocomplete
                            disablePortal
                            id="email"
                            options={leaders && leaders.length ? leaders : []}
                            multiple
                            getOptionLabel={(option) => option}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label={intl.formatMessage({ id: 'email' })}
                                    error={!!formik.values.email.error}
                                    helperText={!!formik.values.email.error && formik.values.email.error}
                                />
                            )}
                            onChange={(_event, value) => formik.setValues({ ...formik.values, email: value })}
                        />
                    </Grid> */}

                    <Grid item xs={12} md={5} className="careployee-form__block">
                        <label htmlFor="leaders" className="careployee-form__label">
                            <FormattedMessage id="new email" />
                        </label>
                        <TextField
                            id="new-admin-email"
                            style={{ width: '100%' }}
                            value={formik.values.email}
                            type="email"
                            onChange={(event) => formik.setValues({ ...formik.values, email: event.target.value })}
                            // onBlur={(event) => {
                            // setLeader('');

                            // if (event.target.value && !leaders.includes(event.target.value))
                            //     setLeaders([...leaders, event.target.value]);
                            // }}
                        />
                    </Grid>
                </Grid>

                <Grid item xs={12}>
                    <LoadingButton
                        loading={isLoading}
                        className="careployee-form__submit-button"
                        variant="contained"
                        size="large"
                        onClick={submitForm}
                    >
                        <FormattedMessage id="Submit" />
                    </LoadingButton>
                </Grid>
            </MainCard>
        </form>
    );
}

export default Users;
