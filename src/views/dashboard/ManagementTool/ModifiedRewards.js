import {
    Avatar,
    Box,
    Button,
    CardActions,
    CardContent,
    CircularProgress,
    Grid,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography
} from '@mui/material';

import Dialog from '@mui/material/Dialog';
import { FormattedMessage } from 'react-intl';
import MainCard from 'ui-component/cards/MainCard';
import PropTypes from 'prop-types';
import React from 'react';
import RewardForm from '../../application/Rewards/RewardForm';
import { deleteCustomRewardToCompanyRewards } from 'services/CompanyService';
import { dispatch } from 'store';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';

// material-ui

// assets

function AddCustomRewardDialog(props) {
    const { onClose, selectedValue, open } = props;

    const handleClose = () => {
        onClose(selectedValue);
    };

    return (
        <Dialog onClose={handleClose} open={open} sx={{ minWidth: '500px' }}>
            {RewardForm(true, selectedValue)}
        </Dialog>
    );
}

AddCustomRewardDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    selectedValue: PropTypes.object.isRequired
};

// ==========================|| DATA WIDGET - ACTIVE TICKETS CARD ||========================== //

function ModifiedRewards(companyRewards, loadLists, loadingCustomRewards) {
    const [open, setOpen] = React.useState(false);
    const [data, setData] = React.useState([]);
    React.useEffect(() => {
        setData(companyRewards);
    }, [companyRewards]);

    const [selectedValue, setSelectedValue] = React.useState({});
    const handleClose = (value) => {
        setOpen(false);
        setSelectedValue(value);
        loadLists();
    };

    function deleteCustomReward(data) {
        deleteCustomRewardToCompanyRewards(loggedInUser.company.id, data.customReward.title)
            .then(() => {
                dispatch(showMessage(<FormattedMessage id="Deleted Successfully" />, 'success'));
                loadLists();
            })
            .catch((e) => {
                dispatch(showMessage(e?.response?.data?.message));
            });
    }

    function updateCustomReward(data) {
        setOpen(true);
        setSelectedValue({ ...data, isUpdateCustomReward: true });

        // deleteCustomRewardToCompanyRewards(loggedInUser.company.id, data.customReward.title)
        //     .then(() => {
        //         dispatch(showMessage(<FormattedMessage id="Deleted Successfully" />, 'success'))
        //         loadLists();
        //     }).catch((e) => {
        //         dispatch(showMessage(e?.response?.data?.message));
        //     });
    }

    return (
        <MainCard title={<FormattedMessage id="Custom Rewards" />} content={false}>
            <CardContent>
                {loadingCustomRewards && (
                    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                        <CircularProgress size={50} style={{ color: '#cbab70' }} />
                    </Box>
                )}
                <TableContainer>
                    <Table sx={{ minWidth: 560 }}>
                        <TableHead>
                            <TableRow>
                                <TableCell>
                                    <FormattedMessage id="Reward" />
                                </TableCell>
                                <TableCell sx={{ pr: 3 }}>
                                    <FormattedMessage id="Info" />
                                </TableCell>
                                <TableCell sx={{ pl: 3 }}>
                                    <FormattedMessage id="Requirements" />
                                </TableCell>
                                <TableCell sx={{ pl: 3 }}>
                                    <FormattedMessage id="Actions" />
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {!data ||
                                (!data.length && (
                                    <Box sx={{ display: 'flex', justifyContent: ' center', alignItems: 'center' }}>Loading...</Box>
                                ))}
                            {(!!data && data.length ? data : []).map((row, index) => (
                                <TableRow hover key={index}>
                                    <TableCell>
                                        <Grid container spacing={2} alignItems="center" sx={{ flexWrap: 'nowrap' }}>
                                            <Grid item>
                                                <Avatar alt="User 1" src={row?.customReward?.picture || row.avatar} />
                                            </Grid>
                                            <Grid item xs zeroMinWidth>
                                                <Typography component="div" align="left" variant="subtitle1">
                                                    {row?.customReward?.title}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </TableCell>
                                    <TableCell sx={{ pr: 3 }}>
                                        <Typography align="left" component="div" variant="subtitle1">
                                            {row?.availableItemsNum} <FormattedMessage id="items left" />
                                        </Typography>
                                        <Typography align="left" component="div" variant="subtitle2">
                                            {row?.customReward?.description}
                                        </Typography>
                                    </TableCell>

                                    <TableCell sx={{ pl: 3 }}>
                                        <Typography align="left" component="div" variant="subtitle1">
                                            {row?.rewardPoints} <FormattedMessage id="points per item" />
                                        </Typography>
                                        {/*<Typography align="left" component="div" variant="subtitle2">*/}
                                        {/*    {row.availableItemsNum} items available */}
                                        {/*</Typography>*/}
                                    </TableCell>

                                    <TableCell sx={{ pl: 3 }}>
                                        <Button align="left" component="div" variant="subtitle1" onClick={() => deleteCustomReward(row)}>
                                            <FormattedMessage id="Delete" />
                                        </Button>
                                        <Button align="left" component="div" variant="subtitle1" onClick={() => updateCustomReward(row)}>
                                            <FormattedMessage id="Edit" />
                                        </Button>
                                        {/*<Typography align="left" component="div" variant="subtitle2">*/}
                                        {/*    {row.availableItemsNum} items available */}
                                        {/*</Typography>*/}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </CardContent>
            <CardActions sx={{ justifyContent: 'flex-end' }}>
                <Button variant="contained" size="small" onClick={() => setOpen(true)}>
                    <FormattedMessage id="Add Custom Reward" />
                </Button>
            </CardActions>
            <AddCustomRewardDialog onClose={handleClose} selectedValue={selectedValue} open={open} />
        </MainCard>
    );
}

// ModifiedRewards.propTypes = {
//     title: PropTypes.string
// };

export default ModifiedRewards;
