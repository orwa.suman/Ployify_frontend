// material-ui

import { Box, Button, CardContent, CardMedia, CircularProgress, Grid, TextField, Typography } from '@mui/material';
import React, { useState } from 'react';

import Dashboard1 from 'assets/images/widget/dashborad-1.jpg';
import { FormattedMessage } from 'react-intl';
import MainCard from 'ui-component/cards/MainCard';
import { addBaseRewardToCompanyRewards } from '../../../services/CompanyService';
import { gridSpacing } from 'store/constant';
import { loggedInUser } from 'contexts/JWTContext';
import { showMessage } from 'utils/form/Communication';
import { useDispatch } from 'store';

// project imports

// assets

const mediaSX = {
    width: 90,
    height: 80,
    borderRadius: '12px'
};

// ===========================|| DATA WIDGET - LATEST POSTS CARD ||=========================== //

function LoadBaseRewards(data, loadLists, loadingBaseRewards) {
    const [showFields, setShowFields] = useState(false);
    const [rewardPoints, setRewardPoints] = useState(0);
    const [availableItemsNum, setAvailableItemsNum] = useState(0);
    const [currentReward, setCurrentReward] = useState(null);

    const dispatch = useDispatch();

    const handleButtonClick = (reward) => {
        setShowFields(false);

        addBaseRewardToCompanyRewards(loggedInUser.company.id, rewardPoints, availableItemsNum, reward)
            .then((r) => {
                dispatch(showMessage(<FormattedMessage id="Reward successfully added!" />, 'success'));
                loadLists();
                setAvailableItemsNum(0);
                setRewardPoints(0);
            })
            .catch((e) => {
                
                dispatch(showMessage(<FormattedMessage id="Reward Already Exist!" />, 'error'));
            });
    };

    const addToCompanyRewards = (reward) => {
        setCurrentReward(reward);
        setShowFields(true);
    };

    return (
        <MainCard title={<FormattedMessage id="Base Rewards" />} content={false}>
            <CardContent>
                {loadingBaseRewards && (
                    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                        <CircularProgress size={50} style={{ color: '#cbab70' }} />
                    </Box>
                )}
                <Grid container spacing={gridSpacing}>
                    {(data && data.length ? data : []).map((reward) => (
                        <Grid key={reward.id} item xs={12}>
                            <Grid container alignItems="center" spacing={2}>
                                <Grid item>
                                    <CardMedia
                                        component="img"
                                        image={reward.picture || Dashboard1}
                                        title="image"
                                        sx={mediaSX}
                                        onClick={() => window.open('https://www.youtube.com/watch?v=mQR0bXO_yI8&ab_channel=RAYLOWESWINGS')}
                                    />
                                </Grid>
                                <Grid item xs zeroMinWidth>
                                    <Grid container spacing={1}>
                                        <Grid item xs={12}>
                                            <Typography align="left" component="div" variant="subtitle1">
                                                {reward.title}
                                            </Typography>
                                            <Typography align="left" component="div" variant="caption">
                                                {reward.defaultAvailableItemsNum} Items available | {reward.defaultBasePoints} points per
                                                item
                                            </Typography>
                                            <Typography align="left" component="div" variant="caption">
                                                {reward.description}
                                            </Typography>
                                            <Button variant="contained" size="small" onClick={() => addToCompanyRewards(reward)}>
                                                Add to company rewards
                                            </Button>
                                        </Grid>
                                        {showFields && currentReward && currentReward.id === reward.id && (
                                            <div style={{ marginTop: '20px' }}>
                                                <TextField
                                                    label="Reward Points"
                                                    value={rewardPoints}
                                                    onChange={(e) => setRewardPoints(e.target.value)}
                                                />
                                                <TextField
                                                    label="Available Items Num"
                                                    value={availableItemsNum}
                                                    onChange={(e) => {
                                                        setAvailableItemsNum(e.target.value);
                                                        // Send e.target.value to the backend here
                                                    }}
                                                    style={{ marginTop: '10px' }}
                                                />
                                                <Button
                                                    style={{ marginTop: '10px' }}
                                                    variant="contained"
                                                    color="primary"
                                                    onClick={() => handleButtonClick(reward)}
                                                >
                                                    Submit 2
                                                </Button>
                                            </div>
                                        )}

                                        {/*<Grid sx={{justifyContent: 'flex-end'}}>*/}
                                        {/*    <Button variant="text" size="small">*/}
                                        {/*        Add to company rewards*/}
                                        {/*    </Button>*/}
                                        {/*</Grid>*/}
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    ))}
                </Grid>
            </CardContent>
        </MainCard>
    );
}

// BaseRewards.propTypes = {
//     title: PropTypes.string,
//     data: PropTypes.array
// };

export default LoadBaseRewards;
