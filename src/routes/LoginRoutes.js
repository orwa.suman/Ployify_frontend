import GuestGuard from 'utils/route-guard/GuestGuard';
import Loadable from 'ui-component/Loadable';
import MinimalLayout from 'layout/MinimalLayout';
import NavMotion from 'layout/NavMotion';
import { lazy } from 'react';

// project imports

// login routing
const AuthLogin = Loadable(lazy(() => import('views/pages/authentication/authentication1/Login1')));
const AuthRegister = Loadable(lazy(() => import('views/pages/authentication/authentication1/Register1')));
const AuthPreRegister = Loadable(lazy(() => import('views/pages/authentication/authentication1/PreRegister')));
const AuthPostRegister = Loadable(lazy(() => import('views/pages/authentication/authentication1/PostRegister')));
const AuthForgotPassword = Loadable(lazy(() => import('views/pages/authentication/authentication1/ForgotPassword1')));
const ResetPassword = Loadable(lazy(() => import('views/pages/authentication/authentication3/ResetPassword3')));
const CheckMail = Loadable(lazy(() => import('views/pages/authentication/authentication3/CheckMail3')));
const CodeVerfication = Loadable(lazy(() => import('views/pages/authentication/authentication3/CodeVerification3')));

const UserQuestionnairePage = Loadable(lazy(() => import('views/application/users/updated-questionnaire/index')));

// ==============================|| AUTH ROUTING ||============================== //

const LoginRoutes = {
    path: '/',
    element: (
        <NavMotion>
            <GuestGuard>
                <MinimalLayout />
            </GuestGuard>
        </NavMotion>
    ),
    children: [
        {
            path: '/login',
            element: <AuthLogin />
        },
        {
            path: '/register',
            element: <AuthRegister />
        },
        {
            path: '/register/code',
            element: <AuthPreRegister />
        },
        {
            path: '/register/email-verify',
            element: <AuthPostRegister />
        },
        {
            path: '/forgot',
            element: <AuthForgotPassword />
        },
        {
            path: '/resetPassword',
            element: <ResetPassword />
        },
        {
            path: '/checkMail',
            element: <CheckMail />
        },
        {
            path: '/codeVerfication',
            element: <CodeVerfication />
        },
        {
            path: '/questionnaire',
            element: <UserQuestionnairePage />
        }
    ]
};

export default LoginRoutes;
