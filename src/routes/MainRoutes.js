import AuthGuard from 'utils/route-guard/AuthGuard';
import Loadable from 'ui-component/Loadable';
import MainLayout from 'layout/MainLayout';
import { lazy } from 'react';

// project imports

const AppUserAccountProfile3 = Loadable(lazy(() => import('views/application/users/account-profile/Profile3')));
const ChangePassword = Loadable(lazy(() => import('views/application/users/ChangePassword')));

const AppProfileListStyle1 = Loadable(lazy(() => import('views/application/users/list/Style1')));

const DashboardDefault = Loadable(lazy(() => import('views/dashboard/Default')));
const ManagementTool = Loadable(lazy(() => import('views/dashboard/ManagementTool/CompanyDashboard')));
const ManagementUserTool = Loadable(lazy(() => import('views/dashboard/ManagementTool/Users')));
const ManagementCodeTool = Loadable(lazy(() => import('views/dashboard/ManagementTool/Codes')));
const ManagementRedeemedRewardTool = Loadable(lazy(() => import('views/dashboard/ManagementTool/RedeemedRewards')));

const CareployeeWelcome = Loadable(lazy(() => import('views/application/landing/WelcomeQuestionnaire')));

const UserQuestionnaire = Loadable(lazy(() => import('views/application/users/questionnaire/UserQuestionnaire')));
const UserQuestionnaireList = Loadable(lazy(() => import('views/application/users/questionnaire/List')));
const UserQuestionnaireForm = Loadable(lazy(() => import('views/application/users/questionnaire/Create')));
const UserQuestionnaireFormEdit = Loadable(lazy(() => import('views/application/users/questionnaire/Edit')));

const UserQuestionnairePage = Loadable(lazy(() => import('views/application/users/updated-questionnaire/index')));

// activities
const TrainingList = Loadable(lazy(() => import('views/application/activities/Training/TrainingList')));
const AllTraining = Loadable(lazy(() => import('views/application/activities/Training/AllTraining')));
const TrainingPlanList = Loadable(lazy(() => import('views/application/activities/TrainingPlan/TrainingPlanList')));
const TrainingForm = Loadable(lazy(() => import('views/application/activities/Training/TrainingForm')));
const TrainingPlanForm = Loadable(lazy(() => import('views/application/activities/TrainingPlan/TrainingPlanForm')));
const TrainingPlanPlayMain = Loadable(lazy(() => import('views/application/activities/TrainingPlan/TrainingPlanPlayMainScreen')));
const TrainingPlanPlayVideo = Loadable(lazy(() => import('views/application/activities/TrainingPlan/TrainingPlanPlayVideo')));
const TrainingPlanFinish = Loadable(lazy(() => import('views/application/activities/TrainingPlan/TrainingPlanFinish')));
const TrainingPlanCards = Loadable(lazy(() => import('views/application/activities/TrainingPlan/TrainingPlanCards')));

const MentalHealthList = Loadable(lazy(() => import('views/application/activities/MentalHealth/MentalHealthList')));
const MentalHealthForm = Loadable(lazy(() => import('views/application/activities/MentalHealth/MentalHealthForm')));

const AuthorList = Loadable(lazy(() => import('views/application/activities/Author/AuthorList')));
const AuthorForm = Loadable(lazy(() => import('views/application/activities/Author/AuthorForm')));
const AuthorInfo = Loadable(lazy(() => import('views/application/activities/Author/AuthorInfo')));

const MentalHealthCards = Loadable(lazy(() => import('views/application/activities/MentalHealth/MentalHealthCards')));
const MentalHealtDetails = Loadable(lazy(() => import('views/application/activities/MentalHealth/MentalHealthDetails')));
const AllMentalHealth = Loadable(lazy(() => import('views/application/activities/MentalHealth/AllMentalHealth')));

const RecipesList = Loadable(lazy(() => import('views/application/activities/Recipes/ReciepesList')));
const RecipesForm = Loadable(lazy(() => import('views/application/activities/Recipes/ReciepesForm')));
const RecipesCard = Loadable(lazy(() => import('views/application/activities/Recipes/RecipesCards')));
const RecipesDetails = Loadable(lazy(() => import('views/application/activities/Recipes/RecipesDetails')));
const EcommerceRecipies = Loadable(lazy(() => import('views/application/activities/Recipes/CheckoutRecipies/RecipeCart')));
const PrivacyPolicy = Loadable(lazy(() => import('views/pages/saas-pages/PrivacyPolicy')));

const CompanyCards = Loadable(lazy(() => import('views/application/activities/Companies/CompanyCards')));
const CompanyForm = Loadable(lazy(() => import('views/application/activities/Companies/CompanyForm')));
const CompanyHierarchyForm = Loadable(lazy(() => import('views/application/activities/Companies/CompanyHierarchyForm')));

const DishForm = Loadable(lazy(() => import('views/application/activities/Recipes/DishForm')));
const AllRecipes = Loadable(lazy(() => import('views/application/activities/Recipes/AllRecipes')));
const DishList = Loadable(lazy(() => import('views/application/activities/Recipes/DishList')));
const IngredientForm = Loadable(lazy(() => import('views/application/activities/Recipes/IngredientForm')));
const IngredientTagForm = Loadable(lazy(() => import('views/application/activities/Recipes/IngredientTagForm')));
const IngredientCard = Loadable(lazy(() => import('views/application/activities/Recipes/IngredientCard')));

const UserReward = Loadable(lazy(() => import('views/application/Rewards/UserReward')));
const RewardsView = Loadable(lazy(() => import('views/application/Rewards/Rewards')));
const RewardForm = Loadable(lazy(() => import('views/application/Rewards/RewardForm')));
const TagManu = Loadable(lazy(() => import('views/application/activities/TrainingPlan/TrainingPlanCards/tagManu')));
const ReceipeTagManu = Loadable(lazy(() => import('views/application/activities/Recipes/RecipesCards/TagManu')));
const MentalHealthTagManu = Loadable(lazy(() => import('views/application/activities/MentalHealth/MentalHealthCards/MentalHealthTagManu')));

//seminare
const SeminareForm = Loadable(lazy(() => import('views/application/activities/Seminare/SeminareForm')));
const SeminareList = Loadable(lazy(() => import('views/application/activities/Seminare/SeminareList')));
const SeminareCards = Loadable(lazy(() => import('views/application/activities/Seminare/SeminareCards')));
const SeminareQuiz = Loadable(lazy(() => import('views/application/activities/Seminare/SeminareQuiz')));

//statistics
const WidgetStatistics = Loadable(lazy(() => import('views/application/Statistics')));

//Feedback

const FeedbackList = Loadable(lazy(() => import('views/application/activities/Feedback/FeedbackList')));

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
    path: '/',
    element: (
        <AuthGuard>
            <MainLayout />
        </AuthGuard>
    ),
    children: [
        {
            path: '/dashboard/default',
            element: <DashboardDefault />
        },

        {
            path: '/user/profile',
            element: <AppUserAccountProfile3 />
        },

        {
            path: '/user/change-password',
            element: <ChangePassword />
        },

        {
            path: '/user/questionnaire',
            element: <UserQuestionnairePage />
        },
        // {
        //     path: '/questionnaire',
        //     element: <UserQuestionnairePage />
        // },

        // {
        //     path: '/dashboard/user/questionnaire/form',
        //     element: <UserQuestionnaireForm />
        // },

        // {
        //     path: '/dashboard/user/questionnaire/form/:id',
        //     element: <UserQuestionnaireFormEdit />
        // },
        // {
        //     path: '/dashboard/user/questionnaire/list',
        //     element: <UserQuestionnaireList />
        // },

        {
            path: '/dashboard/user/list',
            element: <AppProfileListStyle1 />
        },

        {
            path: '/dashboard/activities/training-plan/list',
            element: <TrainingPlanList />
        },
        {
            path: '/dashboard/activities/training-plan/cards',
            element: <TrainingPlanCards />
        },
        {
            path: '/activities/TrainingPlan/cards/:location',
            element: <TrainingPlanCards />
        },
        {
            path: '/dashboard/activities/training/list',
            element: <TrainingList />
        },
        {
            path: '/dashboard/activities/training/all',
            element: <AllTraining />
        },
        {
            path: '/dashboard/author/list',
            element: <AuthorList />
        },
        {
            path: '/dashboard/author/form',
            element: <AuthorForm />
        },

        {
            path: '/dashboard/author/form/:id',
            element: <AuthorForm />
        },

        {
            path: '/activities/Author/info/:id/:firstName/:lastName',
            element: <AuthorInfo />
        },
        {
            path: '/dashboard/activities/mental-health/list',
            element: <MentalHealthList />
        },

        {
            path: '/dashboard/activities/mental-health/all',
            element: <AllMentalHealth />
        },

        {
            path: '/dashboard/activities/mental-health/form',
            element: <MentalHealthForm />
        },

        {
            path: '/dashboard/activities/mental-health/form/:id',
            element: <MentalHealthForm />
        },

        {
            path: '/activities/MentalHealth/cards',
            element: <MentalHealthCards />
        },

        {
            path: '/activities/MentalHealth/details/:id',
            element: <MentalHealtDetails />
        },
        {
            path: '/dashboard/activities/recipes/dish/list',
            element: <DishList />
        },
        {
            path: '/dashboard/activities/recipes/dish/form',
            element: <DishForm />
        },
        {
            path: '/dashboard/activities/recipes/all',
            element: <AllRecipes />
        },
        {
            path: '/dashboard/activities/recipes/dish/form/:id',
            element: <DishForm />
        },

        {
            path: '/dashboard/company/list',
            element: <CompanyCards />
        },
        {
            path: '/dashboard/company/form',
            element: <CompanyForm />
        },

        {
            path: '/dashboard/company/form/:id',
            element: <CompanyForm />
        },
        {
            path: '/dashboard/activities/recipes/ingredient/form',
            element: <IngredientForm />
        },

        {
            path: '/dashboard/activities/recipes/ingredient/form/:id',
            element: <IngredientForm />
        },
        {
            path: '/dashboard/activities/recipes/ingredient-tag/form',
            element: <IngredientTagForm />
        },
        {
            path: '/dashboard/activities/recipes/ingredient/list',
            element: <IngredientCard />
        },

        {
            path: '/dashboard/activities/recipes/form',
            element: <RecipesForm />
        },

        {
            path: '/dashboard/activities/recipes/form/:id',
            element: <RecipesForm />
        },

        {
            path: '/dashboard/activities/recipes/list',
            element: <RecipesList />
        },
        {
            path: '/activities/Reciepts/cards',
            element: <RecipesCard />
        },
        {
            path: '/activities/Reciepts/reciptDetails/:id',
            element: <RecipesDetails />
        },
        {
            path: '/activities/Reciepts/checkout',
            element: <EcommerceRecipies />
        },
        {
            path: '/dashboard/activities/training/form',
            element: <TrainingForm />
        },

        {
            path: '/dashboard/activities/training/form/:id',
            element: <TrainingForm />
        },

        {
            path: '/dashboard/activities/training-plan/form',
            element: <TrainingPlanForm />
        },

        {
            path: '/dashboard/activities/training-plan/form/:id',
            element: <TrainingPlanForm />
        },

        {
            path: '/activities/TrainingPlan/list/TrainingPlanMainScreen',
            element: <TrainingPlanPlayMain />
        },

        {
            path: '/activities/TrainingPlan/list/playVideo',
            element: <TrainingPlanPlayVideo />
        },

        {
            path: '/activities/TrainingPlan/list/finish',
            element: <TrainingPlanFinish />
        },

        {
            path: '/dashboard/management/organization',
            element: <CompanyHierarchyForm />
        },

        {
            path: '/dashboard/management/rewards',
            element: <ManagementTool />
        },

        {
            path: '/dashboard/management/redeemed-rewards',
            element: <ManagementRedeemedRewardTool />
        },

        {
            path: '/dashboard/management/users',
            element: <ManagementUserTool />
        },

        {
            path: '/dashboard/management/codes',
            element: <ManagementCodeTool />
        },
        {
            path: '/dashboard/reward/list',
            element: <RewardsView />
        },

        {
            path: '/dashboard/reward/form',
            element: <RewardForm customReward={false} />
        },

        {
            path: '/dashboard/reward/form/:id',
            element: <RewardForm />
        },

        {
            path: '/reward/info',
            element: <UserReward />
        },

        {
            path: '/dashboard/activities/seminar/form',
            element: <SeminareForm />
        },

        {
            path: '/dashboard/activities/seminar/form/:id',
            element: <SeminareForm />
        },

        {
            path: '/dashboard/activities/seminar/list',
            element: <SeminareList />
        },
        {
            path: '/activities/TrainingPlan/tag-menu/:location/:tagname',
            element: <TagManu />
        },
        {
            path: '/activities/Reciepts/cards/tag-menu/:tagname',
            element: <ReceipeTagManu />
        },
        {
            path: '/activities/MentalHealth/cards/tag-menu/:tagname',
            element: <MentalHealthTagManu />
        },
        {
            path: '/activities/seminare/cards',
            element: <SeminareCards />
        },

        {
            path: '/activities/seminare/quiz/:id',
            element: <SeminareQuiz />
        },

        {
            path: '/dashboard/statistics',
            element: <WidgetStatistics />
        },

        {
            path: '/rights/:page',
            element: <PrivacyPolicy />
        },
        {
            path: '/dashboard/user/feedback/list',
            element: <FeedbackList />
        }
    ]
};

export default MainRoutes;
