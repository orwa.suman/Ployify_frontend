import axios from 'axios';
import { BASE_URL, token } from '../contexts/JWTContext';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function addSeminare(data) {
    
    
    return axios.post(BASE_URL + '/seminare/addSeminar', data, config);
}

export function loadSeminare() {
    
    
    return axios.get(BASE_URL + '/seminare/getAllSeminar', config);
}

export function startCurrentSeminar(userId) {
    return axios.patch(BASE_URL + `/user/startCurrentSeminar/${userId}`, config);
}

export function updateCurrentSeminar(userId) {
    return axios.patch(BASE_URL + `/user/updateCurrentSeminar/${userId}`, config);
}

export function finishCurrentSeminar(userId) {
    return axios.patch(BASE_URL + `/user/finishCurrentSeminar/${userId}`, config);
}

export function activateNextSeminar(userId) {
    return axios.patch(BASE_URL + `/user/activateNextSeminar/${userId}`, config);
}

export function getSeminare(id) {
    
    
    return axios.get(BASE_URL + '/seminare/getSeminar/' + id, config);
}

export function updateSeminar(id, data) {
    
    
    return axios
        .put(BASE_URL + '/seminare/updateSeminar/' + id, data, config)
        .then(console.log)
        .catch(console.log);
}

export function deleteSeminare(id) {
    
    
    return axios
        .delete(BASE_URL + '/seminare/deleteseminare/' + id, config)
        .then(console.log)
        .catch(console.log);
}
