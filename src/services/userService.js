import { BASE_URL, token } from '../contexts/JWTContext';

import axios from 'axios';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

//TODO: check security bug
export function getUserById(id) {
    
    
    return axios.get(BASE_URL + '/user/getUserById/' + id, config);
}

//TODO: check security bug
export function getUserByEmail(email) {
    
    
    config.params = {
        userEmail: email
    };
    return axios.get(BASE_URL + '/user/getUserByEmail', config);
}

export async function addActivityToUserFavorites(data) {
    

    return axios.post(
        BASE_URL + '/user/addActivityToUserFavorites/' + data.userId,
        {},
        {
            ...config,
            params: {
                activityId: data.activityId,
                activityNum: data.activityNum
            }
        }
    );
}

export async function removeActivityFromUserFavorites(data) {
    

    return axios.post(
        BASE_URL + '/user/removeActivityFromUserFavorites/' + data.userId,
        {},
        {
            ...config,
            params: {
                activityId: data.activityId
            }
        }
    );
}

export function loadUsers() {
    
    
    return axios.get(BASE_URL + '/admin/user/getAllUsers', config);
}

export async function deleteUser(id) {
    
    

    await axios.delete(BASE_URL + '/admin/user/delete/' + id, config);
}

export function getUserHistory(userId, activityNum) {
    
    

    return axios.get(BASE_URL + '/user/getUserHistory/' + userId, { ...config, params: { activityNum } });
}

export function getActivitiesOfUser(userId) {
    
    
    return axios.get(BASE_URL + `/user/getAllHistoryForUser/${userId}`, config);
}

export function addActivityToUserHistory(userId, activityId, activityNum) {
    
    
    config.params = {
        activityId: activityId,
        activityNum: activityNum
    };
    return axios.post(BASE_URL + '/user/addActivityToUserHistory/' + userId, null, config);
}

export function signUp(data) {
    
    return axios.post(BASE_URL + '/signUp', data, config);
}

export function updateTrainingPlanForUser(userId, activityId, progress, endDate) {
    
    
    config.params = {
        progress: progress != null ? parseInt(progress, 10) : null, //TODO: remove parseInt when in backend progress is double!
        endDate: endDate
    };
    return axios.post(BASE_URL + '/user/updateActivityToUser/' + userId + '/' + activityId, null, config);
}

export function isFavorit(userId, activityId) {
    
    config.params = {
        activityId: activityId
    };
    return axios.get(BASE_URL + '/user/isFavorit/' + userId, config);
}

export function getUserFavorites(userId, activityNum) {
    
    
    config.params = {
        activityNum: activityNum
    };
    return axios.get(BASE_URL + '/user/getUserFavorites/' + userId, config);
}

export function getUserQuestionnaire(userId) {
    return axios.get(BASE_URL + '/user/getUserQuestionnaire/' + userId, config);
}

export function setUserQuestionnaire(userId, data, _token) {
    // 
    // const userTagList = [{ id: '63a5e61b6d16010dff95f089', name: 'Gergo' }];
    const _config = {
        headers: {
            Authorization: `Bearer ${token || _token}`,
            'Content-Type': 'application/json'
        }
    };
    return axios.post(BASE_URL + '/user/setUserQuestionnaire/' + userId, data, _config);
}
