
import cloudinary from 'cloudinary/lib/cloudinary';

export function uploadToCloudinary(fileContent, fileName, public_id_prefix, folder) {

    // Configuration
    cloudinary.config({
        cloud_name: process.env.REACT_APP_CLOUDINARY_CLOUD_NAME,
        api_key: process.env.REACT_APP_CLOUDINARY_API_KEY,
        api_secret: process.env.REACT_APP_CLOUDINARY_API_SECRET
    });

    if (typeof fileName === 'string' || fileName instanceof String){
        const newFileName = fileName.replace(/[^\w\s]/gi, '');

        const public_id_upload = public_id_prefix + newFileName;
        

        const payload = { 'public_id': public_id_upload, 'folder': folder };
        
        return cloudinary.v2.uploader.upload(fileContent, payload);
    }

    return Promise.resolve(); 


    // Generate
    // const url = cloudinary.url(public_id_upload, {
    //     width: 100,
    //     height: 150,
    //     Crop: 'fill'
    // });

    // 
}
