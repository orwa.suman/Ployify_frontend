import axios from "axios";
import {BASE_URL, token} from "../contexts/JWTContext"

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function updateStartCountTrainingPlanTracking (id) {
    
    
    axios.post(BASE_URL + '/tracking/updateStartCountTrainingPlanTracking/' + id, null, config);
}

export function updateEndCountTrainingPlanTracking (id) {
    
    
    axios.post(BASE_URL + '/tracking/updateEndCountTrainingPlanTracking/' + id, null, config);
}

export function getAllTrainingPlanTracking () {
    
    
    return axios.get(BASE_URL + '/tracking/getAllTrainingPlanTracking/', config);
}

export function updateStartCountRecipeTracking(id) {
    
    
    return axios.post(BASE_URL + '/tracking/updateStartCountRecipeTracking/' + id, null, config);
}

export function updateStartCountSeminarTracking(id) {
    
    
    return axios.post(BASE_URL + '/tracking/updateStartCountSeminarTracking/' + id, null, config);
}


