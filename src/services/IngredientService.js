import axios from "axios";
import {BASE_URL, token} from "../contexts/JWTContext";

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function addIngredient(data) {
    
    
    return axios.post(BASE_URL + '/ingredient/addIngredient', data, config);
}

export function updateIngredient(id, data){
    
    
    return axios.put(BASE_URL + '/ingredient/updateIngredient/' + id, data, config);
}

export function loadIngredients() {
    
    
    return axios.get(BASE_URL + '/ingredient/loadIngredients', config);
}


export function addIngredientTags(data) {
    
    
    return axios.post(BASE_URL + '/tags/addIngredientTags', data, config);
}

export function loadIngredientTags() {
    
    
    return axios.get(BASE_URL + '/tags/loadAllIngredientTags', config);
}

