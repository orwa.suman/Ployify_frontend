import { BASE_URL, token } from '../contexts/JWTContext';

import axios from 'axios';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export const RecipeLevels = {
    SIMPLE: 1,
    NORMAL: 2,
    ADVANCED: 3,
    PRO: 4
};

export function addRecipe(data) {
    
    
    return axios.post(BASE_URL + '/recipes/addRecipe', data, config);
}

export async function deleteRecipes(id) {
    
    
    await axios.delete(BASE_URL + '/recipes/deleteRecipe/' + id, config);
}

export function getRecipe(id) {
    
    
    return axios.get(BASE_URL + '/recipes/getRecipeWithId/' + id, config);
}

export function loadRecipes() {
    
    
    return axios.get(BASE_URL + '/recipes/getAllRecipes', config);
}

export function updateRecipe(id, data) {
    return axios.put(BASE_URL + '/recipes/updateRecipe/' + id, data, config);
}

export function getUserMatchingRecipes(userId) {
    return axios.get(BASE_URL + '/recipes/user/getUserMatchingRecipes/' + userId, config);
}

export function addToShoppingCart(userId, activityId, activityNum) {
    config.params = {
        userId: userId,
        activityId: activityId,
        activityNum: activityNum
    };
    return axios.post(BASE_URL + '/shoppingCart/addToShoppingCart', null, config);
}

export function getShopppingCartData(userId) {
    const userConfig = {
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    };
    return axios.get(BASE_URL + '/user/getShoppingCart/' + userId, userConfig);
}

export function deleteShoppingCartItem(userId, activityId) {
    config.params = {
        userId: userId,
        activityId: activityId
    };
    return axios.delete(BASE_URL + '/shoppingCart/removeFromShoppingCart', config);
}

export function getShoppingCartPdf(userId) {
    config.params = {
        userId: userId
    };
    return axios.get(BASE_URL + '/shoppingCart/getShoppingCartPdf', config);
}
