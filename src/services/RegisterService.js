import { BASE_URL, token } from '../contexts/JWTContext';

import axios from 'axios';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function addRegistrationCodesToCompany(companyId, orderedAccounts = -1) {
    
    const params = { companyId: companyId, orderedAccounts: orderedAccounts };
    return axios.post(BASE_URL + '/register/addRegistrationCodesToCompany', {}, { ...config, params });
}

export function validateRegisterCode(code) {
    const params = { code };
    return axios.get(BASE_URL + '/register/validateRegisterCode', { ...config, params });
}

export function verifyEmail(code, password, email) {
    const params = { verificationCode: code, password, email };
    return axios.post(BASE_URL + '/verifyEmail', params, config);
}
