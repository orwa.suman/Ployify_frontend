import { BASE_URL, token } from '../contexts/JWTContext';

import axios from 'axios';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function addDish(data) {
    
    
    return axios.post(BASE_URL + '/dish/addDish', data, config);
}

export function updateDish(id, data) {
    
    
    return axios.put(BASE_URL + '/dish/updateDish/' + id, data, config);
}

export function addDishTags(data) {
    
    
    return axios.post(BASE_URL + '/tags/addDishTags', data, config);
}

export function loadDishes() {
    
    
    return axios.get(BASE_URL + '/dish/loadAllDishes', config);
}

export function listAllIngredientUnits() {
    
    
    return axios.get(BASE_URL + '/dish/listAllIngredientUnits', config);
}

export function loadExtraDishes(isExtraTag) {
    
    
    return axios.get(BASE_URL + '/dish/getDishesByExtraTag?isExtraTag=' + isExtraTag, config);
}

export function loadDishTags() {
    
    
    return axios.get(BASE_URL + '/tags/loadAllDishTags', config);
}

export function deleteDishyById(dishId) {
    return axios.delete(BASE_URL + `/dish/deleteDishById/${dishId}`, config);
}
