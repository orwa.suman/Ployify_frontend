import axios from 'axios';
import { BASE_URL, token } from '../contexts/JWTContext';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};


export function addTrainingPlanTag(data) {
    
    
    return axios.post(BASE_URL + '/tags/addTrainingPlanTags', data, config);
}

export function loadTrainingPlanTags() {
    
    
    return axios.get(BASE_URL + '/tags/loadAllTrainingPlanTags', config);
}

//------------------------------------------------------------------//

export function addIngredientTags(data) {
    
    
    return axios.post(BASE_URL + '/tags/addIngredientTags', data, config);
}

export function loadAllIngredientTags() {
    
    
    return axios.get(BASE_URL + '/tags/loadAllIngredientTags', config);
}

//------------------------------------------------------------------//

// dish tag is the old name for recipe tag
export function addRecipeTag(data) {
    
    
    return axios.post(BASE_URL + '/tags/addDishTags', data, config);
}

// dish tag is the old name for recipe tag
export function loadRecipeTags() {
    
    
    return axios.get(BASE_URL + '/tags/loadAllDishTags', config);
}

//------------------------------------------------------------------//

export function addSeminarTag(data) {
    
    
    return axios.post(BASE_URL + '/tags/addSeminarTags', data, config);
}


export function loadSeminarTags() {
    
    
    return axios.get(BASE_URL + '/tags/loadAllSeminarTags', config);
}

//------------------------------------------------------------------//

export function addMentalHealthTag(data) {
    
    
    return axios.post(BASE_URL + '/tags/addMentalHealthTags', data, config);
}


export function loadMentalHealthTags() {
    
    
    return axios.get(BASE_URL + '/tags/loadAllMentalHealthTags', config);
}
