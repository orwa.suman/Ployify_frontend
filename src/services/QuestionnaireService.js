import { BASE_URL, token } from '../contexts/JWTContext';

import axios from 'axios';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS'
    }
};

export function createQuestionnaire(data) {
    return axios.post(BASE_URL + '/questionnaire/createQuestionnaire', data, config);
}

export function getQuestionnaire() {
    
    
    return axios.get(BASE_URL + '/questionnaire/getQuestionnaire', config);
}

export function getQuestionnaireById(id) {
    
    
    return axios.get(BASE_URL + '/questionnaire/getQuestionnaireById', { ...config, params: { id } });
}

export function updateQuestionnaire(data) {
    return axios.put(BASE_URL + '/questionnaire/updateQuestionnaireQA', data, config);
}

export function deleteQuestionnaire(id) {
    
    
    return axios.delete(BASE_URL + '/questionnaire/deleteQuestionnaireQA', { ...config, params: { id } });
}

export function getQuestionnaireDesign() {
    
    
    return axios.get(BASE_URL + '/questionnaire/getAllQuestionDesigns', config);
}

export function getNewQuestionnaire() {
    return axios.get(BASE_URL + '/questionnaire/getQuestionnaire', { ...config });
}
