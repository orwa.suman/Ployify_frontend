import axios from 'axios';
import { BASE_URL, token } from '../contexts/JWTContext';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function addTraining(data) {
    
    return axios.post(BASE_URL + '/training/addTraining', data, config);
}

export function loadTrainings() {
    
    return axios.get(BASE_URL + '/training/listAllTraining', config);
}

export function loadAllTags() {
    
    return axios.get(BASE_URL + '/tags/loadAllDishTags', config);
}
export function loadAllTrainingTags() {
    
    return axios.get(BASE_URL + '/tags/loadAllTrainingPlanTags', config);
}

export async function updateTraining(id, data) {
    
    
    await axios.put(BASE_URL + '/training/updateTraining/' + id, data, config);
}

export async function deleteTraining(id) {
    
    
    await axios.delete(BASE_URL + '/training/deleteTraining/' + id, config);
}

export async function deleteTrainingPlan(id) {
    
    
    await axios.delete(BASE_URL + '/trainingPlan/deleteTrainingPlan/' + id, config);
}

export function findTraining(id) {
    
    
    return axios.get(BASE_URL + '/training/getTraining' + id, config);
}

export function loadEquipments() {
    
    
    return axios.get(BASE_URL + '/training/listAllEquipments', config);
}

export function loadMuscleGroups() {
    
    
    return axios.get(BASE_URL + '/training/listAllMuscleGroups', config);
}

export function loadTrainingTags() {
    
    
    return axios.get(BASE_URL + '/tags/loadAllTrainingPlanTags', config);
}

export function addTrainingTags(data) {
    
    
    return axios.post(BASE_URL + '/tags/addTrainingPlanTags', data, config);
}
