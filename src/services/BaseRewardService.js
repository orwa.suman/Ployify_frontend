import axios from "axios";
import {BASE_URL, token} from "../contexts/JWTContext";

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function addReward(data){
    
    
        return axios.post(BASE_URL + '/reward/addReward', data, config);
}


export function loadAllBaseRewards() {
    
    
    return axios.get(BASE_URL + '/reward/getAllRewards', config);
}

export function getRewardById(id) {
    
    
    return axios.get(BASE_URL + '/reward/getRewardById/' + id, config);
}

export function deleteRewardById(id) {
    
    
    return axios.delete(BASE_URL + '/reward/deleteRewardById/' + id, config);
}

export function updateRewardById(data) {
    
    
    return axios.put(BASE_URL + '/reward/updateRewardById/' + data.id, data, config);
}

