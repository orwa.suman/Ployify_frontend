import axios from "axios";
import {BASE_URL, token} from "../contexts/JWTContext"

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function addAuthor(data){
    
    
    return axios.post(BASE_URL + '/admin/author/addAuthor', data, config);
}

export function updateAuthor(id, data){
    
    
    return axios.put(BASE_URL + '/admin/author/updateAuthor/' + id, data, config);
}

export function deleteAuthor(id){
    
    
    return axios.delete(BASE_URL + '/admin/author/deleteAuthor/' + id, config);
}

export function addContentToAuthor(data, authorId){
    
    
    return axios.post(BASE_URL + '/author/addContentToAuthor/' + authorId, data, config);
}

export function getAllCreatedContentOfAuthor(authorId){
    
    
    return axios.post(BASE_URL + '/author/getAllCreatedContentOfAuthor/' + authorId, {}, config);
}

export function getAuthorByName(firstName, lastName) {
    
    config.params = {
        firstName,
        lastName
    };
    
    return axios.get(BASE_URL + '/admin/author/getAuthorByName', config);
}

export function loadAuthors () {
    
    return axios.get(BASE_URL + '/admin/author/getAllAuthors', config);
}
