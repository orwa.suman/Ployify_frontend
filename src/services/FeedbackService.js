import axios from 'axios';
import { BASE_URL, token } from '../contexts/JWTContext';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function createNewFeedback(data) {
    
    

    const custom_config = {
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    };

    return axios.post(BASE_URL + '/feedback/createFeedback', data, custom_config);
}

export function getAllFeedback(section, all, processed, overrideProcessed) {
    
    
    config.params = {
        section: section,
        all: all,
        processed: processed,
        overrideProcessed: overrideProcessed
    };
    config.headers = {
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    };
    return axios.get(BASE_URL + '/feedback/getAllFeedback', config);
}

export function getAllFeedbackByChecked(checked) {
    
    
    config.params = {
        checked: checked
    };
    config.headers = {
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    };
    return axios.get(BASE_URL + '/feedback/getAllFeedbackByChecked', config);
}
