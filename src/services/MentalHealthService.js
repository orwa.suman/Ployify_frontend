import axios from 'axios';
import { BASE_URL, token } from '../contexts/JWTContext';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function addMentalHealth(data) {
    
    return axios.post(BASE_URL + '/mentalHealth/addMentalHealth', data, config);
}

export function updateMentalHealth(id, data) {
    
    
    return axios
        .put(BASE_URL + '/mentalHealth/updateMentalHealth/' + id, data, config)
        .then(console.log)
        .catch(console.log);
}

export async function deleteMentalHealth(id) {
    
    
    await axios.delete(BASE_URL + '/mentalHealth/deleteMentalHealth/' + id, config);
}

export function loadMentalHealth() {
    
    return axios.get(BASE_URL + '/mentalHealth/getAllMentalHealth/', config);
}

export function getMentalHealth(id) {
    
    return axios.get(BASE_URL + '/mentalHealth/getMentalHealth/' + id, config);
}

export function loadCategories() {
    
    return axios.get(BASE_URL + '/mentalHealth/listAllMHCategories', config);
}
