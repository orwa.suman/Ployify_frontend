import { BASE_URL, token } from '../contexts/JWTContext';

import axios from 'axios';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function setRewardAsSelectedForUser(userId, rewardTitle) {
    
    
    return axios.post(BASE_URL + '/points/setSelectedRewardForUser/' + userId + '/' + rewardTitle , config);
}

export function redeemReward(userId, rewardTitle) {
    
    
    return axios.post(BASE_URL + '/points/setSelectedRewardForUser/' + userId + '/' + rewardTitle , config);
}

export function setRewardAsGoalForUser(userId, rewardInfo) {
    
    
    return axios.post(BASE_URL + '/points/setGoalRewardForUser/' + userId, rewardInfo, config);
}

export function markRewardAsFinisihedAPI(companyId, userId, redeemReward) {
    
    
    return axios.post(BASE_URL + '/points/markRewardAsDone/' + companyId + '/' + userId, redeemReward, config);
}

export function changeRewardStatusAPI(companyId, userId, redeemReward) {
    
    
    return axios.post(BASE_URL + '/points/changeRedeemRewardStatus/' + companyId + '/' + userId, redeemReward, config);
}

export function addTrainingPlanPointsToUser(userId, trainingPlanId) {
    
    
    return axios.post(BASE_URL + '/points/addTrainingPlanPointsToUser/' + userId + '/' + trainingPlanId, {}, config);
}

export function addSeminarPointsToUser(userId, seminareId) {
    
    
    return axios.post(BASE_URL + '/points/addSeminarPointsToUser/' + userId + '/' + seminareId, {}, config);
}
