import { BASE_URL, token } from '../contexts/JWTContext';

import axios from 'axios';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function addCompany(data) {
    
    return axios.post(BASE_URL + '/admin/company/addCompany', data, config);
}

export function updateCompany(id, data) {
    
    return axios.put(BASE_URL + '/admin/company/updateCompany/' + id, data, config);
}

export async function deleteCompany(id) {
    
    
    await axios.delete(BASE_URL + '/admin/company/deleteCompany/' + id, config);
}

export function loadCompanies() {
    
    
    return axios.get(BASE_URL + '/admin/company/getAllCompanies', config);
}

export function getCompanyOrganisation(companyId) {
    
    
    return axios.get(BASE_URL + '/company/getCompanyOrganisation/' + companyId, config);
}

export function addCustomReward(companyId, data) {
    
    
    return axios.post(BASE_URL + '/company/addCustomReward/' + companyId, data, config);
}

export function getCompanyById(companyId) {
    
    
    return axios.get(BASE_URL + '/admin/company/getCompanyById/' + companyId, config);
}

export function getAllRegistrationCodesOfCompany(companyId) {
    
    
    return axios.get(BASE_URL + '/company/getAllRegistrationCodes/' + companyId, config);
}

export function loadAllCustomRewardsOfCompany(companyId) {
    
    
    return axios.get(BASE_URL + '/company/getAllCustomRewardByCompanyId/' + companyId, config);
}

export function getAllRedeemRewardOfCompany(companyId) {
    
    
    
    return axios.get(BASE_URL + '/company/getAllRedeemRewardOfCompany/' + companyId, config);
}

export function addSuperUserToCompany(companyId, email) {
    
    config.params = {
        superUserEmail: email
    };
    return axios.post(BASE_URL + '/company/addSuperUserToCompany/' + companyId, null, config);
}

export function addCompanyOrganisation(companyId, data) {
    
    
    // return axios.post(BASE_URL + '/company/addCompanyOrganisation/' + companyId, data, config); // Old
    return axios.post(BASE_URL + '/company/addOneCompanyUnit/' + companyId, data, config);
}

export function addBaseRewardToCompanyRewards(companyId, rewardPoints, rewardAvailableItems, data) {
    
    
    return axios.post(BASE_URL + '/company/addBaseRewardToCompanyRewards/' + companyId, data, {
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json'
        },

        params: {
            rewardPoints: rewardPoints,
            rewardAvailableItems: rewardAvailableItems
        }
    });
}

export function addCustomRewardToCompanyRewards(companyId, data) {
    return axios.post(BASE_URL + '/company/addCustomRewardToCompanyRewards/' + companyId, data, config);
}

export function updateCustomRewardToCompanyRewards(companyId, data) {
    return axios.put(BASE_URL + '/company/updateCustomRewardOfCompanyRewards/' + companyId, data, config);
}

export function deleteCustomRewardToCompanyRewards(companyId, rewardTitle) {
    
    
    config.params = {
        rewardTitle: rewardTitle
    };
    return axios.delete(BASE_URL + '/company/deleteCustomRewardFromCompanyRewards/' + companyId, config);
}
