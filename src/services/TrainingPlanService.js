import axios from 'axios';
import { BASE_URL, token } from '../contexts/JWTContext';

const config = {
    headers: {
        Authorization: `Bearer ${token}`
    }
};

export function loadTrainingPlans() {
    
    
    return axios.get(BASE_URL + '/trainingPlan/getAllTrainingPLan', config);
}

export function getAllTrainingPLanByLocation(location) {
    
    
    return axios.get(BASE_URL + '/trainingPlan/getAllTrainingPLanByLocation?TrainingPLanLocation='+  location, config);
}

export function addTrainingPlan(data) {
    
    return axios.post(BASE_URL + '/trainingPlan/addTrainingPlan', data , config);
}

export function updateTrainingPlan(id, data) {
    
    return axios.put(BASE_URL + '/trainingPlan/updateTrainingPlan/' + id, data , config);
}

export function getTrainingPlan(id) {
    
    
    return axios.get(BASE_URL + '/trainingPlan/getTrainingPlan/' + id, config);
}

const strPad = (string, pad, length) => {
    return (new Array(length + 1).join(pad) + string).slice(-length);
};

export function getDuration(duration, forceSeconds = false) {
    if (forceSeconds && duration < 60) {
        return duration + ' Sekunden';
    } else {
        const minutes = Math.floor(duration / 60);
        const seconds = duration - minutes * 60;
        return strPad(minutes, '0', 2) + ':' + strPad(seconds, '0', 2) + ' Min';
    }
}
