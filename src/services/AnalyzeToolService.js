import axios from 'axios';
import { BASE_URL, token } from '../contexts/JWTContext';

const config = {
    headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
    }
};

export function getMostUsedTrainingPlans() {
    
    
    return axios.get(BASE_URL + '/calcTracking/getMostUsedTrainingPlans', config);
}

export function getMostUsedRecipes() {
    
    
    return axios.get(BASE_URL + '/calcTracking/getMostUsedRecipes', config);
}

export function getMostUsedSeminars() {
    
    
    return axios.get(BASE_URL + '/calcTracking/getMostUsedSeminars', config);
}

export function getMostUsedMentalHealth() {
    
    
    return axios.get(BASE_URL + '/calcTracking/getMostUsedMentalHealth', config);
}

export function getAverageUsageTimeOfTrainingPlan(trainingLocation) {
    
    
    return axios.get(BASE_URL + '/calcTracking/getAverageUsageTimeOfTrainingPlan/' + trainingLocation, config);
}

export function getAverageUsageTimeOfRecipe() {
    
    
    return axios.get(BASE_URL + '/calcTracking/getAverageUsageTimeOfRecipe', config);
}

export function getAverageUsageTimeOfSeminar() {
    
    
    return axios.get(BASE_URL + '/calcTracking/getAverageUsageTimeOfSeminar', config);
}

export function geCountUsedTimeOfReward(rewardId) {
    
    
    return axios.get(BASE_URL + '/calcTracking/geCountUsedTimeOfReward/' + rewardId, config);
}

export function geSumUsedPointsOfReward(rewardId) {
    
    
    return axios.get(BASE_URL + '/calcTracking/geSumUsedPointsOfReward/' + rewardId, config);
}

export function getAllStatisticsForSuperUser(companyId) {
    
    
    return axios.get(BASE_URL + '/calcTracking/getAllStatisticsForSuperUser/' + companyId, config);
}

export function getAllStatisticsForSuperUserInterval(companyId, start, end) {
    
    
    config.params = {
        start,
        end
    };
    return axios.get(BASE_URL + '/calcTracking/getAllStatisticsForSuperUserInterval/' + companyId, config);
}

export function getAllStatisticsForAdmin() {
    
    
    return axios.get(BASE_URL + '/calcTracking/getAllStatisticsForAdmin', config);
}

export function getAllStatisticsForAdminInterval(start, end) {
    
    
    config.params = {
        start,
        end
    };
    return axios.get(BASE_URL + '/calcTracking/getAllStatisticsForAdminInterval', config);
}
